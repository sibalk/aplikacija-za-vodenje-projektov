function popoverHover(id){
  //console.log("hover over popover");
  //need only this... or the other one, now that user has seen it, the change can be deleted from database
}
function popoverOut(id){
  
}
function clickOnNote(id){
  debugger;
  //console.log("hover out")
  var taskId = id;
  var type = 1;
  if($('#tableTasks').find('tr#'+taskId).find('#taskPopover'+taskId)[0].className == "mypopover badge-project badge-note p-1 mr-1 mt-2"){
    $.post('/changes/delete', {taskId,type}, function(data){
      if(data.success){
        console.log("Spremembe opravila "+taskId+" glede obvestila so bile odstranjene");
        //remove badge from this task note
        $('#tableTasks').find('tr#'+taskId).find('#taskPopover'+taskId)[0].className = "mypopover p-1 mr-1 mt-2";
      }
    })
  }
}
function clickOnCompletion(id){
  debugger;
  var taskId = id.target.parentElement.id;
  var type = 0;
  var x = changes.find(c => c.id_task = taskId && c.id_subtask != null && c.type == 0)
  //if x then there are completion changes on subtask, dont make user think he checked completion, he needs to look at subtask modal view
  if(!x){
    //delete taskid && subtaskid == null && type == 0
    $.post('/changes/delete', {taskId,type}, function(data){
      if(data.success){
        console.log("Spremembe opravila "+taskId+" glede odstotka so bile odstranjene");
        //remove badge from this task completion
        id.target.className = "";
      }
    })
  }
}
function deleteSubtaskChanges(id){
  debugger;
  //send to server id of task and let sever delete every subtask related changes of this task id
  var taskId = id;
  var type = 2;
  $.post('/changes/delete', {taskId,type}, function(data){
    if(data.success){
      console.log("Spremembe podopravil opravila "+taskId+" so bile odstranjene");
      //remove badge button
      $('#tableTasks').find('tr#'+taskId).find('.btn-info')[0].className = "btn btn-info";
      //filter out changes that have been just deleted
      changes = changes.filter(function(el){ return el.id_subtask == null})
    }
  })
}
//funtion for all changes---> based on type and var in function send changes to server
function addChange(type,idSubtask){
  //project workers are in dataGet
  //every task has workers names and workers id
  var task = allTasks.find(t => t.id == taskId);
  if(idSubtask){
    debugger;
    var notifyWorkers = [];
    var notifyLeaders = [];
    for(var i=0; i<task.workers.length; i++){
      if(task.workers[i] != loggedUser.name + " " + loggedUser.surname){
        notifyWorkers.push(task.workersId[i]);
      }
    }
    for(var i=0; i<dataGet.length; i++){
      if(dataGet[i].workRole == 'vodja'){
        //debugger;
        var x = notifyWorkers.find(n => n == dataGet[i].id);
        if(x == null && dataGet[i].name != loggedUser.name && dataGet[i].surname != loggedUser.surname){
          notifyLeaders.push(dataGet[i].id);
        }
        //debugger;
      }
    }
    debugger;
    var notify = notifyWorkers.toString();
    var leaders = notifyLeaders.toString();
    if(notifyWorkers.length > 0 || notifyLeaders.length > 0){
      $.post('/changes/note', {projectId, taskId, idSubtask, notify, leaders, type, notifyCompletion, taskIsServis}, function(resp){
        debugger;
      })
    }
  }
  else{
    debugger;
    var notifyWorkers = [];
    var notifyLeaders = [];
    for(var i=0; i<task.workers.length; i++){
      if(task.workers[i] != loggedUser.name + " " + loggedUser.surname){
        notifyWorkers.push(task.workersId[i]);
      }
    }
    for(var i=0; i<dataGet.length; i++){
      if(dataGet[i].workRole == 'vodja'){
        //debugger;
        var x = notifyWorkers.find(n => n == dataGet[i].id);
        if(x == null && dataGet[i].name != loggedUser.name && dataGet[i].surname != loggedUser.surname){
          notifyLeaders.push(dataGet[i].id);
        }
        //debugger;
      }
    }
    var notify = notifyWorkers.toString();
    var leaders = notifyLeaders.toString();
    debugger;
    if(notifyWorkers.length > 0 || notifyLeaders.length > 0){
      $.post('/changes/note', {projectId, taskId, idSubtask, notify, leaders, type, notifyCompletion, taskIsServis}, function(resp){
        debugger;
        console.log(resp.success);
      })
    }
  }
}
var taskIsServis = false;