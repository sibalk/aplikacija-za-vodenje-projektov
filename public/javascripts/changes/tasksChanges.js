$(function(){
  $.get('/changes/mychanges', function(data){
    changes = data.data;
    //console.log(changes)
    var noteElement = `<span class="badge badge-note mr-2 p-2">OBVESTILO</span>`;
    var completionElement = `<span class="badge badge-completion mr-2 p-2">ODSTOTEK</span>`;
    myChanges = changes;
    //myChanges = changes.filter(function(el){ return el.status != 10});
    completionChanges = myChanges.filter(function(el){ return el.type == 0});
    noteChanges = myChanges.filter(function(el){ return el.type == 1});
    for(var i = 0; i < completionChanges.length; i++){
      if(i > 0)
        if(completionChanges[i-1].id_task == completionChanges[i].id_task)
          continue;
      //new task, find if its on the list
      $('#taskList').find('#'+completionChanges[i].id_task).find('#taskItemBadges').append(completionElement);
    }
    for(var i = 0; i < noteChanges.length; i++){
      if(i > 0)
        if(noteChanges[i-1].id_task == noteChanges[i].id_task)
          continue;
      //new task, find if its on the list
      $('#taskList').find('#'+noteChanges[i].id_task).find('#taskItemBadges').append(noteElement);
    }
  })
})
function taskShowChanges(){
  $('#taskBadges').empty();
  var noteElement = `<span class="badge badge-note mr-2">OBVESTILO</span>`;
  var completionElement = `<span class="badge badge-completion mr-2">ODSTOTEK</span>`;
  var x = noteChanges.find(c => c.id_task == taskId && c.id_subtask == null);
  var y = completionChanges.find(c => c.id_task == taskId)
  //debugger;
  if(x)
    $('#taskBadges').append(noteElement);
  if(y)
    $('#taskBadges').append(completionElement);
}
function subtaskShowChanges(){
  var noteElement = `<span class="badge badge-note mr-2">OBVESTILO</span>`;
  var completionElement = `<span class="badge badge-completion mr-2">ODSTOTEK</span>`;
  var x;
  var y;
  for(var i = 0; i < allSubtasks.length; i++){
    x = noteChanges.find(c => c.id_subtask == allSubtasks[i].id);
    y = completionChanges.find(c => c.id_subtask == allSubtasks[i].id);
    //debugger;
    if(x)
      $('#subtaskList').find('#'+allSubtasks[i].id).find('#subtaskBadges').append(noteElement);
    if(y)
      $('#subtaskList').find('#'+allSubtasks[i].id).find('#subtaskBadges').append(completionElement);
  }
}
function deleteChangesForTasks(id){
  debugger;
  var x = changes.find(c => c.id_task == id)
  if(x){
    $.post('/changes/deletetask', {id}, function(data){
      debugger;
      if(data.success)
        console.log("Videne spremembe pobrisane iz baze");
    })
  }
}
function deleteListBaddges(idTask){
  if(noteChanges){
    noteChanges = noteChanges.filter(function(el){ return el.id_task != idTask });
  }
  if(completionChanges){
    completionChanges = completionChanges.filter(function(el){ return el.id_task != idTask });
  }
  if(changes)
    changes = changes.filter(function(el){ return el.id_task != idTask });
  $('#taskList').find('#'+idTask).find('#taskItemBadges').empty();
}
var changes;
var myChanges;
var taskNoteBadge = false;
var taskCompletionBadge = false;
var projectNoteBadge = false;
var projectCompletionBadge = false;
var noteChanges;
var completionChanges;
//impossible for now
//var otherServises;