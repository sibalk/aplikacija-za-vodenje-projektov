$(function(){
  $.get('/changes/mychanges', function(data){
    changes = data.data;
    //console.log(changes)
    var noteElement = `<span class="badge badge-note badge-dashboard mr-2">OBVESTILO</span>`;
    var completionElement = `<span class="badge badge-completion badge-dashboard mr-2">ODSTOTEK</span>`;
    myChanges = changes;
    otherChanges = changes;
    myServises = changes;
    myFinishedChanges = changes;
    myUnfinishedChanges = changes;
    //can not see completed servises (9 and 11 dont exist, dont know who created servis)
    changes = changes.filter(function(el){ return el.status != 10});
    //my tasks or other(for leaders)
    myServises = changes.filter(function(el){ return el.status == 8});
    myChanges = changes.filter(function(el){ return el.status == 0 || el.status == 2 });
    otherChanges = changes.filter(function(el){ return el.status == 1 || el.status == 3 });
    myFinishedChanges = myChanges.filter(function(el){ return el.status == 2});
    myUnfinishedChanges = myChanges.filter(function(el){ return el.status == 0});
    if(myUnfinishedChanges.length > 0 || myServises.length > 0){
      //unfinished my task that was changed
      //add badge on Moja opravila
      debugger;
      var note = myUnfinishedChanges.find(x => x.type == 1)
      if(!note)
        note = myServises.find(x => x.type == 1)
      if(note){
        //add badge for note
        $('#myTasksBadges').append(noteElement);
        taskNoteBadge = true;
      }
      var completion = myUnfinishedChanges.find(x => x.type == 0)
      if(!completion)
        completion = myServises.find(x => x.type == 0)
      if(completion){
        //add badge for completion
        $('#myTasksBadges').append(completionElement);
        taskCompletionBadge = true;
      }
      if(myUnfinishedChanges.length > 0){
        //unfinished task can be viewd also in Moji projeti
        //add badge on Moji projekti
        var note = myUnfinishedChanges.find(x => x.type == 1)
        if(note){
          //add badge for note
          $('#myProjectsBadges').append(noteElement);
          projectNoteBadge = true;
        }
        var completion = myUnfinishedChanges.find(x => x.type == 0)
        if(completion){
          //add badge for completion
          $('#myProjectsBadges').append(completionElement);
          projectCompletionBadge = true;
        }
      }
    }
    if(myFinishedChanges.length > 0){
      debugger;
      //add badge on Moji projekti
      var note = myFinishedChanges.find(x => x.type == 1)
      if(note){
        //add badge for note
        if(!projectNoteBadge)
          $('#myProjectsBadges').append(noteElement);
      }
      var completion = myFinishedChanges.find(x => x.type == 0)
      if(completion){
        //add badge for completion
        if(!projectCompletionBadge)
          $('#myProjectsBadges').append(completionElement);
      }
    }
    if(otherChanges.length > 0){
      debugger;
      var note = otherChanges.find(x => x.type == 1)
      if(note)
        $('#employeesBadges').append(noteElement);
      var completion = otherChanges.find(x => x.type == 0)
      if(completion)
        $('#employeesBadges').append(completionElement);
    }
  })
})
var changes;
var myChanges;
var otherChanges;
var myServises;
var myFinishedChanges;
var myUnfinishedChanges;
var taskNoteBadge = false;
var taskCompletionBadge = false;
var projectNoteBadge = false;
var projectCompletionBadge = false;
//impossible for now
//var otherServises;