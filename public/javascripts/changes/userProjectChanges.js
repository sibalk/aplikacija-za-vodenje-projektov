$(function(){
  $.get('/changes/mychanges', function(data){
    changes = data.data;
    //console.log(changes)
    var noteElement = '<span class="badge badge-note mr-2 p-2">OBVESTILO</span>';
    var completionElement = '<span class="badge badge-completion mr-2 p-2">ODSTOTEK</span>';
    myChanges = changes;
    //on page with projects, no servises here, filter them out
    myChanges = myChanges.filter(function(el){ return el.status < 8});
    //console.log(myChanges)
    completionChanges = myChanges.filter(function(el){ return el.type == 0});
    noteChanges = myChanges.filter(function(el){ return el.type == 1});
    for(var i = 0; i < completionChanges.length; i++){
      if(i > 0)
        if(completionChanges[i-1].id_project == completionChanges[i].id_project)
          continue;
      debugger;
      $('#projectList').find('#'+completionChanges[i].id_project).find('#projectListBadges').append(completionElement);
    }
    for(var i = 0; i < noteChanges.length; i++){
      if(i > 0)
        if(noteChanges[i-1].id_project == noteChanges[i].id_project)
          continue;
      debugger;
      $('#projectList').find('#'+noteChanges[i].id_project).find('#projectListBadges').append(noteElement);
    }
  })
})
function displayTaskChanges(){
  var noteElement = '<span class="badge badge-note mr-2 p-2">OBVESTILO</span>';
  var completionElement = '<span class="badge badge-completion mr-2 p-2">ODSTOTEK</span>';
  for(var i = 0; i < completionChanges.length; i++){
    if(i > 0)
      if(completionChanges[i-1].id_task == completionChanges[i].id_task)
        continue;
    //new task, find if its on the list
    debugger
    $('#projectTasks').find('#'+completionChanges[i].id_task).find('#taskItemBadges').append(completionElement);
  }
  for(var i = 0; i < noteChanges.length; i++){
    if(i > 0)
      if(noteChanges[i-1].id_task == noteChanges[i].id_task)
        continue;
    //new task, find if its on the list
    debugger
    $('#projectTasks').find('#'+noteChanges[i].id_task).find('#taskItemBadges').append(noteElement);
  }
  //if there is change badge on project list and not change badge on any task then there is
  //change on task that its not his, show that there is change on the project but not here
  //no change for note but project has note change?
  if(loggedUser.role == 'admin' || loggedUser.role == 'vodja'){
    noteElement = '<span class="badge badge-note mr-2 mt-2 p-2">OBVESTILO</span>';
    completionElement = '<span class="badge badge-completion mr-2 mt-2 p-2">ODSTOTEK</span>';
    var element = '<a class="btn btn-info mt-2" href="/projects/id?id='+projectId+'">Na projekt</a>';

    $('#activeProjectBadges').empty();
    $('#activeProjectButtons').empty();
    $('#activeProjectButtons').append(element);
    debugger;
    projectNoteBadge = $('#projectList').find('#'+projectId).find('.badge-note')[0];
    if(projectNoteBadge){
      taskNoteBadge = $('#projectTasks').find('.badge-note')[0];
      if(!taskNoteBadge){
        //change is in project but not here (go to project or user's from view)
        $('#activeProjectBadges').append(noteElement);
      }  
    }
    //no change for completion but project has completion change?
    projectCompletionBadge = $('#projectList').find('#'+projectId).find('.badge-completion')[0];
    if(projectCompletionBadge){
      taskCompletionBadge = $('#projectTasks').find('.badge-completion')[0];
      if(!taskCompletionBadge){
        //change is in project but not here (go to project or user's from view)
        $('#activeProjectBadges').append(completionElement);
      }
    }
  }
}
var changes;
var myChanges;
var taskNoteBadge = false;
var taskCompletionBadge = false;
var projectNoteBadge = false;
var projectCompletionBadge = false;
var noteChanges;
var completionChanges;