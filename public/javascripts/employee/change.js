$(function(){
  //$('.modal-dialog').draggable({ handle: ".modal-header" });
  $('#changePasswordform').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var userPassword = user_password.value;
    var userPasswordAgain = user_password_again.value;

    debugger;

    $.post('/employees/password', {userId, userPassword, userPasswordAgain}, function(resp){
      if(resp.success){
        debugger;
        //close modal
        $('#modalChangePassword').modal('toggle');
        $('#user_password').val("");
        $('#user_password_again').val("");
        $('#user_password_again').removeClass("is-valid");
      }
      else{
        $('#messagePass').append(resp.data); 
        debugger;
      }
      //add user in list
      //debugger
      //toggle modal
    })
  })
  $('#changeRoleform').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var userRole = user_role.value;

    debugger;

    $.post('/employees/role', {userId, userRole}, function(resp){
      var selectedRole = roles.find(r => r.id === parseInt(userRole))
      $('#modalChangeRole').modal('toggle');
      if($('#loggedUser').html().split(" (")[0] == $('#employeeName').html()){
        if(selectedRole){
          $('#loggedUser').html($('#loggedUser').html().split(" (")[0]+' ('+selectedRole.text+')');
        }
      }
      if(selectedRole){
        $('#imgEmployee').empty()
        if(selectedRole.text === "delavec")
          $('#imgEmployee').append('<img class="img-fluid" src="/worker64.png" alt="ikona" />');
        else if(selectedRole.text === "električar")
          $('#imgEmployee').append('<img class="img-fluid" src="/electrican64.png" alt="ikona" />');
        else if(selectedRole.text === "strojnik")
          $('#imgEmployee').append('<img class="img-fluid" src="/mechanic64.png" alt="ikona" />');
        else if(selectedRole.text === "plc")
          $('#imgEmployee').append('<img class="img-fluid" src="/plc64.png" alt="ikona" />');
        else if(selectedRole.text === "kuka")
          $('#imgEmployee').append('<img class="img-fluid" src="/kuka64.png" alt="ikona" />');
        else if(selectedRole.text === "konstruktor")
          $('#imgEmployee').append('<img class="img-fluid" src="/builder64.png" alt="ikona" />');
        else if(selectedRole.text === "konstrukter")
          $('#imgEmployee').append('<img class="img-fluid" src="/builder64.png" alt="ikona" />');
        else if(selectedRole.text === "vodja")
          $('#imgEmployee').append('<img class="img-fluid" src="/manager64.png" alt="ikona" />');
        else if(selectedRole.text === "admin")
          $('#imgEmployee').append('<img class="img-fluid" src="/admin64.png" alt="ikona" />');
        else if(selectedRole.text === "tajnik")
          $('#imgEmployee').append('<img class="img-fluid" src="/secratery64.png" alt="ikona" />');
        else
          $('#imgEmployee').append('<img class="img-fluid" src="/worker64.png" alt="ikona" />');
      }
      $('#user_role').val("");
    })
  })
})
function openChangePassModal(id){
  debugger;
  userId = id;
  $("#modalChangePassword").modal();
}
function openChangeRoleModal(id){
  debugger;
  userId = id;
  $.get( "/employees/roles", function( data ) {
    roles = data.data;

    $(".select2-role").select2({
      data: roles,
      tags: false,
      dropdownParent: $('#modalChangeRole'),
    });
    $("#modalChangeRole").modal();
  });
}
function onPassChange(){
  password = $('#user_password').val();
  passwordNew = $('#user_password_again').val();  
  if(password && passwordNew){
    if(password != passwordNew){
      $('#user_password_again').removeClass("is-valid").addClass("is-invalid");
      $('#btnChangePass').prop("disabled", true).removeClass("ui-state-enabled").addClass("ui-state-disabled");
    }
    else{
      $('#user_password_again').removeClass("is-invalid").addClass("is-valid");
      $('#btnChangePass').prop("disabled", false).removeClass("ui-state-disabled").addClass("ui-state-enabled");
    }
  }
}
$('#user_password').on('change', onPassChange);
$('#user_password_again').on('change', onPassChange);
var password = $('#user_password').val();
var passwordNew = $('#user_password_again').val();
var userId;
var roles;