$(document).ready(function() {
  //debugger
  
  $('.modal-dialog').draggable({
    handle: ".modal-header"
  });
  var roles
  loggedUser = {name:$('#loggedUser').html().split(" (")[0].split(" ")[0], surname:$('#loggedUser').html().split(" (")[0].split(" ")[1], role:$('#loggedUser').html().split(" (")[1].split(")")[0]};
  var username = $('#user_username').val();
  $('#user_username').on('change', onUsernameChange);
  
  $('#user_username_edit').on('change', onUsernameEditChange);

  function onUsernameChange(){
    username = $('#user_username').val();
    username = username.toLowerCase();
    //debugger;
    var conflict = usernames.find(u => u.username.toLowerCase() === username)
    if(conflict){
      $('#user_username').removeClass("is-valid").addClass("is-invalid");
      $('#btnAddEmployee').prop("disabled", true).removeClass("ui-state-enabled").addClass("ui-state-disabled");
    }
    else{
      $('#user_username').removeClass("is-invalid").addClass("is-valid");
      $('#btnAddEmployee').prop("disabled", false).removeClass("ui-state-disabled").addClass("ui-state-enabled");
    }
  }
  function onUsernameEditChange(){
    var newUsername = $('#user_username_edit').val();
    newUsername = newUsername.toLowerCase();
    debugger;
    var conflict = usernames.find(u => u.username.toLowerCase() === newUsername)
    if(conflict && newUsername != usernameEdit){
      $('#user_username_edit').removeClass("is-valid").addClass("is-invalid");
      $('#btnEditEmployee').prop("disabled", true).removeClass("ui-state-enabled").addClass("ui-state-disabled");
    }
    else{
      $('#user_username_edit').removeClass("is-invalid").addClass("is-valid");
      $('#btnEditEmployee').prop("disabled", false).removeClass("ui-state-disabled").addClass("ui-state-enabled");
    }
  }

  $('.btnadd').click(function(){
    //get roles
    $.get( "/employees/roles", function( data ) {
      roles = data.data;

      $(".select2-role").select2({
        data: roles,
        tags: false,
        dropdownParent: $('#modalAddEmployeeForm'),
      });
      $('#user_role').val(4).trigger('change')
      $.get( "/employees/usernames", function( data ) {
        usernames = data.data;
        //console.log(usernames);
        debugger;
        
        $("#modalAddEmployeeForm").modal();
      });
    });
  })
  $('#addEmployeeform').submit(function(e)
  {
    e.preventDefault();
    $form = $(this);
    var userUsername = user_username.value;
    var userName = user_name.value;
    var userSurname = user_surname.value;
    var userRole = user_role.value;
    var userPassword = user_password.value;
    var userPasswordAgain = user_password_again.value;

    debugger;
    $.post('/employees/create', {userUsername, userName, userSurname, userRole, userPassword, userPasswordAgain}, function(resp){
      if(resp.success){
        debugger;
        //add new user in table
        var selectedRole = roles.find(r => r.id === parseInt(userRole))
        tableEmployees.row.add({
          id: resp.id.id, 
          name: userName, 
          surname: userSurname, 
          role: selectedRole.text,
          active: true
        }).draw(false)
        //close modal
        $('#modalAddEmployeeForm').modal('toggle');
        $('#user_username').val("");
        $('#user_name').val("");
        $('#user_surname').val("");
        $('#user_password').val("");
        $('#user_password_again').val("");
        $('#user_password_again').removeClass("is-valid");
        $('#user_username').removeClass("is-valid")
      }
      else{
        $('#messageAdd').append(resp.data); 
        debugger;
      }
      //add user in list
      //debugger
      //toggle modal
    })
    
  });
  $('#editEmployeeform').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var newName = user_name_edit.value;
    var newSurname = user_surname_edit.value;
    var newUsername = user_username_edit.value;
    var newActive
    if(typeof activeEdit === 'undefined')
      newActive = true;
    else
      newActive = activeEdit.checked;
    debugger;
    $.post('/employees/update', {userId, newName, newSurname, newActive, newUsername}, function(resp){
      var temp = tableEmployees.row('#'+userId).data()
      temp.name = newName;
      temp.surname = newSurname;
      temp.username = newUsername;
      temp.active = newActive;
      debugger;
      tableEmployees.row('#'+userId).data(temp).invalidate().draw(false);
      $('#modalEditEmployeeForm').modal('toggle');
    })
  })
  $('#addRoleform').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var role = addNewRole.value;
    debugger;
    $.post('/employees/addrole', {role, newRoleType}, function(resp){

      debugger;
      if(resp == 'success')
        $('#modalAddRoleForm').modal('toggle');
      else{
        alert("Pri kreiranju nove vloge je prišlo do napake.");
      }
    })
  })
  activeUser = 1;
  //Get users and display in dataTable
  $.get( "/employees/all", {activeUser}, function( data ) {
    //console.log(data)
    dataGet = data.data;
    //console.log(dataGet[2]);
    $.get('changes/mychanges', function(data){
      changes = data.data;
      //console.log(changes);
      debugger;
      //var sortedChanges = changes.sortBy('from');
      tableEmployees = $('#employeesTable').DataTable( {
        data: dataGet,
        columns: [
            { title: "id", data: "id" },
            { data: "role",
            render: function ( data, type, row ) {
              if(data == 'delavec')
                return '<img class="img-fluid" src="/worker64.png" alt="ikona" />';
              else if(data == 'električar')
                return '<img class="img-fluid" src="/electrican64.png" alt="ikona" />';
              else if(data == 'strojnik')
                return '<img class="img-fluid" src="/mechanic64.png" alt="ikona" />';
                else if(data == 'plc')
                return '<img class="img-fluid" src="/plc64.png" alt="ikona" />';
                else if(data == 'kuka')
                return '<img class="img-fluid" src="/kuka64.png" alt="ikona" />';
                else if(data == 'konstruktor')
                return '<img class="img-fluid" src="/builder64.png" alt="ikona" />';
                else if(data == 'konstrukter')
                return '<img class="img-fluid" src="/builder64.png" alt="ikona" />';
              else if(data == 'vodja')
                return '<img class="img-fluid" src="/manager64.png" alt="ikona" />';
              else if(data == 'admin')
                return '<img class="img-fluid" src="/admin64.png" alt="ikona" />';
              else if(data == 'tajnik')
                return '<img class="img-fluid" src="/secratery64.png" alt="ikona" />';
              else
                return '<img class="img-fluid" src="/worker64.png" alt="ikona" />';
            }},
            { title: "Ime", data: "name" },
            { title: "Priimek", data: "surname" },
            { title: "Vloga", data: "role" },
            { data: "id",
              render: function ( data, type, row ) {
                //debugger;
                if((loggedUser.role == 'admin' || loggedUser.role == 'tajnik') && data != 1)
                  return '<button class="btn btn-danger" onclick="deleteUser('+data+')">Odstrani</button>';
                else
                  return '<div></div>';
              }},
            { data: "id",
              render: function ( data, type, row ) {
                return '<a class="btn btn-programmer" href="/employees/absence?id='+data+'">Odsotnosti</a>';
              }},
            { data: "id",
              render: function ( data, type, row ) {
                if(loggedUser.role == 'admin' || loggedUser.role == 'tajnik')
                  return '<button class="btn btn-primary" onclick="editUser('+data+')">Uredi</button>';
                else
                  return '<div></div>';
              }},
            { data: "id",
              render: function ( data, type, row ) {
                return '<a class="btn btn-info" href="/employees/userId?userId='+data+'">Več</a>';
              }},
            { title: "Aktivno", data: "active"},
        ],
        "language": {
          "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Slovenian.json"
        },
        order: [[3, "asc"]],
        columnDefs: [
          {
            targets: [0,9],
            visible: false,
            searchable: false
          },
          { responsivePriority: 1, targets: 1 },
          { responsivePriority: 2, targets: -2 },
          { responsivePriority: 3, targets: 3 },
          { responsivePriority: 100001, targets: -4 },
        ],
        responsive: true,
        //info: false,
        //searching: false,
        //paging: false
        createdRow: function (row, data, index){
          debugger
          var x = changes.find(c => c.from == data.id)
          $(row).attr('id', data.id);
          $(row).removeClass('table-dark');
          $(row).removeClass('table-info');
          if(!data.active){
            $(row).addClass('table-dark');
          }
          if(x){
            $(row).addClass('table-info');
          }
        },
        rowCallback: function(row, data, index){
          var x = changes.find(c => c.from == data.id)
          $(row).removeClass('table-dark');
          $(row).removeClass('table-info');
          if(!data.active){
            $(row).addClass('table-dark');
          }
          if(x){
            $(row).addClass('table-info');
          }
        },
        dom: 'Bfrtip',
        buttons: [
          {
            extend: 'pdfHtml5',
            exportOptions: {
                columns: [ 2, 3, 4 ]
            }
          }
        ]
      });
      
    })
    //every user is in table, mark users who made changes and logged user is leader of his project
    //showEmployeeChanges();
  });
});
function onPassChange(){
  var password = $('#user_password').val();
  var passwordNew = $('#user_password_again').val();  
  if(password && passwordNew){
    if(password != passwordNew){
      $('#user_password_again').removeClass("is-valid").addClass("is-invalid");
      $('#btnAddEmployee').prop("disabled", true).removeClass("ui-state-enabled").addClass("ui-state-disabled");
    }
    else{
      $('#user_password_again').removeClass("is-invalid").addClass("is-valid");
      $('#btnAddEmployee').prop("disabled", false).removeClass("ui-state-disabled").addClass("ui-state-enabled");
    }
  }
}
function editUser(id){
  userId = id;
  $("#activeEdit").prop('disabled', false);
  if(userId == 1)
  $("#activeEdit").prop('disabled', true);
  debugger
  $.get('/employees/usernames', function(data){
    debugger
    usernames = data.data;
    $.get( "/employees/user", {userId}, function( data ) {
      //console.log(usernames);
      debugger;
      
      $('#user_username_edit').val(data.data.username);
      $('#user_name_edit').val(data.data.name);
      $('#user_surname_edit').val(data.data.surname);
      usernameEdit = $('#user_username_edit').val();
      if(data.data.active){
        //debugger
        $("#activeEdit").prop('checked', 1);
      }
      else{
        //debugger
        $("#activeEdit").prop('checked', 0);
      }
      $("#modalEditEmployeeForm").modal();
    });
  })
}
function deleteUser(id){
  userId = id;
  debugger
  $.post('/employees/delete', {userId}, function(resp){
    //odstrani iz tabele na strani
    var temp = tableEmployees.row('#'+userId+'').data();
    //console.log(temp.active);
    temp.active = false;
    tableEmployees.row('#'+userId).data(temp).invalidate().draw(false);
  })
}
//CHANGE USER TO SEE ONLY ACTIVE OR ALL
function changeActive(seeActive){
  $('#optionAll').removeClass('active');
  $('#optionTrue').removeClass('active');
  $('#optionFalse').removeClass('active');
  activeUser = seeActive;
  $.get( "/employees/all", {activeUser}, function( data ) {
    //console.log(data)
    dataGet = data.data;
    debugger
    //console.log(dataGet[2]);
    tableEmployees.clear();
    tableEmployees.rows.add(dataGet);
    tableEmployees.draw(true);
    if(seeActive == 1)
      $('#optionTrue').addClass('active');
    else if(seeActive == 2)
      $('#optionFalse').addClass('active');
    else
      $('#optionAll').addClass('active');
  });
}
//OPEN MODAL TO ADD NEW ROLE
function openNewRoleModal(type){
  newRoleType = type;
  $("#modalAddRoleForm").modal();
}
$('#user_password').on('change', onPassChange);
$('#user_password_again').on('change', onPassChange);
var password = $('#user_password').val();
var passwordNew = $('#user_password_again').val();
var tableEmployees;
var loggedUser;
var activeUser;
var userId;
var usernames;
var usernameEdit;
var newRoleType;