$(function(){
  //$('.modal-dialog').draggable({ handle: ".modal-header" });
  $('.mypopover').popover({ html: true });
  $('.workerspopover').popover({ html: true });
  $('[data-toggle="popover"]').popover({ trigger: "hover" });
  loggedUser = { name: $('#loggedUser').html().split(" (")[0].split(" ")[0], surname: $('#loggedUser').html().split(" (")[0].split(" ")[1], role: $('#loggedUser').html().split(" (")[1].split(")")[0] };
  
  $('button selector').click(function(){
    window.location.href='/projects/id?id'+task.id_project+'';
  })
  
  //change active task to first on the list
  debugger;
  
  if($('#taskList').children(":first").length > 0){
    $('#taskList').children(":first").toggleClass("active");
    changeActiveTask(parseInt($('#taskList').children(":first")[0].id));
  }

  $('#completion-form').submit(function (e) {
    e.preventDefault();
    $form = $(this);
    var completion = completionPost.value;

    debugger;

    $.post('/projects/task/completion', { taskId, completion }, function (resp) {
      //alert(resp);
      //console.log("post response " + resp);
      $('#activeTaskProgress')[0].style.width = completion + '%';
      task.completion = completion;
      colorMyActiveTask();
      //$('#MyTaskTable').find('tr#'+taskId).find('td#completionTask').html(completion);
      $('#modalCompletionForm').modal('toggle');
      //TODO add changes when completion is changed
      if(completion == 100)
        notifyCompletion = true;
      else
        notifyCompletion = false;
      fixCompletion();
      addChange(0);
    })
  })
  //CHANGE NOTE OF THE SUBTASK
  $('#note-form').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var newNote = noteInput.value;
    $.post('/projects/subtask/note', {subtaskId, newNote}, function(resp){
      debugger;
      $('#subtaskList').find('row#'+subtaskId).find('.notepopover').attr('data-content', newNote);
      $('#noteInput').val("");
      $("#modalNoteForm").modal('toggle');
      //$("#modalSubtasksForm").modal('toggle');
      //add change to all other workers on task
      addChange(1,subtaskId);
    })
  })
  //CHANGE NOTE OF THE TASK
  $('#taskNote-form').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var newNote = taskNoteInput.value;
    $.post('/projects/note', { taskId, newNote }, function (resp) {
      debugger;
      $('#controlTask').find('.taskpopover').attr('data-content', newNote);
      $('#taskNoteInput').val("");
      $("#modalTaskNoteForm").modal('toggle');
      //$("#modalSubtasksForm").modal('toggle');
      //add change to all other workers on task
      addChange(1);
    })
  })
  //SUBTASKS
  $('#subtasks-form').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var newSubtask = subtaskInput.value;
    debugger;
    $.post('/projects/subtask/add', { taskId, newSubtask }, function (resp) {
      debugger;
      var checkedCB = '';
      var popoverText = "";
      var newId = resp.data.id;
      var listElement = `<row class="d-flex list-group-item" id=` + newId + `>
            <label class="p-1 mr-auto" style="width:70%;">`+ newSubtask + `</label>
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input" id="customCheck`+ newId + `" type="checkbox" ` + checkedCB + ` onchange="toggleSubtaskCheckbox(this)" />
              <label class="custom-control-label" for="customCheck`+ newId + `"> </label>
            </div><a class="notepopover p-1 mr-1 mt-2" data-toggle="popover" data-trigger="hover" data-content="`+ popoverText + `" style="height:30px;"><img class="img-fluid" src="/info.png" alt="ikona" style="width:25px;" /></a>
            <button class="invisible-button mr-1 mt-2" onclick="addSubtaskNote(`+ newId + `)"><img class="img-fluid" src="/note-52.png" alt="ikona" style="width:25px;" /></button>
            <button class="btn btn-danger task-button mr-2 mt-1" onclick="deleteSubtask(`+ newId + `)" style="width:25px, height:5px, border:none;">X</button>
          </row>`
      $('#subtaskList').append(listElement);
      $('[data-toggle="popover"]').popover({ trigger: "hover" });
      allSubtasks.push({ id: newId, id_task: taskId, name: newSubtask, note: null, completed: false });
      var numberOfCompleted = 0;
      for (var i = 0; i < allSubtasks.length; i++) {
        if (allSubtasks[i].completed)
          numberOfCompleted++;
      }
      var completion = Math.round((numberOfCompleted / allSubtasks.length) * 100)
      //console.log(completion);
      taskId = task.id;
      $('#activeTaskProgress')[0].style.width = completion + '%';
      task.completion = completion;
      colorMyActiveTask();
      $.post('/projects/task/completion', { taskId, completion }, function (resp) {
        //alert(resp);
        //console.log("post response " + resp);
        fixCompletion();
      })
      $('#subtaskInput').val("");
      $("#modalSubtaskForm").toggle();
    })
  })
  //GET ALL TASKS -> DRAW VIZ OF TASKS
  var taskAssignment = userId;
  $.post('/projects/worker/tasks', {taskAssignment}, function(data){
    debugger;
    allMyTasks = data.data[0];
    drawTimeline();
  })
})
$('.list-group-item').on('click', function() {
  var $this = $(this);
  var id = parseInt(this.id);
  debugger
  //$('#taskList').find('a#46').find('#task_name').html();
  // task_name,project_name,date

  $('.active').removeClass('active');
  $this.toggleClass('active')

  changeActiveTask(id);
})
function changeActiveTask(id) {
  //delete notification from previous task
  deleteListBaddges(taskId);
  //$('#taskList').find('#'+taskId).find('#taskItemBadges').empty();
  taskId = id;
  debugger
  //console.log(id);
  //Get task info 
  $.get('/projects/task', {id}, function(data){
    task = data.data;
    projectId = task.id_project;
    debugger;
    if(task.project_name){
      $('#activeProjectName').html(task.project_name);
      taskIsServis = false;
    }
    else{
      $('#activeProjectName').html(task.subscriber + " (servis)");
      taskIsServis = true;
    }
    $('#activeTaskName').html(task.task_name);
    $('#activeTaskCategory').html(task.category);
    $('#activeTaskProgress')[0].style.width = task.completion+'%';
    var taskStart = '/';
    var taskFinish = '/';
    if(task.task_start){
      var start = new Date(task.task_start);
      var d = start.getDate();
      var m = start.getMonth()+1;
      var y = start.getFullYear();
      if(d < 10)
      d = '0'+d;
      if(m < 10)
      m = '0'+m;
      taskStart = d+'.'+m+'.'+y;
      //$('#startEdit').val(y+'-'+m+'-'+d);
    }
    if(task.task_finish){
      var finish = new Date(task.task_finish);
      var d = finish.getDate();
      var m = finish.getMonth() + 1;
      var y = finish.getFullYear();
      if (d < 10)
        d = '0' + d;
      if (m < 10)
        m = '0' + m;
      taskFinish = d + '.' + m + '.' + y;
      //$('#finishEdit').val(y+'-'+m+'-'+d);
    }
    $('#activeTaskDate').html(taskStart + " - " + taskFinish);
    colorMyActiveTask();
    //Get workers on project
    projectId = task.id_project;
    if(projectId){
      $.get('/projects/workerid', {projectId}, function(data){
        workers = data.data;
        //debugger;
        var element = "";
        for(var i=0; i<workers.length; i++){
          element = element + workers[i].workRole + ": " + workers[i].name + " " + workers[i].surname + "<br />";
        }
        $('.mypopover').attr('data-content', element);
        //get workers for this task
        $.get('/employees/workers', {taskId}, function(data){
          taskWorkers = data.data;
          //debugger;
          var element = "";
          for(var i=0; i<taskWorkers.length; i++){
            element = element + taskWorkers[i].name + " " + taskWorkers[i].surname + "<br />";
          }
          $('.workerspopover').attr('data-content', element);
          //get workers for this task
         // debugger
        })
       // debugger
      })
    }
    else{
      //get workers on servis
      $.get('/projects/workerid', {taskId}, function(data){
        taskWorkers = data.data;
        //debugger;
        var element = "";
        for(var i=0; i<taskWorkers.length; i++){
          element = element + taskWorkers[i].name + " " + taskWorkers[i].surname + "<br />";
        }
        $('.workerspopover').attr('data-content', element);
        $('.mypopover').attr('data-content', "");
       // debugger
      })
    }
    //load subtasks
    //taskId = task.id;
    debugger;
    $('#controlButtons').empty();
    var element;
    if(projectId){
      element = '<a class="btn btn-info mr-2 mt-1" href="/projects/id?id='+task.id_project+'">Več</a>';
      $('#controlButtons').append(element);
    }
    if(loggedUser.role == 'admin' || loggedUser.role == 'vodja'){
      element = '<button class="btn btn-success mr-2 mt-1" onclick="openSubtaskModal('+task.id+')">+</button>';
      $('#controlButtons').append(element);
    }
    //var position = 'ml-auto';
    $.get('/projects/subtasks', {taskId}, function(data){
      debugger;
      allSubtasks = data.data;
      if(allSubtasks.length <=0){
        element = '<button class="button btn btn-primary mr-2 mt-1" onclick="openCompletionModal()">Uredi</button>';
        //position = '';
        $('#controlButtons').append(element);
      }
      $('#subtaskList').empty();
      for(var i = 0; i<data.data.length; i++){
        var checkedCB = '';
        if(data.data[i].completed)
          checkedCB = 'checked=""';
        var popoverText = "";
        if(data.data[i].note)
          popoverText = data.data[i].note;
        if(loggedUser.role == 'admin' || loggedUser.role == 'vodja')
        var listElement = `<div class="list-group-item" id=` + data.data[i].id + `>
          <row class="d-flex">
            <label class="p-1 mr-auto" style="width:70%;">`+ data.data[i].name + `</label>
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input" id="customCheck`+ data.data[i].id + `" type="checkbox" ` + checkedCB + ` onchange="toggleSubtaskCheckbox(this)" />
                <label class="custom-control-label" for="customCheck`+ data.data[i].id + `"> </label>
            </div><a class="notepopover p-1 mr-1 mt-2" data-toggle="popover" data-trigger="hover" data-content="`+ popoverText + `" style="height:30px;" data-original-title="" title=""><img class="img-fluid" src="/info.png" alt="ikona" style="width:25px;" /></a>
            <button class="invisible-button mr-1 mt-2" onclick="addSubtaskNote(`+ data.data[i].id + `)"><img class="img-fluid" src="/note-52.png" alt="ikona" style="width:25px;" /></button>
            <button class="btn btn-danger task-button mr-2 mt-1" onclick="deleteSubtask(`+ data.data[i].id + `)" style="width:25px, height:5px, border:none;">X</button>
          </row>
          <row class="d-flex" id="subtaskBadges"></row>
        </div>`;
        /*
        var listElement = `<row class="d-flex list-group-item" id=` + data.data[i].id + `>
          <label class="p-1 mr-auto" style="width:70%;">`+ data.data[i].name + `</label>
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input" id="customCheck`+ data.data[i].id + `" type="checkbox" ` + checkedCB + ` onchange="toggleSubtaskCheckbox(this)" />
            <label class="custom-control-label" for="customCheck`+ data.data[i].id + `"> </label>
          </div><a class="notepopover p-1 mr-1 mt-2" data-toggle="popover" data-trigger="hover" data-content="`+ popoverText + `" style="height:30px;"><img class="img-fluid" src="/info.png" alt="ikona" style="width:25px;" /></a>
          <button class="invisible-button mr-1 mt-2" onclick="addSubtaskNote(`+ data.data[i].id + `)"><img class="img-fluid" src="/note-52.png" alt="ikona" style="width:25px;" /></button>
          <button class="btn btn-danger task-button mr-2 mt-1" onclick="deleteSubtask(`+ data.data[i].id + `)" style="width:25px, height:5px, border:none;">X</button>
        </row>`
        */
        else
          var listElement = `<div class="list-group-item" id=` + data.data[i].id + `>
            <row class="d-flex">
              <label class="p-1 mr-auto" style="width:70%;">`+ data.data[i].name + `</label>
              <div class="custom-control custom-checkbox">
                <input class="custom-control-input" id="customCheck`+ data.data[i].id + `" type="checkbox" ` + checkedCB + ` onchange="toggleSubtaskCheckbox(this)" />
                  <label class="custom-control-label" for="customCheck`+ data.data[i].id + `"> </label>
              </div><a class="notepopover p-1 mr-1 mt-2" data-toggle="popover" data-trigger="hover" data-content="`+ popoverText + `" style="height:30px;" data-original-title="" title=""><img class="img-fluid" src="/info.png" alt="ikona" style="width:25px;" /></a>
              <button class="invisible-button mr-1 mt-2" onclick="addSubtaskNote(`+ data.data[i].id + `)"><img class="img-fluid" src="/note-52.png" alt="ikona" style="width:25px;" /></button>
            </row>
            <row class="d-flex" id="subtaskBadges"></row>
          </div>`;
        /*
          var listElement = `<row class="d-flex list-group-item" id=` + data.data[i].id + `>
            <label class="p-1 mr-auto" style="width:70%;">`+ data.data[i].name + `</label>
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input" id="customCheck`+ data.data[i].id + `" type="checkbox" ` + checkedCB + ` onchange="toggleSubtaskCheckbox(this)" />
              <label class="custom-control-label" for="customCheck`+ data.data[i].id + `"> </label>
            </div><a class="notepopover p-1 mr-1 mt-2" data-toggle="popover" data-trigger="hover" data-content="`+ popoverText + `" style="height:30px;"><img class="img-fluid" src="/info.png" alt="ikona" style="width:25px;" /></a>
            <button class="invisible-button mr-1 mt-2" onclick="addSubtaskNote(`+ data.data[i].id + `)"><img class="img-fluid" src="/note-52.png" alt="ikona" style="width:25px;" /></button>
          </row>`
        */
        $('#subtaskList').append(listElement);
        $('[data-toggle="popover"]').popover({trigger: "hover"});
      }
      $('#controlTask').empty();
      var isCompleted = '';
      var isDisabled = '';
      if(allSubtasks.length != task.subtasks){
        task.subtasks = allSubtasks.length;
        //fix subtasks count in db...
        var count = allSubtasks.length;
        $.post('/projects/count', {taskId, count}, function(data){
          debugger;
        })
      }
      if(task.completion == '100'){
        isCompleted = 'checked=""';
      }
      if(task.subtasks != '0'){
        isDisabled = 'disabled=""';
      }
      var controlElement = `<div class="custom-control custom-checkbox" id="taskCheckbox">
      <input class="custom-control-input" id="customTaskCheck" type="checkbox" `+ isCompleted + ` ` + isDisabled + ` onchange="toggleTaskCheckbox(this)" />
      <label class="custom-control-label" for="customTaskCheck"> </label>
      </div><a class="taskpopover p-1 mr-1 mt-2" data-toggle="popover" data-trigger="hover" data-content="`+ task.task_note + `" style="height:30px;"><img class="img-fluid" src="/info.png" alt="ikona" style="width:25px;" /></a>
      <button class="invisible-button mr-1 mt-2" onclick="addTaskNote(`+ taskId + `)"><img class="img-fluid" src="/note-52.png" alt="ikona" style="width:25px;" /></button>`;
      $('#controlTask').append(controlElement);
      $('[data-toggle="popover"]').popover({ trigger: "hover" });

      //everything is shown, check if there are changes on task or subtasks
      debugger;
      taskShowChanges();
      subtaskShowChanges();
      deleteChangesForTasks(taskId);
    })
  })
}
function addSubtaskNote(id){
  $("#modalNoteForm").modal();
  $('#noteInput').val("");
  debugger;
  subtaskId = id;
  //$('#subtaskList').find('row#1').find('.mypopover').attr('data-content', "kr neki test sam za foro")
}
function addTaskNote(id){
  taskId = id;
  debugger;
  $("#modalTaskNoteForm").modal();
}
//check/uncheck subtask
function toggleSubtaskCheckbox(element){
  //element.checked = !element.checked;
  debugger;
  subtaskId = parseInt(element.parentElement.parentElement.parentElement.id);
  var completed = element.checked;
  $.post('/projects/subtask', {subtaskId, completed}, function(data){
    //debugger;
    //calculate completion via subtask
    var temp = allSubtasks.find(s => s.id == subtaskId)
    if(temp){
      temp.completed = completed;
      var numberOfCompleted = 0;
      for(var i=0; i<allSubtasks.length; i++){
        if(allSubtasks[i].completed)
        numberOfCompleted++;
      }
      var completion = Math.round((numberOfCompleted/allSubtasks.length)*100)
      if(numberOfCompleted == '0' && allSubtasks.length == '0')
        completion = 0;
      //console.log(completion);
      taskId = task.id;
      if(parseInt(task.completion) != 100 && completion == 100){
        addFinishedDate();
      }
      $('#activeTaskProgress')[0].style.width = completion+'%';
      task.completion = completion;
      colorMyActiveTask();
      $.post('/projects/task/completion', {taskId, completion}, function(resp){
        //alert(resp);
        //console.log("post response " + resp);
        if(completion == 100)
          notifyCompletion = true;
        else
          notifyCompletion = false;
        fixCompletion();
        addChange(0,subtaskId);
      })
      debugger;
    }
  })
  //element.parentElement.parentElement.id + parseInt
}
function toggleTaskCheckbox(element){
  debugger;
  var completion;
  if(element.checked){
    completion = 100;
  }
  else{
    completion = 0;
  }
  //TREBA DODAT
  /*
  
  */
  debugger;

  $.post('/projects/task/completion', {taskId, completion}, function(resp){
    //alert(resp);
    //console.log("post response " + resp);
    if(parseInt(task.completion) != 100 && completion == 100){
      addFinishedDate();
    }
    $('#activeTaskProgress')[0].style.width = completion+'%';
    task.completion = completion;
    colorMyActiveTask();
    //$('#MyTaskTable').find('tr#'+taskId).find('td#completionTask').html(completion);
    if(completion == 100)
      notifyCompletion = true;
    else
      notifyCompletion = false;
    fixCompletion();
    addChange(0);
  })
  debugger;
}
function openSubtaskModal(id){
  taskId = id;
  $("#modalSubtasksForm").modal();
}
function deleteSubtask(id){
  debugger;
  $.post('/projects/subtask/delete', {id, taskId}, function(data){
    debugger
    $('#subtaskList').find('row#'+id).remove();
    allSubtasks = allSubtasks.filter(s => s.id != id);
    var numberOfCompleted = 0;
    for(var i=0; i<allSubtasks.length; i++){
      if(allSubtasks[i].completed)
      numberOfCompleted++;
    }
    var completion = Math.round((numberOfCompleted/allSubtasks.length)*100)
    if(numberOfCompleted == 0 && allSubtasks.length == 0)
      completion = 0;
    //console.log(completion);
    taskId = task.id;
    $('#activeTaskProgress')[0].style.width = completion+'%';
    task.completion = completion;
    colorMyActiveTask();
    $.post('/projects/task/completion', {taskId, completion}, function(resp){
      //alert(resp);
      //console.log("post response " + resp);
      fixCompletion();
    })
  })
}
function colorMyActiveTask(){
  $('#myActiveTask').removeClass('bg-success');
  $('#myActiveTask').removeClass('bg-warning');
  $('#myActiveTask').removeClass('bg-priority-low');
  $('#myActiveTask').removeClass('bg-priority-medium');
  $('#myActiveTask').removeClass('bg-priority-high');
  $('#myActiveTask').removeClass('text-white');
  if(task.completion == 100)
    $('#myActiveTask').addClass('bg-success');
  else if(task.priority === 'nizka')
    $('#myActiveTask').addClass('bg-priority-low');
  else if(task.priority === 'srednja')
    $('#myActiveTask').addClass('bg-priority-medium');
  else if(task.priority === 'visoka')
    $('#myActiveTask').addClass('bg-priority-high');
  else if(task.task_finish){
    if(new Date(task.task_finish).getTime() < new Date().getTime()){
      $('#myActiveTask').addClass('bg-warning');
      $('#myActiveTask').addClass('text-white');
    }
  }
}
function openCompletionModal(){
  debugger;
  taskId = task.id;
  //console.log(task);
  //console.log(taskId);
  var completion = task.completion;
  projectId = task.id_project;
  $('#completionPost').val(completion)
  $("#modalCompletionForm").modal();
}
//FIX COMPLETION OF THE PROJECT, task percentage has changed
function fixCompletion(){
  projectId = task.id_project;
  $.post('/projects/completion', {projectId}, function(resp){
    if(resp.success == true)
      console.log("Successfully updated project percentage." );
    else if(resp.success == false)
      console.log("Unsuccessfully updated project percentage, there was no projectId" );
  })
}
/*
function fixCompletion(){
  projectId = task.id_project;
  //if undefined -->> its a servis, task with no project id
  if(projectId){
    $.get('/projects/tasks', {projectId}, function(allTasks){
      //zračunam procent
      debugger;
      var counter = 0;
      var sum = 0;
      for(let i=0; i < allTasks.data.length; i++){
        if(allTasks.data[i].active){
          counter++;
          sum += allTasks.data[i].completion;
        }
      }
      var p = sum / counter;
      p = Math.round(p);
      //!!!!!!!ko bom mel double v bazi!!!!!!!!!!!
      //Math.round((p + 0.00001) * 100) / 100
      //posodobi procent
      $.post('/projects/completion', {projectId, p}, function(resp){
        //alert(resp);
        console.log("post response " + resp);
        debugger
      })
    })
  }
}
*/
function openProjectPage(){
  window.location.href='/projects/id?id='+task.id_project+'';
}
//ADD FINISHED DATE WHEN TASK WAS GIVEN 100 COMPLETION
function addFinishedDate(){
  debugger;
  $.post('/projects/task/finished', {taskId}, function(resp){
    debugger;
  })
}
//funtion for all changes---> based on type and var in function send changes to server
function addChange(type,idSubtask){
  if(idSubtask){
    debugger;
    var notifyWorkers = [];
    var notifyLeaders = [];
    for(var i=0; i<taskWorkers.length; i++){
      if(taskWorkers[i].name != loggedUser.name && taskWorkers[i].surname != loggedUser.surname){
        if(taskIsServis)
          notifyWorkers.push(taskWorkers[i].id);
        else
          notifyWorkers.push(taskWorkers[i].id_user);
      }
    }
    if(!taskIsServis){
      for(var i=0; i<workers.length; i++){
        if(workers[i].workRole == 'vodja'){
          //debugger;
          var x = notifyWorkers.find(n => n == workers[i].id);
          if(x == null && workers[i].name != loggedUser.name && workers[i].surname != loggedUser.surname){
            notifyLeaders.push(workers[i].id);
          }
          //debugger;
        }
      }
    }
    debugger;
    var notify = notifyWorkers.toString();
    var leaders = notifyLeaders.toString();
    if(notifyWorkers.length > 0 || notifyLeaders.length > 0){
      $.post('/changes/note', {projectId, taskId, idSubtask, notify, leaders, type, notifyCompletion, taskIsServis}, function(resp){
        debugger;
      })
    }
  }
  else{
    debugger;
    var notifyWorkers = [];
    var notifyLeaders = [];
    for(var i=0; i<taskWorkers.length; i++){
      if(taskWorkers[i].name != loggedUser.name && taskWorkers[i].surname != loggedUser.surname){
        if(taskIsServis)
          notifyWorkers.push(taskWorkers[i].id);
        else
          notifyWorkers.push(taskWorkers[i].id_user);
      }
    }
    if(!taskIsServis){
      for(var i=0; i<workers.length; i++){
        if(workers[i].workRole == 'vodja'){
          //debugger;
          var x = notifyWorkers.find(n => n == workers[i].id);
          if(x == null && workers[i].name != loggedUser.name && workers[i].surname != loggedUser.surname){
            notifyLeaders.push(workers[i].id);
          }
          //debugger;
        }
      }
    }
    var notify = notifyWorkers.toString();
    var leaders = notifyLeaders.toString();
    debugger;
    if(notifyWorkers.length > 0 || notifyLeaders.length > 0){
      $.post('/changes/note', {projectId, taskId, idSubtask, notify, leaders, type, notifyCompletion, taskIsServis}, function(resp){
        debugger;
        console.log(resp.success);
      })
    }
  }
}

///////VIZUALIZATION////////
function drawTimeline(){
  //debugger;
  makeVizData();
  //debugger;
  if(dataViz.length == 0)
    return
  chart = TimelinesChart()
      .data(dataViz)
      .width(1068)
      .zQualitative(true)
      (document.getElementById('myPlot'));
}
function makeVizData(){
  dataViz = [
    {group:"Nabava", data:[]},
    {group:"Konstrukcija", data:[]},
    {group:"Strojna izdelava", data:[]},
    {group:"Elektro izdelava", data:[]},
    {group:"Montaža", data:[]},
    {group:"Programiranje", data:[]},
    {group:"Brez", data:[]}
  ];
  var delavec;
  for(var i=0; i<allMyTasks.length; i++){
    debugger;
    if(allMyTasks[i].active != true)
      continue;
    if(allMyTasks[i].project_name){
      delavec = allMyTasks[i].project_name.substring(0,5);
      if(allMyTasks[i].project_name.length > 5)
        delavec += "...";
    }
    else
      delavec = "servis"
    var start = allMyTasks[i].task_start;
    var finish = allMyTasks[i].task_finish;
     if(!allMyTasks[i].task_start && !allMyTasks[i].task_finish)
      continue;
    else if(!allMyTasks[i].task_start)
      start = allMyTasks[i].task_finish;
    else if(!allMyTasks[i].task_finish)
      finish = allMyTasks[i].task_start;
    
    if(allMyTasks[i].category === 'Nabava'){
      //debugger;
      dataViz[0].data.push(
        {
          label: allMyTasks[i].task_name,
          data: [
            {
              timeRange:[start, finish],
              val: delavec
            }
          ]
        }
      );
    }
    else if(allMyTasks[i].category === 'Konstrukcija'){
      //debugger;
      dataViz[1].data.push(
        {
          label: allMyTasks[i].task_name,
          data: [
            {
              timeRange:[start, finish],
              val: delavec
            }
          ]
        }
      );
    }
    else if(allMyTasks[i].category === 'Strojna izdelava'){
      dataViz[2].data.push(
        {
          label: allMyTasks[i].task_name,
          data: [
            {
              timeRange:[start, finish],
              val: delavec
            }
          ]
        }
      );
    }
    else if(allMyTasks[i].category === 'Elektro izdelava'){
      dataViz[3].data.push(
        {
          label: allMyTasks[i].task_name,
          data: [
            {
              timeRange:[start, finish],
              val: delavec
            }
          ]
        }
      );
    }
    else if(allMyTasks[i].category === 'Montaža'){
      dataViz[4].data.push(
        {
          label: allMyTasks[i].task_name,
          data: [
            {
              timeRange:[start, finish],
              val: delavec
            }
          ]
        }
      );
    }
    else if(allMyTasks[i].category === 'Programiranje'){
      dataViz[5].data.push(
        {
          label: allMyTasks[i].task_name,
          data: [
            {
              timeRange:[start, finish],
              val: delavec
            }
          ]
        }
      );
    }
    else if(allMyTasks[i].category === 'Brez'){
      dataViz[6].data.push(
        {
          label: allMyTasks[i].task_name,
          data: [
            {
              timeRange:[start, finish],
              val: delavec
            }
          ]
        }
      );
    }
  }
  if(dataViz[6].data.length == 0) dataViz.splice(6,1)
  if(dataViz[5].data.length == 0) dataViz.splice(5,1)
  if(dataViz[4].data.length == 0) dataViz.splice(4,1)
  if(dataViz[3].data.length == 0) dataViz.splice(3,1)
  if(dataViz[2].data.length == 0) dataViz.splice(2,1)
  if(dataViz[1].data.length == 0) dataViz.splice(1,1)
  if(dataViz[0].data.length == 0) dataViz.splice(0,1)
  //debugger;
}
var urlParams = new URLSearchParams(window.location.search);
var dataViz;
var chart;
var taskId;
var projectId;
var task;
var taskWorkers;
var workers;
var allSubtasks;
var subtaskId;
var loggedUser;
var allMyTasks;
var notifyCompletion;
var taskIsServis = false;
var userId = urlParams.get('userId');