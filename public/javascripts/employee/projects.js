$(function(){
  loggedUser = { name: $('#loggedUser').html().split(" (")[0].split(" ")[0], surname: $('#loggedUser').html().split(" (")[0].split(" ")[1], role: $('#loggedUser').html().split(" (")[1].split(")")[0] };
  if($('#projectList').children(":first").length > 0){
    $('#projectList').children(":first").toggleClass("active");
    changeProject(parseInt($('#projectList').children(":first")[0].id));
  }
})
$('.list-group-item').on('click', function() {
  var $this = $(this);
  projectId = parseInt(this.id);
  //debugger
  //$('#taskList').find('a#46').find('#task_name').html();
  // task_name,project_name,date

  $('.active').removeClass('active');
  $this.toggleClass('active')

  changeProject(projectId);
})
function changeProject(id){
  projectId = id;
  //debugger;
  //get project info
  $.get("/projects/info", {projectId}, function(data){
    project = data.data;
    $('#activeProjectSubscriber').html(project.name);
    $('#activeProjectName').html(project.project_name);
    $('#activeProjectProgress')[0].style.width = project.completion+'%';
    $('#myActiveProject').removeClass("border-success");
    if(project.completion == '100'){
      $('#myActiveProject').addClass("border-success");
    }
    var projectStart = '/';
    var projectFinish = '/';
    if(project.start){
      var start = new Date(project.start);
      var d = start.getDate();
      var m = start.getMonth()+1;
      var y = start.getFullYear();
      if(d < 10)
      d = '0'+d;
      if(m < 10)
      m = '0'+m;
      projectStart = d+'.'+m+'.'+y;
      //$('#startEdit').val(y+'-'+m+'-'+d);
    }
    if(project.finish){
      var finish = new Date(project.finish);
      var d = finish.getDate();
      var m = finish.getMonth() + 1;
      var y = finish.getFullYear();
      if (d < 10)
        d = '0' + d;
      if (m < 10)
        m = '0' + m;
      projectFinish = d + '.' + m + '.' + y;
      //$('#finishEdit').val(y+'-'+m+'-'+d);
    }
    $('#activeTaskDate').html(projectStart + " - " + projectFinish);
    //debugger;
    //get project's tasks for specific user
    $.get("/employees/projectTasks", {projectId, userId}, function(data){
      tasks = data.data;
      //debugger;
      $('#projectTasks').empty();
      for(var i = 0; i<data.data.length; i++){
        var taskStart = '/';
        var taskFinish = '/';
        if(data.data[i].task_start){
          var start = new Date(data.data[i].task_start);
          var d = start.getDate();
          var m = start.getMonth()+1;
          var y = start.getFullYear();
          if(d < 10)
          d = '0'+d;
          if(m < 10)
          m = '0'+m;
          taskStart = d+'.'+m+'.'+y;
          //$('#startEdit').val(y+'-'+m+'-'+d);
        }
        if(data.data[i].task_finish){
          var finish = new Date(data.data[i].task_finish);
          var d = finish.getDate();
          var m = finish.getMonth() + 1;
          var y = finish.getFullYear();
          if (d < 10)
            d = '0' + d;
          if (m < 10)
            m = '0' + m;
          taskFinish = d + '.' + m + '.' + y;
          //$('#finishEdit').val(y+'-'+m+'-'+d);
        }
        var date = taskStart + "-" + taskFinish;
        var expired = "";
        if(data.data[i].completion != '100' && data.data[i].expired)
          expired = '<span class="badge badge-danger mr-2 ml-2 p-2">ZAMUDA</span>';
        /*
        var listElement = `<row class="d-flex list-group-item" id=` + data.data[i].id + `>
          <div class="mr-auto" style="width:60%;">
            <label class="row ml-1">`+ data.data[i].task_name + `</label>
            <label class="row ml-1">`+ date + `</label>
            ` + expired + `
          </div>
          <div class="ml-auto progress mt-3" style="width:30%;">
            <div class="progress-bar progress-bar-striped progress-bar-animated" id="activeProjectTaskProgress" style="width:`+data.data[i].completion+`%;"></div>
          </div>
          <a class="btn btn-primary task-button ml-2 mt-1" href="/employees/project?userId=`+userId+`&projectId=`+projectId+`&taskId=`+data.data[i].id+`" style="width:25px, height:5px, border:none;">-></a>
        </row>`;
        */
        var listElement = `<row class="d-flex list-group-item" id=` + data.data[i].id + `>
          <div class="mr-auto" style="width:60%;">
            <label class="row ml-1">`+ data.data[i].task_name + `</label>
            <label class="row ml-1">`+ date + `</label>
            <div id="taskItemBadges">` + expired + `</div>
          </div>
          <div class="ml-auto progress mt-3" style="width:30%;">
            <div class="progress-bar progress-bar-striped progress-bar-animated" id="activeProjectTaskProgress" style="width:`+data.data[i].completion+`%;"></div>
          </div><a class="btn btn-primary task-button ml-2 mt-1" href="/employees/project?userId=`+userId+`&amp;projectId=`+projectId+`&amp;taskId=`+data.data[i].id+`" style="width:25px, height:5px, border:none;">-></a>
        </row>`;
        $('#projectTasks').append(listElement);
      }
      displayTaskChanges();
    })
    //get workers on project
    $.get( "/projects/workerid", {projectId}, function( data ) {
      workers = data.data;
      //debugger;
      $('#projectWorkers').empty();
      for(var i = 0; i < workers.length; i++){
        var image = '';
        if(workers[i].workRole == 'konstruktor' || workers[i].workRole == 'konstrukter')
          image = '<img class="img-fluid" src="/builder64.png" alt="ikona" />';
        else if(workers[i].workRole == 'vodja')
          image = '<img class="img-fluid" src="/manager64.png" alt="ikona" />';
        else if(workers[i].workRole == 'električar')
          image = '<img class="img-fluid" src="/electrican64.png" alt="ikona" />';
        else if(workers[i].workRole == 'strojnik')
          image = '<img class="img-fluid" src="/mechanic64.png" alt="ikona" />';
        else if(workers[i].workRole == 'plc')
          image = '<img class="img-fluid" src="/plc64.png" alt="ikona" />';
        else if(workers[i].workRole == 'kuka')
          image = '<img class="img-fluid" src="/kuka64.png" alt="ikona" />';
        else
          image = '<img class="img-fluid" src="/worker64.png" alt="ikona" />';
        var element = `<div class="col-sm-4 mt-2">
          <div class="row">
            <div class="col-sm-4" id="worker">`+image+`</div>
            <div class="col-sm-8">
              <div id="workerName">` + workers[i].name + ` ` + workers[i].surname + `</div>
              <div id="workerRole">` + workers[i].workRole + `</div>
            </div>
          </div>
        </div>`;
        $('#projectWorkers').append(element);
      }
    })
  })
}
var urlParams = new URLSearchParams(window.location.search);
var userId = urlParams.get('userId');
var projectId;
var project;
var tasks;
var workers;
var loggedUser;