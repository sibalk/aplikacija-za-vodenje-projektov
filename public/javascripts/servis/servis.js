$(function(){
  $('#modalNoteForm').on('hidden.bs.modal', function () {
    // do something…
    $("#modalSubtasksForm").toggle();
    $("#modalSubtasksForm").focus();
  })
  $('.modal-dialog').draggable({
    handle: ".modal-header"
  });

  loggedUser = {name:$('#loggedUser').html().split(" (")[0].split(" ")[0], surname:$('#loggedUser').html().split(" (")[0].split(" ")[1], role:$('#loggedUser').html().split(" (")[1].split(")")[0]};
  activeTask = 1;

  /*
  $.get( "/projects/categories", {projectId}, function( data ) {
    allCategories = data.data; 
  });
  $.get( "/projects/priorities", {projectId}, function( data ) {
    allPriorities = data.data;
  });
  */
  //Table for services (copy of tasks and trowing out unnecesery stuff)
  //Task table
  $.get( "/servis/all", {categoryTask, activeTask}, function( data ) {
    //console.log(data)
    allTasks = data.data;
    initTasks = allTasks;
    multipleWorkers();
    allTasks = vsaOpravila;
    initTasks = vsaOpravila;
    //debugger
    //console.log(dataGet[2]);
    
    tableTasks = $('#tableTasks').DataTable( {
      data: allTasks,
      rowId: "id",
      columns: [
          { title: "id", data: "id" },
          { title: "Ime", data: "task_name" },
          { title: "Naročnik", data: "subscriber" },
          { title: "Naročnik id", data: "id_subscriber" },
          { title: "Kategorija", data: "category", className: "none"},
          { title: "Čas", data: "task_duration", className: "none", render: function(data, type, row){
            if(data)
              return data;
            else
              return "/";
          }},
          { title: "Začetek", data: "task_start",  className: "none", render: function(data, type, row){
            if(data){
              return pad(new Date(data).getDate())+"/"+pad(new Date(data).getMonth()+1)+"/"+new Date(data).getFullYear();
            }
            else
            return "/";
          } },
          { title: "Konec", data: "task_finish",  className: "none", render: function(data, type, row){
            if(data){
              return pad(new Date(data).getDate())+"/"+pad(new Date(data).getMonth()+1)+"/"+new Date(data).getFullYear();
            }
            else
            return "/";
          }},
          { title: "Končano", data: "finished", className: "none", render: function(data, type, row){
            if(data){
              return pad(new Date(data).getDate())+"/"+pad(new Date(data).getMonth()+1)+"/"+new Date(data).getFullYear();
            }
            else
            return "/";
          }},
          { title: "Dokončano", data: "completion"},
          { title: "Dodeljeno", data: "worker", render: function(data, type, row){
            if(data)
              return data;
            else
              return "/";
          }},
          { title: "Prioriteta", data: "priority", className: "none" },
          { title: "Aktivno", data: "active"},{ title: "Podopravila", data: "subtasks"},
          { title: "workersId", data: "workersId"},
          { title: "Dodeljeno", data: "workers",
            render: function(data, type, row){
              var id = row['id'];
              //debugger;
              var element = "";
              for(var i=0; i<data.length; i++){
                if(!data[i])
                  data[i] = "";
                if(i != data.length-1)
                  element = element + data[i] + ", ";
                else
                  element = element + data[i];
              }
              return `<row>
                <a rel="popover" class="mypopover p-1 mr-1 mt-2" id="workersPopover`+id+`" data-toggle="popover" data-trigger="hover" data-content="`+element+`" style="height:30px;"><img class="img-fluid" src="/employees.png" alt="ikona" style="width:25px;" /></a>
              </row>`;
            }},
          { data: "id", 
            render: function(data, type, row){
              //debugger;
              var hisTask = false;
              for(var i=0; i<row['workers'].length; i++){
                if((loggedUser.name + " " + loggedUser.surname) == row['workers'][i])
                  hisTask = true;
              }
              var hasSubtasks = 'disabled=""';
              if(row['subtasks'] == 0)
                hasSubtasks = '';
              if(!hisTask && !(loggedUser.role == 'admin') && !(loggedUser.role == 'vodja'))
                hasSubtasks = 'disabled=""';
              if(row['completion'] == 100){
                return `<div class="custom-control custom-checkbox table-custom-checkbox-task" id=`+data+`>
                  <input class="custom-control-input" id="customTaskCheck`+data+`" type="checkbox" checked ="" `+hasSubtasks+` onchange="toggleTaskCheckbox(this)" />
                  <label class="custom-control-label" for="customTaskCheck`+data+`"> </label>
                </div>`;
              }
              else{
                return `<div class="custom-control custom-checkbox table-custom-checkbox-task" id=`+data+`>
                  <input class="custom-control-input" id="customTaskCheck`+data+`" type="checkbox" `+hasSubtasks+` onchange="toggleTaskCheckbox(this)" />
                  <label class="custom-control-label" for="customTaskCheck`+data+`"> </label>
                </div>`;
              }
            }},
          { title: "Info", data: "task_note",
            render: function(data, type, row){
              var id = row['id'];
              return `<row>
              <a rel="popover" class="mypopover p-1 mr-1 mt-2" id="taskPopover`+id+`" data-toggle="popover" data-trigger="hover" data-content="`+data+`" style="height:30px;"><img class="img-fluid" src="/info.png" alt="ikona" style="width:25px;" /></a>
              <button class="invisible-button mr-1 mt-2" onclick="addTaskNote(`+id+`)"><img class="img-fluid" src="/note-52.png" alt="ikona" style="width:25px;" /></button>
            </row>`;
            }},
          { data: "id",
            render: function ( data, type, row ) {
              var hisTask = false;
              for(var i=0; i<row['workers'].length; i++){
                if((loggedUser.name + " " + loggedUser.surname) == row['workers'][i])
                  hisTask = true;
              }
              if(loggedUser.role == 'admin' || loggedUser.role == 'vodja')
                return '<button class="btn btn-info" onclick="openSubtaskModal('+data+')">Naloge</button>';
              else if(hisTask)
                return '<button class="btn btn-info" onclick="openSubtaskModal('+data+')">Naloge</button>';
              else
                return '<div></div>';
            }},
          { data: "id", className: "none",
            render: function ( data, type, row ) {
              if(loggedUser.role == 'admin' || loggedUser.role == 'vodja')
                return '<button class="btn btn-danger" onclick="deleteServis('+data+')">Odstrani</button>';
              else
                return '<div></div>';
            }},
          { data: "id",
            render: function ( data, type, row ) {
              //console.log(loggedUser);
              var hisTask = false;
              for(var i=0; i<row['workers'].length; i++){
                if((loggedUser.name + " " + loggedUser.surname) == row['workers'][i])
                  hisTask = true;
              }
              if(loggedUser.role == 'admin' || loggedUser.role == 'vodja')
                return '<button class="btn btn-primary" onclick="editTask('+data+')">Uredi</button>';
              else if(hisTask){
                return '<button class="btn btn-primary" onclick="editCompletion('+data+')">Uredi</button>';
              }
              else
                return '<div></div>';
            }},
      ],
      "language": {
        "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Slovenian.json"
      },
      order: [[0, "desc"]],
      columnDefs: [
        {
          targets: [0,12,13,14,10,3],
          visible: false,
          searchable: false
        },
        {
          targets: [9,10,11],
          name: 'control',
        },
        { responsivePriority: 1, targets: 1 },
        { responsivePriority: 5, targets: -1 },
        { responsivePriority: 100004, targets: -2 },
        { responsivePriority: 2, targets: -3 },
        { responsivePriority: 4, targets: -4 },
        { responsivePriority: 3, targets: -5 },
        { responsivePriority: 100003, targets: 8 },
        { responsivePriority: 100002, targets: 4 },
        { responsivePriority: 100001, targets: 5 },
        { responsivePriority: 100005, targets: 6 },
        { responsivePriority: 100006, targets: 7 },
      ],
      info: false,
      //searching: false,
      paging: true,
      responsive: true,
      //scrollY: "400px",
      //scrollCollapse: true,
      createdRow: function (row, data, index){
        //debugger
        $('[data-toggle="popover"]').popover({trigger: "hover"});
        $(row).attr('id', data.id);
        $(row).removeClass('table-dark');
        $(row).removeClass('table-success');
        $(row).removeClass('table-warning');
        $(row).removeClass('priority-low');
        $(row).removeClass('priority-medium');
        $(row).removeClass('priority-high');
        if(!data.active){
          $(row).addClass('table-dark');
        }
        else if(data.completion == 100){
          $(row).addClass('table-success');
        }
        else if(data.priority == 'nizka'){
          $(row).addClass('priority-low');
        }
        else if(data.priority == 'srednja'){
          $(row).addClass('priority-medium');
        }
        else if(data.priority == 'visoka'){
          $(row).addClass('priority-high');
        }
        else if(data.task_finish){
          if(new Date(data.task_finish).getTime() < new Date().getTime())
            $(row).addClass('table-warning');
        }
      },
      
      rowCallback: function(row, data, index){
        $('[data-toggle="popover"]').popover({trigger: "hover"});
        if(data['active'] == false && loggedUser.role != 'admin'){
          $(row).hide();
          //tableTasks.columns('active').visible(false);
          //tableTasks.columns([9]).visible(false);
        }
        $(row).removeClass('table-dark');
        $(row).removeClass('table-success');
        $(row).removeClass('table-warning');
        $(row).removeClass('priority-low');
        $(row).removeClass('priority-medium');
        $(row).removeClass('priority-high');
        if(!data.active){
          $(row).addClass('table-dark');
        }
        else if(data.completion == 100){
          $(row).addClass('table-success');
        }
        else if(data.priority == 'nizka'){
          $(row).addClass('priority-low');
        }
        else if(data.priority == 'srednja'){
          $(row).addClass('priority-medium');
        }
        else if(data.priority == 'visoka'){
          $(row).addClass('priority-high');
        }
        else if(data.task_finish){
          if(new Date(data.task_finish).getTime() < new Date().getTime())
            $(row).addClass('table-warning');
        }
      },
      
    });
    /*
    if(loggedUser != 'admin' || loggedUser != 'vodja'){
      //tableTasks.columns('active').visible(false);
      //tableTasks.column('control:name').visible(false);
      //tableTasks.columns([9]).visible(false);
    }
    */
    setTimeout(()=>{
      $('[data-toggle="popover"]').popover({trigger: "hover"});
    })
   //debugger;
  });
  //$('[data-toggle="popover"]').popover({trigger: "hover"});

  //ADD NEW TASK TO PROJECT
  $('#addServisform').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var servisName = $("#name").val();
    var servisSubscriber = $("#subscriber").val();
    var servisDuration = $("#duration").val();
    var servisStart = $("#start").val();
    var servisFinish = $("#finish").val();
    var servisAssignment = $("#assignment").val();
    var servisAssignmentArray = $("#assignment").val();
    var servisCategory = category.value;
    var servisPriority = priority.value;
    if(servisStart)
      servisStart = servisStart + " 07:00:00.000000";
    if(servisFinish)
      servisFinish = servisFinish + " 15:00:00.000000";
    debugger
    //chech if subscriber exist
    if(isNaN(servisSubscriber)){
      //new subscriber, create subscriber and then create servis
      var subscriber = servisSubscriber
      $.post('/subscribers/create', {subscriber}, function(resp){
        //resp.id je id novega subscriberja
        var subscriberName = servisSubscriber;
        servisSubscriber = resp.id.id;
        debugger;
        if(servisAssignmentArray.length > 0){
          //workers
          var servisAssignmentArrayString = servisAssignmentArray.toString();
          $.post('/servis/add', {servisSubscriber, servisName, servisDuration, servisStart, servisFinish, servisAssignmentArrayString, servisCategory, servisPriority}, function(resp){
            //alert(resp);
            $('#modalAddTaskForm').modal('toggle');
            $('#name').val("");
            $('#duration').val("");
            $('#start').val("");
            $('#finish').val("");
            $('#subscriber').val(1).trigger('change');
            $('#category').val(1).trigger('change');
            $('#priority').val(1).trigger('change');
            var categoryName = allCategories.find(c => c.id === parseInt(servisCategory));
            var priorityName = allPriorities.find(p => p.id === parseInt(servisPriority));
            var workerName = [];
            for(var i=0; i<servisAssignmentArray.length; i++){
              workerName.push((allWorkers.find(w => w.id === parseInt(servisAssignmentArray[i]))).text);
            }
            debugger;
            //workerName = allWorkers.find(w => w.id === parseInt(taskAssignment));
            //add to table
            initTasks.push({
              id:resp.data, 
              priority:priorityName.text, 
              task_duration:servisDuration, 
              task_finish:new Date(servisFinish), 
              task_name: servisName, 
              task_start:new Date(servisStart), 
              worker:workerName[0], 
              active:true, 
              category: categoryName.text,
              subscriber: subscriberName, 
              id_subscriber: servisSubscriber,
              completion:0,
              subtasks: 0,
              workers: workerName,
              workersId: servisAssignmentArray,
              priority: priorityName.text,
            })
            debugger;
            tableTasks.row.add({
              id: resp.data, 
              task_name: servisName,
              category: categoryName.text,
              subscriber: subscriberName,
              id_subscriber: servisSubscriber,
              task_duration: servisDuration,
              task_start: servisStart,
              task_finish: servisFinish,
              completion: 0,
              subtasks: 0,
              worker: workerName[0],
              workers: workerName,
              workersId: servisAssignmentArray,
              priority: priorityName.text,
              active: true
            }).draw(false)
            debugger;
            $('#modalAddServisForm').modal('toggle');
            $('[data-toggle="popover"]').popover({trigger: "hover"});
          })
        }
        else{
          //no assignment
          debugger;
          var servisAssignmentArrayString = servisAssignmentArray.toString();
          $.post('/servis/add', {servisSubscriber, servisName, servisDuration, servisStart, servisFinish, servisAssignmentArrayString, servisCategory, servisPriority}, function(resp){
            //alert(resp);
            $('#modalAddServisForm').modal('toggle');
            $('#name').val("");
            $('#duration').val("");
            $('#start').val("");
            $('#finish').val("");
            $('#subscriber').val(1).trigger('change');
            $('#category').val(1).trigger('change');
            $('#priority').val(1).trigger('change');
            var categoryName = allCategories.find(c => c.id === parseInt(servisCategory));
            var priorityName = allPriorities.find(p => p.id === parseInt(servisPriority));
            var subscriberName = subscribers.find(s => s.id === parseInt(servisSubscriber));
            //add to table
            initTasks.push({
              id:resp.data, 
              priority:priorityName.text, 
              task_duration:servisDuration, 
              task_finish:new Date(servisFinish), 
              task_name: servisName, 
              task_start:new Date(servisStart), 
              worker:"", 
              active:true, 
              category: categoryName.text,
              subscriber: subscriberName.text,
              id_subscriber: servisSubscriber,
              completion:0, 
              subtasks: 0,
              workersId: [], 
              workers: [], 
              priority: priorityName.text})
            debugger;
            tableTasks.row.add({
              id: resp.data, 
              task_name: servisName,
              category: categoryName.text,
              subscriber: subscriberName.text,
              id_subscriber: servisSubscriber,
              task_duration: servisDuration,
              task_start: servisStart,
              task_finish: servisFinish,
              completion: 0,
              subtasks: 0,
              worker: "",
              workers: [],
              workersId: [],
              priority: priorityName.text,
              active: true
            }).draw(false)
            debugger;
            $('#modalAddServisForm').modal('toggle');
          })
        }
      })
    }
    else{
      //subscriber already exist, create servis
      if(servisAssignmentArray.length > 0){
        //workers
        var servisAssignmentArrayString = servisAssignmentArray.toString();
        $.post('/servis/add', {servisSubscriber, servisName, servisDuration, servisStart, servisFinish, servisAssignmentArrayString, servisCategory, servisPriority}, function(resp){
          //alert(resp);
          $('#modalAddTaskForm').modal('toggle');
          $('#name').val("");
          $('#duration').val("");
          $('#start').val("");
          $('#finish').val("");
          $('#subscriber').val(1).trigger('change');
          $('#category').val(1).trigger('change');
          $('#priority').val(1).trigger('change');
          var categoryName = allCategories.find(c => c.id === parseInt(servisCategory));
          var priorityName = allPriorities.find(p => p.id === parseInt(servisPriority));
          var subscriberName = subscribers.find(s => s.id === parseInt(servisSubscriber));
          var workerName = [];
          for(var i=0; i<servisAssignmentArray.length; i++){
            workerName.push((allWorkers.find(w => w.id === parseInt(servisAssignmentArray[i]))).text);
          }
          debugger;
          //workerName = allWorkers.find(w => w.id === parseInt(taskAssignment));
          //add to table
          initTasks.push({
            id:resp.data, 
            priority:priorityName.text, 
            task_duration:servisDuration, 
            task_finish:new Date(servisFinish), 
            task_name: servisName, 
            task_start:new Date(servisStart), 
            worker:workerName[0], 
            active:true, 
            category: categoryName.text,
            subscriber: subscriberName.text, 
            id_subscriber: servisSubscriber,
            completion:0,
            subtasks: 0,
            workers: workerName,
            workersId: servisAssignmentArray,
            priority: priorityName.text,
          })
          debugger;
          tableTasks.row.add({
            id: resp.data, 
            task_name: servisName,
            category: categoryName.text,
            subscriber: subscriberName.text,
            id_subscriber: servisSubscriber,
            task_duration: servisDuration,
            task_start: servisStart,
            task_finish: servisFinish,
            completion: 0,
            subtasks: 0,
            worker: workerName[0],
            workers: workerName,
            workersId: servisAssignmentArray,
            priority: priorityName.text,
            active: true
          }).draw(false)
          debugger;
          $('#modalAddServisForm').modal('toggle');
          $('[data-toggle="popover"]').popover({trigger: "hover"});
        })
      }
      else{
        //no assignment
        debugger;
        var servisAssignmentArrayString = servisAssignmentArray.toString();
        $.post('/servis/add', {servisSubscriber, servisName, servisDuration, servisStart, servisFinish, servisAssignmentArrayString, servisCategory, servisPriority}, function(resp){
          //alert(resp);
          $('#modalAddServisForm').modal('toggle');
          $('#name').val("");
          $('#duration').val("");
          $('#start').val("");
          $('#finish').val("");
          $('#subscriber').val(1).trigger('change');
          $('#category').val(1).trigger('change');
          $('#priority').val(1).trigger('change');
          var categoryName = allCategories.find(c => c.id === parseInt(servisCategory));
          var priorityName = allPriorities.find(p => p.id === parseInt(servisPriority));
          var subscriberName = subscribers.find(s => s.id === parseInt(servisSubscriber));
          //add to table
          initTasks.push({
            id:resp.data, 
            priority:priorityName.text, 
            task_duration:servisDuration, 
            task_finish:new Date(servisFinish), 
            task_name: servisName, 
            task_start:new Date(servisStart), 
            worker:"", 
            active:true, 
            category: categoryName.text,
            subscriber: subscriberName.text,
            id_subscriber: servisSubscriber,
            completion:0, 
            subtasks: 0,
            workersId: [], 
            workers: [], 
            priority: priorityName.text})
          debugger;
          tableTasks.row.add({
            id: resp.data, 
            task_name: servisName,
            category: categoryName.text,
            subscriber: subscriberName.text,
            id_subscriber: servisSubscriber,
            task_duration: servisDuration,
            task_start: servisStart,
            task_finish: servisFinish,
            completion: 0,
            subtasks: 0,
            worker: "",
            workers: [],
            workersId: [],
            priority: priorityName.text,
            active: true
          }).draw(false)
          debugger;
          $('#modalAddServisForm').modal('toggle');
        })
      }
    }

    $('[data-toggle="popover"]').popover({trigger: "hover"});
  })
  //UPDATE THE CHOSEN TASK
  $('#updateTask_form').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var servisName = $("#task_nameEdit").val();
    var servisSubscriber = $("#subscriberEdit").val();
    var servisDuration = $("#durationEdit").val();
    var servisStart = $("#startEdit").val();
    var servisFinish = $("#finishEdit").val();
    var servisAssignment = $("#assignmentEdit").val();
    var servisAssignmentArray = $("#assignmentEdit").val();
    var servisCompletion = $("#completionEdit").val();
    var servisCategory = $("#categoryEdit").val();
    var servisPriority = $("#priorityEdit").val();
    var taskActive
    if(typeof activeEdit === 'undefined')
      taskActive = true;
    else
      taskActive = activeEdit.checked;
    if(servisStart)
      servisStart = servisStart + " 07:00:00.000000";
    if(servisFinish)
      servisFinish = servisFinish + " 15:00:00.000000";
    debugger;
    if(servisAssignmentArray.length > 0){
      servisAssignment = servisAssignmentArray.toString();
      debugger;
      var servisAssignmentArrayString = servisAssignmentArray.toString();
      $.post('/servis/update', {servisSubscriber, taskId, servisName, servisDuration, servisStart, servisFinish, servisAssignmentArrayString, servisCompletion, taskActive, servisCategory, servisPriority}, function(resp){
        //alert(resp);
        //console.log("post response " + resp);
        
        //popravi tabelo
        var temp = tableTasks.row('#'+taskId+'').data();
        debugger
        //console.log(temp.active);
        temp.task_name = servisName;
        temp.task_duration = servisDuration;
        temp.task_start = servisStart;
        temp.task_finish = servisFinish;
        if(parseInt(temp.completion) != 100 && servisCompletion == 100){
          addFinishedDate();
          temp.finished = new Date();
        }
        temp.completion = servisCompletion;
        temp.active = taskActive;
        var workerName = [];
        for(var i=0; i<servisAssignmentArray.length; i++){
          workerName.push((allWorkers.find(w => w.id === parseInt(servisAssignmentArray[i]))).text);
        }
        var category = allCategories.find(c => c.id === parseInt(servisCategory));
        var priority = allPriorities.find(p => p.id === parseInt(servisPriority));
        var subscriber = subscribers.find(s => s.id === parseInt(servisSubscriber));
        if(workerName)
          temp.worker = workerName[0];
        else
          temp.worker = "";
        temp.workers = workerName;
        temp.workersId = servisAssignmentArray;
        temp.category = category.text;
        temp.priority = priority.text;
        temp.subscriber = subscriber.text;
        temp.id_subscriber = servisSubscriber;
        tableTasks.row('#'+taskId).data(temp).invalidate().draw(false);
        //pos = initTasks.map(function(e) { return e.id; }).indexOf(parseInt(taskId));
        debugger;
        $('#modalUpdateTaskForm').modal('toggle');
      })
    }
    else{
      //brez delavca
      debugger;
      var servisAssignmentArrayString = servisAssignmentArray.toString();
      $.post('/servis/update', {servisSubscriber, taskId, servisName, servisDuration, servisStart, servisFinish, servisAssignmentArrayString, servisCompletion, taskActive, servisCategory, servisPriority}, function(resp){
        //alert(resp);
        //console.log("post response " + resp);
        
        //popravi tabelo
        var temp = tableTasks.row('#'+taskId+'').data();
        debugger
        //console.log(temp.active);
        temp.task_name = servisName;
        temp.task_duration = servisDuration;
        temp.task_start = servisStart;
        temp.task_finish = servisFinish;
        if(parseInt(temp.completion) != 100 && servisCompletion == 100){
          addFinishedDate();
          temp.finished = new Date();
        }
        temp.completion = servisCompletion;
        temp.active = taskActive;
        var category = allCategories.find(c => c.id === parseInt(servisCategory));
        var priority = allPriorities.find(p => p.id === parseInt(servisPriority));
        var subscriber = subscribers.find(s => s.id === parseInt(servisSubscriber));
        temp.worker = "";
        temp.workers = [];
        temp.workersId = [];
        temp.category = category.text;
        temp.priority = priority.text;
        temp.subscriber = subscriber.text;
        temp.id_subscriber = servisSubscriber;
        tableTasks.row('#'+taskId).data(temp).invalidate().draw(false);
        //pos = initTasks.map(function(e) { return e.id; }).indexOf(parseInt(taskId));
        debugger;
        //popravi procent projekta
        $('#modalUpdateTaskForm').modal('toggle');
      })
    }
  })
  //CHANGE NOTE OF THE TASK
  $('#taskNote-form').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var newNote = taskNoteInput.value;
    $.post('/projects/note', {taskId, newNote}, function(resp){
      debugger;
      $('#taskPopover'+taskId).attr('data-content', newNote);
      $('#taskNoteInput').val("");
      $("#modalTaskNoteForm").modal('toggle');
      //$("#modalSubtasksForm").modal('toggle');
    })
  })
  //CHANGE NOTE OF THE SUBTASK
  $('#note-form').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var newNote = noteInput.value;
    $.post('/projects/subtask/note', {subtaskId, newNote}, function(resp){
      debugger;
      $('#subtaskList').find('row#'+subtaskId).find('.mypopover').attr('data-content', newNote);
      $('#noteInput').val("");
      $("#modalNoteForm").modal('toggle');
      //$("#modalSubtasksForm").modal('toggle');
    })
  })
  //SUBTASKS
  $('#subtasks-form').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var newSubtask = subtaskInput.value;
    debugger;
    $.post('/projects/subtask/add', {taskId, newSubtask}, function(resp){
      debugger;
      var checkedCB = '';
      var popoverText = "";
      var newId = resp.data.id;
      var listElement = `<row class="d-flex list-group-item" id=`+newId+`>
        <label class="p-1" style="width:70%;">`+newSubtask+`</label>
        <div class="custom-control custom-checkbox">
          <input class="custom-control-input" id="customCheck`+newId+`" type="checkbox" `+checkedCB+` onchange="toggleSubtaskCheckbox(this)" />
          <label class="custom-control-label" for="customCheck`+newId+`"> </label>
        </div><a rel="popover" class="mypopover p-1 mr-1 mt-2" data-toggle="popover" data-trigger="hover" data-content="`+popoverText+`" style="height:30px;"><img class="img-fluid" src="/info.png" alt="ikona" style="width:25px;" /></a>
        <button class="invisible-button mr-1 mt-2" onclick="addSubtaskNote(`+newId+`)"><img class="img-fluid" src="/note-52.png" alt="ikona" style="width:25px;" /></button>
        <button class="btn btn-danger task-button mr-2 mt-1" onclick="deleteSubtask(`+newId+`)" style="width:25px, height:5px, border:none;">X</button>
      </row>`
      $('#subtaskList').append(listElement);
      $('[data-toggle="popover"]').popover({trigger: "hover"});
      allSubtasks.push({id:newId, id_task: taskId, name: newSubtask, note: null, completed: false});
      //pri novi nalogi ne disabla taska, refresh pa dela normalno6
      //$('#customTaskCheck'+taskId).attr('disabled', 'disabled');
      var numberOfCompleted = 0;
      for(var i=0; i<allSubtasks.length; i++){
        if(allSubtasks[i].completed)
        numberOfCompleted++;
      }
      var completion = Math.round((numberOfCompleted/allSubtasks.length)*100)
      if(numberOfCompleted == '0' && allSubtasks.length == '0')
        completion = 0;
      //console.log(completion);
      $.post('/projects/task/completion', {taskId, completion}, function(resp){
        //alert(resp);
        //console.log("post response " + resp);
        allTasks.find(t=>t.id == taskId).subtasks = allSubtasks.length;
        var temp = tableTasks.row('#'+taskId+'').data();
        temp.tasks = allSubtasks.length;
        if(parseInt(temp.completion) != 100 && completion == 100){
          addFinishedDate();
          temp.finished = new Date();
        }
        temp.completion = completion;
        tableTasks.row('#'+taskId).data(temp).invalidate().draw(false);
      })
      $('#subtaskInput').val("");
    })
  })
})
//open modal for adding new servis; load users, categories, priorities, subscribers
function openAddServisModal(){
  $.get( "/projects/users", function( data ) {
    allWorkers = data.data;
    $(".select2-workersAdd").select2({
      data: allWorkers,
      tags: false,
      multiple: true,
      dropdownParent: $('#modalAddServisForm')
    });
    $.get( "/projects/categories", function( data ) {
      allCategories = data.data;
      $(".select2-categoryAdd").select2({
        data: allCategories,
        tags: false,
        dropdownParent: $('#modalAddServisForm')
      });  
      $.get( "/projects/priorities", function( data ) {
        allPriorities = data.data;
        $(".select2-priorityAdd").select2({
          data: allPriorities,
          tags: false,
          dropdownParent: $('#modalAddServisForm')
        });
        $.get( "/subscribers/all", function( data ) {
          subscribers = data.data;
    
          $(".select2-subscriber").select2({
            data: subscribers,
            tags: true,
            dropdownParent: $('#modalAddServisForm')
          });
          $('#assignment').val(0).trigger('change');
          $("#modalAddServisForm").modal();
        });
      });
    });  
  });
}
//edit servis
function editTask(id){
  taskId = id;
  var row = tableTasks.row('#'+taskId).data();
  debugger;
  $.get( "/projects/users", function( data ) {
    allWorkers = data.data;
    $(".select2-workersEdit").select2({
      data: allWorkers,
      tags: false,
      multiple: true,
      dropdownParent: $('#modalUpdateTaskForm')
    });
    $.get("/projects/categories", function( data ) {
      allCategories = data.data;
      $(".select2-categoryEdit").select2({
        data: allCategories,
        tags: false,
        dropdownParent: $('#modalUpdateTaskForm')
      });
      $.get("/projects/priorities", function( data ) {
        allPriorities = data.data;
        $(".select2-priorityEdit").select2({
          data: allPriorities,
          tags: false,
          dropdownParent: $('#modalUpdateTaskForm')
        });
        $.get( "/subscribers/all", function( data ) {
          subscribers = data.data;
    
          $(".select2-subscriberEdit").select2({
            data: subscribers,
            tags: true,
            dropdownParent: $('#modalUpdateTaskForm')
          });
          if(row.worker){
            //var workerName = allWorkers.find(w => w.text === row.worker)
            $('#assignmentEdit').val(row.workersId).trigger('change');
          }
          else
            $('#assignmentEdit').val(0).trigger('change');
          $('#task_nameEdit').val(row.task_name);
          $('#durationEdit').val(parseInt(row.task_duration));
          if(row.task_start){
            var start = new Date(row.task_start);
            var d = start.getDate();
            var m = start.getMonth()+1;
            var y = start.getFullYear();
            if(d < 10)
              d = '0'+d;
            if(m < 10)
              m = '0'+m;
            $('#startEdit').val(y+'-'+m+'-'+d);
          }
          if(row.task_finish){
            var finish = new Date(row.task_finish);
            var d = finish.getDate();
            var m = finish.getMonth()+1;
            var y = finish.getFullYear();
            if(d < 10)
            d = '0'+d;
            if(m < 10)
            m = '0'+m;
            $('#finishEdit').val(y+'-'+m+'-'+d);
          }
          $('#completionEdit').val(row.completion);
          var category = allCategories.find(c => c.text === row.category);
          var priority = allPriorities.find(p => p.text === row.priority);
          //var subscriber = subscribers.find(s => s.text === row.subscriber);
          debugger;
          $('#categoryEdit').val(category.id).trigger('change');
          $('#priorityEdit').val(priority.id).trigger('change');
          $('#subscriberEdit').val(row.id_subscriber).trigger('change');
          if(row.active){
            //debugger
            $("#activeEdit").prop('checked', 1);
          }
          else{
            //debugger
            $("#activeEdit").prop('checked', 0);
          }
          $("#modalUpdateTaskForm").modal();  
        });
      })  
    })
  });
}

//mark as inactive, could be mistake
function deleteServis(taskId){
  debugger;
  $.post('/projects/removetask', {taskId}, function(resp){
    //odstrani iz tabele na strani
    var temp = tableTasks.row('#'+taskId+'').data();
    //console.log(temp.active);
    temp.active = false;
    tableTasks.row('#'+taskId).data(temp).invalidate().draw(false);
    pos = initTasks.map(function(e) { return e.id; }).indexOf(parseInt(taskId));
    initTasks[pos].active = false;
  })
}
//marks as complete servis; as 100%
function toggleTaskCheckbox(element){
  taskId = element.parentElement.id;
  debugger;
  var completion;
  if(element.checked){
    completion = 100;
  }
  else{
    completion = 0;
  }
  $.post('/projects/task/completion', {taskId, completion}, function(resp){
    //alert(resp);
    //console.log("post response " + resp);
    var temp = tableTasks.row('#'+taskId+'').data();
    if(parseInt(temp.completion) != 100 && completion == 100){
      addFinishedDate();
      temp.finished = new Date();
    }
    temp.completion = completion;
    tableTasks.row('#'+taskId).data(temp).invalidate().draw(false);
  })
  debugger;
}
//open modal to type note for servis
function addTaskNote(id){
  taskId = id;
  $("#modalTaskNoteForm").modal();
  debugger;
}
//TOGGLE BETWEEN MODAL FOR NOTE AND ADD NOTE; open modal for subtask note
function addSubtaskNote(id){
  $("#modalSubtasksForm").toggle();
  $("#modalNoteForm").modal();
  $('#noteInput').val("");
  debugger;
  subtaskId = id;
  //$('#subtaskList').find('row#1').find('.mypopover').attr('data-content', "kr neki test sam za foro")
}
//check/uncheck subtask
function toggleSubtaskCheckbox(element){
  //element.checked = !element.checked;
  debugger;
  subtaskId = parseInt(element.parentElement.parentElement.id);
  var completed = element.checked;
  $.post('/projects/subtask', {subtaskId, completed}, function(data){
    //debugger;
    //calculate completion via subtask
    var temp = allSubtasks.find(s => s.id == subtaskId)
    if(temp){
      temp.completed = completed;
      var numberOfCompleted = 0;
      for(var i=0; i<allSubtasks.length; i++){
        if(allSubtasks[i].completed)
        numberOfCompleted++;
      }
      var completion = Math.round((numberOfCompleted/allSubtasks.length)*100)
      if(numberOfCompleted == '0' && allSubtasks.length == '0')
        completion = 0;
      //console.log(completion);
      $.post('/projects/task/completion', {taskId, completion}, function(resp){
        //alert(resp);
        //console.log("post response " + resp);
        var temp = tableTasks.row('#'+taskId+'').data();
        if(parseInt(temp.completion) != 100 && completion == 100){
          addFinishedDate();
          temp.finished = new Date();
        }
        temp.completion = completion;
        tableTasks.row('#'+taskId).data(temp).invalidate().draw(false);
      })
      debugger;
    }
  })
  //element.parentElement.parentElement.id + parseInt
}
//open modal with subtasks
function openSubtaskModal(id){
  taskId = id;
  $.get('/projects/subtasks', {taskId}, function(data){
    allSubtasks = data.data;
    debugger;
    if(!(allTasks.find(t=>t.id == taskId).subtasks == allSubtasks.length)){
      allTasks.find(t=>t.id == taskId).subtasks = allSubtasks.length;
      var temp = tableTasks.row('#'+taskId+'').data();
      temp.tasks = allSubtasks.length;
      var count = temp.tasks;
      //fix number of subtasks in database...
      $.post('/projects/count', {taskId, count}, function(data){
        debugger;
        tableTasks.row('#'+taskId).data(temp).invalidate().draw(false);
      })
    }
    $('#subtaskList').empty();
    for(var i = 0; i<data.data.length; i++){
      var checkedCB = '';
      if(data.data[i].completed)
        checkedCB = 'checked=""';
      var popoverText = "";
      if(data.data[i].note)
        popoverText = data.data[i].note;
      if(loggedUser.role == 'admin' || loggedUser.role == 'vodja')
        var listElement = `<row class="d-flex list-group-item" id=`+data.data[i].id+`>
          <label class="p-1" style="width:70%;">`+data.data[i].name+`</label>
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input" id="customCheck`+data.data[i].id+`" type="checkbox" `+checkedCB+` onchange="toggleSubtaskCheckbox(this)" />
            <label class="custom-control-label" for="customCheck`+data.data[i].id+`"> </label>
          </div><a rel="popover" class="mypopover p-1 mr-1 mt-2" data-toggle="popover" data-trigger="hover" data-content="`+popoverText+`" style="height:30px;"><img class="img-fluid" src="/info.png" alt="ikona" style="width:25px;" /></a>
          <button class="invisible-button mr-1 mt-2" onclick="addSubtaskNote(`+data.data[i].id+`)"><img class="img-fluid" src="/note-52.png" alt="ikona" style="width:25px;" /></button>
          <button class="btn btn-danger task-button mr-2 mt-1" onclick="deleteSubtask(`+data.data[i].id+`)" style="width:25px, height:5px, border:none;">X</button>
        </row>`
      else
        var listElement = `<row class="d-flex list-group-item" id=`+data.data[i].id+`>
          <label class="p-1" style="width:70%;">`+data.data[i].name+`</label>
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input" id="customCheck`+data.data[i].id+`" type="checkbox" `+checkedCB+` onchange="toggleSubtaskCheckbox(this)" />
            <label class="custom-control-label" for="customCheck`+data.data[i].id+`"> </label>
          </div><a rel="popover" class="mypopover p-1 mr-1 mt-2" data-toggle="popover" data-trigger="hover" data-content="`+popoverText+`" style="height:30px;"><img class="img-fluid" src="/info.png" alt="ikona" style="width:25px;" /></a>
          <button class="invisible-button mr-1 mt-2" onclick="addSubtaskNote(`+data.data[i].id+`)"><img class="img-fluid" src="/note-52.png" alt="ikona" style="width:25px;" /></button>
        </row>`
      $('#subtaskList').append(listElement);
      $('[data-toggle="popover"]').popover({trigger: "hover"});
    }
    
    $("#modalSubtasksForm").modal();
  })
}
//DELETE SELECTED SUBTASK
function deleteSubtask(id){
  debugger;
  $.post('/projects/subtask/delete', {id, taskId}, function(data){
    debugger
    $('#subtaskList').find('row#'+id).remove();
    allSubtasks = allSubtasks.filter(s => s.id != id);
    var numberOfCompleted = 0;
    for(var i=0; i<allSubtasks.length; i++){
      if(allSubtasks[i].completed)
      numberOfCompleted++;
    }
    var completion = Math.round((numberOfCompleted/allSubtasks.length)*100)
    if(numberOfCompleted == 0 && allSubtasks.length == 0)
      completion = 0;
    //console.log(completion);
    $.post('/projects/task/completion', {taskId, completion}, function(resp){
      //alert(resp);
      //console.log("post response " + resp);
      allTasks.find(t=>t.id == taskId).subtasks = allSubtasks.length;
      var temp = tableTasks.row('#'+taskId+'').data();
      if(parseInt(temp.completion) != 100 && completion == 100){
        addFinishedDate();
        temp.finished = new Date();
      }
      temp.completion = completion;
      temp.tasks = allSubtasks.length;
      tableTasks.row('#'+taskId).data(temp).invalidate().draw(false);
    })
  })
}
//ADD FINISHED DATE WHEN TASK WAS GIVEN 100 COMPLETION
function addFinishedDate(){
  $.post('task/finished', {taskId}, function(resp){
    debugger;
  })
}
//CHANGE DATA IN TASKS TABLE BY CATEGORY
function changeTaskTable(category){
  debugger;
  $('#option0').removeClass('active');
  $('#option1').removeClass('active');
  $('#option2').removeClass('active');
  $('#option3').removeClass('active');
  $('#option4').removeClass('active');
  $('#option5').removeClass('active');
  $('#option6').removeClass('active');
  $('#option7').removeClass('active');
  setTimeout(()=>{
    $('#option'+category+'').addClass('active');
  })
  categoryTask = category;
  if($('#optionAll').hasClass('active'))
    activeTask = 0;
  else if($('#optionTrue').hasClass('active'))
    activeTask = 1;
  else if($('#optionFalse').hasClass('active'))
    activeTask = 2;
  $.get( "/servis/all", {categoryTask,activeTask}, function( data ) {
    //console.log(data)
    allTasks = data.data;
    initTasks = allTasks;
    multipleWorkers();
    allTasks = vsaOpravila;
    initTasks = vsaOpravila;
    debugger
    //console.log(dataGet[2]);
    tableTasks.clear();
    tableTasks.rows.add(allTasks);
    tableTasks.draw(false);
    $('[data-toggle="popover"]').popover({trigger: "hover"});
  });
}
//CHANGE TASKS TO SEE ONLY ACTIVE OR ALL
function changeActive(seeActive){
  debugger;
  $('#optionAll').removeClass('active');
  $('#optionTrue').removeClass('active');
  $('#optionFalse').removeClass('active');
  activeTask = seeActive;
  if($('#option0').hasClass('active'))
    categoryTask = 0;
  else if($('#option1').hasClass('active'))
    categoryTask = 1;
  else if($('#option2').hasClass('active'))
    categoryTask = 2;
  else if($('#option3').hasClass('active'))
    categoryTask = 3;
  else if($('#option4').hasClass('active'))
    categoryTask = 4;
  else if($('#option5').hasClass('active'))
    categoryTask = 5;
  else if($('#option6').hasClass('active'))
    categoryTask = 6;
  else if($('#option7').hasClass('active'))
    categoryTask = 7;
  $.get( "/servis/all", {categoryTask,activeTask}, function( data ) {
    //console.log(data)
    allTasks = data.data;
    initTasks = allTasks;
    multipleWorkers();
    allTasks = vsaOpravila;
    initTasks = vsaOpravila;
    debugger
    //console.log(dataGet[2]);
    tableTasks.clear();
    tableTasks.rows.add(allTasks);
    tableTasks.draw(true);
    if(seeActive == 1)
      $('#optionTrue').addClass('active');
    else if(seeActive == 2)
      $('#optionFalse').addClass('active');
    else
      $('#optionAll').addClass('active');
    $('[data-toggle="popover"]').popover({trigger: "hover"});
  });
}
//fix data, group same task_id with different workers as one task
function multipleWorkers(){
  vsaOpravila = [];
  var idTasks = [];
  var counter = 0;
  for(var i=0; i<allTasks.length; i++){
    //var conflict = idTasks.find(t => (parseInt(projectId) != t.project_id));
    //debugger;
    if(!contains.call(idTasks, allTasks[i].id)){
      idTasks.push(allTasks[i].id);
      vsaOpravila.push(allTasks[i]);
      vsaOpravila[counter].workers = [allTasks[i].worker];
      vsaOpravila[counter].workersId = [allTasks[i].worker_id]
      counter++;
    }
    else{
      var opravilo = vsaOpravila.find(o => o.id === allTasks[i].id);
      if(opravilo){
        opravilo.workers.push(allTasks[i].worker);
        opravilo.workersId.push(allTasks[i].worker_id);
      }
    }
    //debugger;
  }
}
function pad(n) {return n < 10 ? "0"+n : n;}
var contains = function(needle) {
  // Per spec, the way to identify NaN is that it is not equal to itself
  var findNaN = needle !== needle;
  var indexOf;

  if(!findNaN && typeof Array.prototype.indexOf === 'function') {
      indexOf = Array.prototype.indexOf;
  } else {
      indexOf = function(needle) {
          var i = -1, index = -1;

          for(i = 0; i < this.length; i++) {
              var item = this[i];

              if((findNaN && item !== item) || item === needle) {
                  index = i;
                  break;
              }
          }

          return index;
      };
  }

  return indexOf.call(this, needle) > -1;
};
var loggedUser;
var activeTask = 0;
var categoryTask = 0;
var allTasks;
var initTasks;
var vsaOpravila;
var allWorkers;
var allCategories;
var allPriorities;
var subscribers;
var allCategories;
var allPriorities;
var taskId;
var allSubtasks;