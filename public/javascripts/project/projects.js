$(document).ready(function() {
  //debugger
  
  $('.modal-dialog').draggable({
    handle: ".modal-header"
  });
  //var dataGet;
  var subscribers;
  loggedUser = {name:$('#loggedUser').html().split(" (")[0].split(" ")[0], surname:$('#loggedUser').html().split(" (")[0].split(" ")[1], role:$('#loggedUser').html().split(" (")[1].split(")")[0]};

  $('.btnadd').click(function(){
    //grem po delavce
    $.get( "/subscribers/all", function( data ) {
      subscribers = data.data;

      $(".select2-subscriber").select2({
        data: subscribers,
        tags: true,
        dropdownParent: $('#modalAddProjectForm')
      });
      $("#modalAddProjectForm").modal();
    });
  })
  //UPDATE THE PROJECT
  $('#editProjectform').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var name = project_name_edit.value;
    var number = project_number_edit.value;
    var start = project_start_edit.value;
    var finish = project_finish_edit.value;
    var subscriber = project_subscriber_edit.value;
    var notes = project_notes_edit.value;
    var active;
    if(typeof activeEdit === 'undefined')
      active = true;
    else
      active = activeEdit.checked;
    if(start)
      start = start + " 07:00:00.000000";
    if(finish)
      finish = finish + " 15:00:00.000000";
    debugger;
    //check if subscriber is new
    if(isNaN(subscriber)){
      debugger;
      //new subscriber, create subscriber and then update project
      $.post('/subscribers/create', {subscriber}, function(resp){
        var subscriberName = subscriber;
        subscriber = resp.id.id;
        $.post('/projects/update', {projectId, number, name, subscriber, start, finish, notes, active}, function(resp){
          debugger;
          var temp = tableProjects.row('#'+projectId+'').data();
          temp.project_name = name;
          temp.project_number = number;
          temp.name = subscriberName;
          temp.start = start;
          temp.finish = finish;
          temp.active = active;
          tableProjects.row('#'+projectId).data(temp).invalidate().draw(false);
          $('#modalEditProjectForm').modal('toggle');
        })
      })
    }
    else{
      debugger;
      //nothing new, update project
      $.post('/projects/update', {projectId, number, name, subscriber, start, finish, notes, active}, function(resp){
        var subscriberName = allSubscribers.find(s => s.id === parseInt(subscriber)).text;
        debugger;
        var temp = tableProjects.row('#'+projectId+'').data();
          temp.project_name = name;
          temp.project_number = number;
          temp.name = subscriberName;
          temp.start = start;
          temp.finish = finish;
          temp.active = active;
          tableProjects.row('#'+projectId).data(temp).invalidate().draw(false);
          $('#modalEditProjectForm').modal('toggle');
      })
    }

  })
  //ADD NEW PROJECT
  $('#addProjectform').submit(function(e)
  {
    e.preventDefault();
    //console.log("test");
    $form = $(this);
    var name = project_name.value;
    var number = project_number.value;
    var start = project_start.value;
    var finish = project_finish.value;
    var subscriber = project_subscriber.value;
    //var subscriberNew = project_subscriber_new.value;
    var notes = project_notes.value;
    //console.log($form);
    if(start)
      start = start + " 07:00:00.000000";
    if(finish)
      finish = finish + " 15:00:00.000000";


    debugger;
    //check if project number already exists
    //console.log(dataGet)
    var conflict = dataGet.find(d => d.project_number === number)
    //console.log(conflict);
    if(conflict){
      //conflict
      var alertElement = `
      <div class="alert alert-danger alert-dismissible average" role="alert">
      <button class="close" type="button" data-dismiss="alert">×</button>
      <p>Ta številka projekta je že zasedena</p>
      </div>`
      $('#messageAdd').append(alertElement); 
    }
    else{
      //no conflict with project number
      debugger
      if(isNaN(subscriber)){
        //new subscriber, create subscriber and then add create project with new subscriber
        $.post('/subscribers/create', {subscriber}, function(resp){
          //resp.id je id novega subscriberja
          var subscriberName = subscriber;
          subscriber = resp.id.id;
          debugger;
          $.post('/projects/create', {number, name, subscriber, start, finish, notes}, function(resp){
            //add row on top
            debugger
            
            tableProjects.row.add({
              id: resp.id.id, 
              project_number: number, 
              project_name: name, 
              name: subscriberName, 
              start: start, 
              finish: finish, 
              active: true,
              completion: 0
            }).draw(false)
            debugger
            $('#project_number').val('');
            $('#project_name').val('');
            $('#project_start').val('');
            $('#project_finish').val('');
            $('#project_notes').val('');
            $('#project_subscriber').val(1).trigger('change');
            //toggle modal
            $('#modalAddProjectForm').modal('toggle');
          })
        })
        debugger
      }
      else{
        debugger
        //subscriber already exists, create project
        $.post('/projects/create', {number, name, subscriber, start, finish, notes}, function(resp){
          //add row on top
          var subscriberName = subscribers.find(o => o.id === parseInt(subscriber)).text
          debugger
          
          tableProjects.row.add({
            id: resp.id.id, 
            project_number: number, 
            project_name: name, 
            name: subscriberName, 
            start: start, 
            finish: finish, 
            active: true,
            completion: 0,
            active: true
          }).draw(false)
          $('#project_number').val('');
          $('#project_name').val('');
          $('#project_start').val('');
          $('#project_finish').val('');
          $('#project_notes').val('');
          $('#project_subscriber').val(1).trigger('change');
          //toggle modal
          $('#modalAddProjectForm').modal('toggle');
        })
      }
    }
  });
  activeProject = 1;
  $.get("/projects/all", {activeProject}, function( data ) {
    //console.log(data)
    dataGet = data.data;
    //console.log(dataGet[2]);

    tableProjects = $('#projectsTable').DataTable( {
      data: dataGet,
      columns: [
          { title: "projectId", data: "id" },
          { title: "ID", data: "project_number" },
          { title: "Ime", data: "project_name" },
          { title: "Naročnik", data: "name" },
          { title: "Začetek", data: "start", render: function(data, type, row){
            if(data){
              return pad(new Date(data).getDate())+"."+pad(new Date(data).getMonth()+1)+"."+new Date(data).getFullYear();
            }
            else
              return "/";
          } },
          { title: "Konec", data: "finish", render: function(data, type, row){
            if(data){
              return pad(new Date(data).getDate())+"."+pad(new Date(data).getMonth()+1)+"."+new Date(data).getFullYear();
            }
            else
              return "/";
          }},
          { title: "Dokončano", data: "completion" },
          { data: "id",
            render: function ( data, type, row ) {
              if(loggedUser.role == 'admin' || loggedUser.role == 'vodja')
                return '<button class="btn btn-danger" onclick="deleteProject('+data+')">Odstrani</button>';
              else
                return '<div></div>';
            }},
          { data: "id",
            render: function ( data, type, row ) {
              if(loggedUser.role == 'admin' || loggedUser.role == 'vodja')
                return '<button class="btn btn-primary" onclick="editProject('+data+')">Uredi</button>';
              else
                return '<div></div>';
            }},
          { data: "id",
            render: function ( data, type, row ) {
              return '<a class="btn btn-info" href="/projects/id?id='+data+'">Več</a>';
            }},
          { title: "Aktivno", data: "active"},
      ],
      "language": {
        "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Slovenian.json"
      },
      order: [[0, "desc"]],
      columnDefs: [
        {
          targets: [0,10],
          visible: false,
          searchable: false
        },
        { responsivePriority: 1, targets: 1 },
        { responsivePriority: 2, targets: -2 },
        { responsivePriority: 3, targets: 2 },
      ],
      "createdRow": function ( row, data, index ) {
        $(row).attr('id', data.id);
        $(row).removeClass('table-dark');
        if(!data.active){
          $(row).addClass('table-dark');
        }
        else if ( data.completion == 100 ) {
            //$('tr', row).addClass('table-success');
            $(row).addClass('table-success');
        }
        else if( data.finish)
          if(new Date(data.finish).getTime() < new Date().getTime())
            $(row).addClass('table-warning');
      },
      rowCallback: function(row, data, index){
        $(row).removeClass('table-dark');
        if(!data.active){
          $(row).addClass('table-dark');
        }
        else if ( data.completion == 100 ) {
          //$('tr', row).addClass('table-success');
          $(row).addClass('table-success');
      }
      else if( data.finish)
        if(new Date(data.finish).getTime() < new Date().getTime())
          $(row).addClass('table-warning');
      },
      responsive: true,
      //info: false,
      //searching: false,
      //paging: false
      dom: 'Bfrtip',
      buttons: [
        {
          extend: 'excelHtml5',
          exportOptions: {
              columns: [ 1, 2, 3, 4, 5, 6 ]
          }
        },
        {
          extend: 'pdfHtml5',
          exportOptions: {
              columns: [ 1, 2, 3, 4, 5, 6 ]
          }
        }
      ]
    });
    drawTimeline();
  });
  function onNumberChange(){
    console.log(dataGet)
    debugger
    projectNumber = $('#project_number').val();
    var conflict = dataGet.find(d => d.project_number === projectNumber)
    debugger
    if(conflict){
      $('#project_number').removeClass("is-valid").addClass("is-invalid");
      $('#btnAddProject').prop("disabled", true).removeClass("ui-state-enabled").addClass("ui-state-disabled");
    }
    else{
      $('#project_number').removeClass("is-invalid").addClass("is-valid");
      $('#btnAddProject').prop("disabled", false).removeClass("ui-state-disabled").addClass("ui-state-enabled");
    }
  }
  $('#project_number').on('change', onNumberChange);
  var projectNumber = $('#project_number').val();
});
function onDataChange(){
  debugger
  projectStart = $('#project_start').val();
  projectFinish = $('#project_finish').val();
  if(projectStart && projectFinish){
    if(new Date(projectStart).getTime() > new Date(projectFinish).getTime()){
      $('#project_start').val(projectFinish);
    }
    else if(new Date(projectFinish).getTime() < new Date(projectStart).getTime())
    $('#project_finish').val(projectStart);
  }
}
//CHANGE PROJECTS TO SEE ONLY ACTIVE OR ALL
function changeActive(seeActive){
  $('#optionAll').removeClass('active');
  $('#optionTrue').removeClass('active');
  $('#optionFalse').removeClass('active');
  activeProject = seeActive;
  $.get( "/projects/all", {activeProject}, function( data ) {
    //console.log(data)
    dataGet = data.data;
    debugger
    //console.log(dataGet[2]);
    tableProjects.clear();
    tableProjects.rows.add(dataGet);
    tableProjects.draw(true);
    if(seeActive == 1)
      $('#optionTrue').addClass('active');
    else if(seeActive == 2)
      $('#optionFalse').addClass('active');
    else
      $('#optionAll').addClass('active');
    $('#myPlot').empty();
    drawTimeline();
  });
}
function deleteProject(id){
  debugger;
  projectId = id;
  $.post('/projects/delete', {projectId}, function(resp){
    var temp = tableProjects.row('#'+projectId).data();
    temp.active = false;
    tableProjects.row('#'+projectId).data(temp).invalidate().draw(false);
  })
}
//CHECK IF PROJECT NUMBER IS NOT USED YET
function onProjectNumberChange(){
  $.get( "/projects/all", function( data ) {
    var allProjects = data.data;
    var number = $('#project_number_edit').val();
    var conflict = allProjects.find(p => p.project_number === number)
    if(conflict){
      if(conflict.project_number != projectNumber){
        $('#project_number_edit').removeClass("is-valid").addClass("is-invalid");
        $('#btnEditProject').prop("disabled", true).removeClass("ui-state-enabled").addClass("ui-state-disabled");
        //debugger;
      }
      else{
        $('#project_number_edit').removeClass("is-invalid").addClass("is-valid");
        $('#btnEditProject').prop("disabled", false).removeClass("ui-state-disabled").addClass("ui-state-enabled");
        //debugger;
      }
    }
    else{
      $('#project_number_edit').removeClass("is-invalid").addClass("is-valid");
      $('#btnEditProject').prop("disabled", false).removeClass("ui-state-disabled").addClass("ui-state-enabled");
    }
    //debugger;
  });
}
//CHECK IF DATES ARE OUT OF ORDER
function onStartDateEditChange(){
  var start = $('#project_start_edit').val();
  var finish = $('#project_finish_edit').val();
  if(start && finish){
    if(new Date(start).getTime() > new Date(finish).getTime())
      $('#project_start_edit').val($('#project_finish_edit').val());
  }
}
//CHECK IF DATES ARE OUT OF ORDER
function onFinishDateEditChange(){
  var start = $('#project_start_edit').val();
  var finish = $('#project_finish_edit').val();
  if(start && finish){
    if(new Date(finish).getTime() < new Date(start).getTime())
      $('#project_finish_edit').val($('#project_start_edit').val());
  }
}
//OPEN MODAL FOR EDIT PROJECT
function editProject(id){
  debugger;
  projectId = id;
  $.get( "/subscribers/all", function( data ) {
    allSubscribers = data.data;
    $(".select2-subscriber-edit").select2({
      data: allSubscribers,
      tags: true,
      dropdownParent: $('#modalEditProjectForm')
    });
    $.get("/projects/info", {projectId}, function(data){
      project = data.data;
      projectNumber = project.project_number;
      $('#project_start_edit').val('');
      $('#project_finish_edit').val('');
      $('#project_number_edit').removeClass("is-invalid");
      $('#project_number_edit').removeClass("is-valid");
      $('#btnEditProject').prop("disabled", false).removeClass("ui-state-disabled").addClass("ui-state-enabled");
      $('#project_number_edit').val(project.project_number);
      $('#project_name_edit').val(project.project_name);
      $('#project_notes_edit').val(project.notes);
      $('#project_subscriber_edit').val(project.subscriber).trigger('change');
      if(project.start){
        var start = new Date(project.start);
        var d = start.getDate();
        var m = start.getMonth()+1;
        var y = start.getFullYear();
        if(d < 10)
          d = '0'+d;
        if(m < 10)
          m = '0'+m;
          $('#project_start_edit').val(y+'-'+m+'-'+d);
      }
      if(project.finish){
        var finish = new Date(project.finish);
        var d = finish.getDate();
        var m = finish.getMonth()+1;
        var y = finish.getFullYear();
        if(d < 10)
        d = '0'+d;
        if(m < 10)
        m = '0'+m;
        $('#project_finish_edit').val(y+'-'+m+'-'+d);
      }
      if(project.active){
        //debugger
        $("#activeEdit").prop('checked', 1);
      }
      else{
        //debugger
        $("#activeEdit").prop('checked', 0);
      }
      debugger;
      $("#modalEditProjectForm").modal();
    })
  });
}
function pad(n) {return n < 10 ? "0"+n : n;}
function drawTimeline(){
  //debugger;
  makeVizData();
  //debugger;
  //$("#collapseTimetable").addClass("show");
  //var widthCollapseTT = $("#myPlot").parent().width();
  //$("#collapseTimetable").removeClass("show");
  if(dataViz.length == 0)
    return
  chart = TimelinesChart()
      .data(dataViz)
      .width(1068)
      .zQualitative(true)
      (document.getElementById('myPlot'));
}
function refreshTimeline(){
  makeVizData();
  //debugger;
  if(chart)
    chart.data(dataViz).refresh();
  else{
    chart = TimelinesChart()
      .data(dataViz)
      .width(1068)
      .zQualitative(true)
      (document.getElementById('myPlot'));
  }
}
function makeVizData(){
  dataViz = [
    {group:"Aktivni", data:[]},
    {group:"Ne aktivni", data:[]},
  ];
  var subscriber;
  for(var i=0; i<dataGet.length; i++){
    //debugger;
    if(dataGet[i].completion == '100')
      continue;
    subscriber = dataGet[i].name.substring(0,5);
    if(dataGet[i].name.length > 5)
      subscriber += "...";
    var start = dataGet[i].start;
    var finish = dataGet[i].finish;
     if(!dataGet[i].start && !dataGet[i].finish)
      continue;
    else if(!dataGet[i].start)
      start = dataGet[i].finish;
    else if(!dataGet[i].finish)
      finish = dataGet[i].start;
    
    if(dataGet[i].active){
      //debugger;
      dataViz[0].data.push(
        {
          label: dataGet[i].project_name,
          data: [
            {
              timeRange:[start, finish],
              val: subscriber
            }
          ]
        }
      );
    }
    else if(!dataGet[i].active){
      //debugger;
      dataViz[1].data.push(
        {
          label: dataGet[i].project_name,
          data: [
            {
              timeRange:[start, finish],
              val: subscriber
            }
          ]
        }
      );
    }
  }
  if(dataViz[1].data.length == 0) dataViz.splice(1,1)
  if(dataViz[0].data.length == 0) dataViz.splice(0,1)
  //debugger;
}
var dataViz;
var chart;
$('#project_start').on('change', onDataChange);
$('#project_finish').on('change', onDataChange);
var tableProjects;
var activeProject;
var loggedUser;
var projectId;
var allSubscribers;
var project;
var projectNumber;
var dataGet;
//var subscribers
//var dataGet
var projectStart = $('#project_start').val();
var projectFinish = $('#project_finish').val();
$('#project_number_edit').on('change', onProjectNumberChange);
$('#project_start_edit').on('change', onStartDateEditChange);
$('#project_finish_edit').on('change', onFinishDateEditChange);
