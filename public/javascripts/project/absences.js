//IN THIS SCRIPT//
////  ////

$(function(){
  
})
//OPEN TASK ABSENCE COLLAPSE
function openTaskAbsence(taskId){
  //debugger;
  if(taskLoaded){
    makeTaskAbsence(taskId);
  }
  else{
    $.get( "/projects/tasks", {projectId,categoryTask,activeTask, completionTask}, function( data ) {
      //console.log(data)
      //debugger
      allTasks = data.data;
      taskLoaded = true;
      makeTaskAbsence(taskId);
    });
  }

  //$('#collapseTaskAbsence'+taskId).collapse("toggle");
  //taskAbsenceCollapseOpen = true;
  
}
//FILL ABSENCE LIST
function makeTaskAbsence(taskId){
  //debugger;
  collapseNewTaskAbsenceOpen = false;
  collapseNewTaskAbsenceContent = false;
  var task = allTasks.find(t => t.id == taskId);
  var workersId = task.workers_id;
  var start = task.task_start;
  var finish = task.task_finish;
  if(start && finish){
    $.post('/projects/absence', {workersId, projectId, taskId, start, finish}, function(data){
      //debugger;
      allAbsence = data.data;
      //nazaj dobim array arrayev, za vsakega delavca posebi
      //first make collapse frame
      var addAbsenceButton = '';
      if(loggedUser.role == 'vodja' || loggedUser.role == 'admin' || leader)
        if(task.workers)
          addAbsenceButton = '<button class="btn btn-success" onclick="addNewTaskAbsence('+taskId+')">Dodaj odostnost</button>';
      var frame = `<hr />
      <div class="list-group d-flex ml-3 mr-3 span-collapse-content" id="absenceList`+taskId+`"></div><br />
      <div class="collapse multi-collapse task-collapse" id="collapseNewTaskAbsence`+taskId+`"></div>
      <div id="addAbsenceRow`+taskId+`" class="text-center">
          `+addAbsenceButton+`
      </div><br />`;
      $('#collapseTaskAbsence'+taskId).append(frame);
      for(var i = 0; i < allAbsence.length; i++){
        for(var j = 0; j < allAbsence[i].length; j++){
          var start = new Date(allAbsence[i][j].start);
          var finish = new Date(allAbsence[i][j].finish);
          var sd = start.getDate();
          var sm = start.getMonth()+1;
          var sy = start.getFullYear();
          var fd = finish.getDate();
          var fm = finish.getMonth()+1;
          var fy = finish.getFullYear();
          if(sd < 10) sd = '0'+sd;
          if(sm < 10) sm = '0'+sm;
          if(fd < 10) fd = '0'+fd;
          if(fm < 10) fm = '0'+fm;
          var deleteButton = '';
          if(loggedUser.role == 'vodja' || loggedUser.role == 'admin' || leader)
            if(allAbsence[i][j].id_task)
              deleteButton = '<button class="btn btn-danger" onclick="deleteAbsence('+allAbsence[i][j].id+')" style="width:25px, height:5px, border:none;">X</button>';
          var listElement = `<div class="list-group-item" id="absence`+allAbsence[i][j].id+`">
            <div class="row">
              <div class="col-md-3 mt-2">
                <label>` + allAbsence[i][j].name + " " + allAbsence[i][j].surname + `</label>
              </div>
              <div class="col-md-4 mt-2">
                <label>`+ sd+"."+sm+"."+sy+" - "+fd+"."+fm+"."+fy+`</label>
              </div>
              <div class="col-md-4 mt-2">
                <label>`+allAbsence[i][j].reason+`</label>
              </div>
              <div class="col-md-1">
                `+deleteButton+`
              </div>
            </div>
          </div>`;
          $('#absenceList'+taskId).append(listElement);
        }
      }
      $('#collapseTaskAbsence'+taskId).collapse("toggle");
      taskAbsenceCollapseOpen = true;
    })
  }
  else{
    //ni datuma, pol sploh lahka rečemo da uporabnik lahka dodaja odsotnosti?
    taskAbsenceCollapseOpen = false;
    activeTaskId = 0;
  }
}
function addNewTaskAbsence(taskId){
  debugger;
  if(!collapseNewTaskAbsenceContent){
    debugger;
    var element = `<div class="row d-flex justify-content-end">
      <button class="close mr-4" type="button" aria-label="Close" onclick="closeCollapseNewTaskAbsence(`+taskId+`)"><span aria-label="true">x</span></button>
    </div>
    <div class="form-row">
      <div class="col-md-6">
        <label for="absenceWorker" style="width:100%">Delavec
          <select class="custom-select mr-sm-2 form-control select2-absence-workers" id="absenceWorker" name="absenceWorker" style="width:100%"></select>
        </label>
      </div>
      <div class="col-md-6">
        <label for="absenceReason" style="width:100%">Razlog
          <select class="custom-select mr-sm-2 form-control select2-absence-reasons" id="absenceReason" name="absenceReason" style="width:100%"></select>
        </label>
      </div>
    </div>
    <div class="form-row">
      <div class="col-md-6">
        <label for="absenceStart" style="width:100%">Začetek odsotnosti
          <input class="form-control" id="absenceStart" type="date" name="absenceStart" required=""/>
          <div class="invalid-feedback">Manjkajoče polje!</div>
        </label>
      </div>
      <div class="col-md-6">
        <label for="absenceFinish" style="width:100%">Konec odsotnosti
          <input class="form-control" id="absenceFinish" type="date" name="absenceFinish" required=""/>
          <div class="invalid-feedback">Manjkajoče polje!</div>
        </label>
      </div>
    </div>`;
    $('#collapseNewTaskAbsence'+taskId).append(element);
    $('#absenceStart').on('change', onAbsenceStartChange);
    $('#absenceFinish').on('change', onAbsenceFinishChange);
    //task's workers
    var task = allTasks.find(t => t.id == taskId);
    var workersId = task.workers_id.split(",")
    var taskWorkers = task.workers.split(", ")
    $('#absenceWorker').empty();
    for(var i = 0; i < workersId.length; i++){
      //debugger;
      var data = {id: workersId[i], text: taskWorkers[i]};
      var newOption = new Option(data.text, data.id, false, false);
      $('#absenceWorker').append(newOption).trigger('change');;
    }
    //TODO tags can be false if user is not admin/vodja/tajnik
    //reasons
    if(!allReasons){
      $.get('/absence/reasons', function(data){
        allReasons = data.data;
        $(".select2-absence-reasons").select2({
          data: allReasons,
          tags: permissionTag
        });
      })
    }
    else{
      $(".select2-absence-reasons").select2({
        data: allReasons,
        tags: permissionTag
      });
    }
    collapseNewTaskAbsenceContent = true;
  }
  if(!collapseNewTaskAbsenceOpen){
    debugger;
    $('#collapseNewTaskAbsence'+taskId).collapse("toggle");
    collapseNewTaskAbsenceOpen = true;
  }
  else{
    debugger;
    if($('#absenceStart').val() != '' && $('#absenceFinish').val() != ''){
      var start = $('#absenceStart').val() + " 07:00:00.000000";
      var finish = $('#absenceFinish').val() + " 15:00:00.000000";
      var worker = $('#absenceWorker').val();
      var reason = $('#absenceReason').val();
      var taskId = activeTaskId;
      //no empty inputs, add new absence
      debugger;

      $.post('/absence/add', {start, finish, worker, reason, taskId, projectId}, function(data){
        debugger;
        if(data.success){
          start = new Date(start);
          finish = new Date(finish);
          var sd = start.getDate();
          var sm = start.getMonth()+1;
          var sy = start.getFullYear();
          var fd = finish.getDate();
          var fm = finish.getMonth()+1;
          var fy = finish.getFullYear();
          if(sd < 10) sd = '0'+sd;
          if(sm < 10) sm = '0'+sm;
          if(fd < 10) fd = '0'+fd;
          if(fm < 10) fm = '0'+fm;
          var task = allTasks.find(t=> t.id == taskId);
          var index = task.workers_id.split(',').findIndex(i => i == $('#absenceWorker').val());
          var worker = task.workers.split(', ')[index];
          if(!data.reasonId)
            reason = allReasons.find(r=> r.id == $('#absenceReason').val());
          else{
            reason = $('#absenceReason').val();
          }
          var deleteButton = '<button class="btn btn-danger" onclick="deleteAbsence('+data.absenceId+')" style="width:25px, height:5px, border:none;">X</button>';
          var listElement = `<div class="list-group-item" id="absence`+data.absenceId+`">
            <div class="row">
              <div class="col-md-3 mt-2">
                <label>` + worker + `</label>
              </div>
              <div class="col-md-4 mt-2">
                <label>`+ sd+"."+sm+"."+sy+" - "+fd+"."+fm+"."+fy+`</label>
              </div>
              <div class="col-md-4 mt-2">
                <label>`+reason.text+`</label>
              </div>
              <div class="col-md-1">
                `+deleteButton+`
              </div>
            </div>
          </div>`;
          debugger;
          $('#absenceList'+taskId).append(listElement);
          //on success do: empty inputs, (remove is-invalid class), close collaps
          $('#absenceStart').val('');
          $('#absenceFinish').val('');
          $('#collapseNewTaskAbsence'+taskId).collapse("toggle");
          collapseNewTaskAbsenceOpen = false;
        }
      })
      
    }
    else{
      if($('#absenceStart').val() == '')
      $('#absenceStart').addClass('is-invalid');
      if($('#absenceFinish').val() == '')
        $('#absenceFinish').addClass('is-invalid');
      }
  }
}
function deleteAbsence(id){
  $.post('/absence/delete', {id}, function(resp){
    debugger
    if(resp.success)
      $('#absence'+id).remove();
  })
}
function closeCollapseNewTaskAbsence(taskId){
  $('#collapseNewTaskAbsence'+taskId).collapse("toggle");
  collapseNewTaskAbsenceOpen = false;
}
function onAbsenceStartChange(){
  $('#absenceStart').removeClass('is-invalid');
  if($('#absenceStart').val() == '')
    $('#absenceStart').addClass('is-invalid');
  else{
    debugger;
    var absenceStart = new Date($('#absenceStart').val());
    var absenceFinish = new Date($('#absenceFinish').val());
    debugger;
    //first check if it is within the same task
    var task = allTasks.find(t => t.id == activeTaskId);
    var taskStart = new Date(task.task_start);
    var taskFinish = new Date(task.task_finish);
    if(absenceStart >= taskStart && absenceStart <= taskFinish){
      //absenceStart is within task
      if($('#absenceFinish').val() && absenceStart > absenceFinish){
        $('#absenceStart').val($('#absenceFinish').val());
      }
    }
    else{
      //absenceStart is out of task date window
      //absenceStart is before task, set it as start
      if(absenceStart < taskStart){
        var y = taskStart.getFullYear();
        var m = taskStart.getMonth()+1;
        var d = taskStart.getDate();
        if(m < 10) m = '0'+m;
        if(d < 10) d = '0'+d;
        $('#absenceStart').val(y+"-"+m+"-"+d);
      }
      //absenceStart is after task, set it as finish
      else if(absenceStart > taskFinish){
        if($('#absenceFinish').val() && absenceStart > absenceFinish){
          $('#absenceStart').val($('#absenceFinish').val());
        }
        else{
          var y = taskFinish.getFullYear();
          var m = taskFinish.getMonth()+1;
          var d = taskFinish.getDate();
          if(m < 10) m = '0'+m;
          if(d < 10) d = '0'+d;
          $('#absenceStart').val(y+"-"+m+"-"+d);
        }
      }
    }
  }
}
function onAbsenceFinishChange(){
  $('#absenceFinish').removeClass('is-invalid');
  if($('#absenceFinish').val() == '')
    $('#absenceFinish').addClass('is-invalid');
    else{
      debugger;
      var absenceStart = new Date($('#absenceStart').val());
      var absenceFinish = new Date($('#absenceFinish').val());
      debugger;
      //first check if it is within the same task
      var task = allTasks.find(t => t.id == activeTaskId);
      var taskStart = new Date(task.task_start);
      var taskFinish = new Date(task.task_finish);
      if(absenceFinish >= taskStart && absenceFinish <= taskFinish){
        //absenceFinish is within task
        if($('#absenceStart').val() && absenceStart > absenceFinish){
          $('#absenceFinish').val($('#absenceStart').val());
        }
      }
      else{
        //absenceFinish is out of task date window
        //absenceFinish is before task, set it as start
        if(absenceFinish < taskStart){
          if($('#absenceStart').val() && absenceStart > absenceFinish){
            $('#absenceFinish').val($('#absenceStart').val());
          }
          else{
            var y = taskStart.getFullYear();
            var m = taskStart.getMonth()+1;
            var d = taskStart.getDate();
            if(m < 10) m = '0'+m;
            if(d < 10) d = '0'+d;
            $('#absenceFinish').val(y+"-"+m+"-"+d);
          }
        }
        //absenceStart is after task, set it as finish
        else if(absenceFinish > taskFinish){
          var y = taskFinish.getFullYear();
          var m = taskFinish.getMonth()+1;
          var d = taskFinish.getDate();
          if(m < 10) m = '0'+m;
          if(d < 10) d = '0'+d;
          $('#absenceFinish').val(y+"-"+m+"-"+d);
        }
      }
  }
}
var collapseNewTaskAbsenceOpen = false;
var collapseNewTaskAbsenceContent = false;
var permissionTag = false;