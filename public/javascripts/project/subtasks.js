//IN THIS SCRIPT//
////  ////

$(function(){
  $('#note-form').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var newNote = noteInput.value;
    var subtaskId = subtaskIdNote.value;
    $.post('/projects/subtask/note', {subtaskId, newNote}, function(resp){
      //debugger;
      //update note
      $('#subtask'+subtaskId).find('.notepopover').attr('data-content', newNote);
      //empty input for subtask note
      $('#noteInput').val("");
      //close modal for inputing note for subtask
      $("#modalNoteForm").modal('toggle');
    })
  })
})
//TOGGLE BETWEEN MODAL FOR NOTE AND ADD NOTE
function addSubtaskNote(id){
  $("#modalNoteForm").modal();
  $('#noteInput').val("");
  //debugger;
  $('#subtaskIdNote').val(id);
  //subtaskId = id;
}
//OPEN SUBTASKS COLLAPSE AND FILL SUBSTASK LIST
function openSubtasks(taskId){
  //debugger;
  $.get('/projects/subtasks', {taskId}, function(data){
    allSubtasks = data.data;
    //debugger;
    //collapse frame
    var addElement = '';
    if(loggedUser.role == 'vodja' || loggedUser.role == 'admin' || leader)
      addElement = `<div class="d-flex">
        <input class="form-control p-1 mr-auto" onKeyDown="if(event.keyCode==13) addNewSubtask(`+taskId+`);" id="subtaskInput`+taskId+`" placeholder="" type="text" name="subtaskInput" />
        <div class="input-group-append">
          <button class="btn btn-success" onclick="addNewSubtask(`+taskId+`)" style="width:25px, height:5px, border:none;">
            <img class="img-fluid" src="/add.png" alt="ikona" style="width:15px">
          </button>
        </div>
      </div>`;
    var frame = `<hr />
    <div class="list-group d-flex ml-3 mr-3 span-collapse-content" id="subtaskList`+taskId+`"></div><br />
    `+addElement+`
    <br />`;
    $('#collapseSubtasks'+taskId).append(frame);

    for(var i = 0; i < allSubtasks.length; i++){
      var checkedCB = '';
      if(allSubtasks[i].completed)
        checkedCB = 'checked=""';
      var popoverText = "";
      if(allSubtasks[i].note)
        popoverText = allSubtasks[i].note;
      var deleteElement = '';
      if(loggedUser.role == 'vodja' || loggedUser.role == 'admin' || leader)
        deleteElement = `<button class="btn btn-danger task-button mr-2 mt-1" onclick="deleteSubtask(`+allSubtasks[i].id+`)" style="width:25px, height:5px, border:none;">
          <img class="img-fluid" src="/remove.png" alt="ikona" style="width:15px">
        </button>`;
      var listElement = `<div class="list-group-item" id="subtask`+allSubtasks[i].id+`">
        <div class="row">
          <div class='col-12 col-xl-10 col-lg-9 col-md-8'>
            <label class="p-1 mr-auto" style="width:70%;">`+allSubtasks[i].name+`</label>
          </div>
          <div class='col-12 col-xl-2 col-lg-3 col-md-4 d-flex'>
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input" id="customCheck`+allSubtasks[i].id+`" type="checkbox" `+checkedCB+` onchange="toggleSubtaskCheckbox(this)" />
              <label class="custom-control-label" for="customCheck`+allSubtasks[i].id+`"> </label>
            </div><a class="notepopover p-1 mr-1 mt-2" data-toggle="popover" data-trigger="hover" data-content="`+popoverText+`" style="height:30px;" data-original-title="" title=""><img class="img-fluid" src="/info.png" alt="ikona" style="width:25px;" /></a>
            <button class="invisible-button mr-1 mt-2" onclick="addSubtaskNote(`+allSubtasks[i].id+`)"><img class="img-fluid" src="/note-52.png" alt="ikona" style="width:25px;" /></button>
            `+deleteElement+`
          </div>
        </div>
        <row class="d-flex" id="subtaskBadges`+allSubtasks[i].id+`"></row>
      </div>`;
      $('#subtaskList'+taskId).append(listElement);
    }
    $('[data-toggle="popover"]').popover({trigger: "hover"});
    $('#collapseSubtasks'+taskId).collapse("toggle");
    subtaskCollapseOpen = true;
  })
}
function addNewSubtask(taskId){
  //debugger;
  if($('#subtaskInput'+taskId).val() == ''){
    $('#subtaskInput'+taskId).addClass('is-invalid');
    $('#subtaskInput'+taskId).attr('placeholder', 'Vnesi naziv nove naloge!');
  }
  else{
    var newSubtask = $('#subtaskInput'+taskId).val();
    $.post('/projects/subtask/add', {taskId, newSubtask}, function(resp){
      //debugger;
      var checkedCB = '';
      var popoverText = "";
      var newId = resp.data.id;
      var listElement = `<div class="list-group-item" id="subtask`+newId+`">
        <div class="row">
          <div class='col-12 col-xl-10 col-lg-9 col-md-8'>
            <label class="p-1 mr-auto" style="width:70%;">`+newSubtask+`</label>
          </div>
          <div class='col-12 col-xl-2 col-lg-3 col-md-4 d-flex'>
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input" id="customCheck`+newId+`" type="checkbox" `+checkedCB+` onchange="toggleSubtaskCheckbox(this)" />
              <label class="custom-control-label" for="customCheck`+newId+`"> </label>
            </div><a class="notepopover p-1 mr-1 mt-2" data-toggle="popover" data-trigger="hover" data-content="`+popoverText+`" style="height:30px;" data-original-title="" title=""><img class="img-fluid" src="/info.png" alt="ikona" style="width:25px;" /></a>
            <button class="invisible-button mr-1 mt-2" onclick="addSubtaskNote(`+newId+`)"><img class="img-fluid" src="/note-52.png" alt="ikona" style="width:25px;" /></button>
            <button class="btn btn-danger task-button mr-2 mt-1" onclick="deleteSubtask(`+newId+`)" style="width:25px, height:5px, border:none;">
              <img class="img-fluid" src="/remove.png" alt="ikona" style="width:15px">
            </button>
          </div>
        </div>
        <row class="d-flex" id="subtaskBadges`+newId+`"></row>
      </div>`;
      $('#subtaskList'+taskId).append(listElement);
      //$('[data-toggle="popover"]').popover({trigger: "hover"});
      allSubtasks.push({id:newId, id_task: taskId, name: newSubtask, note: null, completed: false});
      var numberOfCompleted = 0;
      for(var i=0; i<allSubtasks.length; i++){
        if(allSubtasks[i].completed)
        numberOfCompleted++;
      }
      var completion = Math.round((numberOfCompleted/allSubtasks.length)*100)
      if(numberOfCompleted == '0' || allSubtasks.length == '0')
        completion = 0;
      //console.log(completion);
      //fix completion on task in db
      fixTaskCompletion(taskId, completion);
      if(allTasks){
        var task = allTasks.find(t => t.id == taskId);
        task.subtasks_count = allSubtasks.length;
      }
      //change button to black cuz now task has subtasks
      if($('#taskButtonSubtask'+taskId).find('img').attr('src') == '/subtaskWhite.png'){
        //change button to black
        $('#taskButtonSubtask'+taskId).find('img').attr('src', '/subtaskBlack.png');
        //remove checkbox on task
        $('#taskCheckbox'+taskId).empty()
      }
      $('#subtaskInput'+taskId).val('');
      $('#subtaskInput'+taskId).attr('placeholder', '');
      $('#subtaskInput'+taskId).removeClass('is-invalid');
    })
  }
}
//DELETE SELECTED SUBTASK
function deleteSubtask(id){
  var taskId = activeTaskId;
  //debugger;
  //not sure why i need taskId for deletin subtask
  $.post('/projects/subtask/delete', {id}, function(data){
    //debugger
    $('#subtask'+id).remove();
    //remove deleted task from list of all subtasks
    allSubtasks = allSubtasks.filter(s => s.id != id);
    var numberOfCompleted = 0;
    for(var i=0; i<allSubtasks.length; i++){
      if(allSubtasks[i].completed)
      numberOfCompleted++;
    }
    //calc new completion of task
    var completion = Math.round((numberOfCompleted/allSubtasks.length)*100)
    if(numberOfCompleted == 0 || allSubtasks.length == 0)
      completion = 0;
    //console.log(completion);
    //post new completion to fix in db
    fixTaskCompletion(taskId, completion);
    if(allTasks){
      var task = allTasks.find(t => t.id == taskId);
      task.subtasks_count = allSubtasks.length;
    }
    if(allSubtasks.length == 0){
      //change button to white so it can be seen from button it has no subtasks
      $('#taskButtonSubtask'+taskId).find('img').attr('src', '/subtaskWhite.png');
      //add checkbox so it can be checked as task
      var checkboxElement = `<input class="custom-control-input" id="customTaskCheck`+taskId+`" type="checkbox"  onchange="toggleTaskCheckbox(this)" />
      <label class="custom-control-label" for="customTaskCheck`+taskId+`"> </label>`;
      $('#taskCheckbox'+taskId).append(checkboxElement)
    }
  })
}
function toggleSubtaskCheckbox(element){
  var subtaskId = parseInt(element.id.match(/\d+/g)[0]);
  var completed = element.checked;
  var taskId = activeTaskId;
  $.post('/projects/subtask', {subtaskId, completed}, function(data){
    //debugger;
    //calculate completion via subtask
    var temp = allSubtasks.find(s => s.id == subtaskId)
    if(temp){
      //fix completion in allSubtasks
      temp.completed = completed;
      //calc task completion
      var numberOfCompleted = 0;
      for(var i=0; i<allSubtasks.length; i++){
        if(allSubtasks[i].completed)
        numberOfCompleted++;
      }
      var completion = Math.round((numberOfCompleted/allSubtasks.length)*100)
      if(numberOfCompleted == '0' || allSubtasks.length == '0')
        completion = 0;
      //console.log(completion);
      fixTaskCompletion(taskId, completion);
      //debugger;
    }
  })
  //element.parentElement.parentElement.id + parseInt
}