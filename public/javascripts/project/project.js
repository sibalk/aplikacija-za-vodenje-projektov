//IN THIS SCRIPT//
//// control over collaps, drawing tasks, drawing page, changing category and activity of tasks ////

$(function(){
  $('[data-toggle="popover"]').popover();
  $('[data-toggle="tooltip"]').tooltip();
  //if($('#msg').html() != ''){
  //  $("#modalMsg").modal();
  //}
  loggedUser = {name:$('#loggedUser').html().split(" (")[0].split(" ")[0], surname:$('#loggedUser').html().split(" (")[0].split(" ")[1], role:$('#loggedUser').html().split(" (")[1].split(")")[0]};
  if(loggedUser.role == 'admin' || loggedUser.role == 'vodja' || loggedUser.role == 'tajnik')
    permissionTag = true;
  
  //UPDATE EDIT PROJECT
  $('#editProjectform').submit(function(e){
    e.preventDefault();
    $form = $(this);
    
    var name = $('#project_name').val();
    var number = $('#project_number').val();
    var start = $('#project_start').val() + " 07:00:00.000000";
    var finish = $('#project_finish').val() + " 15:00:00.000000";
    var subscriber = $('#project_subscriber').val();
    var notes = $('#project_notes').val();

    if(isNaN(subscriber)){
      debugger;
      //new subscriber, create subscriber and then update project
      $.post('/subscribers/create', {subscriber}, function(resp){
        var subscriberName = subscriber;
        subscriber = resp.id.id;
        $.post('/projects/update', {projectId, number, name, subscriber, start, finish, notes}, function(resp){
          debugger;
          $('#pinfo_name').html(name);
          $('#pinfo_number').html(number);
          $('#pinfo_notes').html(notes);
          $('#pinfo_subscriber').html(subscriberName);
          var result1 = '/';
          var result2 = '/';
          //var result = pad(dateobj.getDate())+"/"+pad(dateobj.getMonth()+1)+"/"+dateobj.getFullYear();
          if(start && finish){
            result1 = start.split(' ')[0].split('-')[2] + '.' + start.split(' ')[0].split('-')[1] + '.' + start.split(' ')[0].split('-')[0];
            result2 = finish.split(' ')[0].split('-')[2] + '.' + finish.split(' ')[0].split('-')[1] + '.' + finish.split(' ')[0].split('-')[0];
          }
          else if(start && !finish)
            result1 = start.split(' ')[0].split('-')[2] + '.' + start.split(' ')[0].split('-')[1] + '.' + start.split(' ')[0].split('-')[0];
          else if(!start && finish)
            result2 = finish.split(' ')[0].split('-')[2] + '.' + finish.split(' ')[0].split('-')[1] + '.' + finish.split(' ')[0].split('-')[0];
          else{
            result1 = 'Ni datuma';
            result2 = '';
          }
          $('#pinfo_date').html(result1+'-'+result2);
          //close collapse for edit Project
          $('#collapseEditProject').collapse("toggle");
        })
      })
    }
    else{
      debugger;
      //nothing new, update project
      $.post('/projects/update', {projectId, number, name, subscriber, start, finish, notes}, function(resp){
        var subscriberName = allSubscribers.find(s => s.id === parseInt(subscriber)).text;
        debugger;
        $('#pinfo_name').html(name);
        $('#pinfo_number').html(number);
        $('#pinfo_notes').html(notes);
        $('#pinfo_subscriber').html(subscriberName);
        var result1 = '/'
        var result2 = '/';
        //var result = pad(dateobj.getDate())+"/"+pad(dateobj.getMonth()+1)+"/"+dateobj.getFullYear();
        if(start && finish){
          result1 = start.split(' ')[0].split('-')[2] + '.' + start.split(' ')[0].split('-')[1] + '.' + start.split(' ')[0].split('-')[0];
          result2 = finish.split(' ')[0].split('-')[2] + '.' + finish.split(' ')[0].split('-')[1] + '.' + finish.split(' ')[0].split('-')[0];
        }
        else if(start && !finish)
          result1 = start.split(' ')[0].split('-')[2] + '.' + start.split(' ')[0].split('-')[1] + '.' + start.split(' ')[0].split('-')[0];
        else if(!start && finish)
          result2 = finish.split(' ')[0].split('-')[2] + '.' + finish.split(' ')[0].split('-')[1] + '.' + finish.split(' ')[0].split('-')[0];
        else{
          result1 = 'Ni datuma';
          result2 = '';
        }
        $('#pinfo_date').html(result1+'-'+result2);
        //close collapse for edit Project
        $('#collapseEditProject').collapse("toggle");
      })
    }
    
  })

})
//OPEN EDIT PROJECT COLLAPSE
function editProjectCollapse(projectId){
  //debugger;
  //edit project is closed
  if(!editProjectCollapseOpen){
    if(!allSubscribers){
      $.get( "/subscribers/all", function( data ) {
        allSubscribers = data.data;
        fillEditProjectData();
        $('#collapseEditProject').collapse("toggle");
        editProjectCollapseOpen = true;
      });
    }
    else{
      fillEditProjectData();
      $('#collapseEditProject').collapse("toggle");
      editProjectCollapseOpen = true;
    }
  }
  //edit project is open
  else{
    $('#collapseEditProject').collapse("toggle");
    editProjectCollapseOpen = false;
  }
}
function fillEditProjectData(){
  $(".select2-subscriber").select2({
    data: allSubscribers,
    tags: true,
  });
  $('#project_number').removeClass("is-invalid");
  $('#project_number').removeClass("is-valid");
  $('#btnEditProject').prop("disabled", false).removeClass("ui-state-disabled").addClass("ui-state-enabled");
  debugger;
  $('#project_number').val($('#pinfo_number').html());
  $('#project_name').val($('#pinfo_name').html());
  var x = allSubscribers.find(s => s.text == $('#pinfo_subscriber').html());
  $('#project_subscriber').val(x.id).trigger('change');
  var date = $('#pinfo_date').html().split(" - ");
  var s = date[0];
  var f = date[1];
  $('#project_start').val(s.split('.')[2]+'-'+s.split('.')[1]+'-'+s.split('.')[0]);
  $('#project_finish').val(f.split('.')[2]+'-'+f.split('.')[1]+'-'+f.split('.')[0]);
  $('#project_notes').val($('#pinfo_notes').html());
}
//OPEN EDIT TASK COLLAPSE
function toggleCollapse(taskId, type){
  //debugger;
  //if == 0 then there is no open collapse or open is one of this task's collapse
  if(activeTaskId == 0){
    activateTask(taskId, type);
  }
  //there is open collapse
  //close collapse, empty it and open this collapse
  else{
    if(editTaskCollapseOpen){
      $('#collapseEditTask'+activeTaskId).collapse("toggle");
      editTaskCollapseOpen = false;
      $('#collapseEditTask'+activeTaskId).empty();
      //it was open, want to close collapse edit
      if(type == 0 && taskId == activeTaskId)
        activeTaskId = 0;
      else
        activateTask(taskId, type);
    }
    else if(subtaskCollapseOpen){
      $('#collapseSubtasks'+activeTaskId).collapse("toggle");
      subtaskCollapseOpen = false;
      $('#collapseSubtasks'+activeTaskId).empty();
      //it was open, want to close collapse subtasks
      if(type == 1 && taskId == activeTaskId)
        activeTaskId = 0;
      else
        activateTask(taskId, type);
    }
    else if(taskFilesCollapseOpen){
      $('#collapseTaskFiles'+activeTaskId).collapse("toggle");
      taskFilesCollapseOpen = false;
      $('#collapseTaskFiles'+activeTaskId).empty();
      //it was open, want to close collapse files
      if(type == 2 && taskId == activeTaskId)
        activeTaskId = 0;
      else
        activateTask(taskId, type);
    }
    else if(taskAbsenceCollapseOpen){
      $('#collapseTaskAbsence'+activeTaskId).collapse("toggle");
      taskAbsenceCollapseOpen = false;
      $('#collapseTaskAbsence'+activeTaskId).empty();
      //it was open, want to close collapse absence
      if(type == 3 && taskId == activeTaskId)
        activeTaskId = 0;
      else
        activateTask(taskId, type);
    }
  }
}
function activateTask(taskId, type){
  activeTaskId = taskId;
  if(type == 0)
    editTask(taskId);
  else if(type == 1)
    openSubtasks(taskId);
  else if(type == 2)
    openTaskFiles(taskId);
  else if(type == 3)
    openTaskAbsence(taskId);
}
//CHANGE CATEGORY FOR ALL TASKS
function changeTaskTable(category){
  //debugger;
  $('#option0').removeClass('active');
  $('#option1').removeClass('active');
  $('#option2').removeClass('active');
  $('#option3').removeClass('active');
  $('#option4').removeClass('active');
  $('#option5').removeClass('active');
  $('#option6').removeClass('active');
  $('#option7').removeClass('active');
  setTimeout(()=>{
    $('#option'+category+'').addClass('active');
  })
  categoryTask = category;
  if($('#optionAll').hasClass('active'))
    activeTask = 0;
  else if($('#optionTrue').hasClass('active'))
    activeTask = 1;
  else if($('#optionFalse').hasClass('active'))
    activeTask = 2;
  $.get( "/projects/tasks", {projectId,categoryTask,activeTask, completionTask}, function( data ) {
    //console.log(data)
    debugger
    allTasks = data.data;
    //shownTasks = data.data.splice(0,10);
    taskLoaded = true;
    drawPagination(Math.ceil(allTasks.length/10));
    drawTasks(1);
    //$('[data-toggle="popover"]').popover({trigger: "hover"});
  });
}
//CHANGE TASKS TO SEE ONLY ACTIVE OR ALL
function changeActive(seeActive){
  //debugger;
  $('#optionAll').removeClass('active');
  $('#optionTrue').removeClass('active');
  $('#optionFalse').removeClass('active');
  activeTask = seeActive;
  if($('#option0').hasClass('active'))
    categoryTask = 0;
  else if($('#option1').hasClass('active'))
    categoryTask = 1;
  else if($('#option2').hasClass('active'))
    categoryTask = 2;
  else if($('#option3').hasClass('active'))
    categoryTask = 3;
  else if($('#option4').hasClass('active'))
    categoryTask = 4;
  else if($('#option5').hasClass('active'))
    categoryTask = 5;
  else if($('#option6').hasClass('active'))
    categoryTask = 6;
  else if($('#option7').hasClass('active'))
    categoryTask = 7;
  $.get( "/projects/tasks", {projectId,categoryTask,activeTask, completionTask}, function( data ) {
    //console.log(data)
    allTasks = data.data;
    //shownTasks = data.data.splice(0,10);
    taskLoaded = true;
    debugger
    drawPagination(Math.ceil(allTasks.length/10));
    drawTasks(1);
    if(seeActive == 1)
      $('#optionTrue').addClass('active');
    else if(seeActive == 2)
      $('#optionFalse').addClass('active');
    else
      $('#optionAll').addClass('active');
    //$('[data-toggle="popover"]').popover({trigger: "hover"});
  });
}
//CHANGE TASK TO SEE ALL/UNFINISHED/FINISHED
function changeCompletion(seeCompletion){
  $('#optionAllCompletion').removeClass('active');
  $('#optionUncomplete').removeClass('active');
  $('#optionComplete').removeClass('active');
  completionTask = seeCompletion;
  setTimeout(()=>{
    if(seeCompletion == 1)
      $('#optionUncomplete').addClass('active');
    else if(seeCompletion == 2)
      $('#optionComplete').addClass('active');
    else
      $('#optionAllCompletion').addClass('active');
  })
  changeTaskTable(categoryTask);
}
//REMOVE/EMPTY TASK LIST AND DRAW/SHOW NEEDED TASKS
function drawTasks(page){
  $('#pagePrev').removeClass('disabled');
  $('#pageNext').removeClass('disabled');
  if(page == 1)
    $('#pagePrev').addClass('disabled');
  if($('#page'+page).next().attr('id') == 'pageNext')
    $('#pageNext').addClass('disabled');
  $('#page'+activePage).removeClass("active");
  $('#page'+page).addClass("active");
  activePage = page;
  var limit = page * 10;
  var start = limit - 10;
  limit = Math.min(limit, allTasks.length);

  debugger;
  //first empty task list and reset collapse and active task id
  $('#taskList').empty();
  debugger;
  editProjectCollapseOpen = false;
  editTaskCollapseOpen = false;
  subtaskCollapseOpen = false;
  taskFilesCollapseOpen = false;
  taskAbsenceCollapseOpen = false;
  activeTaskId = 0;
  var deleteTaskClass;
  var myTask = false;
  //draw 10 tasks
  for(var i = start; i < limit; i++){
    myTask = false;
    var id = allTasks[i].id;
    deleteTaskClass = '';
    if((loggedUser.role == 'admin' && allTasks[i].active == false))
      deleteTaskClass = ' bg-secondary';
    //check if has no subtasks -> then add checkbox
    //if has no subtasks and completion == 100 then checked else not checked
    //checkbox
    var checkboxElement = '';
    var noteElement = '';
    var isChecked = '';
    var category = allTasks[i].category.toUpperCase();
    var categoryClass = ''; 
    if(category == 'NABAVA')
      categoryClass = 'badge-purchase';
    else if(category == 'KONSTRUKCIJA')
      categoryClass = 'badge-construction';
    else if(category == 'STROJNA IZDELAVA')
      categoryClass = 'badge-mechanic';
    else if(category == 'ELEKTRO IZDELAVA')
      categoryClass = 'badge-electrican';
    else if(category == 'MONTAŽA')
      categoryClass = 'badge-assembly';
    else if(category == 'PROGRAMIRANJE')
      categoryClass = 'badge-programmer';
    else if(category == 'BREZ')
      category = '';
    //debugger;
    var taskWorkers = [];
    if(allTasks[i].workers){
      taskWorkers = allTasks[i].workers.split(", ");
      var tmp = taskWorkers.find(w => w == (loggedUser.name +" "+ loggedUser.surname))
      if(tmp)
        myTask = true;
    }
    if(leader || myTask || loggedUser.role == 'admin' || loggedUser.role == 'vodja'){
      if(!allTasks[i].subtask_count){
        if(allTasks[i].completion == 100)
          isChecked = 'checked = ""';
        checkboxElement = `<input class="custom-control-input" id="customTaskCheck`+id+`" type="checkbox" `+isChecked+` onchange="toggleTaskCheckbox(this)" />
        <label class="custom-control-label" for="customTaskCheck`+id+`"> </label>`;
      }
      noteElement = `<a class="taskpopover ml-auto" data-toggle="popover" data-trigger="hover" data-content="`+allTasks[i].task_note+`", style="height:30px;" data-original-title="" title=""><img class="img-fluid" src="/info.png" alt="ikona" style="width:25px;" /></a>
      <button class="invisible-button ml-2" onclick="addTaskNote(`+id+`)"><img class="img-fluid" src="/note-52.png" alt="ikona" style="width:25px;" /></button>`;
    }
    else{
      noteElement = `<a class="taskpopover ml-auto" data-toggle="popover" data-trigger="hover" data-content="", style="height:30px;" data-original-title="" title=""><img class="img-fluid" src="/info.png" alt="ikona" style="width:25px;" /></a>
      <button class="invisible-button ml-2" disabled onclick="addTaskNote(`+id+`)"><img class="img-fluid" src="/note-52.png" alt="ikona" style="width:25px;" /></button>`;
    }
    //date
    var date = '';
    if(allTasks[i].formatted_start && allTasks[i].formatted_finish)
      date = allTasks[i].formatted_start.date + " - " + allTasks[i].formatted_finish.date;
    else if(allTasks[i].formatted_start)
      date = allTasks[i].formatted_start.date + " - /";
    else if(allTasks[i].formatted_finish)
      date = "/ - " + allTasks[i].formatted_finish.date;
    //priority
    var priorityClass = '';
    if(allTasks[i].priority == 'visoka')
      priorityClass = 'border-high-priority';
    else if(allTasks[i].priority == 'srednja')
      priorityClass = 'border-medium-priority';
    else if(allTasks[i].priority == 'nizka')
      priorityClass = 'border-low-priority';
    //workers
    var workers = "";
    if(allTasks[i].workers)
      workers = allTasks[i].workers;

    //control buttons
    //delete button
    var deleteControlButton = '';
    if(loggedUser.role == 'admin' || loggedUser.role == 'vodja' || leader)
      deleteControlButton = `<button class="btn btn-danger ml-2" onclick="deleteTask(`+id+`)" style="width:25px, height:5px, border:none;" id="taskButtonDelete`+id+`">
        <img class="img-fluid" src="/remove.png" alt="ikona" style="width:15px" data-toggle="tooltip" data-original-title="Odstrani">
      </button>`;
    //edit button
    var editControlButton = '';
    if(loggedUser.role == 'admin' || loggedUser.role == 'vodja' || leader){
      editControlButton = `<button class="btn btn-primary ml-1" onclick="toggleCollapse(`+id+`, 0)" style="width:25px, height:5px, border:none;" id='taskButtonEdit`+id+`'>
        <img class="img-fluid" src="/edit.png" alt="ikona" style="width:15px" data-toggle="tooltip" data-original-title="Uredi">
      </button>`;
    }
    //absence button
    var absenceControlButton = '';
    if(loggedUser.role == 'vodja' || loggedUser.role == 'admin' || leader || myTask){
      absenceControlButton = `<button class="btn btn-programmer ml-1" onclick="toggleCollapse(`+id+`, 3)" style="width:25px, height:5px, border:none;" id='taskButtonAbsence`+id+`'>
                                <img class="img-fluid" src="/absenceWhite.png" alt="ikona" style="width:15px" data-toggle="tooltip" data-original-title="Odsotnosti">
                              </button>`;
      //debugger;
      if(allTasks[i].full_absence_count && allTasks[i].full_absence_count > 0)
        absenceControlButton = `<button class="btn btn-programmer ml-1" onclick="toggleCollapse(`+id+`, 3)" style="width:25px, height:5px, border:none;" id='taskButtonAbsence`+id+`'>
                                  <img class="img-fluid" src="/absenceBlack.png" alt="ikona" style="width:15px" data-toggle="tooltip" data-original-title="Odsotnosti">
                                </button>`;
      if(allTasks[i].task_absence_count && allTasks[i].task_absence_count > 0)
        absenceControlButton = `<button class="btn btn-programmer ml-1" onclick="toggleCollapse(`+id+`, 3)" style="width:25px, height:5px, border:none;" id='taskButtonAbsence`+id+`'>
                                <img class="img-fluid" src="/absenceBlack.png" alt="ikona" style="width:15px" data-toggle="tooltip" data-original-title="Odsotnosti">
                              </button>`;
      if(!allTasks[i].workers || !allTasks[i].task_start || !allTasks[i].task_finish)
        absenceControlButton = `<button class="btn btn-programmer ml-1" onclick="toggleCollapse(`+id+`, 3)" style="width:25px, height:5px, border:none;" disabled id='taskButtonAbsence`+id+`'>
          <img class="img-fluid" src="/absenceWhite.png" alt="ikona" style="width:15px" data-toggle="tooltip" data-original-title="Odsotnosti">
        </button>`;
    }
    //files button
    var filesControlButton = '';
    if(loggedUser.role == 'vodja' || loggedUser.role == 'admin' || leader || myTask){
      filesControlButton = `<button class="btn btn-purchase ml-1" onclick="toggleCollapse(`+id+`, 2)" style="width:25px, height:5px, border:none;" id='taskButtonFile`+id+`'>
                                  <img class="img-fluid" src="/filesWhite01.png" alt="ikona" style="width:15px" data-toggle="tooltip" data-original-title="Datoteke">
                                </button>`;
      if(allTasks[i].files_count && allTasks[i].files_count > 0)
        filesControlButton = `<button class="btn btn-purchase ml-1" onclick="toggleCollapse(`+id+`, 2)" style="width:25px, height:5px, border:none;" id='taskButtonFile`+id+`'>
                                <img class="img-fluid" src="/filesBlack01.png" alt="ikona" style="width:15px" data-toggle="tooltip" data-original-title="Datoteke">
                              </button>`;
    }
    //subtask button
    var subtaskControlButton = '';
    if(loggedUser.role == 'vodja' || loggedUser.role == 'admin' || leader || myTask){
      subtaskControlButton = `<button class="btn btn-info ml-1" onclick="toggleCollapse(`+id+`, 1)" style="width:25px, height:5px, border:none;" id='taskButtonSubtask`+id+`'>
                                    <img class="img-fluid" src="/subtaskWhite.png" alt="ikona" style="width:15px" data-toggle="tooltip" data-original-title="Naloge">
                                  </button>`;
      if(allTasks[i].subtask_count && allTasks[i].subtask_count > 0)
      subtaskControlButton = `<button class="btn btn-info ml-1" onclick="toggleCollapse(`+id+`, 1)" style="width:25px, height:5px, border:none;" id='taskButtonSubtask`+id+`'>
                                <img class="img-fluid" src="/subtaskBlack.png" alt="ikona" style="width:15px" data-toggle="tooltip" data-original-title="Naloge">
                              </button>`;
    }
    //putting everything together into one element
    var element = `<div class="list-group-item `+priorityClass+deleteTaskClass+`" id="task`+id+`">
      <div class="row">
        <div class="col-md-6">
          <div id="taskName`+id+`">`+allTasks[i].task_name+`</div>
        </div>
        <div class="col-md-6 d-flex">
          <div class="custom-control custom-checkbox table-custom-checkbox" id="taskCheckbox`+id+`">
            `+checkboxElement+`
          </div>
          `+noteElement+`
          <div class="ml-2" id="taskInfo`+id+`"><a class="workerspopover" data-toggle="popover" data-trigger="hover" data-content="`+workers+`" data-original-title="" title=""><img class="img-fluid" src="/employees.png" alt="ikona" style="width:25px;" /></a></div>
          <div class="progress ml-2" style="width:70%;">
            <div class="progress-bar" id="taskProgress`+id+`" style="width:`+allTasks[i].completion+`%;"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-7 d-flex">
          <div id="taskDate`+id+`">`+date+`</div>
          <div id="taskBadges`+id+`">
            <span class="badge badge-pill `+categoryClass+` ml-2">`+category+`</span>
          </div>
        </div>
        <div class="col-md-5 d-flex">
          <div class="ml-auto" id="taskButtons`+id+`">
            `+deleteControlButton+`
            `+absenceControlButton+`
            `+filesControlButton+`
            `+subtaskControlButton+`
            `+editControlButton+`
          </div>
        </div>
      </div>
      <div class="collapse multi-collapse task-collapse" id="collapseEditTask`+id+`"></div>
      <div class="collapse multi-collapse task-collapse" id="collapseSubtasks`+id+`"></div>
      <div class="collapse multi-collapse task-collapse" id="collapseTaskFiles`+id+`"></div>
      <div class="collapse multi-collapse task-collapse" id="collapseTaskAbsence`+id+`"></div>
    </div>`;
    //debugger;
    $('#taskList').append(element);
  }
  //popover
  $('[data-toggle="popover"]').popover();
  $('[data-toggle="tooltip"]').tooltip();
}
//CHANGE PAGE OF TASKS LIST
function changePage(page){
  debugger;
  //page is prev or next
  if(isNaN(page)){
    //check if task are loaded
    if(page == 'next')
      page = activePage + 1;
    else if(page == 'prev')
      page = activePage - 1;
    if(taskLoaded){
      drawTasks(page);
    }
    else{
      $.get( "/projects/tasks", {projectId,categoryTask,activeTask, completionTask}, function( data ) {
        allTasks = data.data;
        taskLoaded = true;
        debugger
        drawTasks(page);
      });
    }
  }
  //page is number
  else{
    //page isnt active otherwise do nothing
    if(!$('#page'+page).hasClass('active')){
      debugger;
      if(taskLoaded){
        drawTasks(page);
      }
      else{
        $.get( "/projects/tasks", {projectId,categoryTask,activeTask, completionTask}, function( data ) {
          allTasks = data.data;
          taskLoaded = true;
          debugger
          drawTasks(page);
        });
      }
    }
  }
}
//DRAW PAGINATION
function drawPagination(pages){
  debugger;
  $('#taskPages').empty();
  var missingPages = '';
  for(var i = 2; i <= pages; i++){
    missingPages += '<li class="page-item" id="page'+i+'"><a class="page-link" onclick="changePage('+i+')">'+i+'</a></li>';
  }
  var pagesElement = `<ul class="pagination pagination-sm">
    <li class="page-item disabled" id="pagePrev"><a class="page-link" onclick="changePage(&quot;prev&quot;)">«</a></li>
    <li class="page-item active" id="page1"><a class="page-link" onclick="changePage(1)">1</a></li>
    `+missingPages+`
    <li class="page-item" id="pageNext"><a class="page-link" onclick="changePage(&quot;next&quot;)">»</a></li>
  </ul>`;
  $('#taskPages').append(pagesElement);
}
function openPDFModal(){
  debugger
  if(allWorkersTable)
    $("#modalPDFForm").modal();
  else{
    $.get('/projects/worker/get', {projectId}, function(resp){
      allWorkersTable = resp.data;
      $("#modalPDFForm").modal();
    })
  }
}
//Make PDF and include only the ticked options from modal
function toPDF(){
  var categoryTask = 0;
  var activeTask = 1;
  $.get( "/projects/tasks", {projectId, categoryTask, activeTask}, function( data ) {
    var projectTasks = data.data;

    debugger;
    var c = document.createElement('canvas');
    var imgB = document.getElementById('subImg');
    c.height = imgB.naturalHeight;
    c.width = imgB.naturalWidth;
    var imgBHeight = imgB.height;
    var marginForInfo = 105 - imgBHeight;
    marginForInfo = -90 + marginForInfo;
    var ctx = c.getContext('2d');
    ctx.drawImage(imgB, 0, 0, c.width, c.height);
    var base64String = c.toDataURL();
    var logo = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAD6CAYAAAAbbXrzAAAABGdBTUEAALGPC/xhBQAAAAlwSFlzAAAOwgAADsIBFShKgAAAABh0RVh0U29mdHdhcmUAcGFpbnQubmV0IDQuMS42/U4J6AAAAyVJREFUeF7t1AENAAAMw6D7N73raAIiuAFECAvIEBaQISwgQ1hAhrCADGEBGcICMoQFZAgLyBAWkCEsIENYQIawgAxhARnCAjKEBWQIC8gQFpAhLCBDWECGsIAMYQEZwgIyhAVkCAvIEBaQISwgQ1hAhrCADGEBGcICMoQFZAgLyBAWkCEsIENYQIawgAxhARnCAjKEBWQIC8gQFpAhLCBDWECGsIAMYQEZwgIyhAVkCAvIEBaQISwgQ1hAhrCADGEBGcICMoQFZAgLyBAWkCEsIENYQIawgAxhARnCAjKEBWQIC8gQFpAhLCBDWECGsIAMYQEZwgIyhAVkCAvIEBaQISwgQ1hAhrCADGEBGcICMoQFZAgLyBAWkCEsIENYQIawgAxhARnCAjKEBWQIC8gQFpAhLCBDWECGsIAMYQEZwgIyhAVkCAvIEBaQISwgQ1hAhrCADGEBGcICMoQFZAgLyBAWkCEsIENYQIawgAxhARnCAjKEBWQIC8gQFpAhLCBDWECGsIAMYQEZwgIyhAVkCAvIEBaQISwgQ1hAhrCADGEBGcICMoQFZAgLyBAWkCEsIENYQIawgAxhARnCAjKEBWQIC8gQFpAhLCBDWECGsIAMYQEZwgIyhAVkCAvIEBaQISwgQ1hAhrCADGEBGcICMoQFZAgLyBAWkCEsIENYQIawgAxhARnCAjKEBWQIC8gQFpAhLCBDWECGsIAMYQEZwgIyhAVkCAvIEBaQISwgQ1hAhrCADGEBGcICMoQFZAgLyBAWkCEsIENYQIawgAxhARnCAjKEBWQIC8gQFpAhLCBDWECGsIAMYQEZwgIyhAVkCAvIEBaQISwgQ1hAhrCADGEBGcICMoQFZAgLyBAWkCEsIENYQIawgAxhARnCAjKEBWQIC8gQFpAhLCBDWECGsIAMYQEZwgIyhAVkCAvIEBaQISwgQ1hAhrCADGEBGcICMoQFZAgLyBAWkCEsIENYQIawgAxhARnCAjKEBWQIC8gQFpAhLCBDWECGsIAMYQEZwgIyhAVkCAvIEBaQISwgQ1hAhrCADGEBGcICMoQFZAgLiNgezGuQgq0IIfsAAAAASUVORK5CYII=';
    if($('#checkboxLogo')[0].checked){
      //logo = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAD6CAYAAAAbbXrzAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjEuNWRHWFIAACeXSURBVHhe7Z0J/G3VFMfNKTOZp8xTJBkzlCJDiMxJUo8GGk2RpJ4SGTOXqSIlQ8hcZAhNiEKaU0oSpdF0vO/21v+t/7rrnHvu/d/7/+/3/M7n8/2cc9cezh7XXnufc8++TtM0QgixXJAKhRCiRlKhEELUSCoUQogaSYVCCFEjqVAIIWokFQohRI2kQiGEqJFUKIQQNZIKhRCiRlKhEELUSCoUQogaSYVCCFEjqVAIIWokFQohRI2kQiGEqJFUKIQQNZIKhRCiRlKhEELUSCoUQogaSYVCCFEjqVAIIWokFQohRI2kQiGEqJFUKIQQNZIKhRCiRlKhEELUSCoUQogaSYVCCFEjqVAIIWokFQohRI2kQiGEqJFUKIQQNZIKhRCiRlKhEELUSCoUQogaSYVCCFEjqVAIIWokFQohRI2kQiGEqJFUKIQQNZIKhRCiRlKhEELUSCoUQogaSYVCCFEjqVAIIWokFQohRI2kQiGEqJFUKIQQNZIKhRCiRlKhEELUSCoUQogaSYVCCFEjqVAIIWokFQohRI2kQiGEqJFUKIQQNZIKhRCiRlKhEELUSCoUQogaSYVCCFEjqVAIIWokFQohRI2kQiGEqJFUKIQQNZIKhRCiRlKhEELUSCoUQogaSYVCCFEjqVAIIWokFQohRI2kQiGEqJFUKIQQNZIKhRCiRlKhEELUSCoUQogaSYVCCFEjqVAIIWokFQohRI2kQiGEqJFUKIQQNZIKhRCiRlKhEELUSCoUQogaSYVCCFEjqVAIIWokFQohRI2kQiGEqJFUKIQQNZIKhRCiRlKhEELUSCqMfO1rX2s+8IEPNG9729uaxYsXF/baa6/yG+K1d+fs3b2/Npl3i9dt9+4Kk53bwL0rTHa99957z/yGrvLxsr7+/O9Ytt4tyr179BvBPaYnunO2vGZubTK7zmSe6E562u5n7hbGZHbt/Ucyf8NkYOnh7P3EMFk5xrx4d4vThzV376+vzNM3TAzv8xDdjEyOLMt/dLfrD37wgw36JeqcNlKh51WvelXz+Mc/vrnHPe7RrLrqqs2tb33rAte3uc1tCnbN+ba3ve0sd87er+HdTUbY6ObduSZu8wcxTNu1+ePs3SPePYbltw/b5t5WPv6aMH3KKabX5z/693Fx9mGjX5MZw9Jj13bvNpl3M/eusB4vs/RYGB+f/c7SC1l6DO+exW1nf23ulp5h5WTuXt6WF84+zhjWzv7au/s4zZ8n+vMyHyZe+zwS1rt7f/Hs8+L9Gshx5xq/97rXvZonPOEJzfbbb79E3eQ6yJMK4TOf+Uyz2mqrNde73vWW+LqOEEJMlfvf//7NF77whSXqJ9dJkArh9re/fXPd6153IFJPm/uwcJOAe8zHfSJt912ItEybrrzOR35rK9Pa0jMJasvTne985yXqJ9dJkApf//rXD0TUxqQzPKn4iKfWBjaJdE0jb21xTvJexDWp+KZRBpMgS9ewtEZ3fk86f9OIM2Mu92BG96Y3vWmJGhrUS5AKN9poo4GIIMvwfBRAZCHu2YesfFYEVMfLWBHrd6HI+gu/N9xwwyVqaFAvQSp84hOfOCsCH+GkUMXPJqu8NiZVdqPcc1SmGfeKziTLbpp1MG7cXeFwe+xjH7tEDQ3qJUiF66677kBEoh6m2QjFwkP9/r/W8VgKy1tYoi6krOplEnVjyur/tZ7J9+Me97glamhQL0EqlMISQiwE86awbES4wQ1u0NzpTndq7n73u8/A+1xg194to83PXe5yl4bXLVZZZZU5jUA3uclNSjx3vetdm7vd7W7pvcaFOO9whzs0t7jFLcZK4/Wvf/3yYl0Wt+HLx5ftMCg/0nazm91s6Pt1pL0t/YQnHvKa3QdGSReYX+Kkbm5605sO3NeniTP1iP+u+4yThiwMMh653/CGNxxIF9DuKZMYzsJm8j4Q1oen7fv7xjqy36Tzdre73ay4ePmblzVJqw8zKtxj5ZVXnlX2nOlLvBCahekLcU9dYXETK6g73vGODS+dHnfccc0JJ5zQHH/88eUa/LWXmTxzN/lPfvKT5hvf+EbzqU99qtl8881Lo/b3j2e7jtDoFi1aVNJ41FFHlXiH3b8LwvzsZz+bOX/ve99rPve5zzWve93rmgc84AHNjW50ozQdbVDhO+2000ycw9KEP87D/P30pz9tjj766ObQQw9tXvOa15TGlt2/rdwAJXfve9+72WGHHUoeyavdP6PLDSx/lnYrP+pmu+22Kx2hrXPd+MY3bp761KeWPP385z8fiHsUfBp8mzU5/OpXv2qOOOKI0ukzZc9gcMABB8y0p7714jG/dk+7PvHEE2fO66+//sC9szqj7N7znveU/Fj8p512WrPPPvvM6jvjgDJce+21S12ddNJJJW7q4Mc//nHz5je/OQ3TF/Iyr1PCe97zns1vfvOb5h//+Efz73//u/nXv/7V/POf/yxw7WmTm5u/BuK89tprlySxafbbb7+ifLinrzC75hwrklGA/y0Rh8Vp+HuPQgxLGuGqq64q5bDOOusMdLqYLp9WFP773//+Eu9//vOfWXF7utLc5oacvBMvyn/NNdeclQ5PLD8sv4c85CHNscceW+KJZPczhrkbPr4rr7yy+eEPf1jaU6YgGOFf9rKXNZdccklpD1l8k4TjjDPOaB74wAeWsojpuc997lM6rm/3ozKsnDhe+MIXztwztiMPg+W3v/3tEsbSw/GlL32pKN0sTF9WWmml8urBX/7ylxKnxU3bOuigg9IwfSFPIysse0rYViCxMXsojF//+tczmZjWQeEwCtt9uyoPcN93332bv/71r0tjmP5BGXz2s58to6+lwdLZll6mFSgslMq0j7e//e1lWmX37ipDFMRHP/rR5u9///vS0NM/UFpYCbe61a3S9KCw5rM+zz777OZBD3pQavVheWLFohymeWy66aYD987gby7f/OY3l4ZadqCwsL6yMH1h1oB1+7e//W1prP876JPMgMxfV3tqgzAjK6zM7OwLVswvfvGLAYVFRZIhs0CGYSNtVwOgA7VNuci4LzA6JiOgHfOhELjHueeeWywTn67s2kBhvfe97x1I39VXX92cf/75xaxnpD/99NOb3//+9+Xsr3Hj2s5nnnnmQMOy4zvf+U7z8Ic/vDUtHiwd8jLtg3xb3ql78sQfZGN6FlJhZRYWCospUWyv9AMU7xVXXDEnGCjoP89+9rMH7p1xv/vdr/n617++NBXLji9+8YutywEZ1i442zUW1lOe8pSZdmX1dc0115RpsQ8/KtxjXhfdsbBOPvnkomw4LDOnnHJKWZth/eiVr3xls9VWW5XzK17xinLtf3MG/PIvbtZLqPR4HHLIIWVhMabBru03Z6aPNH5/kDbS9YY3vKF54xvfWP4SYGd/3ebm5azrfOtb35qlqImfSuRrF5auLH0eFNb73ve+mYZv5fe73/2upPNJT3pS84xnPKOAWe6v4ZnPfOaM/OlPf3oZCbfZZpvmu9/9bonHH6w70PDs3ll6gA7KuzFWB5Ym8sogsPPOO8+Ux6677lqw3xFffnYNrKux7mMH9wDuudZaaw2kCSuH6Rn3Zt1kl112mYmTs6WB3/jZf//9U4WLVYSl+drXvnYmLGefB37vvvvuzbbbblvWGH052TVKNVNYrPFsvfXWxTLabLPNxualL31pOfdVNlhYTPvtsDqbhMLCSPAKy45MYflwfcDvyArrMY95zEBEw7BEYWH98pe/HFBYX/nKV9KRqQ9YKD/60Y9KPP5AYdHBzV9bwXBf1hgYIf1BGlmEzsKMAwvtVFo8vFIw2tJqFlZUWOT/kY98ZBqmDxtvvHGJxx8oLBSa+WlLE4usG2ywwYDCIq98zygLMw7k2w5/Dxpw5n8UaNPHHHNMidMf73rXu2YNeqNiZYbCYs0tKizaV1u5ThMsrCOPPHJpKpaVJ1NCnuZlYYZh+ehSWB//+McHwoySf/yOrLBsROtzo+inbUrIfPqWt7zlLL99wTqiY8QDhdXniQeLtiisc845Z2nI/x2TVlj8aZzpbDyweDL/GSgs1m2sgdnBEyKsK/xYmY/SELCQYmfyCqsrLlNYTE38wRTlwx/+cBpmHFi7iwdT4UxhjZJ3IA4sIA5ftlizftAbFxQWg0osY54sjtPuyV+fPLb5iQrLjkmsYTElpN3YdNwPLlFhjQr5GVlh2brGODAlxMLKFFa2eJoRK4ERAVMzHpNQWIcddlgaZhyYlmQKi+lZ5j/DLKyosHg8vd5666Vh2vDlyLpkrJNxLSw7yCvriOavLXxfPvShDy2NednRprDA7td1X3PjI3FM/+LBQGhPm/uS3Y81LF5psDK2+kNh3fzmNx/wPwrjlCtTwmwNi+9NjTIlzMDCoj30sbCA9PfNA/5GVliPeMQjBiLqi00JY+dgfYcXIrMww6CAWYOIR1zDasMUVpwSksbPf/7zaZhxmKbCorNla2F9QTHFg3d7/AOWtkbVV2HNFT7DHY8uhTUKlB0KJR4oLF50zsKMQpuFdfjhh7c+GJombYvuPLWe68udrB9isccHHpOysBhclhypbkqF01BYvP/Ek50szDBQWB/72MeWxrTsiGtYbbRZWMuTwqJMX/SiF828vUw5c7Y39e23yfjNGesBZcVUIB4HH3xwr/WM+VJYo1pYozBthYWFxTtqUWExDWVJgPvzulAXvLMXp499LZNI25SQBxs8PGBNk3e6+vCCF7xg5vp5z3tes8kmm5Rvssf2MCmFxUO/JUeqm1LhNBQWj9B5KZL5L4qLvxjYGXhz2WS8gsCZkQlzmicsPKaPB08P+ygsCoEGtTxbWKwf8aSQp3L2Nrad7drwv7nmVYi4/sSaWPYwIEMKazi0Lywsa/dWf7yOQPmfeuqp5UXiLs4666xZVvS4ygraLCzawXnnnVf6E/frA6/GeJBdeOGFA8p5hVJYZJBRHpMUy8hA6XBGzrWdTcZjWF5E5Yid+MADD+y1LrYiWFiTOFizo27YreRhD3tYUURZGiJSWMOx97C8whqnDnkVxeKMCmsUBRZfa+AYN019jxVKYU3j4GlaloaIFNb/DhQW7wWRTl4Eze6fIYU1HFNY0eoY9fAKy0NHnqvCmvaxYAprLu/7RIU1ilY3f338Y0L3fV2AQljep4T8NxFznCd7vJjbB+oBMOEJ7w8aM39gzdIQmS+FhfKIx/K+6M7CNHXBQEHddcEfrNvqZBRlBW1Twssvv7y8QM0U9be//e1Q6Gd2NvjNtLKaKeEkFZYdNG4q79JLL50F7/L4w3dU5tv8uZU/WeKX85///OfynteznvWsmXsOq8wVwcLijfwtt9yydAxepO3iwQ9+cGH11VcvZxZKv/zlLy+NadnB/75YV7R7t5XjfCms5fkpIQMii+6x3fNFEN5rpN7ue9/7dkIb9f/thFEVldG26P6DH/ygtIdHP/rRZZF/GDyxszNQFzxZ5iVplJ8/VhgLi4OFx9122638hYW/Suy4447laQUjfZsZzdvx/MGZzVzxv2jRovLEa9THsivKaw1z6biEjYMDa4P8lcf7yzqIFNZw2qaEvPfEA6UsTF/GUVptFhbrw3wTKwvTF9oD5Tmt1xqqsLAoPB7Z2tNA4H0OHqvzqZjsYAq0xRZblHjxa08OrQI596nMFUFh8cRv1BdHPSyyX3zxxbPixax/yUtekvr3aA1rOG0Ki0F31DfdfbuO5760/ZcQBWpfDxkX+uCTn/zk5U9hZYXYpbCyr0gCnXTPPfdc6nNZ4VL5TOP22GOPgbDcu28ldiksXuzLwozDpBTWsL/mePqWwxprrNFccMEFs+LlyS1fPMC9Kw4UFq9AxFcj5kNhsfaGssn8j0KXwvJvuvdtU5G2NayvfvWrc35RM9JW517WZmHx1L3Pu3ddoLCY6WRvun/iE59Iw/SFPExFYbVVbJvC4k33rorDTOUf8XEU57jsssuaT37yk2U9ZtgfqLN0obBYI4gKiw7H6xPR/7jwxQFb3PaKwSssS188G20Ki87G28Xe7yiwjhLXHFBYfL01+o1pooFiYUWFhfWDdez9zgXyHQ86waQUFmtM8ZjWorvV3yT/qxrJ2rrJJq2wiNfi7quwsvQNgzALqrCs4uJ/CbPwfCucz3xcdNFFJYw/6BxUAKZotiZAfEZ0a7OweMxPuvg6oy1Qx4XrvmBm0+FsncgrHCrX0mLpi2ejbUrIUyTeTuZb8SyUd0EcdubNeL7htHjx4qUxLTtQWC9/+ctn3R9iOdJAse6yKSH/xeQVCcoAKy5b/O8D/2bg08h2WP7pBON8PQR8HlgwnqbCiovuln4+68N/c/kUTsxzG1aOUc4XYqlTG7Rj2/GyNoU16p+fLT7Odk176PO1Bh+mL/ivYtG9j8IyOQvs8dtVHIxedFymMfEPpcMKhw7MB+38Qdr++Mc/lnUGTHfgL0Rgv/tAeNYGeNwbFQ0W1yhrMG0Ki/Un7sXXBehkTJ+AryWA/QZz5wyUvX3O1h9da1i+PFH4PFWKCos0MmVn1CZtVm529uU6DJ5ixqe4HFiFdN6Yvj749oDCmo81rDiz+NOf/lQ+VTxKWbS1Qb6hzppu25+pfX4nrbA8XRZWXMPy7agP+K1uSmhhuzLDh/x45yM7mCLyKZfsz9Rt8fGXIL5XZEdUCNM4uAdKEgsnS1MGCitOCbmeRnopD69M28oOmEbw16D5PGhDPMmkTWVpGoW+i+5dZdBFNiWcRp0xYHVtwGK0vTg6iSkhfYkXXJerKWEbfF6GF+AyhTXMwooy/nSJ1dJ2fOQjHxm6YGoyrAT+sMl7XJM+2hom00PW3kb5fAkKi0Y5SmPv6hxdbnxJ075PnpWdh2k4ll98MjSpI0sjHYI6G/c7ah7eJWr7vExmYQ0rjwgWFvH7dt9V9uMe73znOwfWgn1a7RqFhWUdDxTWKBZWhn0PKyos2nu1i+5tUHGY9vFpCX9+7vP+R2wofMO6a2Tn3+d+Ubaroa266qrl/4nZk7xJH6yP8cdjyjI+KOhKI50HRTzphu4PpsFYqPHTPD5dXBv8RuEzzWBNZppps4M6YpDz1kBXubVhYViD44XjePAVkLneA1AQDNSx3U/6wPru80kl1rx4STQeTE2xBrMwfUFh8eJ2VFi0i/iakG9DfcDvvCoslALf2OYlQFtH4Y1qXv6Mb/FCW4ZMhgXA6IjVYWs1xE28aHMaHN9/t41Ls7g8KE0WDHmixx5tPp1+3acvMSyf3OU74M95znPKVIZXAuzePn1t6WR9gjeRiY/8Wl75bTJ/P+/m5SYDvuTJd8v5JjyfqKExc5+2NGRyZCgtOjcvm/KmM3n1afDX9js7R5m/xoKgbrgHyrstjcOI4XhKjJJmjcXuxzWfTJnLJ13ML+2KMqG+Yr3ZeRiZP2Qmpx/RrtpeD/Jgqb/61a8ug5+F//SnP12WWub6mgV9kmUO6p/+R/y83kKe+fa8+bOy4dy3TPE3rwqLzDDH5qkZT34408hRZDT4LMwwWOSjAojP4rT4GS14SoWfLGwG6aDSidOn08fdlxiWxX06QNfrF12VRzjCE5+n7X52HeUmA6ak1ElXuvo2KKD8UHjktS0N/jqeo8xfUyd9dqbui+ULq4AyoC3a/bgeVld9od1j+fi8cI7XXWT+kJmcqRwDc5+yIT0oJh8X4ZHNNb+UKUsEtCtfnpyj8h+VsRRW29caiGyUhi1yRdBVhgtVvitqvaq9Ll9QXxOzsIhMDUAIMS3mrLCkoIQQ88VYCmsuH/ATQohxkYUlhFhumJjC4izlJYSYJnNWWBaJV1ZSXEKIaTCWwuKt3SwyIYSYJigsPjYZdZKRCnnTmG848bcY2GijjQr222TezRPdfZgueRc+jD9H2TC8PwtveJn56cKHsXObrIssrL/m7K/N3cjc/TnK2q6H+ctkbWEyeRfefwzr4zD3LmIYj3cfhvcXw0a3YcSwdh3l3t2TuQ8Lm7lnMs6RGKaLGLYN8xvPds3ffTjvuuuuS9TQoF6CVCiEEDWSCoUQokZSoRBC1EgqFEKIGkmFfFubz1LwyQg+H8FZCCHmAz6jE3WSMSDgW9J8L4nPRfDpCD5JwVkIIeYDPhkV9ZIxIGBn2D4fCBNCiGkR9ZIxIGD3EvsGud5oF0LMN3wEMOolY0DANkB8STKLCAUmJSaEmCZ8VTXqJWNAgMKKu4gspKLivnwXnU8gS1n+/2D1zqeN+RyvBxmfAM7CieWf5Uphxfuw9yAbErBDcdfGCdOk7Z7TTMtC5HMuDEuvd+c68+/lNFr+prHNNtuUzTPYkszYdtttR962v+2e88lC3n+h8z4KIyssPyX0GZ2vSvf3eP7zn1/2wmM/QbaxZ3Q1N7YUe+hDHzqzM42F4yP7bE3PTimsx7FLDts88WFCWHfddZsNN9ywbCGOX8KxkQW787AV/vrrr1/8s1swTywsXubW+Mu2K8MPT1SzPe4oT9JiH+hnEwE2RFhvvfVK2thVea211ir3s9/4Y5chtji3bZ14GEL6N9hggwKboBIO/+TX7meQVrbmst2KfLkyEFhclmf+dLryyivP+GHXH7bx99aMxUEeeJJM+qwMzQ/5JZzfh9LAn/n1YSLU7fe///3m0ksvLRugHnXUUWWLMa7PP//8sgO43Rv/1DPbvZEf6g4oX/7Ij1WGH86UCVvfs68eZU07WHvttWftJEN+iZuvBlA2xMUZJWk7+TB4PupRjyobilJvlB3xUI6kwTb5pU34uiFftD/uz5k6ICznuJEK5ccTe86xrFZZZZWSRssrkH828fDh2TXb71Zl8VB/pJH+g4Iwd8A/4chr24YVXXU3V5YbCyveB2XBPmcXXnhhc+SRR5b99Hjsae4vfvGLy/bgu+2224wM2N6IreM32WST0qnZUJINJE844YTmxBNPLDviIiMclUODYxdkOoVtKc6ZrcGR276HNCpe+2DLJK844S1veUtz3HHHlW30vZzGz16IKNwddthhRkbjP/TQQ0u66IR0QPZupPzZew4/a6yxRumkW265ZQnHriSLFy9ujjjiiOaggw5qDj/88OaUU04pezPalvNWfjR+nvhedNFFxUpBZu40VDZ4Jb3kh/goj1NPPbVcWxmzNRgPYXwnMCg3yo+ypPOanI6w3377lT3rSKsPMwpsI0V62OqdzsgGvXR8OhjxX3311c3Tnva04pcOT16pW9JvW72TJ3aO3myzzYo/lAXv+bBDM9vGvfvd72723XffUsf7779/GUQot0WLFs20B+KjPdD+UKDUB8oRxfeOd7yjbAfPVvTsdk4Y6o+2s/rqq5fdf6hLv1cf22DxniMyzuwdSD7ZMgv/5o9ypB1Tf7wL6ZUZdbjVVluVcJQ/eeW+7ItIGlGG+EMZUya0I+Kz8MDAxJZhJ510UlHs3o0ZDeVO/ikT7zYfTExhWWeYFvEee++9d+mM/IubkYbKY/dhsziwHmiUF1xwwcx25lg/dGIqEoVHJ6eRoNzOO++85owzzijKBcsNZcYoxNcp2LF3jz32aJ773Oc2m266abPxxhuXDsfGmOypSIUzerJ9PrI999xzJp34Jw0cxGVyRif2TCSNbFlPw2f0xQ2lQBr4KgY7BqPQ6ET8c52Rn7CM8Jdffnmz++67lzA0aGSEY2SngXKwPyN5tfvCLrvsUjrx6aefXjobo7G50ZnOPffc0tnIJzts0wFo+AwAdGz8HXbYYSU8VoKFNWhUKL0//OEPxTqxDoHCRsmxYzedx5QKZO3HZLHusYxQqNQZAw+DBUqMwYMB4Morr5zpmJQx5U/5Up7UB6Dor7rqqlKu+FtzzTVLe6IN0CbYk5BpJ50WhYMVjPJFkTA40k4oH+7PgEA8bJDLAEc7Q5ESHjkKlPtzX9oa7QrFRt3TZixfKFzaM3XMPoEcDFhYcH4PS5QG8R199NGlnmmXZv1S9tQ5gwL54J60CRQbSpP04o+6YHNT0hsVFm2JgYyDfTBNThkxGNIOaCNMv80Ci3U0LeassGC+EmvQOE8++eSyWy+dlIZPo6LBoDjMHw2UgmVTTCrlrW99a+ksVmkGaUeJYZXQaEzONI11kbPPPruMRD4MIzfW3U477VQUCI0Ki4hORMPfeuuti9I75phjyshLevFr4ZkykRZGSsx/RiyUgr8HsAElDZP4vZzOSd7YoNPLyScdiEaFUvf1hRvWCB2YDouCJH0HH3zwjB+mkYygTLFNhkVHepkmYz1SXoccckhJP/ETL4OCNV46D5bmmWeeWTonMtyOPfbYsr080y6sXMrJd8SItanYvrCmiAvFRPq5RrFgJdMOqAcbpBgkGKQYWCy8wUDBvzaoP5Q2u0mjAOiIKLotttiixI9yRllhfaLoqSvaoI+LsrnmmmuK5eItHgYP6p56ohxNzqBEGaB4TWYwjScPWGOx3TF1RWmi7BiUqTviZ5qNOwMESpu+4cNRD7Rtaw8oziuuuKLUBb+xlmy5h/rD8uRAGVocDNKUL3lnUGbXdfqfufs6mhYTUVgQG9UkifEyWjA6nnXWWaWRMXJg3nNQmZjk+GOkYFGWEY6RhgLGIokbOhI/SgHFwtqDyfFHeBQTDRKZjUY0KtZQdtxxxyJjtGVUpKPTSWgMdKK99tqrmNek0ywvGg8W27XXXlsaAWHpdCgKRkO7P5BXOiIjr5ejsFivwVoyGSM7ivKSSy4pUxIz2a386HTsPEzZMd3BemLagXXKGh1+aNSkA2Vn8aKk6NAoMzoj+aUTo7C4p00jGfnplPhBEZxzzjkzU0KmiKw3MnhQHrQl0sHgYVNo4ieObM0NLB90VMqE8iedKHXywBb2BxxwQLGGLMzOO+9cLEFTwMSBgqJtMLhQHuSHDs+0G6sQS51pGGWIsiOvrBHSHlBYWL1+YAPqhwGTwTNaQyhu6tkrLCxTrE3aiMlIG5YZCoh6Ya3I5OaH+iWNpJX6o31w0A5QZkzHUWKnnXZaqQfySjgGHCwk6ov4GNRQsFinuNPOsRrJI+GwwDhom7hj9VHfxEv9sYM5Mw/Kr+1Vp2kwksJC68c1CzLvC3TaYMJiIWAqM1qz1TpmN9uYo3Q42HLb/NP4adwcnOOIBaSfKQqNwCoQ6Gw0eDoGIy5WFVYTyotRmCkZIzfhqUQsJCqanayZatHwUE4ol+OPP35m+kYcTCtolNyT0ZK1Djo4aeS+Vqasn7CGxXTQ0gVYZShSU1iEQVFykA7uydSJT1rbIEPZXXbZZcVqsLU68o2lwf3pxCgDLEo6JmVBnrFUSSv3Y8pIXLgznWFEZ2qN8mUNiPsC+ced0RqrjsZOPiyvQPkwBTNriA7MwfoRv8HKgbNdo1wI66ekdF7WbWgDKD5bTGdqQ/5IG0qXxW/KH8WCAkU54Q8FxLok1hPpQdmjpMySZIBCGRx44IHlPptvvnkpG4sPhUwZoHC8YkKRobCw7r0i4x5M6ygTfpM34sFqBuJHydh0Fz+0KwZVBirKimum7lg9KDHaBP7IKwoc64e6QFGhWKhXLHr8MFCg4G0gpl4Y0FGwWF0MBCgk4iRtDDT0AwYayoj2g4VMGaJEiWM+QKEuOQZ0EwwISKxpU2s88wkjIVYMysqmfshIC42EBkknYYQxK4vRm85OhfL6A09QkPv0c80oykI3lWtyRhoqnQaBNUeFAqMMvzG7aWT4xfQnbbawjtVBR+aaOKhgKpaORIdEaZi7QfporNttt125NzIUMWHjlBBLkNHZGgsWEhYD611Ma0gz6cRCoNNyXywRFopRIBYP5Yb5z33pXHRSRm3Ki45OZ+VMp6Mj2BohFhSWLfmkTClzyp57okhQRFgojPD77LNPUcx+PwDqjc5A58NKpTPzNUmsLqaT+GlrY6yjUSa0A1+GlBELyVjbWAbcAwVL57744otLXqwOyRPlZQ8ksMpQTihlyg8oO9LHoIiyoi0xTcSNzu/jIs8odW/dWZqwnuOUkHSz1kQ++E3c9C+sY8qatkV5khfaJuWDFYRCtnUlygc5gwzpwGpm4GL9jXyQJqtDFBjKDQVLWAZY6sgsxe233774497ERVjuheKmLBlgbEmDOqVsKRPaAIOXDTrThvxFvWQMCJhTZwmj4Noa1ySwuDnTWJn/2zTCQyHiDv4/j3RWLC1kPp3+mtGMeG3x0uA+jOIs7GOi+zNySwfWCY0wTjeB+1JuWFs0MBSGrf1EfzQ+0mFpw6IlrM8PkE7yZI/cqUjShPXBiMw11iRppdHRyFh8t2sfFw2P++JGmkgnvy088NsvoJN+4qMTklYUF37wSyeg41Im+GfwoGxMCRv8xh/55TfWKkqD6Yf3FyEcZcj9vdWCnLhIA/XJvckrC9xWHlZ/XBOHDWDUI2m0sgPLB+VBHoGyon358uVMOOrKysPShH/uY1Mxk5Nu0mUWImmlPhl0rQ6JG7gf+cAv+TPr0bCw3Id80DYoG8JaOlHc1JnVAVMr6svaO+mknvBr/m02xZm4qU+7p0F6CEf46DYNRpoSgq11iBUP36EWAp5qsYZnSnEuTCKOmuhTN/hZ6DqcJuQNRRp1kpEK5/L+jBBtYEEwWtuUU4gIgxAPBqJOMlIhsCazImtyIURdMIVmdhd1kScVGiza2fyeOTURCiHEJGAtkPU49AtrZCwVRB0USYUeHvHy+JXH5Tx+BR6T8jSIM7+59iDnCZe5G97dzt7dwng3rjl7ucmM6ObdTe7jjm4+vLnBKOnxYaNb5p6lx4f14c3NyzyZO2eTZ2G9uw/T5ubxshgWLGx0tzxnYbi2cFlYzt4tc89knMHCejnpMbl38+Ez9+gWw2Tu3s1knL3cysfHacTwMax34zpzt3CZm8fcszbKtQ/DtbmBD2NuFt7L7cwrGTzB5u36qHsyUqEQQtRIKhRCiBpJhUIIUSOpUAghaiQVCiFEjaRCIYSokVQohBA1kgqFEKJGUqEQQtRIKhRCiBpJhUIIUSOpUAghaiQVCiFEjaRCIYSokVQohBA1kgqFEKJGUqEQQtRIKhRCiBpJhUIIUSOpUAghaiQVCiFEjaRCIYSokVQohBA1kgqFEKJGUqEQQtRIKhRCiBpJhUIIUSOpUAghaiQVCiFEjaRCIYSokVQohBA1kgqFEKJGUqEQQtRIKhRCiBpJhUIIUSOpUAghaiQVCiFEjaRCIYSokVQohBA1kgqFEKJGUqEQQtRIKhRCiBpJhUIIUSOpUAghaiQVCiFEjaRCIYSokVQohBA1kgqFEKJGUqEQQtRIKhRCiBpJhUIIUSOpUAghaiQVCiFEjaRCIYSokVQohBA1kgqFEKJGUqEQQtRIKhRCiBpJhUIIUSOpUAghaiQVCiFEjaRCIYSokVQohBA1kgqFEKJGUqEQQtRIKhRCiBpJhUIIUSOpUAghaiQVCiFEjaRCIYSokVQohBA1kgqFEKJGUqEQQtRIKhRCiBpJhUIIUSOpUAghaiQVCiFEjaRCIYSokVQohBA1kgqFEKJGUqEQQtRIKhRCiBpJhUIIUSOpUAghaiQVCiFEjaRCIYSokVQohBA1kgqFEKJGUqEQQtRHc53/AuP+PmqNG5GCAAAAAElFTkSuQmCC';
      logo = base64String;
    }
    var date = '';
    if($('#checkboxDate')[0].checked){
      date = $('#pinfo_date').html();
    }
    var number = '';
    if($('#checkboxProjectNumber')[0].checked){
      number = $('#pinfo_number').html()
    }
    var tableStyle = '';
    if($('#radioCrte')[0].checked)
      tableStyle = 'lightHorizontalLines';
    if($('#radioBrez')[0].checked)
      tableStyle = 'noBorders';
    //$('#checkboxLogo')[0].checked
    var pdfTable = {margin:[0,5,0,15],table:{widths:[30,'*',50,'*'], body: []},layout:'noBorders'};
    var index = 0;
    var row = [];
    var worker;
    var imgManeger = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjEuNWRHWFIAAAbaSURBVHhe7ZtraFxFFMf3lc1uXptkqyFxY9ISbZpKUGOL7/iqWB9oxUhVfFENUgwaJNhvUURUrApaQVRoJS1WrFqs8fFBaSkWqVqqVj+U+kEtFTEKUVs1TRt/Z9i77E1Okn3Mpndl//DnP3fu3JlzZs7Mnb33rq+EEk4MEolEFGkUxuPxatQv+f9X+CORyMV+v//pQCCwC/2NvONwIsnj5I3C3cFg8Hl4OXkBWPwIh8MrcOw7ko6zGZFrDtBZt5Au2sgIM5IbUNXBTEknvNPQ0FBJuqgQwvD3UNWpbEldO9AILA4w8o8gqjO5kjpfQL2P8vLyVubvGEnVkVxJncfKyso6SXsbjNSziOpEvqQTXkM9DWz0H0RVB/Ildf/Z0dERJu1NxGKx+YhqvC1Go9ElqDeR3MCohtsid4SbUc9iOVQNt8hboWdR8A4gAm5HvQn2+ysR1XBb5DZ7P+pNcJ++DVENt0UiwLsdAAreAdCbHVBZWdnAffp9kprR1kgbO5gGcrv1FgjNQUQ12ja53b6IegsYtR5RDbZNomAb6i1gVN6//TMlbb2LegsY9SiiGmybRNs61FtgYboEUQ22TTpgBeo5BFkIv0dVo22RSPsFLYfeAxuhuxHVcFsMhUL9qGdBEAS2o6rx+ZLR/xINQe+ioqKiEUN/JKk6kSupcyQcDp9G2vvA0EXIOFSdyYWE/rlo8YAR+xhRncmWTKuv0OICUXA9ojqULZOLa9HBb2NBpI6v0SAsPrA5WsBU+J2k6txs5NojjP5ZpIsPg4ODoe7u7hCdcKk4Qpbq5AwcYxrd2N/fH5V6OPY+cDogjgvluKenx4RuJBK5gE44RFJzdAolatjuXkXa19vbWyYqdUpa2pBjL8HvGCdpyYjFYrXJFxjmnORVV1fHceolnPuXw+kcP0aZTdFoNMGx6cCkw/6qqqqTJE+OpU4nX/JOCCYZYkDILsSJ5+Ao9+3zJc8ZQQfy5IhyD+DkB+g+Frk9lH+b9ACRcmqymIETQTjfTpnDdM7LbLRSa4K0TZmwU26u4JdGReWgrq4uhhOrMHAnh6mvPuiAh+S8dJJoOqbJmxLaTjkWwjuRVKTQ3hfwvpqamno5PzEx4U+uE4WPCCfUcbCbEdmA439xnDLOIflb0Ww6YNo86noF0dr4G76OHcs4lmgIFiwaMCbQ0tISoed7aXTWT14o8ytqRnWyc42NjfOYAk3yu6G5uXlxIpE4o7W1tSV52iDdEdrcj6jtOKS9AwzKg+3t7XGxlTx76OrqKpMXEjTyM4eqARoJ3TPRKeuAGIu4yiY7LIXkNPPV1tZKx7jKzkTqkR9NA21tbXaeGVBZu8w5kmqDM5HrBtDUtJG0gHyZHq6yGP4JmoITAXTiKsRVNhNS3z4bmyjZxPyBqo3MRq6Vb3sM0qcBoboGcZVlHj+JGkwK/ymdlQWPMIA3oNkDI5fiwGGSWsWZcpzb3smoqwOYTlcgrrIYehNq4JRlrajAhlx2kSly/RidK6/tM4fcXrjwB5JqpdmQjlyNCpzbp49bVgSjlsNrhDh/dWdnp/kUrq+vT+aumS6EsHwLoNabDfFlhEX3FNKZAaM2I2pl2ZIQ3o0aOIthU1NTHKO2Q/lqdBdldsbj8SY553SSgHxrr9po50N0dhCeck9VK8mV3O7ORg0kvGXUSbrK4Owd6XcL7GhFrD5VItJ60Bkhv+E/R9UKciUOD6EGssCBtSQnl1mfvvhx/AziKpMv8U32E9NvlJivFyLqxfmQ8DvKiC4gbcCxTIvJZeSdggF7/3kcqzvMfEnHXofq4KS65bRBHJKXpwaMxJQXKJwfQc3oYEfBvjWk7S2oDoz4CVEvtMBxIuwcVH2BwjmzaWKeno4d/6Sfs0nqHkVdW3MD+ZmKqBfZIr0voS+jHMCQvU4+adkam9WfMh85+YUinbwQdYPf4uch6gU2yUg/jMoqf5mTh0HyBHlOXq0Jae9a1A0yZcuoXmCTEt7OQw3m+hAjbj52qK+vX5QMT/U6m6TNu1A36ABZHdULbBNH98uDFHnMJU+B5P9D5O3RyhaCagfIcziMyGvfnQ0xYhg1z/WIhI3p5wpJfBxnqi0mPRXMz4swTDZC6X9oKhhxfDN8VTtXCOL8XiJddqEzgwVqPh2xkgseg2+S/mYuoyNfYqs8KvsWvgUfF1/wqY1zeUH++tZMRcuosJeRe4LK34CfwoPwKGVUg2yTtsbhIfgZ3IIta4ne1eiV2ChPkOw+EssE8lRWFjQMWSq3NTrpHjiAgdJR6+AQ3AqHMVT2Ay6SJ84Mc802dBPH8g7hKY7XwHulTupeIoMgj+i4poQSSighT/h8/wGpetzYEv9Z0gAAAABJRU5ErkJggg==';
    var imgKuka = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjEuNWRHWFIAAAfmSURBVHhe7ZtrbBRVFMd3u+1uu9vdPti2WgmFAqKYSrBA2TZajaEmIqKxRTFFkGgRpAQi8sFIKiREa4VQo5JqI37xg40aiDGKktRIfCtIoiKJzxgeGhAReUml/s7uTJndvbPvLbuk/+TPOXfmzuz5n/uYe2eKZQQjGMEIRjCMqKioKPd4PLPcbvf9LpdrBVwO55eWls6sqqrK16pdWvB6vZfbbLYnrFbrd3CQQ0py7mxOTs6O/Pz8exsbG3M5lv1wOBxLEXUCVynajFyzJy8vrwY/e4GATRilwFhIEk7SGxrxsw+M8RUYpbB4SBL+LC4ursLPHjQ0NIzJzc09hasUFS+ZP/qw2YOioqL1GKWYRMjk+B9zyVj8zEdHR4edgD/DVYpJlPQoGVKZj4ULF+aTgH9xlUISJT0gO4ZBeXl5BUYpIhnyRPkAm/loamq6CqMUkQyZCLMjASxn05IAu93+MTbzUVlZeSVGKSIZsiD6Gpv5qKurq8UoRSRDekBWJCDX6XS+gVWKSIayn2AiXICfuSgrK6vHKAWkgiThe2zmorCwcC5GGXyKeAhmLpioGjCqwFNCesBP2MyFz+crZxV4DlcpIFlm/KZo1apVBUxUb+EqBSRLngQt2MzF1q1b891ut0yE56FSRKKk9ffX1tbm4Wc2Nm7cWMDO7UVcpZAEOeByuW7EDj8GBwetbHFzhOL39fXZxGqnw0C93Pr6ejdD4ROKKjFxk6fLY9jhh4gR4VoxCHLOLBGSJNkVMiEOUFSKipUk8l3s8COSQB09PT2mY5Lr7QS/HVcpLFZetInPrOWNiFSnu7vb4fF4psvrLIpKcdHIxPdNS0uLDX/4IT1Ac00hXV1zlWhvb3fQCzbjKgVGoqwnCgoKZuJfPESa8Pr7+6MOEbm+tbXVxVPhPYpKoSY8X1FRsQZ78SEihdLdjb52OipkbTB58uRCxvJrFFVig0jLn/F6vaYvQKkk39ZyxBrK/mP+CpmCUaNGTWdvsIYhsK64uHhBc3NzGc/ye1jP7+M08QZT5grqvj1p0qRpMu559N3gcDge4Jp5M0pLPVSywSHhKmjndUbsnemCjXE7h9beiU8MF4i4o6wS19fV1Y2lzgyGxSPU2yJzBGKXjR49eoI8VUQ4k99uuYYm3Wu3WObgxw1JAByeRJSUlFyDkC5a8SDFIdEqUucUifiiqKjoKRLxZFlZ2Qan07kW0S+RFNnvy8Ryhhl4Nb4TJvUU4Ef9PUIrpg60mGx8FtGlZcWXsrU/4g8jfga+i2S9A+UNkwMmBW6emiQwO8usvgYeoRgmIBkS4SG6/ET8EoR/BKXHCN/nWCGMF/Iov1pjHj+SXBJkfNNVD+CGBZ8saflTLCuvw78MwXs14QPwvOZ/yrkJ8A54F5QPMGaQcb+aa45o1wqPov7RjkBS4gfC12FSvs3VSR9/GCuYS7BnobwFljfMD+JLInQhOk/Dz0NJfXlx+jR+aH0/bVbLM/xgfD2B7r4WExZ0qkg00rrG2fpaaNxnPGQQcRQeNpSNlLdR07B6wvZTvh3Owd+nHRvwWCzj5aYxgV3dnXIhbtpI68/CRoKeAPmbAy904h/Qjhn5A+fu08v4s6GOWw3HW/knei+ora310vp/4A4Fm2oShSyMoqFVC15a+GYom6zjuiADf+Ncs17GXwR1LDIcb+Gf6AlgBfcCZijYdJB+L3NLEHi0LsGMC5QsxGzZrAcfhTJH1WH15PxDWf5OaZP42rHjlEv4ccmEcdgFY+rUqZUEcho3KOBUk+5/E3YIOTmWxXrw+CuxL2tl4c9Qfyp8RXWE+cXpdht8k+Pzseegfp1O6UHz5XcEBGDeC1ityUosLOBUs5JtA9aIPAJ9NSRwqdsNJeASKPNAKOTcLq3+FvwbsZKQ3wO0yMsZn1TUwU3VCZB9PK0f7xY2bsqzH6tCDgE/r4mRujy+o2KFXl/jr3ADx++GypUkN1YPAba7hSRAxkpQwGngMWgKkiB/bNUeKEXEeMTqYzyInJNteHxoamqSFVdosCkn6T+DTRbSWz5UiYfyBCsPVAsHQaiHgM/nm44JCzgdrGLpi00GyxXC/aQXz9PqKEEA6gTU1NRUY8KCTQcZnLdgE0U1Qk+ECg/Q8rpWxxQEoE5AT0+PkwVQWv7QIZRs9juxiYCub+0PFx4g5+UpZgoqmK8D5J2ffOLiUXgbiejlhrvpTic5NRR4qkgT/IJVt0RkLA0VHUJZBMlcpgQ/Hvk35eVnX1+fXRJBQuzy5be6unqiw+GYRVKW2e32Z+UdHv4+dopJJcce2N7Gg3EI/DtEcBip91ygejg4ab4KjAR5REpS5G2vJKitrS1PEjRlypQr5EUoiZlNb1lMYh4nMbKEfQVu59guuIdjP3LuIMk7gj1GQv9yFhTsiiMgbmfdKQKjUNYYyh7AbyXS46JDeo0soqS3SFJkLuns7HT39va6JXHQ2d3d7ZF62hcfv+iuri7Xl7F/+m5TiA0jyVZ+UJVEpy0BySKGwKoQF7Xrw2+py8gKR8aKF2itYzYU0GXdYRBpRvkWeX3gkgvQ7p254nUoApX/MiPb4yV065VYP2WnGFo2+NT1X+N/b5g14o2QgPMtljG0qOrFR0zkNj65T9aJN0AmPLa0lm34srX1M+CHlgO+XpdV0vbCwOvw7Act6O/CkVozljqXDDSR/slSrE7/ybhgsfwPQ7z78GosWxcAAAAASUVORK5CYII=';
    var imgPlc = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjEuNWRHWFIAAAeiSURBVHhe7ZtriJRVGMdnbzO7szuz67qX2qQV0QzLzDZdd4WsD26BSkQaFYoVImgqiOGHSLxAqWyGG13YXLIvfWihDxJSRmUhlGVoQmT3C5FamRalaZn2+w/vsbOvZ97Zua0zsX/48zznvO+ZPc9znnPOc87MhoYxjGEMYxhDiObm5qZ4PD4zFovdX11dvQIug/fU19dPa21trfRe+3+hoaHh8rKysnUlJSWfwPNUOcmzM6WlpbsqKyvvnTFjRjl1xY9IJLIEo35HdRqdjLQ5UFFRMRG9eIEBTyCcBg6GOOEk0TADvfjAHF+BcBqWDnHC8bq6ulb04sH06dOvLC8vP4XqNCpdsn70I4sHtbW1GxBOYzIhi+M/rCWj0Qsfa9euDdPh91GdxmRKIkpTqvCxcOHCShzwF6rTkExJBBTHNGhqampGOI3IhuwobyMLH11dXVcjnEZkQxbC4nAA6WxeHBAOh99FFj5aWlquQjiNyIYkRB8hCx/t7e1tCKcR2ZAIKAoHlEej0ZeRTiOyoc4TLIQL0AsXjY2NnQinAbkgTvgUWbioqam5HeHsfI54BBYuWKimI1wdzwmJgK+RhYuOjo4mssC/UZ0GZMuCPxStXLmyioXqFVSnAdmSnWAesnCxffv2ylgspoXwHHQakSkZ/c/a2toq0AsbW7ZsqeLk9hyq05AMeba6uvpm5NDj/PnzJRxxS0Xp/f39ZZLe44vAe+WdnZ0xpsJ7FF3GpE12l4eRQw8ZI8O94gDoWTJHyEk6FbIgnqXoNGqwxJGvIYceQQYa9Pb2Jp2TtA/T+R2oTsMGy0u28CUbeRtB7/T09ETi8fgUXWdRdBqXiix8H8+bN68MfeihCPDUpFCoe6oTy5cvjxAFW1GdBgZR+URVVdU09EuHoAVv9+7dKaeI2s+fP7+aXeF1ik5Dk/Bcc3PzauSlh4wUFe627j1OCeUGEyZMqGEuv0TRZewAMvKnGxoakl6A8pK+WyuVtMqJusQLhYKRI0dO4Wywmimwvq6ubsHcuXMb2cvvJp8/xGP6O5BaK3h35/jx42/UvGfruykSiSyizV1T6+vjvFQGLxjugvfcMDA684Uy5u0cRvsNdPrwHzHuF7LEDe3t7aN5ZyrTYhXvPas1AmOXjho1aqx2FRnO4rdfbRjSg+FQaA562pADhswJI0aMuAZDuhnFwxQHGO4n75zCEftqa2s34YiNjY2Nj0aj0TUYvQ2n6LyvheU0K/BD6Kl2ABkYFPol7KGJyPHKuQMjpoPPfYS0Mr6c5f5YdBTjp6K7jFOdXS+pcjLk3nBWZ63qq+Exik4jMiW9PULIj0M3RsZgHNZ6rIEGSsJUp+eG9g8spJt25nl2hynNb0L1B1SnAdkQi0/RuxvQzShvh7pbUDpteBougXLEPuh/fgLqUkZX80e9OptfwEaYPjB8PSLnx1zDSCj0IFKGa95Lfq56B9+Ck3x1Nh+Dy3x1NmfB9EC4r0G4PiwnJNb3ImW0PWc1Wnp+HD4OTUotB1zn6eJB+KJV3gjlTFPWBY19Mk3PAZzq7mDlNo3zQkZ/JlIw4S+YCPgZroXGAbvh9Z4ufgC3WWVFgO0AJWDvWOWEA1DM30mOtra2Bkb/J1TTOOdkyJUY2TAdMxFwBr7p6aIcYEeA5noib/Dod4C21u+tciKvQLGjzQ0yuGcQpmFeiLVaWwxMpyRNBHwLZ0MtYir7p8BOuMoqb4L2GqAfazxvlfVZJn1OHgWTJ09uYY//E9U0zAsJ/1uQ6oiMNh2StBdB+/cGfgf4n/sjQI6zj+SaAglHU0geBWRrysRMo7yxhWMD0g854Eto3nva0v0O0NbXZ5X9DngVJjJMj6kdoHM8o5/uETZtYqV+SCWYkbdhR8BTlu53gHaJoEUwyAGuv5u4DKnBAb+hmkb5okbPhhkR/xSwHaBF0M4D/A7wb4MuBwSjq6trLMI0yBuxUpmdDeMASdsB31i6PwKUDX5nlf0R8CP8wypfcAAF9xTo6OiYgjAN8srWUOgypIE6ZMLSbIN+ygFBmaAiICgT1DaYOGVScDtg4sSJYxD+hnkhu8CtSAO7Q3YE2PQnQn76I8BPbYOJv0PB7YDe3t4oCVBefujgJ0OxGWlgOqQoCIoA/zZoM1UEJByAkjwP0J2fvuJiK5yNI/pIhfezKJ7kkf1BOSEWK9GxDZcuGRQBqQ5DQREwuExQl5/9/f1hOQKHhPXN75gxY8ZFIpGZOGVpOBx+Und46Ic4KWblnHAodCfSQMYHOSBVBAxmCgjBDkgGbZFyim575aDFixdXyEGTJk26QhehOGYW0fIAjnkEx2wlel6AO6jbAw9Q9xXPDuO8Y8gTOPTXaFXVHnpmh6M6F+SAbCIg9TaYKRQ1SqIULXKK1pLNmzfH+vr6YnIcjPb09MT1nveNT8Lo7u7u6g8v/uo72RqQ7RQwi6Dt8MIAvbPDMl9TIH8RkGNkOgUGkwcUBTKNAH8q7GfiOAwLHskcsAte66uzuQ4u8tXZvA0WBZZC3eXtZbh0Zyjqakv/RKUFVAckU2+oyxH9f5GO2Pptgv+5LkaK79/xGDZlbRe+9/OqB8D3jkK8KMI8I1hGGsODr7eSIhT6F1TwlILk0JxkAAAAAElFTkSuQmCC';
    var imgMechanic = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjEuNWRHWFIAAAjwSURBVHhe7Zp9jFxVGcbvzs7O7M7szG63+yHrhsXyUVKstWzbZbeGoklXDRBCbI0lhSKYotg2aTD9A20qRKObdQ1LVLK4UAJRyhoTCtYUMZakBJQCxcRQwCDE1OJHaetHSyml9ffcuWc5c/fMbGdndjtD9kmevO/5uDPv895zzj33w5vBDGYwgxlMI9ra2lrT6fTyVCr15WQyuQGug6uampou6+zsrA26fbjQ3Nx8TnV19berqqpehqepcpK2dyORyBO1tbXXLVu2LEpd5SMej38NUf/FdYrORY7ZW1NTMx+/coGAH2KcAs+EJOEoo2EZfuWBOb4B4xRWCEnCocbGxk78ysHSpUvPjUajx3Cdogol68cotnLQ0NBwJ8YpZjJkcXyfteQ8/PLHli1bYgT8B1ynmMmSEaUpVf5Ys2ZNLQk4gesUMlkyAipjGrS2trZhnCKKIVeUp7Dlj76+vosxThHFkIWwMhLAdnZKEhCLxZ7Blj/a29svwjhFFEM2RC9hyx/d3d1dGKeIYsgIqIgERBOJxC+xThHFUPcTLITX45cvWlpaejFOAaUgSXgFW76or6+/BuMMvkR8C5YvWKiWYlyBl4SMgL9gyxc9PT2t7ALfw3UKKJZlf1O0cePGOhaqx3GdAoolV4KV2PLF1q1ba1OplBbCU9ApYrLk7L/a1dVVg1/eGBwcrOPO7V5cp5BJ8mQymbwCO/04ffp0Fbe4EVH+6OhotWzQPA70i/b29qaYCs9SdIkpmFxdbsdOPyRGwoNiFtSWKxFKku4KWRBPUnSKOlOSyJ3Y6Uc+gQbDw8M55yTHxwh+O65T2JnyrC18uc68jXx9hoaG4ul0erEeZ1F0ipuILHx/WrlyZTX+9EMjIHBzQkM9cJ1Yv359nFFwF65TYD5qP1FXV3cZ/tlDvgVv165dE04RHb969eokV4XfUHQKzcFTbW1tm7BnHxIparjbftA8IbQ3mDdvXj1z+RGKLrFZ5Mwfb25uzvkAlE56txaRtcp+nd+hXDB79uzF3BtsYgrc0djYeP2KFStauJZ/if38PpqJN5taK+i7Y+7cuYs077n0XR6Px7/CMV9c0tSUplM1HBPuQtBumHd0ThWqmbdXc7Z/i08MHxBxb7NLvLO7u/s8+ixhWtxGv3u0RiD21o6Ojgt0VZFwFr8XdQyn9I8xz7sav2AoAdOWhFmzZl2CkAHO4gGKWcLDpM8xErGnoaHh+yTiey0tLd9NJBKbEf1TkqL7fS0sx1mBv4E/0RVAAvMN/Squof7ICcqlA2dMNz43MqS14yvZ3h9Ff0f8EnyXONXZ9bIq50LphbM6a1XfBA9SdIqYLIn2LYb8hfhh8d9k5Oip8OpM0Yfa8wk07fkSVBg0vxmqf8N1CiiGRHmMbeWl+ArYDjqC+Ocg08d7iLJpD/ezoXqTnHz9zhwIvwNT8ttcw7jnfR3bDn8OPwEV9LlwA+LfySSgan8k4t1CXTOUwCuoux+bhDbMmTfCZU1CCgfDfTPGGXgpSGS/x6LF2xEIfTfw3wvK/4L6vOZwUD5G+07sqaB8N8cb2EJNIgwKTwJ3ddfqT3CnjJz95VjhEv7rf4EoiXtAddCIkIBF1D8W9BE1JVv91kw/W7CBET7Wxh+7+mWjq6urmbP/T9xxQZeKRKaN0RgQ9CTUVLsZhgOXNXVbggSMBGW15RJlt/nH8+fmd3KDHdxPMOOCLiWJSmuLQU8g6h58W4wd7Fg9/X4F9RBWa4X6aN+gmzjjy5pjjfWTwZ/n3ygtXLiwnWv8O7hZAZeaDP9PY/V9oL4I26YEYM+HfqDQBC7YvvCZTMI8rQF6KctVNAu2QNv3f4c/Cv/eB2C3pp1YVrBTQZb92Yg4kRHi8wj1YeGCSYiNpHWc+FpQL+j48E7S/Gb+BOg+nrNf6C1swUSNPqTSUD5kifiP6hxwBZuyjhN1DyGYBIYTZsomAeH2DLjdrScB/8YdF3SJeRgKCqQDASMSgq+VPwwFbUQZ/3NB/+9AfZkiqF28hTZNjUWqLAh9fX0XYOxAp4REeRxr45MErcufdnwTIUq/p6B+4xxYD02CNKpegYyKrEU2Af12gSDUfzx6enoWY8YFPBXs9LyPYA207f21Ase/LVPlhMTfFQj8MeWP4f8Z7sDXR5XL8U9m2v0v1vTRRh/+G/A+fP9hLn/iTsD8+fPnYLICnSpyFfgs1uBSAjTbXlGv2fSprHn6XAc/T/3TVp9/MF2/ijW7QlntFJ/APgxfgO9D038/v6GttAJwJ2B4eDjBBmhKPnQIk2W6H+uD4H4RBMlu0HswY/0yW+Oqt6HZGh+k/WemzKEDlAcDX+8RtDO0xX2KtgPwZDTq6dWd/8RIzDSHoGd+esXFpfAqEqGF6UWyfJQmP+hSkijfxJpg0/yXFi6zD2iA11F3FEr4G5SvhZrLgha4H0Bd/4dofxWrUSLoeFtgL+3aYV6uAn/uPvsGevg5OjoaUyJISExvfufMmXNhPB5fTlJujcVid+sZHv4+7hSLSg7RfwFrYAfuB0ngzwQJ2OrXZmAS9Cj1WvD0Fir8JNmI9H8T6LZao2oPFfqct3DoEqmk6GmvErR27doaJWjBggUf1YNQEnMlo+UmEvMtEqOF6gG4nbrdcC91r9N2gOQdxB4moUcSdXW7yYQRLdhnR77m+DaseT7oC4J6/abE+KR8gxoDmD6GysB2q7/uQEsLjRptojRalBStJf39/amRkZGUEgcTQ0NDafUL3vj4gQ0MDCSfH//qO5wEU5b1jwMa+poStyPoTfgjvzaDLPFAV46/YndyItZhe/zacgKn0BYtGLEu8WGsQ6B2lguhOcb0rUL0Zto1TToyVZUDiTCUKCPMiBPiiNsDNbS1u9SLFe0MNao+Tp2/wxQjEU8PdyoKtnhbtKC6Gs6utr1mbv8O7g987Qm08msRfT2o0x2uvfkqe4ytFYF1jQDV9ZOIVYGv+e7vIfD1VEngztEbxF6ZKVYG9FIjfNZt5GxD/LNBAjZmaioYSoIWSMOgOguhPkqMbui6YZPaP1SwRBrhPv3GguB5/wcCk414nJ4A5AAAAABJRU5ErkJggg==';
    var imgElectrican = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjEuNWRHWFIAAAhgSURBVHhe7ZtpbFRVFMeHLjPtTGdaShctBJCAKIYQrFAKBkSlJgIiEVARgqIhioDBhQ9GwmIUCEGtJpJqk2riB230AzHEXTS4iyCGiBi3uIAaBERBVAR//8lcvX1zZ7rMTJkh/Sf/nvPeve/NO+du59z36utBD3rQgx50I6qrq6sikcikcDh8QygUWgIXwWvLy8vHDBgwoChW7fRCRUXFmfn5+St79er1KTzJKScp+zMvL++loqKi2RMmTCjgXO4jEAjcglG/oTqNTkSu2VFYWDgcPXeBAQ8gnAZ2hDjhCL1hAnrugTG+BOE0rDPECQfKysoGoOcOxo0b17+goOAoqtOozpL5oxWZOygtLV2NcBrTFTI5/sNcMhA9+7FixQo/D/w+qtOYrpIepSGV/Zg3b14RDvgL1WlIV0kPyI1hUFVVVY1wGpEKWVHeQGY/GhoazkE4jUiFTIS54QDC2Yw4wO/3v4PMftTU1JyNcBqRCgmIPkZmP+rq6moRTiNSIT0gJxxQEAwGn0M6jUiFyieYCOeiZy8qKyvHIpwGpIM44TNk9qKkpGQawvnwaeI+mL1gohqHcD14WkgP+AqZvaivr68iCvwb1WlAqsz6pGjp0qXFTFTPozoNSJWsBDOR2YuWlpaicDisifAEdBrRVdL6e2prawvRsxsbNmwoJnN7DNVpSBd5PBQKXYTsfpw8ebIXKW6eKL21tTVfMlYcB+oVjB07NsxQeJdDlzGdJqvL3cjuh4yR4bHDNlBZIkfIScoKmRCPc+g0qqPEkS8iux/JDDRoampKOCa53s/Db0J1GtZRnrKJL1HL20hWp7GxMRCJREZpO4tDp3HtkYlv18yZM/PRux/qATE1IdTVY6oTixcvDtALHkJ1GpiMiieKi4vHoJ86JJvwtmzZ0u4Q0fVz5swJsSq8zKHT0AQ8UV1dvQx56iEjRXV3W48VtwvFBsOGDSthLD/DocvYNqTlj1VUVCTcAKWS3q3lSVrH0XPRCtmCPn36jCI3WMYQWFVWVjZ3xowZlazl1xDP76aY521LzRXU3Tx06NALNO5Z+sYHAoGbuGbW6PLyCJXy4X+GuxArN0zaOzOFfMbtVFr7VXSe4X9i3C9Eiavr6uoGUmc0w+IO6m3UHIGxC/v16zdYq4oMZ/Lbrmto0p1+n28qenu4GF5uk8lpsoh+LswsevfufR6GrKcV93LYxnAvqXMUR3xYWlq6FkesqaysvC8YDC7H6MdxivJ9TSzHmIHvRE80yap1y2F/qHeHzt+Ch3HgUJT0Dw9aTInP9XRpRXxpi/2x7EeMH40uI70PrnB4I/wa6jdnw53QeS84H6YXzM6a1ZfB/Ry6frTLxNp9tNgQdK/x4+F70K6/Fd7oOWdTqbMcFoTpgcY3XfUHVNcPpkQsPkpYeT66jBcF/OF7EJpg6md4PxwBS2GiIfcNvAQeg59D3Tc1YPgqRNrTXMOAz3crUjDG63MZ5QEq1ybLGhiCBmth3H2g6l4I6+Ce2LnfoRzSNdDdlyO8P5Q20tfVvY3hgvRnocoPw0nQQGV6U5zo9ftKaBCGZoPmCOx8TyCrm87M7f2RtJLWtw0UboMqk5HaYDHOMfJpGHcf+Db0hvBK2DZDlX8BS6AObIe7UVtbW0Hra9zZP5JW0voKjGycAdXqKl+oE8B2gCZE11A8ABN9SdIbfgtV716dQPGuMPEggnsUYf9I2olFmltsaJJT2Q5oHtKWH8C4+8AWOAVOh1dABUHapu8LBS2ZqncIhlGiIbQKnBg5cmQNa/wfqOYHMkK6/0SkgQw0LaW3QHpAu6XmwLh7JOEL0Hx7qKDKvrcU+95tQbSmSMy+WUZYQ9qANBgGdV4fWGgCs6EV4HsYd48E1ArijQEaocqe1AGK2wHK42n9zqawnSbNq0nOhnZ/VLYretQWnVmJXoEy3tvF1fIq36YDFPcQIN0twQG/oto3zQQPQhs3Q51XMmVDM7sCIu02G26B3vuJr8FE0Z9WG9X5LnqUCA0NDYMR3hunnbhfkZoNBUMq68jXIHEZJ5TxdrDkbeEGqHqaC6S4h0B9ff0ohH3jjJF1S8uewZVQ5xXBJYNSZO+9Xoe28YLXwAVQdRUvSHE7YPjw4YMQ9s0zRlaBy5AGZ0GdV+xfpRMOaDiYENdQPaY944VmqPpNOkBxO6CpqSlIAJSRDx28ZG1ah7QR3ROAi6NH8VBwZN9DWaHXeMFrnJZDBUu6Zhp/EscB2vPTKy6Wwik4oplQeDuTomJp86NpI0+pzM1+2LugyvQqnA7SBsoA7cj0LehdLg1knG2gHKprlM0Wobhb30Cbn62trX45Aof49eZ30KBBQwKBwCScstDv9z+sPTz03WSKKTmHnPcqpIEM+gmqTBmgDfUWc52+GovAZDBGKnnSiqPrFukEirv124OWSDlFu71y0IIFCwrloBEjRvTVRiiOmUxvmY9j7sExD9F7noCbOLcV7uDcl5TtxXn7kQdx6KFgcfFWzwOZeEBzwdU6AbT9ZTJAZY/qDclgeoDq6QMrXadPeL3JUvqgXqMgSr1FTtFcsm7dunBzc3NYjoPBxsbGiOrF3vhEjV6/fn1oW/yrb/O/BsrvlQA9FTuWEe0ZL+jeavnoBivU5zXZ/cE1T2mPSxmg/T/t+mgzQxmgEqGOGK+dJAVVptsr8FGYnZPQh5dvQoWvl0LtBcyCyvJshyn602aqPtXXxCrDRcUHJiPMSWg++AjqE1wzORpqq0sJklYG7x6BYoXrYHSY5SrUlbW7q3cAAsmj73aoTE9G22+eNUF+Ah+BCnlz2nCDiayJdrpsQwaq2yuUVp3TwmAnaN7oy0/D2Ok28NQ5fZ0h2EZKGkYLOwWf71/HDu2zWmXy5gAAAABJRU5ErkJggg==';
    var imgBuilder = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjEuNWRHWFIAAAk5SURBVHhe7ZsJjF1VGcens7w382btdBYZKsUKVou11qEdplWqho6JliixJS4lBcUqaDF1adRAKkQidVLDYCKMjClGTXTiRpTELdbYxF0qEUFUXKIsEjaj1KJI/f0e97zeeT3vdZY30zdm/sk/3zn3nvvmfN/5zne+c+6dmgUsYAELWMAcore3t6etrW1Ta2vrJc3NzVfAd8E3dnZ2nrNs2bLGpNn/F7q6uk6pq6v78KJFi+6CR7kUJfeerK2t/VZjY+ObNm7cWM+1+Y9sNnsZSv2DYlTpUuSZQw0NDasoz1+gwMcRUQUnQ4zwBN6wkfL8A3P8CkRUsakQIzza0dGxjPL8wYYNG06rr68/TDGq1FRJ/BhHzh+0t7dfg4gqMx0SHP9LLDmdcvVjz549GTr8E4pRZaZLPMopVf3Yvn17Iwb4N8WoItMlHjA/pkFPT08vIqrETMiK8n1k9WNoaOj5iKgSMyGBcH4YgHR2VgyQyWR+iKx+9PX1PQ8RVWImJCH6JbL6MTAw0I+IKjET4gHzwgD1uVzuy8ioEjOh+wkC4UWUqxfd3d3rEVEFKkGM8Btk9aKlpeW1iGjnK8QHYPWCQLUBEet4RYgH/AFZvRgcHOwhC/wPxagCM2XVb4p27drVRKD6OsWoAjMlK8FWZPVi//79ja2trQbCp2FUiemS0b+nv7+/gXJ1Y9++fU3s3D5FMarINPlUc3Pzy5Fzj6NHjy5ii1srLY+Pj9cpk9vHgXb169evb2Uq/IhqTJkpk9XlQ8i5h8qoeFKdAO+VMoRGcldIQHyKalSpyRJDfhM59yinYMDo6GjJOcnzGTp/K8WoYpPlSQt8pUY+jXJtRkZGsm1tbWs9zqIaVe5EJPDduXXr1jrKcw89ICmWhK6eFKPYuXNnFi+4nmJUwXI0n2hqajqH8slDuYB34MCBE04Rn9+2bVszq8K3qUYVLcGne3t7dyNPPlRS6u7pcnL7hDA3WLlyZQtz+YtUY8pOICN/pKurq+QBKI18t1arTNXz1/INqgVLlixZy95gN1Pg6o6Ojou2bNnSzVr+BvL5u7lNfyfSWEHb21asWHG2856l79xsNnspz1y4rrOzjUZ1sKB4DMn9wLLeOVuoY96ez2h/lzJ9OEaUe4Qs8ZqBgYHTabOOafFe2t1ojEDZy5cuXXqGq4qKE/xu9xmG9I5MTc35lKcMDQDnxhCLFy8+C0WGGcX7qRaUjpE2hzHEz9rb26/DEB/t7u6+NpfLXYXSN2MU9/sGliNE4PdRNsh6xngtvDSpTwn80bxHJNXKgRFz43MxLm3GV7HcH+UfRPl1lB25V8K/h3vQ06YsnAxeBL8A/wQ1rKtRD5wZiM5G9d3wYaoTOj9TMkwP4PJnUlb5C+C/oMbVA34Nbef0aoXl8Hp4BNred5ThRc2foR41PTi/cdX7KBY6XSmi8WHSypdQVvlLoOcKptK6vuiC4ZWb0noMb4U+p+E+CM1nOmBYhR6CL4ZTA4pfjaj4NjcQv34nUrwH+nccQUdSgwS0wO9An7kLngad34EfgKGPO2Aa3r8Jes8pYX1ywN2vQhQ6W2nSkx8j7dBHkmt+UXIeDEh31u+Iwsmz89v3EBppb3It8AfQkfdZaRtlmEpnwxODXd0FRO70D1ecjP4m5LuTurHFICgMZF+FnfnaMW9wNfg0fAR6AHMz9NnHoV6TX0bhIei7yjTy23TmxUufqZZBf39/F6PvnCl0ttJkSEyMhOd9XjPgCQ9YH4Vec1oIDRDoaC6HX4K2eRCGud0O9QCv/xb6bYHPvCO59lB/TU0OWR5kcJ9E+MCskV4ZW8S5MHxVMgafSMpXQpVtgulPZdLx4I/Q1UPoHSpr+3A++Ve4Lyk/CTdTyKfQlONYs2ZNH2u8y5APzRpx/1cgA14GdeNw38Co8mFETbRWwiUwrAjO6aVQZaTtQ9kV4LMw/J5GfTXMgwu2jYNszUwsPDhr7HtGmTR0Y93Z+85zmuTnsvWD8CwYApnBM/18UCgYwQMbf8O2TienVQFcjBvAfTyjP9Ut7JRJD3X5GHRn3dp2YVrcBlfDcF33dxoEFCvjFPgKtK1vlAyoE8ANjXQ82O62YIB0CjpbfAyWwqnwTmi7z0NXhuAZBr50SlysiJli2JDdC58LJ4+hoaEzEKGTs0Z6bbJTDrr3+6EfS4bY4JJXvClKG8AM8afQthrwBTB6dkmD+BQYHBxciyh0dDZJWH8Wshw2wzANTHaKkVbCYGiGaFu/KDFe3AGdPsctezSKG2DVqlWur4VOzibx41chS2EbdE9gehs7InPkw+ibEZoZ+rsepRsfTILSAdTMsAAuxg0wOjqaIwGalQ8diokvx0ZVeCym4ukNUTGC8m6i/gb9TbfAbCoLyqWTIr8yyXscldJ5gGd+vuJiKdyMIcZIhW8nKIbEpKKkl45a8UiYHHlfA2zxQgSh88aHELBvhC6j7hfS3uGK8A1om9/B51CIj36Ah5/j4+MZDYFBMr75Xb58+ZnZbHYTRrk8k8nc4Bke5bvZKc7IOAyX+XvA22H6/i3QhKYYKudxWYgPptBOp3/CA9DAl1bS+uegbe+j8kIvThkukRrF014NtGPHjgYNtHr16lM9CMUwr8Fb3oJhrsQw1+M9t8BbuXYQHuLavdy7H+M9jHwMgz6ea2o6SK/CaOnCdvJ7MByx+YYp/d8kKub3QiE+uFe4EJrm+iJmJ0x7QIDP3QD9zbAHqRz0GpMovUWjGEv27t3bOjY21qrhYG5kZKTNdskbn3wHh4eHm39+7NW3mxc7aBpsMP59UndU26BKuHNUcQ1wMdRrVNyTnzdDYQD0EMQzgzTeBv09jVtdYEk0SquYHQzb1VOgS5nXfgE/lpTdp7wOPhuG/0gJX5C4+7sHes1UOMADE685bQp7gmqCO0I76Gimz/0Ww/TrdoNeehNllhjOKD8D/5KUw4qgpw0n10yoqva/UMKhiGcQuvZ10PnvtAgvWb2nwgbGNP3/oqC4HIVOMxk2RC6VLplVC0cvKFBMp4aGKHeqq+uHGGL2p+cUH59VNX4F7aybHleBTxDxLkM6NYq3zaVg8AtHYiE2mB6bJlc9PN87TlE0CK+5Sr7lSbch9TOYfg262TItLnWEPj+RKJpfQhPFyx9vlURNzf8Auxv0kegactIAAAAASUVORK5CYII=';
    var img;
    for(var i=0;i<allWorkersTable.length; i++){
      if(allWorkersTable[i].workRole == 'vodja' && $('#checkboxVodja')[0].checked){
        img = imgManeger;
        worker = [
          {width:25,margin:[0,0,0,0],image: img},
          {text: allWorkersTable[i].name + " " + allWorkersTable[i].surname, italics: true}
        ];
        row.push(worker[0], worker[1]);
        index++;
      }
      else if(allWorkersTable[i].workRole == 'kuka' && $('#checkboxKuka')[0].checked){
        img = imgKuka;
        worker = [
          {width:25,margin:[0,0,0,0],image: img},
          {text: allWorkersTable[i].name + " " + allWorkersTable[i].surname, italics: true}
        ];
        row.push(worker[0], worker[1]);
        index++;
      }
      else if(allWorkersTable[i].workRole == 'plc' && $('#checkboxPlc')[0].checked){
        img = imgPlc;
        worker = [
          {width:25,margin:[0,0,0,0],image: img},
          {text: allWorkersTable[i].name + " " + allWorkersTable[i].surname, italics: true}
        ];
        row.push(worker[0], worker[1]);
        index++; 
      }
      else if(allWorkersTable[i].workRole == 'strojnik' && $('#checkboxStro')[0].checked){
        img = imgMechanic;
        worker = [
          {width:25,margin:[0,0,0,0],image: img},
          {text: allWorkersTable[i].name + " " + allWorkersTable[i].surname, italics: true}
        ];
        row.push(worker[0], worker[1]);
        index++;
      }
      else if(allWorkersTable[i].workRole == 'električar' && $('#checkboxElek')[0].checked){
        img = imgElectrican;
        worker = [
          {width:25,margin:[0,0,0,0],image: img},
          {text: allWorkersTable[i].name + " " + allWorkersTable[i].surname, italics: true}
        ];
        row.push(worker[0], worker[1]);
        index++;
      }
      else if((allWorkersTable[i].workRole == 'konstruktor' || allWorkersTable[i].workRole == 'konstrukter') && $('#checkboxKons')[0].checked){
        img = imgBuilder;
        worker = [
          {width:25,margin:[0,0,0,0],image: img},
          {text: allWorkersTable[i].name + " " + allWorkersTable[i].surname, italics: true}
        ];
        row.push(worker[0], worker[1]);
        index++;
      }
      if(index != 0 && ((index)%2) == 0 && row.length != 0){
        pdfTable.table.body.push(row);
        row = [];
      }
    }
    if(index%2 != 0){
      worker = [
        {text: '', italics: true},
        {text: '', italics: true}
      ];
      row.push(worker[0], worker[1]);
      pdfTable.table.body.push(row);
    }
    if(pdfTable.table.body.length == 0){
      pdfTable = {text: ''};
    }
    var labelNabava = {text: 'Nabava', margin: [20, 0, 0, 0], bold: true, fontSize: 13};
    var pdfTableNabava = {style: 'tableCategory',layout: tableStyle ,table:{widths:['*','auto','auto','auto',/* 'auto'*/ ], body: [
      [
        {fillColor: '#cccccc', text: 'Ime'},
        {fillColor: '#cccccc', text: 'Št. dni'},
        {fillColor: '#cccccc', text: 'Začetek'},
        {fillColor: '#cccccc', text: 'Konec'},
        //{fillColor: '#cccccc', text: 'Dokončano'},
      ]
    ]}};
    var labelKonst = {text: 'Konstrukcija', margin: [20, 0, 0, 0], bold: true, fontSize: 13};
    var pdfTableKonstr = {style: 'tableCategory',layout: tableStyle ,table:{widths:['*','auto','auto','auto',/* 'auto'*/], body: [
      [
        {fillColor: '#cccccc', text: 'Ime'},
        {fillColor: '#cccccc', text: 'Št. dni'},
        {fillColor: '#cccccc', text: 'Začetek'},
        {fillColor: '#cccccc', text: 'Konec'},
        //{fillColor: '#cccccc', text: 'Dokončano'},
      ]
    ]}};
    var labelStrIzd = {text: 'Strojna Izdelava', margin: [20, 0, 0, 0], bold: true, fontSize: 13};
    var pdfTableStrIzdel = {style: 'tableCategory',layout: tableStyle ,table:{widths:['*','auto','auto','auto',/* 'auto'*/], body: [
      [
        {fillColor: '#cccccc', text: 'Ime'},
        {fillColor: '#cccccc', text: 'Št. dni'},
        {fillColor: '#cccccc', text: 'Začetek'},
        {fillColor: '#cccccc', text: 'Konec'},
        //{fillColor: '#cccccc', text: 'Dokončano'},
      ]
    ]}};
    var labelEleIzd = {text: 'Elektro Izdelava', margin: [20, 0, 0, 0], bold: true, fontSize: 13};
    var pdfTableEleIzdel = {style: 'tableCategory',layout: tableStyle ,table:{widths:['*','auto','auto','auto',/* 'auto'*/], body: [
      [
        {fillColor: '#cccccc', text: 'Ime'},
        {fillColor: '#cccccc', text: 'Št. dni'},
        {fillColor: '#cccccc', text: 'Začetek'},
        {fillColor: '#cccccc', text: 'Konec'},
        //{fillColor: '#cccccc', text: 'Dokončano'},
      ]
    ]}};
    var labelMontaza = {text: 'Montaža', margin: [20, 0, 0, 0], bold: true, fontSize: 13};
    var pdfTableMontaza = {style: 'tableCategory',layout: tableStyle ,table:{widths:['*','auto','auto','auto',/* 'auto'*/], body: [
      [
        {fillColor: '#cccccc', text: 'Ime'},
        {fillColor: '#cccccc', text: 'Št. dni'},
        {fillColor: '#cccccc', text: 'Začetek'},
        {fillColor: '#cccccc', text: 'Konec'},
        //{fillColor: '#cccccc', text: 'Dokončano'},
      ]
    ]}};
    var labelProg = {text: 'Programiranje', margin: [20, 0, 0, 0], bold: true, fontSize: 13};
    var pdfTableProgram = {style: 'tableCategory',layout: tableStyle ,table:{widths:['*','auto','auto','auto',/* 'auto'*/], body: [
      [
        {fillColor: '#cccccc', text: 'Ime'},
        {fillColor: '#cccccc', text: 'Št. dni'},
        {fillColor: '#cccccc', text: 'Začetek'},
        {fillColor: '#cccccc', text: 'Konec'},
        //{fillColor: '#cccccc', text: 'Dokončano'},
      ]
    ]}};
    var labelBrez = {text: 'Brez', margin: [20, 0, 0, 0], bold: true, fontSize: 13};
    var pdfTableBrez = {style: 'tableCategory',layout: tableStyle ,table:{widths:['*','auto','auto','auto',/* 'auto'*/], body: [
      [
        {fillColor: '#cccccc', text: 'Ime'},
        {fillColor: '#cccccc', text: 'Št. dni'},
        {fillColor: '#cccccc', text: 'Začetek'},
        {fillColor: '#cccccc', text: 'Konec'},
        //{fillColor: '#cccccc', text: 'Dokončano'},
      ]
    ]}};
    for(var j = 0; j < projectTasks.length; j++){
      //debugger;
      var startDate = '/';
      var finishDate = '/';
      var currentDuration = '/'
      if(projectTasks[j].task_start)
        startDate = projectTasks[j].task_start.split('T')[0].split('-')[2] + '.' + projectTasks[j].task_start.split('T')[0].split('-')[1] + '.' + projectTasks[j].task_start.split('T')[0].split('-')[0];
      if(projectTasks[j].task_finish)
        finishDate = projectTasks[j].task_finish.split('T')[0].split('-')[2] + '.' + projectTasks[j].task_finish.split('T')[0].split('-')[1] + '.' + projectTasks[j].task_finish.split('T')[0].split('-')[0];
      if(projectTasks[j].task_duration)
        currentDuration = projectTasks[j].task_duration;
      var currentTask = [
        {text: projectTasks[j].task_name},
        {text: currentDuration, noWrap: true},
        {text: startDate, noWrap: true},
        {text: finishDate, noWrap: true},
        //{text: projectTasks[j].completion + '%', noWrap: true}
      ];
      if(projectTasks[j].category == 'Nabava'){
        pdfTableNabava.table.body.push(currentTask);
      }
      else if(projectTasks[j].category == 'Konstrukcija'){
        pdfTableKonstr.table.body.push(currentTask);
      }
      else if(projectTasks[j].category == 'Strojna izdelava'){
        pdfTableStrIzdel.table.body.push(currentTask);
      }
      else if(projectTasks[j].category == 'Elektro izdelava'){
        pdfTableEleIzdel.table.body.push(currentTask);
      }
      else if(projectTasks[j].category == 'Montaža'){
        pdfTableMontaza.table.body.push(currentTask);
      }
      else if(projectTasks[j].category == 'Programiranje'){
        pdfTableProgram.table.body.push(currentTask)
      }
      else if(projectTasks[j].category == 'Brez'){
        pdfTableBrez.table.body.push(currentTask);
      }
    }
    if(!$('#checkboxNabava')[0].checked){
      labelNabava = {text: ''};
      pdfTableNabava = {text: ''};
    }
    if(!$('#checkboxKonstrukcija')[0].checked){
      labelKonst = {text: ''};
      pdfTableKonstr = {text: ''};
    }
    if(!$('#checkboxStrIzd')[0].checked){
      labelStrIzd = {text: ''};
      pdfTableStrIzdel = {text: ''};
    }
    if(!$('#checkboxEleIzd')[0].checked){
      labelEleIzd = {text: ''};
      pdfTableEleIzdel = {text: ''};
    }
    if(!$('#checkboxMontaza')[0].checked){
      labelMontaza = {text: ''};
      pdfTableMontaza = {text: ''};
    }
    if(!$('#checkboxProg')[0].checked){
      labelProg = {text: ''};
      pdfTableProgram = {text: ''};
    }
    if(!$('#checkboxBrez')[0].checked){
      labelBrez = {text: ''};
      pdfTableBrez = {text: ''};
    }
    debugger;
    var docDefinition = {
      info: {
        title: $('#pinfo_name').html(),
        author: 'Roboteh-IS',
        subject: 'Info about project',
        keywords: $('#pinfo_name').html() + ", " + $('#pinfo_subscriber').html(),
      },
      content: [
        {
          width: 150,
          style: 'logo',
          image: logo
        },
        {
          style: 'info',
          stack: [
            $('#pinfo_name').html(),
            {
              style: 'additionalInfo',
              text: $('#pinfo_name').html() + '\n' + $('#pinfo_date').html() + '\n' + $('#pinfo_number').html()
            },
          ],
        },
        pdfTable,
        labelNabava,
        pdfTableNabava,
        labelKonst,
        pdfTableKonstr,
        labelStrIzd,
        pdfTableStrIzdel,
        labelEleIzd,
        pdfTableEleIzdel,
        labelMontaza,
        pdfTableMontaza,
        labelProg,
        pdfTableProgram,
        labelBrez,
        pdfTableBrez,
      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
          alignment: 'right',
          margin: [0, 190, 0, 80]
        },
        subheader: {
          fontSize: 14
        },
        superMargin: {
          margin: [20, 0, 40, 0],
          fontSize: 15
        },
        tableCategory: {
          margin: [0, 0, 0, 0]
        },
        info: {
          fontSize: 18,
          bold: true,
              margin: [180, marginForInfo, 0, 20],
        },
        additionalInfo: {
          fontSize: 13,
        },
        logo: {
          margin: [0, -40, 0, 0],
        }
      }
    };
    //pdfMake.createPdf(docDefinition).open();
    pdfMake.createPdf(docDefinition).download($('#pinfo_name').html()+'.pdf');
  });
}
//SMOOTH SCROLL ANIMATION TO THE JQUERY SELECTOR
function smoothScroll(selector, diff){
  if(!diff)
    diff = 0;
  $("html, body").animate({ scrollTop: $(selector).offset().top-diff }, 1000);
}
/*
function smoothScroll (selector) {
  document.querySelector(selector).scrollIntoView({
    behavior: 'smooth'
  });
}
*/
//FLASH ANIMATION WITH FADE OUT AND IN
function flashAnimation(element){
  $(element).delay(100).fadeOut().fadeIn().fadeOut().fadeIn().fadeOut().fadeIn()
}
var urlParams = new URLSearchParams(window.location.search);
var projectId = urlParams.get('id');
var taskLoaded = false;
//tables//
var allTasks;
var allSubtasks;
var allAbsence;
var allReasons;
var allWorkersTable;
//tables//
var shownTasks;
var loggedUser;
var categoryTask = 0;
var activeTask = 1;
var activePage = 1;
//0 = all, 1 = unfinished, 2 = finished
var completionTask = 1;
//for select2 --> (id, text)//
var allSubscribers;
var allWorkers;
var allCategories;
var allPriorities;
//for select2 --> (id, text)//
var editProjectCollapseOpen = false;
var editTaskCollapseOpen = false;
var subtaskCollapseOpen = false;
var taskFilesCollapseOpen = false;
var taskAbsenceCollapseOpen = false;
var activeTaskId = 0;