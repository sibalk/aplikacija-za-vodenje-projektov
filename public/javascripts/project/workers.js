//IN THIS SCRIPT//
////  ////

$(function(){
  //ADD WORKER TO THE PROJECT
  $('#addWorkerform').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var worker = userAdd.value;
    var roleNew = roleAdd.value;
    if(isNaN(roleNew)){
      debugger;
      //new role, create role and then add worker with new role
      $.post('/employees/addprojectrole', {roleNew}, function(data){
        var roleName = roleNew;
        roleNew = data.id.id;
        allRoles.push({id:roleNew, text:roleName});
        debugger;
        $.post('/projects/worker/add', {worker, roleNew, projectId}, function(resp){
          $('#modalAddWorker').modal('toggle');
          var selectedWorker = allUsers.find(u => u.id === parseInt(worker))
          debugger
          if(allWorkers){
            var tmp = allWorkers.find(w => w.id == selectedWorker.id);
            if(!tmp){
              allWorkers.push({id:selectedWorker.id, text:selectedWorker.text});
              $(".select2-workersAdd").select2({
                data: allWorkers,
                tags: false,
                multiple: true,
              });
            }
            if(allWorkersTable){
              allWorkersTable.push({id_wp:resp.id.id, id:selectedWorker.id, workRole:roleName, role:null, name:selectedWorker.text.split(" ")[0], surname:selectedWorker.text.split(" ")[1]});
            }
          }
          var tmpImg = 'worker64';
          if(roleName == 'strojnik')
            tmpImg = 'mechanic64';
          else if(roleName == 'električar')
            tmpImg = 'electrican64';
          else if(roleName == 'konstruktor' || roleName == 'konstrukter')
            tmpImg = 'builder64';
          else if(roleName == 'plc')
            tmpImg = 'plc64';
          else if(roleName == 'kuka')
            tmpImg = 'kuka64';
          else if(roleName == 'vodja')
            tmpImg = 'manager64';
          else if(roleName == 'nabavnik' || roleName == 'komercialist' || roleName == 'komercijalist' || roleName == 'tajnik' || roleName == 'tajnica')
            tmpImg = 'secratery64';
          //add to the list of workers on project
          var element = `<row class="list-group-item flex-column" id=projectWorker`+resp.id.id+`>
            <div class="row">
              <div class="col-12 col-sm-12 col-md-5 d-flex">
                <div class="mr-4" id="workerImg`+resp.id.id+`">
                  <img class="img-fluid" src="/`+tmpImg+`.png" alt="ikona" style="width:50px;">
                </div>
                <div class="ml-3 mt-1" id="workerInfo`+resp.id.id+`">
                  <div class="h5 mb-1 row" id="workerName`+resp.id.id+`">`+selectedWorker.text+`</div>
                </div>
              </div>
              <div class="col-4 col-sm-4 col-md-3 mt-1 text-center">
                <div class="h5" id="workerRole`+resp.id.id+`">`+roleName+`</div>
              </div>
              <div class="col-12 col-sm-8 col-md-4 d-flex">
                <div class="ml-auto mt-2" id="workerControl`+resp.id.id+`">
                  <button class="btn btn-danger" onclick="deleteWorker(`+resp.id.id+`)">
                    <img class="img-fluid" src="/remove.png" alt="ikona" style="width:15px" data-toggle="tooltip" data-original-title="Odstrani">
                  </button>
                  <a class="btn btn-programmer ml-3" href="/employees/absence?id=`+selectedWorker.id+`"> 
                    <img class="img-fluid" src="/absenceWhite.png" alt="ikona" style="width:15px" data-toggle="tooltip" data-original-title="Odsotnosti">
                  </a>
                  <a class="btn btn-info ml-3" href="/employees/tasks?userId=`+selectedWorker.id+`"> 
                    <img class="img-fluid" src="/pointer1.png" alt="ikona" style="width:15px" data-toggle="tooltip" data-original-title="Opravila">
                  </a>
                  <a class="btn btn-primary ml-3" href="/employees/userId?userId=`+selectedWorker.id+`"> 
                    <img class="img-fluid" src="/pointer2.png" alt="ikona" style="width:15px" data-toggle="tooltip" data-original-title="Projekti">
                  </a>
                </div>
              </div>
            </div>
          </row>`;
          $('#workersList').append(element);
          //if I ever add tableWorkers + redo
          /*
          tableWorkers.row.add({
            id_wp: resp.id.id, 
            name: selectedWorker.text.split(' ')[0], 
            surname: selectedWorker.text.split(' ')[1], 
            workRole: roleName,
            id: worker
          }).draw(false)
          */
        })
      })
    }
    else{
      debugger;
      $.post('/projects/worker/add', {worker, roleNew, projectId}, function(resp){
        $('#modalAddWorker').modal('toggle');
        var selectedRole = allRoles.find(r => r.id === parseInt(roleNew))
        var selectedWorker = allUsers.find(u => u.id === parseInt(worker))
        debugger
        if(allWorkers){
          var tmp = allWorkers.find(w => w.id == selectedWorker.id);
          if(!tmp){
            allWorkers.push({id:selectedWorker.id, text:selectedWorker.text});
            $(".select2-workersAdd").select2({
              data: allWorkers,
              tags: false,
              multiple: true,
            });
          }
          if(allWorkersTable){
            allWorkersTable.push({id_wp:resp.id.id, id:selectedWorker.id, workRole:selectedRole.text, role:null, name:selectedWorker.text.split(" ")[0], surname:selectedWorker.text.split(" ")[1]});
          }
        }
        var tmpImg = 'worker64';
        if(selectedRole.text == 'strojnik')
          tmpImg = 'mechanic64';
        else if(selectedRole.text == 'električar')
          tmpImg = 'electrican64';
        else if(selectedRole.text == 'konstruktor' || selectedRole.text == 'konstrukter')
          tmpImg = 'builder64';
        else if(selectedRole.text == 'plc')
          tmpImg = 'plc64';
        else if(selectedRole.text == 'kuka')
          tmpImg = 'kuka64';
        else if(selectedRole.text == 'vodja')
          tmpImg = 'manager64';
        else if(selectedRole.text == 'nabavnik' || selectedRole.text == 'komercialist' || selectedRole.text == 'komercijalist' || selectedRole.text == 'tajnik' || selectedRole.text == 'tajnica')
          tmpImg = 'secratery64';
        //add to the list of workers on project
        var element = `<row class="list-group-item flex-column" id=projectWorker`+resp.id.id+`>
          <div class="row">
            <div class="col-12 col-sm-12 col-md-5 d-flex">
              <div class="mr-4" id="workerImg`+resp.id.id+`">
                <img class="img-fluid" src="/`+tmpImg+`.png" alt="ikona" style="width:50px;">
              </div>
              <div class="ml-3 mt-1" id="workerInfo`+resp.id.id+`">
                <div class="h5 mb-1 row" id="workerName`+resp.id.id+`">`+selectedWorker.text+`</div>
              </div>
            </div>
            <div class="col-4 col-sm-4 col-md-3 mt-1 text-center">
              <div class="h5" id="workerRole`+resp.id.id+`">`+selectedRole.text+`</div>
            </div>
            <div class="col-12 col-sm-8 col-md-4 d-flex">
              <div class="ml-auto mt-2" id="workerControl`+resp.id.id+`">
                <button class="btn btn-danger" onclick="deleteWorker(`+resp.id.id+`)">
                  <img class="img-fluid" src="/remove.png" alt="ikona" style="width:15px" data-toggle="tooltip" data-original-title="Odstrani">
                </button>
                <a class="btn btn-programmer ml-3" href="/employees/absence?id=`+selectedWorker.id+`"> 
                  <img class="img-fluid" src="/absenceWhite.png" alt="ikona" style="width:15px" data-toggle="tooltip" data-original-title="Odsotnosti">
                </a>
                <a class="btn btn-info ml-3" href="/employees/tasks?userId=`+selectedWorker.id+`"> 
                  <img class="img-fluid" src="/pointer1.png" alt="ikona" style="width:15px" data-toggle="tooltip" data-original-title="Opravila">
                </a>
                <a class="btn btn-primary ml-3" href="/employees/userId?userId=`+selectedWorker.id+`"> 
                  <img class="img-fluid" src="/pointer2.png" alt="ikona" style="width:15px" data-toggle="tooltip" data-original-title="Projekti">
                </a>
              </div>
            </div>
          </div>
        </row>`;
        $('#workersList').append(element);
        //if I ever add tableWorkers + redo
        /*
        tableWorkers.row.add({
          id_wp: resp.id.id, 
          name: selectedWorker.text.split(' ')[0], 
          surname: selectedWorker.text.split(' ')[1], 
          workRole: selectedRole.text,
          id: worker
        }).draw(false)
        */
      })
    }
  })
})

//OPEN MODAL TO ADD WORKER TO PROJECT
function openWorkerModal(){
  debugger;
  if(!allUsers || !allRoles){
    $.get('/projects/users', function(resp){
      allUsers = resp.data
      debugger
      $(".select2-users").select2({
        data: allUsers,
        tags: false,
        dropdownParent: $('#modalAddWorker'),
      });
      $.get('/projects/roles', function(resp){
        allRoles = resp.data
        debugger
        $(".select2-roles").select2({
          data: allRoles,
          tags: false,
          dropdownParent: $('#modalAddWorker'),
        });
        $("#modalAddWorker").modal();
      })
    })
  }
  else{
    $("#modalAddWorker").modal();
  }
}
//REMOVE WORKER FROM PROJECT
function deleteWorker(id){
  debugger;
  //console.log(tableWorkers.row(id))
  $.post('/projects/worker/remove', {id}, function(resp){
    var workerName = $('#workerName'+id).html();
    debugger
    if(allWorkers){
      var deletingWorker = allWorkers.find(w => w.text == workerName);
      if(!allWorkersTable){
        $.get('/projects/worker/get', {projectId}, function(resp){
          allWorkersTable = resp.data;
          var tmp = allWorkersTable.find(w => w.id == deletingWorker.id);
          if(!tmp){
            allWorkers = allWorkers.filter(w => w.id != deletingWorker.id)
            $(".select2-workersAdd").select2({
              data: allWorkers,
              tags: false,
              multiple: true,
            });
          }
        })  
      }
      else{
        allWorkersTable = allWorkersTable.filter(w => w.id_wp != id);
        var tmp = allWorkersTable.find(w => w.id == deletingWorker.id);
        if(!tmp){
          allWorkers = allWorkers.filter(w => w.id != deletingWorker.id)
          $(".select2-workersAdd").select2({
            data: allWorkers,
            tags: false,
            multiple: true,
          });
        }
      }
    }
    $('#projectWorker'+id).remove();
  })
}
var allUsers;
var allRoles;