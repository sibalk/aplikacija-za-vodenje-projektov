//IN THIS SCRIPT//
////  ////

$(function(){
  //CHANGE NOTE OF THE TASK
  $('#taskNote-form').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var newNote = taskNoteInput.value;
    var taskId = taskIdNote.value;
    $.post('/projects/note', {taskId, newNote}, function(resp){
      debugger;
      $('#task'+taskId).find('.taskpopover').attr('data-content', newNote);
      $('#taskNoteInput').val("");
      $("#modalTaskNoteForm").modal('toggle');
    })
  })
})
function addTaskNote(id){
  $('#taskNoteInput').val("");
  $('#taskIdNote').val(id);
  $("#modalTaskNoteForm").modal();
  debugger;
}
function toggleTaskCheckbox(element){
  var taskId = parseInt(element.id.match(/\d+/g)[0]);
  debugger;
  var completion;
  if(element.checked){
    completion = 100;
  }
  else{
    completion = 0;
  }
  fixTaskCompletion(taskId, completion);
  debugger;
}
//FIX COMPLETION OF THE PROJECT, task percentage has changed
function fixCompletion(){
  $.post('completion', {projectId}, function(resp){
    if(resp.success == true)
      console.log("Successfully updated project percentage." );
    else if(resp.success == false)
      console.log("Unsuccessfully updated project percentage, there was no projectId" );
  })
}
//FIX Task percentage
function fixTaskCompletion(taskId, completion){
  $.post('/projects/task/completion', {taskId, completion}, function(resp){
    //fix subtask count
    if(allTasks){
      var task = allTasks.find(t=>t.id == taskId);
      task.completion = completion;
    }
    //fix task display
    $('#taskProgress'+taskId).width(completion+"%");
    //fix procentage of this project overall (so it can be correct value in list of all projects)
    fixCompletion();
  })
}
//ADD FINISHED DATE WHEN TASK WAS GIVEN 100 COMPLETION
function addFinishedDate(taskId){
  $.post('task/finished', {taskId}, function(resp){
    debugger;
  })
}
//OPEN EDIT TASK COLLAPSE
function editTask(taskId){
  //debugger;
  if(!allCategories || !allPriorities || !allWorkers){
    $.get( "/projects/workers", {projectId}, function( data ) {
      allWorkers = data.data;
      $.get( "/projects/categories", {projectId}, function( data ) {
        allCategories = data.data;
        $.get( "/projects/priorities", {projectId}, function( data ) {
          allPriorities = data.data;
          if(taskLoaded){
            makeEditTask();
            $('#collapseEditTask'+taskId).collapse("toggle");
            editTaskCollapseOpen = true;
          }
          else{
            $.get( "/projects/tasks", {projectId,categoryTask,activeTask, completionTask}, function( data ) {
              //console.log(data)
              //debugger
              allTasks = data.data;
              taskLoaded = true;
              makeEditTask();
              $('#collapseEditTask'+taskId).collapse("toggle");
              editTaskCollapseOpen = true;
            });
          }
        });
      });  
    });
  }
  else{
    if(taskLoaded){
      makeEditTask();
      $('#collapseEditTask'+taskId).collapse("toggle");
      editTaskCollapseOpen = true;
    }
    else{
      $.get( "/projects/tasks", {projectId,categoryTask,activeTask, completionTask}, function( data ) {
        //console.log(data)
        //debugger
        allTasks = data.data;
        taskLoaded = true;
        makeEditTask();
        $('#collapseEditTask'+taskId).collapse("toggle");
        editTaskCollapseOpen = true;
      });
    }
  }
}
//FILL EDIT TASK DATA (form)
function makeEditTask(){
  //debugger;
  var editTaskActivity = ''; 
  if(loggedUser.role == 'admin')
    editTaskActivity = '<div class="d-flex justify-content-center"><div class="custom-control custom-checkbox"><input class="custom-control-input" id="taskActiveEdit" type="checkbox" checked="" /><label class="custom-control-label" for="taskActiveEdit">Opravilo je aktivno</label></div></div>';
  var collapseElement = `<hr />
  <div class="form-row">
    <div class="col-md-6">
      <div class="form-group">
        <label for="taskNameEdit" style="width:100%;">Ime opravila
          <input class="form-control" id="taskNameEdit" type="text" name="taskNameEdit" required="" />
            <div class="invalid-feedback">Za dodajanje novega opravila je potrebno vnesti vsaj ime opravila!</div>
        </label>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label for="taskDurationEdit" style="width:100%;">Število dni
          <input class="form-control" id="taskDurationEdit" type="number" name="taskDurationEdit" min="0" disabled placeholder="Se izračuna samo po vnosu obeh datumov."/>
        </label>
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-6">
      <div class="form-group">
        <label for="taskStartEdit" style="width:100%;">Začetek opravila
          <input class="form-control" id="taskStartEdit" type="date" name="taskStartEdit" />
            <div class="valid-feedback">Začetek opravila je znotraj datuma projekta.</div>
            <div class="invalid-feedback">Začetek opravila je izven datuma projekta!</div>
        </label>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label for="taskFinishEdit" style="width:100%;">Konec opravila
          <input class="form-control" id="taskFinishEdit" type="date" name="taskFinishEdit" />
            <div class="valid-feedback">Konec opravila je znotraj datuma projekta.</div>
            <div class="invalid-feedback">Konec opravila je izven datuma projekta!</div>
        </label>
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-6">
      <div class="form-group">
        <label for="taskCompletionEdit" style="width:100%;">Dokončano
          <input class="form-control" id="taskCompletionEdit" type="number" name="taskCompletionEdit" min="0" max="100" />
        </label>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label for="taskAssignmentEdit" style="width:100%;">Dodeli opravilo
          <select class="custom-select mr-sm-2 form-control select2-workersEdit" id="taskAssignmentEdit" name="worker" style="width:100%;"></select>
        </label>
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-6">
      <div class="form-group">
        <label for="taskCategoryEdit" style="width:100%;">Kategorija
          <select class="custom-select mr-sm-2 form-control select2-taskCategoryEdit" id="taskCategoryEdit" name="worker" style="width:100%;"></select>
        </label>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label for="taskPriorityEdit" style="width:100%;">Prioriteta
          <select class="custom-select mr-sm-2 form-control select2-taskPriorityEdit" id="taskPriorityEdit" name="worker" style="width:100%;"></select>
        </label>
      </div>
    </div>
  </div>
  `+editTaskActivity+`
  <div class="text-center">
    <button class="btn btn-success" onclick="updateEditTask(0)">Posodobi</button>
  </div>`;
  $('#collapseEditTask'+activeTaskId).append(collapseElement);
  $('#taskStartEdit').on('change', onStartEditChange);
  $('#taskFinishEdit').on('change', onFinishEditChange);
  $('#taskNameEdit').on('input',function(e){
    if($('#taskNameEdit').val() == '')
      $('#taskNameEdit').addClass('is-invalid');
    else
      $('#taskNameEdit').removeClass('is-invalid');
  });
  $(".select2-workersEdit").select2({
    data: allWorkers,
    tags: false,
    multiple: true,
  });
  $(".select2-taskCategoryEdit").select2({
    data: allCategories,
    tags: false,
  });
  $(".select2-taskPriorityEdit").select2({
    data: allPriorities,
    tags: false,
  });  
  //$('#project_notes').val(project.notes);
  var x = allTasks.find(t => t.id == activeTaskId);
  $('#taskNameEdit').val(x.task_name);
  $('#taskDurationEdit').val(x.task_duration);
  if(x.task_start){
    $('#taskStartEdit').val(new Date(x.task_start).toISOString().split("T")[0]);
    onStartEditChange();
  }
  if(x.task_finish){
    $('#taskFinishEdit').val(new Date(x.task_finish).toISOString().split("T")[0]);
    onFinishEditChange();
  }
  $('#taskCompletionEdit').val(x.completion);
  if(x.workers)
    $('#taskAssignmentEdit').val(x.workers_id.split(',')).trigger('change');
  var c = allCategories.find(c => c.text == x.category);
  var p = allPriorities.find(p => p.text == x.priority);
  $('#taskCategoryEdit').val(c.id).trigger('change');
  $('#taskPriorityEdit').val(p.id).trigger('change');
  //activity of task
  if(x.active == true)
    $('#taskActiveEdit').prop("checked", true);
  else if(x.active == false){
    $('#taskActiveEdit').prop("checked", false);
    $('#collapseEditTask'+activeTaskId).removeClass('task-collapse');
    $('#collapseEditTask'+activeTaskId).addClass('task-collapse-deleted');
  }
}
function addNewTask(or){
  debugger;
  if(!collapseAddNewTaskOpen){
    if(!allCategories || !allPriorities || !allWorkers){
      $.get( "/projects/workers", {projectId}, function( data ) {
        allWorkers = data.data;
        $.get( "/projects/categories", {projectId}, function( data ) {
          allCategories = data.data;
          $.get( "/projects/priorities", {projectId}, function( data ) {
            allPriorities = data.data;
            $(".select2-workersAdd").select2({
              data: allWorkers,
              tags: false,
              multiple: true,
            });
            $(".select2-taskCategoryAdd").select2({
              data: allCategories,
              tags: false,
            });
            $(".select2-taskPriorityAdd").select2({
              data: allPriorities,
              tags: false,
            });  
            $('#collapseAddTask').collapse("toggle");
            collapseAddNewTaskOpen = true;
            if(!taskLoaded){
              $.get( "/projects/tasks", {projectId,categoryTask,activeTask, completionTask}, function( data ) {
                //console.log(data)
                //debugger
                allTasks = data.data;
                taskLoaded = true;
              });
            }
          });
        });  
      });
    }
    else{
      debugger;
      $(".select2-workersAdd").select2({
        data: allWorkers,
        tags: false,
        multiple: true,
      });
      $(".select2-taskCategoryAdd").select2({
        data: allCategories,
        tags: false,
      });
      $(".select2-taskPriorityAdd").select2({
        data: allPriorities,
        tags: false,
      });
      $('#collapseAddTask').collapse("toggle");
      collapseAddNewTaskOpen = true;
    }
  }
  else{
    //debugger;
    //first check if there are any empty fields that are not allowed
    if($('#taskNameAdd').val() == '')
      $('#taskNameAdd').addClass("is-invalid")
    else{
      var override = or;
      //otherwise add and while adding check if there are conflicts
      var taskName = $('#taskNameAdd').val();
      var taskDuration = $('#taskDurationAdd').val();
      var taskStart = $('#taskStartAdd').val();
      var taskFinish = $('#taskFinishAdd').val();
      var taskCompletion = $('#taskCompletionAdd').val();
      var taskAssignment = $('#taskAssignmentAdd').val();
      taskAssignment = taskAssignment.toString();
      var taskCategory = $('#taskCategoryAdd').val();
      var taskPriority = $('#taskPriorityAdd').val();
      if(!taskDuration)
        taskDuration = 0;
      if(!taskCompletion)
        taskCompletion = 0;
      else
        taskCompletion = parseInt(taskCompletion);
      if(taskStart)
        taskStart = taskStart + " 07:00:00.000000";
      if(taskFinish)
        taskFinish = taskFinish + " 15:00:00.000000";
      debugger;
      $.post('/projects/addtask', {projectId, taskName, taskDuration, taskStart, taskFinish, taskCompletion, taskAssignment, taskCategory, taskPriority, override}, function(resp){
  
        debugger;
        if(resp.conflicts){
          openConflictModal(resp.conflicts);
          //console.log(resp.conflicts);
        }
        if(override == 1){
          $('#modalConflicts').modal('toggle');
        }
        if(resp.success == true){
          $('#collapseAddTask').collapse("toggle");
          collapseAddNewTaskOpen = false;
          //categoryPriority should be created with loop or through database but since its always the same (for) i did in this way
          var categoryPriority = [{id:allCategories[1].id, text:allCategories[1].text},
          {id:allCategories[2].id, text:allCategories[2].text},
          {id:allCategories[3].id, text:allCategories[3].text},
          {id:allCategories[4].id, text:allCategories[4].text},
          {id:allCategories[5].id, text:allCategories[5].text},
          {id:allCategories[6].id, text:allCategories[6].text},
          {id:8, text:allCategories[0].text}];
          debugger;
          //put new task in "correct" index of allTasks
          var testTaskCategory = categoryPriority.find(cp => cp.text == allCategories.find(c => c.id == taskCategory).text).id;
          var st = 0;
          for(var i = 0; i < allTasks.length; i++){
            if(categoryPriority.find(c => c.text == allTasks[i].category).id < testTaskCategory){
              //not in right category yet, first need to get to correct category
              st++;
              debugger;
            }
            else if(categoryPriority.find(c => c.text == allTasks[i].category).id == testTaskCategory){
              //correct category, move so long as long there is date or no more dates
              if(allTasks[i].task_start && taskStart && (new Date(allTasks[i].task_start) <= new Date(taskStart)))
                st++;
              //if new task has no start put it after tasks with start
              if(allTasks[i].task_start && !taskStart)
                st++;
              debugger;
            }
            debugger;
          }
          debugger;
          //time
          var taskStartFormat = null;
          if(taskStart){
            taskStart = new Date(taskStart).toISOString();
            var sdate = taskStart.split('T')[0];
            var stime = taskStart.split('T')[1];
            taskStartFormat = {date:sdate.split('-')[2]+"."+sdate.split('-')[1]+"."+sdate.split('-')[0],
            time:stime.substring(0,5)};
          }
          else
            taskStart = null;
          var taskFinishFormat = null;
          if(taskFinish){
            taskFinish = new Date(taskFinish).toISOString();
            var fdate = taskFinish.split('T')[0];
            var ftime = taskFinish.split('T')[1];
            taskFinishFormat = {date:fdate.split('-')[2]+"."+fdate.split('-')[1]+"."+fdate.split('-')[0],
            time:ftime.substring(0,5)};
          }
          else
            taskFinish = null;
          if(!taskDuration)
            taskDuration = 0;
          //workers
          var taskWorkers = "";
          for(var i = 0; i < $('#taskAssignmentAdd').val().length; i++){
            if(i != 0)
              taskWorkers += ", ";
            taskWorkers += allWorkers.find(w => w.id == parseInt($('#taskAssignmentAdd').val()[i])).text;
          }
          if(!taskWorkers){
            taskWorkers = null;
            taskAssignment = null;
          }
          var newTask = {id:resp.newTaskId, task_name: taskName, task_duration: taskDuration, 
          task_start:taskStart, task_finish: taskFinish, active:true, 
          category:categoryPriority.find(c => c.id == testTaskCategory).text, completion:taskCompletion, 
          files_count:null, finished:null, 
          formatted_start:taskStartFormat, 
          formatted_finish:taskFinishFormat, 
          full_absence_count:null, task_absence_count:null, priority:allPriorities.find(p => p.id == taskPriority).text, 
          subtask_count:null, task_note:"", 
          workers: taskWorkers, workers_id: taskAssignment};
          //fit new task in i place in allTasks
          allTasks.splice(st,0,newTask);
          debugger;
          var myCurrentPage = activePage;
          var pageCounter = Math.ceil(allTasks.length/10);
          drawPagination(pageCounter);
          debugger;
          activePage = 1
          if(st == 0) st++;
          myCurrentPage = Math.ceil(st/10);
          drawTasks(myCurrentPage);
          debugger;
          $('#taskNameAdd').val("");
          $('#taskDurationAdd').val("");
          $('#taskStartAdd').val("");
          $('#taskStartAdd').removeClass('is-valid');
          $('#taskStartAdd').removeClass('is-invalid');
          $('#taskFinishAdd').val("");
          $('#taskFinishAdd').removeClass('is-valid');
          $('#taskFinishAdd').removeClass('is-invalid');
          $('#taskCompletionAdd').val("0");
          $('#taskAssignmentAdd').val(0).trigger('change');
          $('#taskCategoryAdd').val(1).trigger('change');
          $('#taskPriorityAdd').val(1).trigger('change');
          setTimeout(()=>{
            smoothScroll('#task'+resp.newTaskId, 100); flashAnimation('#task'+resp.newTaskId);
          })
        }
      })
      debugger;
    }
  }
}
/*
//when data is loaded and user wants to add new task
function addTask(projectId, taskName, taskDuration, taskStart, taskFinish, taskAssignment, taskCategory, taskPriority, override){
  //taskAssignmentArrayString
  $.post('/projects/addtask', {projectId, taskName, taskDuration, taskStart, taskFinish, taskAssignment, taskCategory, taskPriority, override}, function(resp){
    
    debugger;
    
  })
}
*/
//open conflicts modal and fill modal with new conflicts
function openConflictModal(conflicts){
  debugger;
  $('#conflictsBody').empty();
  for(var i = 0; i < conflicts.length; i++){
    if(conflicts[i].length > 0){
      var conflictWorker = conflicts[i][0].name + " " +conflicts[i][0].surname;
      var listElements = ``;
      for(var j = 0; j < conflicts[i].length; j++){
        listElements += `<li class="list-group-item">
          <div>
            <u>`+conflicts[i][j].project_name+`</u> na opravilu <u>`+conflicts[i][j].task_name+`</u> v času <u>`+new Date(conflicts[i][j].task_start).toLocaleDateString('sl')+` - `+new Date(conflicts[i][j].task_finish).toLocaleDateString('sl')+`</u>.
          </div>
        </li>`;
      }
      var element = `<div><strong>`+conflictWorker+`:</strong></div>
      <ul class="list-group mb-2">
        `+listElements+`
      </ul>`;
      $('#conflictsBody').append(element);
    }
  }
  $('#modalConflicts').modal();
}
function closeCollapseAddTask(){
  $('#collapseAddTask').collapse("toggle");
  collapseAddNewTaskOpen = false;
}
//update task from edit form
function updateEditTask(or){
  //debugger;
  //first check if there are any empty fields that are not allowed
  if($('#taskNameEdit').val() == '')
    $('#taskNameEdit').addClass("is-invalid")
  else{
    var override = or;
    var taskId = activeTaskId;
    var currentWorkers = allTasks.find(t => t.id == taskId).workers_id;
    //otherwise add and while adding check if there are conflicts
    var taskName = $('#taskNameEdit').val();
    var taskDuration = $('#taskDurationEdit').val();
    var taskStart = $('#taskStartEdit').val();
    var taskFinish = $('#taskFinishEdit').val();
    var taskCompletion = $('#taskCompletionEdit').val();
    var taskAssignment = $('#taskAssignmentEdit').val();
    taskAssignment = taskAssignment.toString();
    var taskCategory = $('#taskCategoryEdit').val();
    var taskPriority = $('#taskPriorityEdit').val();
    var taskActive;
    if(typeof $('#taskActiveEdit').prop('checked') === 'undefined') 
      taskActive = true
    else
      taskActive = $('#taskActiveEdit').prop('checked');
    if(!taskDuration)
      taskDuration = 0;
    if(!taskCompletion)
      taskCompletion = 0;
    else
      taskCompletion = parseInt(taskCompletion);
    if(taskStart)
      taskStart = taskStart + " 07:00:00.000000";
    if(taskFinish)
      taskFinish = taskFinish + " 15:00:00.000000";
    debugger;
    $.post('/projects/updatetask', {projectId,taskId, taskName, taskDuration, taskStart, taskFinish, taskCompletion, taskAssignment, currentWorkers, taskCategory, taskPriority, taskActive, override}, function(resp){

      debugger;
      if(resp.conflicts){
        openConflictEditModal(resp.conflicts);
        //console.log(resp.conflicts);
      }
      if(override == 1){
        $('#modalConflictsEdit').modal('toggle');
      }
      if(resp.success == true){
        //update task in allTask && task on page
        var x = allTasks.find(t => t.id == taskId);
        //name
        x.task_name = taskName;
        $('#taskName'+taskId).html(taskName);
        //date
        var dateString = "";
        var taskStartFormat = null;
        if(taskStart){
          taskStart = new Date(taskStart).toISOString();
          var sdate = taskStart.split('T')[0];
          var stime = taskStart.split('T')[1];
          taskStartFormat = {date:sdate.split('-')[2]+"."+sdate.split('-')[1]+"."+sdate.split('-')[0],
          time:stime.substring(0,5)};
          x.task_start = taskStart;
          x.formatted_start = taskStartFormat;
          dateString = taskStartFormat.date + " - ";
        }
        else{
          taskStart = null;
          x.task_start = taskStart;
          x.formatted_start = taskStartFormat;
          dateString = "/ - ";
        }
        var taskFinishFormat = null;
        if(taskFinish){
          taskFinish = new Date(taskFinish).toISOString();
          var fdate = taskFinish.split('T')[0];
          var ftime = taskFinish.split('T')[1];
          taskFinishFormat = {date:fdate.split('-')[2]+"."+fdate.split('-')[1]+"."+fdate.split('-')[0],
          time:ftime.substring(0,5)};
          x.task_finish = taskFinish;
          x.formatted_finish = taskFinishFormat;
          dateString += taskFinishFormat.date ;
        }
        else{
          taskFinish = null;
          x.task_finish = taskFinish;
          x.formatted_finish = taskFinishFormat;
          dateString += "/";
        }
        if(dateString == "/ - /") dateString = "";
        $('#taskDate'+taskId).html(dateString);
        //category
        debugger;
        $('#taskBadges'+taskId).empty();
        var badge = '';
        var taskBadge = '';
        if(taskCategory == "2"){
          badge = 'badge-purchase';
          taskBadge = 'Nabava';
          x.category = taskBadge;
        }
        else if(taskCategory == "3"){
          badge = 'badge-construction';
          taskBadge = 'Konstrukcija';
          x.category = taskBadge;
        }
        else if(taskCategory == "4"){
          badge = 'badge-mechanic';
          taskBadge = 'Strojna izdelava';
          x.category = taskBadge;
        }
        else if(taskCategory == "5"){
          badge = 'badge-electrican';
          taskBadge = 'Elektro izdelava';
          x.category = taskBadge;
        }
        else if(taskCategory == "6"){
          badge = 'badge-assembly';
          taskBadge = 'Montaža';
          x.category = taskBadge;
        }
        else if(taskCategory == "7"){
          badge = 'badge-programmer';
          taskBadge = 'Programiranje';
          x.category = taskBadge;
        }
        else{
          x.category = "Brez";
        }
        //priority
        var badgeElement = '<span class="badge badge-pill '+badge+' ml-2">'+taskBadge.toUpperCase()+'</span>';
        $('#taskBadges'+taskId).append(badgeElement);
        $('#task'+taskId).removeClass('border-low-priority');
        $('#task'+taskId).removeClass('border-medium-priority');
        $('#task'+taskId).removeClass('border-high-priority');
        if(taskPriority == "2"){
          $('#task'+taskId).addClass('border-low-priority');
          x.priority = 'nizka';
        }
        else if(taskPriority == "3"){
          $('#task'+taskId).addClass('border-medium-priority');
          x.priority = 'srednja';
        }
        else if(taskPriority == "4"){
          $('#task'+taskId).addClass('border-high-priority');
          x.priority = 'visoka';
        }
        else{
          x.priority = 'brez';
        }
        //workers
        var taskWorkers = "";
        for(var i = 0; i < $('#taskAssignmentEdit').val().length; i++){
          if(i != 0)
            taskWorkers += ", ";
          taskWorkers += allWorkers.find(w => w.id == parseInt($('#taskAssignmentEdit').val()[i])).text;
        }
        if(taskWorkers)
          $('#taskInfo'+taskId).find('.workerspopover').attr('data-content', taskWorkers);
        else
          $('#taskInfo'+taskId).find('.workerspopover').attr('data-content', " ");
        if(!taskWorkers){
          taskWorkers = null;
          taskAssignment = null;
        }
        x.workers_id = taskAssignment;
        x.workers = taskWorkers;
        x.task_duration = taskDuration;
        //absence button (if there is no start || finish || workers button is disabled)
        if(taskAssignment && taskStart && taskFinish)
          $('#taskButtonAbsence'+taskId).prop('disabled', false);
        else
          $('#taskButtonAbsence'+taskId).prop('disabled', true)
        //TODO x.active = taskActive
        x.active = taskActive;
        $('#task'+taskId).removeClass('bg-secondary');
        if(taskActive == false)
          $('#task'+taskId).addClass('bg-secondary');
        //completion and checkbox if 100 or opposite
        if(!x.subtask_count > 0){
          if(taskCompletion == 100)
            $('#customTaskCheck'+taskId).prop("checked", true);
          else
            $('#customTaskCheck'+taskId).prop("checked", false);
        }
        $('#taskProgress'+taskId).width(taskCompletion+"%");
        x.completion = taskCompletion;
        debugger;
        toggleCollapse(taskId, 0);
      }
    })
    debugger;
  }
}
//open conflicts modal and fill modal with new conflicts
function openConflictEditModal(conflicts){
  debugger;
  $('#conflictsEditBody').empty();
  for(var i = 0; i < conflicts.length; i++){
    if(conflicts[i].length > 0){
      var conflictWorker = conflicts[i][0].name + " " +conflicts[i][0].surname;
      var listElements = ``;
      for(var j = 0; j < conflicts[i].length; j++){
        listElements += `<li class="list-group-item">
          <div>
            <u>`+conflicts[i][j].project_name+`</u> na opravilu <u>`+conflicts[i][j].task_name+`</u> v času <u>`+new Date(conflicts[i][j].task_start).toLocaleDateString('sl')+` - `+new Date(conflicts[i][j].task_finish).toLocaleDateString('sl')+`</u>.
          </div>
        </li>`;
      }
      var element = `<div><strong>`+conflictWorker+`:</strong></div>
      <ul class="list-group mb-2">
        `+listElements+`
      </ul>`;
      $('#conflictsEditBody').append(element);
    }
  }
  $('#modalConflictsEdit').modal();
}
function deleteTask(taskId){
  debugger;
  //needs to make a call to server to actually delete task (change activity to false)
  $.post('/projects/removetask', {taskId}, function(resp){
    if(loggedUser.role == 'admin'){
      if(taskLoaded){
        allTasks.find(t => t.id == taskId).active = false
      }
      $('#task'+taskId).addClass('bg-secondary');
    }
    else{
      if(!taskLoaded){
        $.get( "/projects/tasks", {projectId,categoryTask,activeTask, completionTask}, function( data ) {
          //console.log(data)
          //debugger
          allTasks = data.data;
          taskLoaded = true;
          fixTaskTable(taskId);
        });
      }
      else{
        fixTaskTable(taskId);
      }
    }
    fixCompletion();
  })
}
//fix table list when user deletes a task
function fixTaskTable(taskId){
  $('#task'+taskId).remove();
  var myCurrentPage = activePage;
  allTasks = allTasks.filter(t => t.id != taskId);
  var pageCounter = Math.ceil(allTasks.length/10);
  drawPagination(pageCounter);
  activePage = 1
  if(myCurrentPage > pageCounter)
    myCurrentPage = pageCounter;
  drawTasks(myCurrentPage);
}
var collapseAddNewTaskOpen = false;
$('#taskNameAdd').on('input',function(e){
  if($('#taskNameAdd').val() == '')
    $('#taskNameAdd').addClass('is-invalid');
  else
    $('#taskNameAdd').removeClass('is-invalid');
});
//check if date for new task is ok
function onStartAddChange(){
  $('#taskStartAdd').removeClass('is-invalid');
  $('#taskStartAdd').removeClass('is-valid');
  $('#taskDurationAdd').val(0);
  if($('#taskStartAdd').val()){
    var newTaskStart = new Date($('#taskStartAdd').val()+" 9:00");
    var newTaskFinish = new Date($('#taskFinishAdd').val()+" 13:00");
    //first check if start is before finish otherwise start = finish
    if(isValidDate(newTaskStart) && isValidDate(newTaskFinish)){
      if(new Date($('#taskStartAdd').val()) >= new Date($('#taskFinishAdd').val())){
        $('#taskStartAdd').val($('#taskFinishAdd').val());
        newTaskStart = new Date($('#taskStartAdd').val()+" 9:00");
      }
    }
    //to get time from project date from html
    var s = $('#pinfo_date').html().split(' - ')[0].split(".");
    s = new Date(s[2]+"."+s[1]+"."+s[0]+" 07:00");
    
    var f = $('#pinfo_date').html().split(' - ')[1].split(".");
    f = new Date(f[2]+"."+f[1]+"."+f[0]+" 15:00");
    debugger;
    var hasValidClass = false;
    var hasInvalidClass = false;
    //check if start is after project start
    if(isValidDate(s)){
      if(newTaskStart < s){
        $('#taskStartAdd').addClass('is-invalid');
        hasInvalidClass = true;
        debugger;
      }
      else{
        $('#taskStartAdd').addClass('is-valid');
        hasValidClass = true;
        debugger;
      }
    }
    if(isValidDate(f)){
      if(newTaskStart > f){
        if(!hasInvalidClass){
          hasValidClass = false;
          $('#taskStartAdd').removeClass('is-valid');
          hasInvalidClass = true;
          $('#taskStartAdd').addClass('is-invalid');
          debugger;
        }
        else if(hasValidClass){
          $('#taskStartAdd').removeClass('is-valid');
          hasValidClass = false;
          hasInvalidClass = true;
          $('#taskStartAdd').addClass('is-invalid');
          debugger;
        }
      }
      else{
        if(!hasValidClass && !hasInvalidClass){
          hasValidClass = true;
          $('#taskStartAdd').addClass('is-valid');
          debugger;
        }
      }
    }
    if(isValidDate(newTaskStart) && isValidDate(newTaskFinish))
      countDays();
  }
}
//check if finish date of new task is ok
function onFinishAddChange(){
  $('#taskFinishAdd').removeClass('is-invalid');
  $('#taskFinishAdd').removeClass('is-valid');
  $('#taskDurationAdd').val(0);
  if($('#taskFinishAdd').val()){
    var newTaskStart = new Date($('#taskStartAdd').val()+" 9:00");
    var newTaskFinish = new Date($('#taskFinishAdd').val()+" 13:00");
    //first check if finish after start otherwise finish = start
    if(isValidDate(newTaskStart) && isValidDate(newTaskFinish)){
      if(new Date($('#taskStartAdd').val()) >= new Date($('#taskFinishAdd').val())){
        $('#taskFinishAdd').val($('#taskStartAdd').val());
        newTaskFinish = new Date($('#taskFinishAdd').val()+" 13:00");
      }
    }
    //to get time from project date from html
    var s = $('#pinfo_date').html().split(' - ')[0].split(".");
    s = new Date(s[2]+"."+s[1]+"."+s[0]+" 07:00");
    
    var f = $('#pinfo_date').html().split(' - ')[1].split(".");
    f = new Date(f[2]+"."+f[1]+"."+f[0]+" 15:00");
    debugger;
    var hasValidClass = false;
    var hasInvalidClass = false;
    //check if finish is after project start
    if(isValidDate(s)){
      if(newTaskFinish < s){
        $('#taskFinishAdd').addClass('is-invalid');
        hasInvalidClass = true;
        debugger;
      }
      else{
        $('#taskFinishAdd').addClass('is-valid');
        hasValidClass = true;
        debugger;
      }
    }
    if(isValidDate(f)){
      if(newTaskFinish > f){
        if(!hasInvalidClass){
          hasValidClass = false;
          $('#taskFinishAdd').removeClass('is-valid');
          hasInvalidClass = true;
          $('#taskFinishAdd').addClass('is-invalid');
          debugger;
        }
        else if(hasValidClass){
          $('#taskFinishAdd').remove('is-valid');
          hasValidClass = false;
          hasInvalidClass = true;
          $('#taskFinishAdd').addClass('is-invalid');
          debugger;
        }
      }
      else{
        if(!hasValidClass && !hasInvalidClass){
          hasValidClass = true;
          $('#taskFinishAdd').addClass('is-valid');
          debugger;
        }
      }
    }
    if(isValidDate(newTaskFinish) && isValidDate(newTaskStart))
      countDays();
  }
}
//check if edit date of selected task is ok
function onStartEditChange(){
  $('#taskStartEdit').removeClass('is-invalid');
  $('#taskStartEdit').removeClass('is-valid');
  $('#taskDurationEdit').val(0);
  if($('#taskStartEdit').val()){
    var newTaskStart = new Date($('#taskStartEdit').val()+" 9:00");
    var newTaskFinish = new Date($('#taskFinishEdit').val()+" 13:00");
    //first check if start is before finish otherwise start = finish
    if(isValidDate(newTaskStart) && isValidDate(newTaskFinish)){
      if(new Date($('#taskStartEdit').val()) >= new Date($('#taskFinishEdit').val())){
        $('#taskStartEdit').val($('#taskFinishEdit').val());
        newTaskStart = new Date($('#taskStartEdit').val()+" 9:00");
      }
    }
    //to get time from project date from html
    var s = $('#pinfo_date').html().split(' - ')[0].split(".");
    s = new Date(s[2]+"."+s[1]+"."+s[0]+" 07:00");
    
    var f = $('#pinfo_date').html().split(' - ')[1].split(".");
    f = new Date(f[2]+"."+f[1]+"."+f[0]+" 15:00");
    debugger;
    var hasValidClass = false;
    var hasInvalidClass = false;
    //check if start is after project start
    if(isValidDate(s)){
      if(newTaskStart < s){
        $('#taskStartEdit').addClass('is-invalid');
        hasInvalidClass = true;
        debugger;
      }
      else{
        $('#taskStartEdit').addClass('is-valid');
        hasValidClass = true;
        debugger;
      }
    }
    if(isValidDate(f)){
      if(newTaskStart > f){
        if(!hasInvalidClass){
          hasValidClass = false;
          $('#taskStartEdit').removeClass('is-valid');
          hasInvalidClass = true;
          $('#taskStartEdit').addClass('is-invalid');
          debugger;
        }
        else if(hasValidClass){
          $('#taskStartEdit').removeClass('is-valid');
          hasValidClass = false;
          hasInvalidClass = true;
          $('#taskStartEdit').addClass('is-invalid');
          debugger;
        }
      }
      else{
        if(!hasValidClass && !hasInvalidClass){
          hasValidClass = true;
          $('#taskStartEdit').addClass('is-valid');
          debugger;
        }
      }
    }
    if(isValidDate(newTaskStart) && isValidDate(newTaskFinish))
      countEditDays();
  }
}
function onFinishEditChange(){
  $('#taskFinishEdit').removeClass('is-invalid');
  $('#taskFinishEdit').removeClass('is-valid');
  $('#taskDurationEdit').val(0);
  if($('#taskFinishEdit').val()){
    var newTaskStart = new Date($('#taskStartEdit').val()+" 9:00");
    var newTaskFinish = new Date($('#taskFinishEdit').val()+" 13:00");
    //first check if finish after start otherwise finish = start
    if(isValidDate(newTaskStart) && isValidDate(newTaskFinish)){
      if(new Date($('#taskStartEdit').val()) >= new Date($('#taskFinishEdit').val())){
        $('#taskFinishEdit').val($('#taskStartEdit').val());
        newTaskFinish = new Date($('#taskFinishEdit').val()+" 13:00");
      }
    }
    //to get time from project date from html
    var s = $('#pinfo_date').html().split(' - ')[0].split(".");
    s = new Date(s[2]+"."+s[1]+"."+s[0]+" 07:00");
    
    var f = $('#pinfo_date').html().split(' - ')[1].split(".");
    f = new Date(f[2]+"."+f[1]+"."+f[0]+" 15:00");
    debugger;
    var hasValidClass = false;
    var hasInvalidClass = false;
    //check if finish is after project start
    if(isValidDate(s)){
      if(newTaskFinish < s){
        $('#taskFinishEdit').addClass('is-invalid');
        hasInvalidClass = true;
        debugger;
      }
      else{
        $('#taskFinishEdit').addClass('is-valid');
        hasValidClass = true;
        debugger;
      }
    }
    if(isValidDate(f)){
      if(newTaskFinish > f){
        if(!hasInvalidClass){
          hasValidClass = false;
          $('#taskFinishEdit').removeClass('is-valid');
          hasInvalidClass = true;
          $('#taskFinishEdit').addClass('is-invalid');
          debugger;
        }
        else if(hasValidClass){
          $('#taskFinishEdit').remove('is-valid');
          hasValidClass = false;
          hasInvalidClass = true;
          $('#taskFinishEdit').addClass('is-invalid');
          debugger;
        }
      }
      else{
        if(!hasValidClass && !hasInvalidClass){
          hasValidClass = true;
          $('#taskFinishEdit').addClass('is-valid');
          debugger;
        }
      }
    }
    if(isValidDate(newTaskFinish) && isValidDate(newTaskStart))
      countEditDays();
  }
}
function countDays(){
  var start = $('#taskStartAdd').val();
  var finish = $('#taskFinishAdd').val();
  var oneDay = 24*60*60*1000;
  var firstDay = new Date(start);
  var secondDay = new Date(finish);
  var diffDays = Math.round(Math.abs((firstDay.getTime() - secondDay.getTime()) / (oneDay)))+1;
  $('#taskDurationAdd').val(diffDays);
}
function countEditDays(){
  var start = $('#taskStartEdit').val();
  var finish = $('#taskFinishEdit').val();
  var oneDay = 24*60*60*1000;
  var firstDay = new Date(start);
  var secondDay = new Date(finish);
  var diffDays = Math.round(Math.abs((firstDay.getTime() - secondDay.getTime()) / (oneDay)))+1;
  $('#taskDurationEdit').val(diffDays);
}
function isValidDate(d) {
  return d instanceof Date && !isNaN(d);
}
$('#taskStartAdd').on('change', onStartAddChange);
$('#taskFinishAdd').on('change', onFinishAddChange);