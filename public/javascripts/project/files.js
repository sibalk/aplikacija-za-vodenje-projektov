//IN THIS SCRIPT//
//// drawing all tasks of project, opening/closing collaps for files, drawing tasks files ////

$(function(){
  //$('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover()
  if($('#msg').html() != ''){
    $("#modalMsg").modal();
    //set timeout so i dont loose msg right away
    setTimeout(()=>{
      window.history.pushState({}, document.title, "/" + "projects/id?id="+projectId);
    })
  }
  $('#modalTaskDocumentAddForm').on('hidden.bs.modal', function () {
    $("#modalTaskFilesForm").toggle();
    $("#modalTaskFilesForm").focus();
  })
  //get project files
  //no need, every time new file is added page is reloaded so server can already fetch files
})
function newDocumentModal(){
  $("#modalDocumentAddForm").modal();
}
function newTaskDocumentModal(taskId){
  //open modal for adding files that are connected to task
  //$("#modalTaskFilesForm").toggle();
  $("#modalTaskDocumentAddForm").modal();
  $('#taskIdTaskFile').val(taskId);
  debugger;
}
function disableFunction(){
  $('#btnAddDoc').attr('disabled','disabled');
  $('#btnAddTaskDoc').attr('disabled','disabled');
}
function disableFunctionTask(){
  $('#btnAddTaskDoc').attr('disabled','disabled');
}
function toggleTaskCollapse(){
  //tasks are open, files are closed, i want to close tasks
  if(taskCollapseOpen && !fileCollapseOpen){
    $('#collapseTableTask').collapse("toggle");
    $('#btnCollapseTask').removeClass('active');
    taskCollapseOpen = false;
  }
  //tasks are closed, files are open, i want to open tasks
  else if(!taskCollapseOpen && fileCollapseOpen){
    $('#collapseFileTab').collapse("toggle");
    $('#collapseTableTask').collapse("toggle");
    $('#btnCollapseFile').removeClass('active');
    $('#btnCollapseTask').addClass('active');
    taskCollapseOpen = true;
    fileCollapseOpen = false;
  }
  //both are closed, i want to open tasks
  else if(!taskCollapseOpen && !fileCollapseOpen){
    $('#collapseTableTask').collapse("toggle");
    $('#btnCollapseTask').addClass('active');
    taskCollapseOpen = true;
  }
}
function toggleFileCollapse(){
  //files are open, tasks are closed, i want to close files
  if(fileCollapseOpen && !taskCollapseOpen){
    $('#collapseFileTab').collapse("toggle");
    $('#btnCollapseFile').removeClass('active');
    fileCollapseOpen = false;
  }
  //files are closed, tasks are open, i want to open files
  else if(!fileCollapseOpen && taskCollapseOpen){
    if(!filesLoaded){
      //files are not loaded, get files and show them
      $.get( "/projects/files", {projectId}, function( data ) {
        var allFiles = data.data;
        debugger
        drawProjectFiles(allFiles);
      });
      filesLoaded = true;
    }
    $('#collapseTableTask').collapse("toggle");
    $('#collapseFileTab').collapse("toggle");
    $('#btnCollapseTask').removeClass('active');
    $('#btnCollapseFile').addClass('active');
    fileCollapseOpen = true;
    taskCollapseOpen = false;
  }
  //both are closed, i want to open files
  else if(!taskCollapseOpen && !fileCollapseOpen){
    if(!filesLoaded){
      //files are not loaded, get files and show them
      $.get( "/projects/files", {projectId}, function( data ) {
        var allFiles = data.data;
        debugger
        drawProjectFiles(allFiles);
      });
      filesLoaded = true;
    }
    $('#collapseFileTab').collapse("toggle");
    $('#btnCollapseFile').addClass('active');
    fileCollapseOpen = true;
  }
}

function drawProjectFiles(allFiles){
  debugger;
  for(var i = 0; i < allFiles.length; i++){
    var downloadButton = `<a class="btn btn-primary mt-3" href="/sendMeDoc?filename=`+allFiles[i].path_name+`&amp;projectId=`+allFiles[i].id_project+`">Prenos</a>`;
    if(allFiles[i].active == false)
      downloadButton = `<a class="btn btn-primary mt-3 disabled" href="">Prenos</a>`;
    var fullDate = new Date(allFiles[i].date).toLocaleString("sl");
    var date = fullDate.substring(0,11);
    var time = fullDate.substring(12,17);
    var element = `<div class="list-group-item" id="file`+allFiles[i].id+`">
      <div class="row">
        <div class="col-sm-2">
          <img class="img-fluid" src="/file`+allFiles[i].type+`.png" alt="ikona" style="width:70px">
        </div>
        <div class="col-sm-9">
          <label class="row mt-2">`+allFiles[i].original_name+`</label>
          <label class="row mt-2" style="font-size:12px">Naloženo: `+date+` ob `+time+` Avtor: ` + allFiles[i].name + " " + allFiles[i].surname + `</label>
        </div>
        <div class="col-sm-1">
          `+downloadButton+`
        </div>
      </div>
    </div>`;
    $('#fileList').append(element);
  }
}
//OPEN TASK FILES COLLAPSE AND FILL TASK FILES LIST
function openTaskFiles(taskId){
  //debugger;
  $.get('/projects/taskFiles', {projectId, taskId}, function(data){
    let files = data.data;
    //debugger;
    //collapse frame
    var frame = `<hr />
    <div class="list-group span-collapse-content" id="fileList`+taskId+`"></div><br />
    <div id="addFileRow`+taskId+`">
      <div class="text-center">
        <button class="btn btn-success" onclick="newTaskDocumentModal(`+taskId+`)">Dodaj datoteko</button>
      </div>
    </div><br />`;
    $('#collapseTaskFiles'+taskId).append(frame);

    for(var i = 0; i < files.length; i++){
      var date = new Date(files[i].date);
      var d = date.getDate();
      var m = date.getMonth()+1;
      var y = date.getFullYear();
      var hour = date.getHours();
      var minutes = date.getMinutes();
      if(d < 10) d = '0'+d;
      if(m < 10) m = '0'+m;
      if(hour < 10) hour = '0'+hour;
      if(minutes < 10) minutes = '0'+minutes;
      date = d+"."+m+"."+y;
      var time = hour+":"+minutes; 

      var listElement = `<div class="list-group-item" id="taskFile`+files[i].id+`">
        <div class="row">
          <div class="col-sm-2"><img class="img-fluid" src="/file`+files[i].type+`.png" alt="ikona" style="width:70px;" /></div>
          <div class="col-sm-9">
            <label class="row mt-2">`+files[i].original_name+`</label>
            <label class="row mt-2" style="font-size:12px;">Naloženo: `+date+` ob `+time+` Avtor: `+files[i].name+" "+files[i].surname+`</label>
          </div>
          <div class="col-sm-1"><a class="btn btn-primary mt-3" href="/sendMeDoc?filename=`+files[i].path_name+`&projectId=`+projectId+`">Prenos</a></div>
        </div>
      </div>`;
      
      $('#fileList'+taskId).append(listElement);
    }
    $('#collapseTaskFiles'+taskId).collapse("toggle");
    taskFilesCollapseOpen = true;
  });
}

var taskCollapseOpen = true;
var fileCollapseOpen = false;
var filesLoaded = false;