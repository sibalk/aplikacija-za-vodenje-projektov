$(document).ready(function(){
  //get projects (id, text) for select2
  $.get( "/viz/projects", function( data ) {
    projects = data.data;

    $(".select2-projects").select2({
      data: projects,
      tags: true,
    });
  });
  //get users (id, text) for select2
  $.get( "/projects/users", function( data ) {
    employees = data.data;

    $(".select2-employees").select2({
      data: employees,
      tags: true,
    });
  });
  //get all projects for viz projects
  $.get("/projects/all", {activeProject}, function( data ){
    projectsData = data.data;
    drawTimelineProject();
  })
})
////SKUPNO PROJEKTI////
///nariši časovnico projektov
function drawTimelineProject(){
  //debugger;
  makeProjectVizData();
  //debugger;
  //$("#collapseTimetable").addClass("show");
  //var widthCollapseTT = $("#projectPlot").parent().width();
  //$("#collapseTimetable").removeClass("show");
  if(projectDataViz.length == 0)
    return
  chart = TimelinesChart()
      .data(projectDataViz)
      .width(1068)
      .zQualitative(true)
      (document.getElementById('projectPlot'));
}
//osveži podatke za časovnico projekti
function refreshTimelineProject(){
  makeProjectVizData();
  //debugger;
  if(chart)
    chart.data(projectDataViz).refresh();
  else{
    chart = TimelinesChart()
      .data(projectDataViz)
      .width(1068)
      .zQualitative(true)
      (document.getElementById('projectPlot'));
  }
}
//naredi/sestavi podatke za časovnico projektov
function makeProjectVizData(){
  projectDataViz = [
    {group:"Aktivni", data:[]},
    {group:"Ne aktivni", data:[]},
  ];
  var subscriber;
  for(var i=0; i<projectsData.length; i++){
    //debugger;
    if(projectsData[i].completion == '100')
      continue;
    subscriber = projectsData[i].name.substring(0,5);
    if(projectsData[i].name.length > 5)
      subscriber += "...";
    var start = projectsData[i].start;
    var finish = projectsData[i].finish;
     if(!projectsData[i].start && !projectsData[i].finish)
      continue;
    else if(!projectsData[i].start)
      start = projectsData[i].finish;
    else if(!projectsData[i].finish)
      finish = projectsData[i].start;
    
    if(projectsData[i].active){
      //debugger;
      projectDataViz[0].data.push(
        {
          label: projectsData[i].project_name,
          data: [
            {
              timeRange:[start, finish],
              val: subscriber
            }
          ]
        }
      );
    }
    else if(!projectsData[i].active){
      //debugger;
      projectDataViz[1].data.push(
        {
          label: projectsData[i].project_name,
          data: [
            {
              timeRange:[start, finish],
              val: subscriber
            }
          ]
        }
      );
    }
  }
  if(projectDataViz[1].data.length == 0) projectDataViz.splice(1,1)
  if(projectDataViz[0].data.length == 0) projectDataViz.splice(0,1)
  //debugger;
}
/////OPRAVILA ZA IZBRANI PROJEKT////
//pridobi podatke za izbrani projekt
function showProjectTasks(){
  debugger;
  var projectId = $('#projectSelect').val();
  $.get("/projects/tasks", {projectId, categoryTask, activeTask}, function( data ){
    allTasks = data.data;
    initTasks = allTasks;
    //multipleWorkers();
    //allTasks = vsaOpravila;
    //initTasks = vsaOpravila;
    //check if chart is already drawn
    if(firstTasks){
      drawTimelineTasks();
      firstTasks = false;
    }
    else{
      refreshTimelineTasks();
    }
  })
}
//nariši opravila za izbrani projekt (za prvič)
function drawTimelineTasks(){
  //debugger;
  makeTasksVizData();
  //debugger;
  //$("#collapseTimetable").addClass("show");
  //var widthCollapseTT = $("#myPlot").parent().width();
  //$("#collapseTimetable").removeClass("show");
  if(tasksDataViz.length == 0)
    return
  tasksChart = TimelinesChart()
      .data(tasksDataViz)
      .width(1068)
      .zQualitative(true)
      (document.getElementById('tasksPlot'));
}
//osveži podatke za izbran (nov) projekt (za nadaljno risanje)
function refreshTimelineTasks(){
  makeTasksVizData();
  //debugger;
  if(tasksChart)
    tasksChart.data(tasksDataViz).refresh();
  else{
    tasksChart = TimelinesChart()
      .data(tasksDataViz)
      .width(1068)
      .zQualitative(true)
      (document.getElementById('tasksPlot'));
  }
}
//sestavi podatke za opravila
function makeTasksVizData(){
  tasksDataViz = [
    {group:"Nabava", data:[]},
    {group:"Konstrukcija", data:[]},
    {group:"Strojna izdelava", data:[]},
    {group:"Elektro izdelava", data:[]},
    {group:"Montaža", data:[]},
    {group:"Programiranje", data:[]},
    {group:"Brez", data:[]}
  ];
  var delavec;
  for(var i=0; i<initTasks.length; i++){
    //debugger;
    if(initTasks[i].active != true)
      continue;
    delavec = "Ni dodeljeno";
    if(initTasks[i].worker){
      delavec = initTasks[i].worker;
      var element = "";
      for(var j=0; j<initTasks[i].workers.length; j++){
        //debugger
        if(!initTasks[i].workers[j])
          initTasks[i].workers[j] = "";
        if(j != initTasks[i].workers.length-1)
          element = element + initTasks[i].workers[j].split(" ")[0][0] + ". " + initTasks[i].workers[j].split(" ")[1][0] + "." + ", ";
        else
          element = element + initTasks[i].workers[j].split(" ")[0][0] + ". " + initTasks[i].workers[j].split(" ")[1][0] + ".";
      }
      delavec = element;
    }
    var start = initTasks[i].task_start;
    var finish = initTasks[i].task_finish;
     if(!initTasks[i].task_start && !initTasks[i].task_finish)
      continue;
    else if(!initTasks[i].task_start)
      start = initTasks[i].task_finish;
    else if(!initTasks[i].task_finish)
      finish = initTasks[i].task_start;
    
    if(initTasks[i].category === 'Nabava'){
      //debugger;
      tasksDataViz[0].data.push(
        {
          label: initTasks[i].task_name,
          data: [
            {
              timeRange:[start, finish],
              val: delavec
            }
          ]
        }
      );
    }
    else if(initTasks[i].category === 'Konstrukcija'){
      //debugger;
      tasksDataViz[1].data.push(
        {
          label: initTasks[i].task_name,
          data: [
            {
              timeRange:[start, finish],
              val: delavec
            }
          ]
        }
      );
    }
    else if(initTasks[i].category === 'Strojna izdelava'){
      tasksDataViz[2].data.push(
        {
          label: initTasks[i].task_name,
          data: [
            {
              timeRange:[start, finish],
              val: delavec
            }
          ]
        }
      );
    }
    else if(initTasks[i].category === 'Elektro izdelava'){
      tasksDataViz[3].data.push(
        {
          label: initTasks[i].task_name,
          data: [
            {
              timeRange:[start, finish],
              val: delavec
            }
          ]
        }
      );
    }
    else if(initTasks[i].category === 'Montaža'){
      tasksDataViz[4].data.push(
        {
          label: initTasks[i].task_name,
          data: [
            {
              timeRange:[start, finish],
              val: delavec
            }
          ]
        }
      );
    }
    else if(initTasks[i].category === 'Programiranje'){
      tasksDataViz[5].data.push(
        {
          label: initTasks[i].task_name,
          data: [
            {
              timeRange:[start, finish],
              val: delavec
            }
          ]
        }
      );
    }
    else if(initTasks[i].category === 'Brez'){
      tasksDataViz[6].data.push(
        {
          label: initTasks[i].task_name,
          data: [
            {
              timeRange:[start, finish],
              val: delavec
            }
          ]
        }
      );
    }
  }
  if(tasksDataViz[6].data.length == 0) tasksDataViz.splice(6,1)
  if(tasksDataViz[5].data.length == 0) tasksDataViz.splice(5,1)
  if(tasksDataViz[4].data.length == 0) tasksDataViz.splice(4,1)
  if(tasksDataViz[3].data.length == 0) tasksDataViz.splice(3,1)
  if(tasksDataViz[2].data.length == 0) tasksDataViz.splice(2,1)
  if(tasksDataViz[1].data.length == 0) tasksDataViz.splice(1,1)
  if(tasksDataViz[0].data.length == 0) tasksDataViz.splice(0,1)
  //debugger;
}
////OPRAVILA ZA IZBRANEGA DELAVCA////
function showEmployeeTasks(){
  debugger;
  var taskAssignment = $('#employeesSelect').val();
  $.post("/projects/worker/tasks", {taskAssignment}, function( data ){
    employeesData = data.data[0];
    //check if chart is already drawn
    if(firstEmployee){
      drawTimelineEmployee();
      firstEmployee = false;
    }
    else{
      refreshTimelineEmployee();
    }
  })
}
function drawTimelineEmployee(){
  //debugger;
  makeEmployeeVizData();
  //debugger;
  if(employeesDataViz.length == 0)
    return
  employeesChart = TimelinesChart()
      .data(employeesDataViz)
      .width(1068)
      .zQualitative(true)
      (document.getElementById('employeePlot'));
}
function refreshTimelineEmployee(){
  makeEmployeeVizData();
  //debugger;
  if(employeesChart)
    employeesChart.data(employeesDataViz).refresh();
  else{
    employeesChart = TimelinesChart()
      .data(employeesDataViz)
      .width(1068)
      .zQualitative(true)
      (document.getElementById('employeePlot'));
  }
}
function makeEmployeeVizData(){
  employeesDataViz = [
    {group:"Nabava", data:[]},
    {group:"Konstrukcija", data:[]},
    {group:"Strojna izdelava", data:[]},
    {group:"Elektro izdelava", data:[]},
    {group:"Montaža", data:[]},
    {group:"Programiranje", data:[]},
    {group:"Brez", data:[]}
  ];
  var delavec;
  for(var i=0; i<employeesData.length; i++){
    debugger;
    if(employeesData[i].active != true)
      continue;
    if(employeesData[i].project_name){
      delavec = employeesData[i].project_name.substring(0,5);
      if(employeesData[i].project_name.length > 5)
        delavec += "...";
    }
    else
      delavec = "servis"
    var start = employeesData[i].task_start;
    var finish = employeesData[i].task_finish;
     if(!employeesData[i].task_start && !employeesData[i].task_finish)
      continue;
    else if(!employeesData[i].task_start)
      start = employeesData[i].task_finish;
    else if(!employeesData[i].task_finish)
      finish = employeesData[i].task_start;
    
    if(employeesData[i].category === 'Nabava'){
      //debugger;
      employeesDataViz[0].data.push(
        {
          label: employeesData[i].task_name,
          data: [
            {
              timeRange:[start, finish],
              val: delavec
            }
          ]
        }
      );
    }
    else if(employeesData[i].category === 'Konstrukcija'){
      //debugger;
      employeesDataViz[1].data.push(
        {
          label: employeesData[i].task_name,
          data: [
            {
              timeRange:[start, finish],
              val: delavec
            }
          ]
        }
      );
    }
    else if(employeesData[i].category === 'Strojna izdelava'){
      employeesDataViz[2].data.push(
        {
          label: employeesData[i].task_name,
          data: [
            {
              timeRange:[start, finish],
              val: delavec
            }
          ]
        }
      );
    }
    else if(employeesData[i].category === 'Elektro izdelava'){
      employeesDataViz[3].data.push(
        {
          label: employeesData[i].task_name,
          data: [
            {
              timeRange:[start, finish],
              val: delavec
            }
          ]
        }
      );
    }
    else if(employeesData[i].category === 'Montaža'){
      employeesDataViz[4].data.push(
        {
          label: employeesData[i].task_name,
          data: [
            {
              timeRange:[start, finish],
              val: delavec
            }
          ]
        }
      );
    }
    else if(employeesData[i].category === 'Programiranje'){
      employeesDataViz[5].data.push(
        {
          label: employeesData[i].task_name,
          data: [
            {
              timeRange:[start, finish],
              val: delavec
            }
          ]
        }
      );
    }
    else if(employeesData[i].category === 'Brez'){
      employeesDataViz[6].data.push(
        {
          label: employeesData[i].task_name,
          data: [
            {
              timeRange:[start, finish],
              val: delavec
            }
          ]
        }
      );
    }
  }
  if(employeesDataViz[6].data.length == 0) employeesDataViz.splice(6,1)
  if(employeesDataViz[5].data.length == 0) employeesDataViz.splice(5,1)
  if(employeesDataViz[4].data.length == 0) employeesDataViz.splice(4,1)
  if(employeesDataViz[3].data.length == 0) employeesDataViz.splice(3,1)
  if(employeesDataViz[2].data.length == 0) employeesDataViz.splice(2,1)
  if(employeesDataViz[1].data.length == 0) employeesDataViz.splice(1,1)
  if(employeesDataViz[0].data.length == 0) employeesDataViz.splice(0,1)
  //debugger;
}
function multipleWorkers(){
  vsaOpravila = [];
  var idTasks = [];
  var counter = 0;
  for(var i=0; i<allTasks.length; i++){
    //var conflict = idTasks.find(t => (parseInt(projectId) != t.project_id));
    //debugger;
    if(!contains.call(idTasks, allTasks[i].id)){
      idTasks.push(allTasks[i].id);
      vsaOpravila.push(allTasks[i]);
      vsaOpravila[counter].workers = [allTasks[i].worker];
      vsaOpravila[counter].workersId = [allTasks[i].worker_id]
      counter++;
    }
    else{
      var opravilo = vsaOpravila.find(o => o.id === allTasks[i].id);
      if(opravilo){
        opravilo.workers.push(allTasks[i].worker);
        opravilo.workersId.push(allTasks[i].worker_id);
      }
    }
    //debugger;
  }
}
var contains = function(needle) {
  // Per spec, the way to identify NaN is that it is not equal to itself
  var findNaN = needle !== needle;
  var indexOf;

  if(!findNaN && typeof Array.prototype.indexOf === 'function') {
      indexOf = Array.prototype.indexOf;
  } else {
      indexOf = function(needle) {
          var i = -1, index = -1;

          for(i = 0; i < this.length; i++) {
              var item = this[i];

              if((findNaN && item !== item) || item === needle) {
                  index = i;
                  break;
              }
          }

          return index;
      };
  }

  return indexOf.call(this, needle) > -1;
};
var projectChart;
var projectDataViz;
var tasksChart;
var tasksDataViz;
var employeesChart;
var employeesDataViz;
var firstTasks = true;
var firstEmployee = true;
var projects;
var employees;
var projectsData;
var employeesData;
var tasksData;
var activeProject = 1;
var categoryTask = 0;
var activeTask = 1;