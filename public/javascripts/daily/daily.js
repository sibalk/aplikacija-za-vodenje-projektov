$(function(){
  loggedUser = {name:$('#loggedUser').html().split(" (")[0].split(" ")[0], surname:$('#loggedUser').html().split(" (")[0].split(" ")[1], role:$('#loggedUser').html().split(" (")[1].split(")")[0]};
  if(loggedUser.role == 'admin' || loggedUser.role == 'vodja'){
    $('#header').text("Opravila danes, " + new Date().getDate() + "." + (new Date().getMonth()+1) + "." + new Date().getFullYear());
  }
  if(loggedUser.role == 'admin'){
    adminView = true;
  }
  if(loggedUser.role == 'info' || loggedUser.role == 'tajnik')
    animationCheck = true;
  hiddenScroll = $('#dailyList').prop('scrollHeight') - $('#dailyList').height() + 15;
  if( hiddenScroll > 0 && animationCheck){
    //big list, animate the list up and down slowly
    animateList();
  }
  hiddenScrollWeek = $('#weeklyList').prop('scrollHeight') - $('#weeklyList').height() + 15;
  if( hiddenScrollWeek > 0 && animationCheck){
    //big list, animate the list up and down slowly
    animateListWeek();
  }
  //get all users so the coloring the names is possible
  $.get('/employees/all', function(resp){
    allUsers = resp.data
    changeTaskTable(0);
    changeWeekTable(0);
  })
  function checkTime(i){
    return (i < 10) ? "0" + i : i;
  }
  function startTime(){
    var today = new Date(),
        h = checkTime(today.getHours()),
        m = checkTime(today.getMinutes()),
        s = checkTime(today.getSeconds()),
        day = today.getDate(),
        month = today.getMonth() + 1,
        year = today.getFullYear();
    $('#time').html(day+"."+month+"."+year+", "+h+":"+m+":"+s);
    t = setTimeout(function () {startTime()}, 500);
  }
  startTime();
})

function changeDate(){
  date = $('#dateInput').val();
  changeTaskTable(categoryTask);
  /*
  if(date){
    //debugger;
    //pridobi opravila za ta dan in posodobi dan
    $.get('/daily/date', {date, categoryTask, adminView}, function(resp){
      if(resp.success){
        var tasks = resp.data;
        dailyTasks = tasks;
        $('#dailyList').empty();
        for(var i = 0; i < tasks.length; i++){
          //old with category and subscriber info
          /*
          var element = `<row class="list-group-item flex-column" id=`+tasks[i].id+`>
            <div class="d-flex w-100 justify-content-between">
              <div class="h5 mb-1" id="taskName">`+tasks[i].task_name+`</div>
              <div id="taskCategory">`+tasks[i].category+`</div>
            </div>
            <div class="d-flex justify-content-between">
              <div class="mb-1" id="workers">`+tasks[i].workers+`</div>
              <div id="subscribers">`+tasks[i].subscriber+`</div>
            </div>
          </row>`;
          //
          var workers = ``;
          if(tasks[i].workersId[0]){
            for(var j = 0; j < tasks[i].workersId.length; j++){
              var x = allUsers.find(x => x.id == tasks[i].workersId[j]);
              if(x.role == 'konstrukter' || x.role == 'konstruktor')
                workers += `<div class="mb-1 mr-2 color-builder" id="workers">`+x.name +" "+ x.surname+`</div>`;
              else if(x.role == 'strojnik')
                workers += `<div class="mb-1 mr-2 color-mechanic" id="workers">`+x.name +" "+ x.surname+`</div>`;
              else if(x.role == 'električar')
                workers += `<div class="mb-1 mr-2 color-electrican" id="workers">`+x.name +" "+ x.surname+`</div>`;
              else if(x.role == 'plc')
                workers += `<div class="mb-1 mr-2 color-plc" id="workers">`+x.name +" "+ x.surname+`</div>`;
              else if(x.role == 'kuka')
                workers += `<div class="mb-1 mr-2 color-kuka" id="workers">`+x.name +" "+ x.surname+`</div>`;
              else if(x.role == 'vodja')
                workers += `<div class="mb-1 mr-2 font-weight-bold" id="workers">`+x.name +" "+ x.surname+`</div>`;
              else if(x.role == 'admin')
                workers += `<div class="mb-1 mr-2 font-weight-bold font-italic" id="workers">`+x.name +" "+ x.surname+`</div>`;
              else
                workers += `<div class="mb-1 mr-2" id="workers">`+x.name +" "+ x.surname+`</div>`;
            }
          }
          else{
            workers = `<div class="mb-1 mr-2" id="workers">Ni dodeljeno</div>`;
          }
          var start = '/';
          var finish = '/';
          if(tasks[i].task_start)
            start = tasks[i].task_start.split("T")[0].split("-")[2] + "." + tasks[i].task_start.split("T")[0].split("-")[1] + "." + tasks[i].task_start.split("T")[0].split("-")[0];
          if(tasks[i].task_finish)
            finish = tasks[i].task_finish.split("T")[0].split("-")[2] + "." + tasks[i].task_finish.split("T")[0].split("-")[1] + "." + tasks[i].task_finish.split("T")[0].split("-")[0];
          var servis = "";
          var head = "";
          var expired = "";
          if(tasks[i].subscriberservis){
            servis = '<span class="badge badge-info ml-2">Servis</span>';
            head = `<div class="d-flex w-100 justify-content-between">
              <div class="h5 mb-1 font-italic" id="projectName">`+tasks[i].subscriberservis+`</div>
            </div>`;
          }
          else if(tasks[i].project_name){
            head = `<div class="d-flex w-100 justify-content-between">
              <div class="h5 mb-1 font-italic" id="projectName">`+tasks[i].project_name+`</div>
            </div>`;
          }
          if(tasks[i].expired)
            expired = '<span class="badge badge-danger ml-2">ZAMUDA</span>';
          var element = `<row class="list-group-item flex-column" id=`+tasks[i].id+`>
            `+head+`
            <div class="d-flex w-100 justify-content-between">
              <div class="h5 mb-1" id="taskName">`+tasks[i].task_name+`</div>
            </div>
            <div class="d-flex w-100 justify-content-start">
              <div class="h6 mb-1 mr-2" id="weekDate">`+ start +" - " + finish +`</div>
              ` + servis + expired + `
            </div>
            <div class="d-flex justify-content-start flex-wrap">
              `+workers+`
            </div>
          </row>`;
          $('#dailyList').append(element);
        }
        //if admin or leader change date info
        if(loggedUser.role == 'vodja' || loggedUser.role == 'admin'){
          $('#header').text("Opravila za dan: "+date.split("-")[2] + "." + date.split("-")[1] + "." + date.split("-")[0])
        }
        //calculate new hiddenScroll
        hiddenScroll = $('#dailyList').prop('scrollHeight') - $('#dailyList').height() + 15;
        $( "#dailyList" ).stop();
        $('#dailyList').animate({ 'scrollTop': '-='+$('#dailyList').scrollTop() }, 'slow');
        if(document.getElementById('customAnimationCheck').checked)
          animateList();
        //debugger;
        changeWeekTable();
      }
    })
  }
  */

}
function changeTaskTable(category){
  //debugger;
  $('#option0').removeClass('active');
  $('#option1').removeClass('active');
  $('#option2').removeClass('active');
  $('#option3').removeClass('active');
  $('#option4').removeClass('active');
  $('#option5').removeClass('active');
  $('#option6').removeClass('active');
  $('#option7').removeClass('active');
  setTimeout(()=>{
    $('#option'+category+'').addClass('active');
  })
  categoryTask = category;
  date = $('#dateInput').val();
  $.get('/daily/date', {date, categoryTask, adminView}, function( resp ) {
    if(resp.success){
      var tasks = resp.data;
      dailyTasks = tasks;
      $('#dailyList').empty();
      //also need to get all absences so i can ignore workers who are absent
      $.get('/absence/date', {date}, function( data ){
        if(data.success){
          var absences = data.data;
          allAbsences = absences;
          debugger;
          //have daily tasks, workers and absences
          for(var i = 0; i < tasks.length; i++){
            var workers = ``;
            if(tasks[i].workersId[0]){
              for(var j = 0; j < tasks[i].workersId.length; j++){
                //x is worker, y is his absence if x exists
                var x = allUsers.find(x => x.id == tasks[i].workersId[j]);
                var y;
                if(x){
                  y = allAbsences.find(a => a.id_user == x.id && a.id_project == null && a.id_task == null);
                  debugger;
                  if(!y)
                    y = allAbsences.find(a => a.id_user == x.id && a.id_project == tasks[i].id_project && a.id_task == tasks[i].id);
                }
                debugger;
                if(!y){
                  if(x.role == 'konstrukter' || x.role == 'konstruktor')
                    workers += `<div class="mb-1 mr-2 color-builder" id="workers">`+x.name +" "+ x.surname+`</div>`;
                  else if(x.role == 'strojnik')
                    workers += `<div class="mb-1 mr-2 color-mechanic" id="workers">`+x.name +" "+ x.surname+`</div>`;
                  else if(x.role == 'električar')
                    workers += `<div class="mb-1 mr-2 color-electrican" id="workers">`+x.name +" "+ x.surname+`</div>`;
                  else if(x.role == 'plc')
                    workers += `<div class="mb-1 mr-2 color-plc" id="workers">`+x.name +" "+ x.surname+`</div>`;
                  else if(x.role == 'kuka')
                    workers += `<div class="mb-1 mr-2 color-kuka" id="workers">`+x.name +" "+ x.surname+`</div>`;
                  else if(x.role == 'vodja')
                    workers += `<div class="mb-1 mr-2 font-weight-bold" id="workers">`+x.name +" "+ x.surname+`</div>`;
                  else if(x.role == 'admin')
                    workers += `<div class="mb-1 mr-2 font-weight-bold font-italic" id="workers">`+x.name +" "+ x.surname+`</div>`;
                  else
                    workers += `<div class="mb-1 mr-2" id="workers">`+x.name +" "+ x.surname+`</div>`;
                }
              }
            }
            else{
              workers = `<div class="mb-1 mr-2" id="workers">Ni dodeljeno</div>`;
            }
            debugger;
            var start = '/';
            var finish = '/';
            if(tasks[i].task_start)
              start = tasks[i].task_start.split("T")[0].split("-")[2] + "." + tasks[i].task_start.split("T")[0].split("-")[1] + "." + tasks[i].task_start.split("T")[0].split("-")[0];
            if(tasks[i].task_finish)
              finish = tasks[i].task_finish.split("T")[0].split("-")[2] + "." + tasks[i].task_finish.split("T")[0].split("-")[1] + "." + tasks[i].task_finish.split("T")[0].split("-")[0];
            var servis = "";
            var head = "";
            var expired = "";
            if(tasks[i].subscriberservis){
              servis = '<span class="badge badge-info ml-2">Servis</span>';
              head = `<div class="d-flex w-100 justify-content-between">
                <div class="h5 mb-1 font-italic" id="projectName">`+tasks[i].subscriberservis+`</div>
              </div>`;
            }
            else if(tasks[i].project_name){
              head = `<div class="d-flex w-100 justify-content-between">
                <div class="h5 mb-1 font-italic" id="projectName">`+tasks[i].project_name+`</div>
              </div>`;
            }
            if(tasks[i].expired)
              expired = '<span class="badge badge-danger ml-2">ZAMUDA</span>';
            var element = `<row class="list-group-item flex-column" id=`+tasks[i].id+`>
              `+head+`
              <div class="d-flex w-100 justify-content-between">
                <div class="h5 mb-1" id="taskName">`+tasks[i].task_name+`</div>
              </div>
              <div class="d-flex w-100 justify-content-start">
                <div class="h6 mb-1 mr-2" id="weekDate">`+ start +" - " + finish +`</div>
                ` + servis + expired + `
              </div>
              <div class="d-flex justify-content-start flex-wrap">
                `+workers+`
              </div>
            </row>`;
            $('#dailyList').append(element);
          }
          //if admin or leader change date info
          if(loggedUser.role == 'vodja' || loggedUser.role == 'admin'){
            if(!date)
              $('#header').text("Opravila danes");
            else
              $('#header').text("Opravila za dan: "+date.split("-")[2] + "." + date.split("-")[1] + "." + date.split("-")[0])
          }
          //calculate new hiddenScroll
          hiddenScroll = $('#dailyList').prop('scrollHeight') - $('#dailyList').height() + 15;
          $( "#dailyList" ).stop();
          $('#dailyList').animate({ 'scrollTop': '-='+$('#dailyList').scrollTop() }, 'slow');
          if(document.getElementById('customAnimationCheck').checked)
            animateList();
          //debugger;
          changeWeekTable(categoryTask);
        }
        else{
          //have dailyTasks and workers, no absences
          for(var i = 0; i < tasks.length; i++){
            //old with category and subscriber info
            /*
            var element = `<row class="list-group-item flex-column" id=`+tasks[i].id+`>
              <div class="d-flex w-100 justify-content-between">
                <div class="h5 mb-1" id="taskName">`+tasks[i].task_name+`</div>
                <div id="taskCategory">`+tasks[i].category+`</div>
              </div>
              <div class="d-flex justify-content-between">
                <div class="mb-1" id="workers">`+tasks[i].workers+`</div>
                <div id="subscribers">`+tasks[i].subscriber+`</div>
              </div>
            </row>`;
            */
            var workers = ``;
            if(tasks[i].workersId[0]){
              for(var j = 0; j < tasks[i].workersId.length; j++){
                var x = allUsers.find(x => x.id == tasks[i].workersId[j]);
                if(x.role == 'konstrukter' || x.role == 'konstruktor')
                  workers += `<div class="mb-1 mr-2 color-builder" id="workers">`+x.name +" "+ x.surname+`</div>`;
                else if(x.role == 'strojnik')
                  workers += `<div class="mb-1 mr-2 color-mechanic" id="workers">`+x.name +" "+ x.surname+`</div>`;
                else if(x.role == 'električar')
                  workers += `<div class="mb-1 mr-2 color-electrican" id="workers">`+x.name +" "+ x.surname+`</div>`;
                else if(x.role == 'plc')
                  workers += `<div class="mb-1 mr-2 color-plc" id="workers">`+x.name +" "+ x.surname+`</div>`;
                else if(x.role == 'kuka')
                  workers += `<div class="mb-1 mr-2 color-kuka" id="workers">`+x.name +" "+ x.surname+`</div>`;
                else if(x.role == 'vodja')
                  workers += `<div class="mb-1 mr-2 font-weight-bold" id="workers">`+x.name +" "+ x.surname+`</div>`;
                else if(x.role == 'admin')
                  workers += `<div class="mb-1 mr-2 font-weight-bold font-italic" id="workers">`+x.name +" "+ x.surname+`</div>`;
                else
                  workers += `<div class="mb-1 mr-2" id="workers">`+x.name +" "+ x.surname+`</div>`;
              }
            }
            else{
              workers = `<div class="mb-1 mr-2" id="workers">Ni dodeljeno</div>`;
            }
            //debugger;
            var start = '/';
            var finish = '/';
            if(tasks[i].task_start)
              start = tasks[i].task_start.split("T")[0].split("-")[2] + "." + tasks[i].task_start.split("T")[0].split("-")[1] + "." + tasks[i].task_start.split("T")[0].split("-")[0];
            if(tasks[i].task_finish)
              finish = tasks[i].task_finish.split("T")[0].split("-")[2] + "." + tasks[i].task_finish.split("T")[0].split("-")[1] + "." + tasks[i].task_finish.split("T")[0].split("-")[0];
            var servis = "";
            var head = "";
            var expired = "";
            if(tasks[i].subscriberservis){
              servis = '<span class="badge badge-info ml-2">Servis</span>';
              head = `<div class="d-flex w-100 justify-content-between">
                <div class="h5 mb-1 font-italic" id="projectName">`+tasks[i].subscriberservis+`</div>
              </div>`;
            }
            else if(tasks[i].project_name){
              head = `<div class="d-flex w-100 justify-content-between">
                <div class="h5 mb-1 font-italic" id="projectName">`+tasks[i].project_name+`</div>
              </div>`;
            }
            if(tasks[i].expired)
              expired = '<span class="badge badge-danger ml-2">ZAMUDA</span>';
            var element = `<row class="list-group-item flex-column" id=`+tasks[i].id+`>
              `+head+`
              <div class="d-flex w-100 justify-content-between">
                <div class="h5 mb-1" id="taskName">`+tasks[i].task_name+`</div>
              </div>
              <div class="d-flex w-100 justify-content-start">
                <div class="h6 mb-1 mr-2" id="weekDate">`+ start +" - " + finish +`</div>
                ` + servis + expired + `
              </div>
              <div class="d-flex justify-content-start flex-wrap">
                `+workers+`
              </div>
            </row>`;
            $('#dailyList').append(element);
          }
          //if admin or leader change date info
          if(loggedUser.role == 'vodja' || loggedUser.role == 'admin'){
            if(!date)
              $('#header').text("Opravila danes");
            else
              $('#header').text("Opravila za dan: "+date.split("-")[2] + "." + date.split("-")[1] + "." + date.split("-")[0])
          }
          //calculate new hiddenScroll
          hiddenScroll = $('#dailyList').prop('scrollHeight') - $('#dailyList').height() + 15;
          $( "#dailyList" ).stop();
          $('#dailyList').animate({ 'scrollTop': '-='+$('#dailyList').scrollTop() }, 'slow');
          if(document.getElementById('customAnimationCheck').checked)
            animateList();
          //debugger;
          changeWeekTable(categoryTask);
        }
      })
    }
  });
}
function changeWeekTable(category){
  //debugger;
  /*
  $('#option0').removeClass('active');
  $('#option1').removeClass('active');
  $('#option2').removeClass('active');
  $('#option3').removeClass('active');
  $('#option4').removeClass('active');
  $('#option5').removeClass('active');
  $('#option6').removeClass('active');
  $('#option7').removeClass('active');
  setTimeout(()=>{
    $('#option'+category+'').addClass('active');
  })
  categoryTask = category;
  */
 date = $('#dateInput').val();
  $.get('/daily/week', {date, categoryTask, adminView}, function( resp ) {
    if(resp.success){
      var tasks = resp.data;
      $('#weeklyList').empty();
      for(var i = 0; i < tasks.length; i++){
        //old with category and subscriber info
        /*
        var element = `<row class="list-group-item flex-column" id=`+tasks[i].id+`>
          <div class="d-flex w-100 justify-content-between">
            <div class="h5 mb-1" id="taskName">`+tasks[i].task_name+`</div>
            <div id="taskCategory">`+tasks[i].category+`</div>
          </div>
          <div class="d-flex justify-content-between">
            <div class="mb-1" id="workers">`+tasks[i].workers+`</div>
            <div id="subscribers">`+tasks[i].subscriber+`</div>
          </div>
        </row>`;
        */
        var workers = ``;
        if(tasks[i].workersId[0]){
          for(var j = 0; j < tasks[i].workersId.length; j++){
            var x = allUsers.find(x => x.id == tasks[i].workersId[j]);
            if(x.role == 'konstrukter' || x.role == 'konstruktor')
              workers += `<div class="mb-1 mr-2 color-builder" id="workers">`+x.name +" "+ x.surname+`</div>`;
            else if(x.role == 'strojnik')
              workers += `<div class="mb-1 mr-2 color-mechanic" id="workers">`+x.name +" "+ x.surname+`</div>`;
            else if(x.role == 'električar')
              workers += `<div class="mb-1 mr-2 color-electrican" id="workers">`+x.name +" "+ x.surname+`</div>`;
            else if(x.role == 'plc')
              workers += `<div class="mb-1 mr-2 color-plc" id="workers">`+x.name +" "+ x.surname+`</div>`;
            else if(x.role == 'kuka')
              workers += `<div class="mb-1 mr-2 color-kuka" id="workers">`+x.name +" "+ x.surname+`</div>`;
            else if(x.role == 'vodja')
              workers += `<div class="mb-1 mr-2 font-weight-bold" id="workers">`+x.name +" "+ x.surname+`</div>`;
            else if(x.role == 'admin')
              workers += `<div class="mb-1 mr-2 font-weight-bold font-italic" id="workers">`+x.name +" "+ x.surname+`</div>`;
            else
              workers += `<div class="mb-1 mr-2" id="workers">`+x.name +" "+ x.surname+`</div>`;
          }
        }
        else{
          workers = `<div class="mb-1 mr-2" id="workers">Ni dodeljeno</div>`;
        }
        //debugger;
        var start = '/';
        var finish = '/';
        if(tasks[i].task_start)
          start = tasks[i].task_start.split("T")[0].split("-")[2] + "." + tasks[i].task_start.split("T")[0].split("-")[1] + "." + tasks[i].task_start.split("T")[0].split("-")[0];
        if(tasks[i].task_finish)
          finish = tasks[i].task_finish.split("T")[0].split("-")[2] + "." + tasks[i].task_finish.split("T")[0].split("-")[1] + "." + tasks[i].task_finish.split("T")[0].split("-")[0];
        var servis = "";
        var head = "";
        var expired = "";
        if(tasks[i].subscriberservis){
          servis = '<span class="badge badge-info ml-2">Servis</span>';
          head = `<div class="d-flex w-100 justify-content-between">
            <div class="h5 mb-1 font-italic" id="projectName">`+tasks[i].subscriberservis+`</div>
          </div>`;
        }
        else if(tasks[i].project_name){
          head = `<div class="d-flex w-100 justify-content-between">
            <div class="h5 mb-1 font-italic" id="projectName">`+tasks[i].project_name+`</div>
          </div>`;
        }
        if(tasks[i].expired)
          expired = '<span class="badge badge-danger ml-2">ZAMUDA</span>';
        var element = `<row class="list-group-item flex-column" id=`+tasks[i].id+`>
          `+head+`
          <div class="d-flex w-100 justify-content-between">
            <div class="h5 mb-1" id="taskName">`+tasks[i].task_name+`</div>
          </div>
          <div class="d-flex w-100 justify-content-start">
            <div class="h6 mb-1 mr-2" id="weekDate">`+ start +" - " + finish +`</div>
            ` + servis + expired + `
          </div>
          <div class="d-flex justify-content-start flex-wrap">
            `+workers+`
          </div>
        </row>`;
        //debugger
        var x = dailyTasks.find(d => d.id == tasks[i].id);
        if(!x){
          $('#weeklyList').append(element);
          //debugger;
        }
        //else
        //debugger;
      }
      //calculate new hiddenScroll
      hiddenScrollWeek = $('#weeklyList').prop('scrollHeight') - $('#weeklyList').height() + 15;
      $( "#weeklyList" ).stop();
      $('#weeklyList').animate({ 'scrollTop': '-='+$('#weeklyList').scrollTop() }, 'slow');
      if(document.getElementById('customAnimationCheck').checked)
        animateListWeek();
      //debugger;
      
    }
  });
}
function animateList(){
  $('#dailyList').animate({ 'scrollTop': '-=-'+hiddenScroll }, {
    duration: 50000,
    easing: 'linear', 
    complete: function() {
        $('#dailyList').animate({ 'scrollTop': '-='+hiddenScroll }, {
            duration: 1000, 
            complete: animateList});
    }});
}
function toggleAnimation(element){
  //debugger;
  if(element.checked){
    $('#dailyList').animate({ 'scrollTop': '-='+$('#dailyList').scrollTop() }, 'slow');
    animateList();
    $('#weeklyList').animate({ 'scrollTop': '-='+$('#weeklyList').scrollTop() }, 'slow');
    animateListWeek();
  }
  else{
    $( "#dailyList" ).stop();
    $( "#weeklyList" ).stop();
  }
}
function animateListWeek(){
  $('#weeklyList').animate({ 'scrollTop': '-=-'+hiddenScrollWeek }, {
    duration: 50000,
    easing: 'linear', 
    complete: function() {
        $('#weeklyList').animate({ 'scrollTop': '-='+hiddenScrollWeek }, {
            duration: 1000, 
            complete: animateListWeek});
    }});
}
function toggleAnimationWeek(element){
  //debugger;
  if(element.checked){
    $('#weeklyList').animate({ 'scrollTop': '-='+$('#weeklyList').scrollTop() }, 'slow');
    animateListWeek();
  }
  else{
    $( "#weeklyList" ).stop();
  }
}
function toggleAdmin(element){
  debugger;
  if(element.checked){
    adminView = true;
    $('#buttonCategoryGroup').empty();
    var x = `<div class="btn-toolbar mr-auto" role="toolbar" aria-label="Toolbar with button groups">
    <div class="btn-group btn-group-toggle mr-2 mt-2 flex-wrap" data-toggle="buttons" aria-label="Category group">
      <button class="btn btn-primary active" id="option0" type="radio" name="options" autocomplete="off" onclick="changeTaskTable(0)">Vsa</button>
      <button class="btn btn-purchase" id="option2" type="radio" name="options" autocomplete="off" onclick="changeTaskTable(2)">Nabava</button>
      <button class="btn btn-construction" id="option3" type="radio" name="options" autocomplete="off" onclick="changeTaskTable(3)">Konstrukcija</button>
      <button class="btn btn-mechanic" id="option4" type="radio" name="options" autocomplete="off" onclick="changeTaskTable(4)">Strojna izdelava</button>
      <button class="btn btn-electrican" id="option5" type="radio" name="options" autocomplete="off" onclick="changeTaskTable(5)">Elektro izdelava</button>
      <button class="btn btn-assembly" id="option6" type="radio" name="options" autocomplete="off" onclick="changeTaskTable(6)">Montaža</button>
      <button class="btn btn-programmer" id="option7" type="radio" name="options" autocomplete="off" onclick="changeTaskTable(7)">Programiranje</button>
      <button class="btn btn-dark" id="option1" type="radio" name="options" autocomplete="off" onclick="changeTaskTable(1)">Brez</button>
    </div>
  </div>`;
    $('#buttonCategoryGroup').append(x);
    $('#workerButton').empty();
    var y = '<button class="btn btn-primary" onclick="openWorkerTab()">Delavci</button>';
    $('#workerButton').append(y);
    changeTaskTable(0);
    changeWeekTable(0);
    debugger
  }
  else{
    adminView = false;
    $('#buttonCategoryGroup').empty();
    var x = `<div class="btn-toolbar mr-auto" role="toolbar" aria-label="Toolbar with button groups">
    <div class="btn-group btn-group-toggle mr-2 mt-2 flex-wrap" data-toggle="buttons" aria-label="Category group">
      <button class="btn btn-primary active" id="option0" type="radio" name="options" autocomplete="off" onclick="changeTaskTable(0)">Vsa</button>
      <button class="btn btn-mechanic" id="option4" type="radio" name="options" autocomplete="off" onclick="changeTaskTable(4)">Strojna izdelava</button>
      <button class="btn btn-electrican" id="option5" type="radio" name="options" autocomplete="off" onclick="changeTaskTable(5)">Elektro izdelava</button>
      <button class="btn btn-assembly" id="option6" type="radio" name="options" autocomplete="off" onclick="changeTaskTable(6)">Montaža</button>
    </div>
  </div>`;
    $('#buttonCategoryGroup').append(x);
    $('#workerButton').empty();
    var y = `<button class="btn btn-mechanic mr-2" onclick="openWorkerCategory(2)">Strojniki</button>
    <button class="btn btn-electrican mr-2" onclick="openWorkerCategory(3)">Električarji</button>
    <button class="btn btn-secondary mr-2" onclick="openWorkerCategory(0)">Ostali</button>`;
    $('#workerButton').append(y);
    changeTaskTable(0);
    changeWeekTable(0);
    debugger
  }
}
function openWorkerTab(){
  $('#categoryWorkerList').empty();
  $('#modalWorkerCategory').modal('toggle');
}
function openWorkerCategory(role){
  //debugger;
  var activeUser = 1;
  $.get('/employees/all', {activeUser}, function(resp){
    $('#categoryWorkerList').empty();
    allUsers = resp.data
    debugger;
    for(var i = 0; i < allUsers.length; i++){
      var element = `<div class="list-group-item btn btn-outline-secondary" 
      onclick="openWorkerTasks(`+allUsers[i].id+`)">`+allUsers[i].name+" "+allUsers[i].surname+`</div>`;
      if(allUsers[i].role == 'strojnik' && role == '2'){
        //debugger;
        element = `<div class="list-group-item btn btn-outline-mechanic" 
      onclick="openWorkerTasks(`+allUsers[i].id+`)">`+allUsers[i].name+" "+allUsers[i].surname+`</div>`;
        $('#categoryWorkerList').append(element);
      }
      else if(allUsers[i].role == 'električar' && role == '3'){
        //debugger;
        element = `<div class="list-group-item btn btn-outline-electrican" 
      onclick="openWorkerTasks(`+allUsers[i].id+`)">`+allUsers[i].name+" "+allUsers[i].surname+`</div>`;
        $('#categoryWorkerList').append(element);
      }
      else if((allUsers[i].role == 'konstrukter' || allUsers[i].role == 'konstruktor') && role == '1'){
        //debugger;
        element = `<div class="list-group-item btn btn-outline-success" 
      onclick="openWorkerTasks(`+allUsers[i].id+`)">`+allUsers[i].name+" "+allUsers[i].surname+`</div>`;
        $('#categoryWorkerList').append(element);
      }
      else if(allUsers[i].role == 'plc' && role == '4'){
        //debugger;
        element = `<div class="list-group-item btn btn-outline-plc" 
      onclick="openWorkerTasks(`+allUsers[i].id+`)">`+allUsers[i].name+" "+allUsers[i].surname+`</div>`;
        $('#categoryWorkerList').append(element);
      }
      else if(allUsers[i].role == 'kuka' && role == '5'){
        //debugger;
        element = `<div class="list-group-item btn btn-outline-warning" 
      onclick="openWorkerTasks(`+allUsers[i].id+`)">`+allUsers[i].name+" "+allUsers[i].surname+`</div>`;
        $('#categoryWorkerList').append(element);
      }
      else if((allUsers[i].role != 'admin' && allUsers[i].role != 'vodja' && allUsers[i].role != 'plc'
      && allUsers[i].role != 'kuka' && allUsers[i].role != 'konstruktor'
      && allUsers[i].role != 'konstrukter' && allUsers[i].role != 'strojnik'
      && allUsers[i].role != 'električar') && role == '0'){
        //debugger;
        $('#categoryWorkerList').append(element);
      }
    }
    if(adminView)
      $('#modalWorkerCategory').modal('toggle');
    $('#modalWorkerTab').modal('toggle');
  })
}
function openWorkerTasks(workerid){
  debugger;
  category = 0;
  $('#option0').removeClass('active');
  $('#option1').removeClass('active');
  $('#option2').removeClass('active');
  $('#option3').removeClass('active');
  $('#option4').removeClass('active');
  $('#option5').removeClass('active');
  $('#option6').removeClass('active');
  $('#option7').removeClass('active');
  setTimeout(()=>{
    $('#option'+category+'').addClass('active');
  })
  categoryTask = category;
  date = $('#dateInput').val();
  $.get('/daily/date', {date, categoryTask, adminView}, function( resp ) {
    if(resp.success){
      var tasks = resp.data;
      dailyTasks = tasks;
      $('#dailyList').empty();
      for(var i = 0; i < tasks.length; i++){
        //dailyTasksId.push(tasks[i].id);
        debugger;
        if(contains.call(tasks[i].workersId, workerid)){
          //old with category and subscriber info
          /*
          var element = `<row class="list-group-item flex-column" id=`+tasks[i].id+`>
            <div class="d-flex w-100 justify-content-between">
              <div class="h5 mb-1" id="taskName">`+tasks[i].task_name+`</div>
              <div id="taskCategory">`+tasks[i].category+`</div>
            </div>
            <div class="d-flex justify-content-between">
              <div class="mb-1" id="workers">`+tasks[i].workers+`</div>
              <div id="subscribers">`+tasks[i].subscriber+`</div>
            </div>
          </row>`;
          */
         var workers = ``;
          if(tasks[i].workersId[0]){
            for(var j = 0; j < tasks[i].workersId.length; j++){
              var x = allUsers.find(x => x.id == tasks[i].workersId[j]);
              if(x.role == 'konstrukter' || x.role == 'konstruktor')
                workers += `<div class="mb-1 mr-2 color-builder" id="workers">`+x.name +" "+ x.surname+`</div>`;
              else if(x.role == 'strojnik')
                workers += `<div class="mb-1 mr-2 color-mechanic" id="workers">`+x.name +" "+ x.surname+`</div>`;
              else if(x.role == 'električar')
                workers += `<div class="mb-1 mr-2 color-electrican" id="workers">`+x.name +" "+ x.surname+`</div>`;
              else if(x.role == 'plc')
                workers += `<div class="mb-1 mr-2 color-plc" id="workers">`+x.name +" "+ x.surname+`</div>`;
              else if(x.role == 'kuka')
                workers += `<div class="mb-1 mr-2 color-kuka" id="workers">`+x.name +" "+ x.surname+`</div>`;
              else if(x.role == 'vodja')
                workers += `<div class="mb-1 mr-2 font-weight-bold" id="workers">`+x.name +" "+ x.surname+`</div>`;
              else if(x.role == 'admin')
                workers += `<div class="mb-1 mr-2 font-weight-bold font-italic" id="workers">`+x.name +" "+ x.surname+`</div>`;
              else
                workers += `<div class="mb-1 mr-2" id="workers">`+x.name +" "+ x.surname+`</div>`;
            }
          }
          else{
            workers = `<div class="mb-1 mr-2" id="workers">Ni dodeljeno</div>`;
          }
          var start = '/';
          var finish = '/';
          if(tasks[i].task_start)
            start = tasks[i].task_start.split("T")[0].split("-")[2] + "." + tasks[i].task_start.split("T")[0].split("-")[1] + "." + tasks[i].task_start.split("T")[0].split("-")[0];
          if(tasks[i].task_finish)
            finish = tasks[i].task_finish.split("T")[0].split("-")[2] + "." + tasks[i].task_finish.split("T")[0].split("-")[1] + "." + tasks[i].task_finish.split("T")[0].split("-")[0];
          var servis = "";
          var head = "";
          var expired = "";
          if(tasks[i].subscriberservis){
            servis = '<span class="badge badge-info ml-2">Servis</span>';
            head = `<div class="d-flex w-100 justify-content-between">
              <div class="h5 mb-1 font-italic" id="projectName">`+tasks[i].subscriberservis+`</div>
            </div>`;
          }
          else if(tasks[i].project_name){
            head = `<div class="d-flex w-100 justify-content-between">
              <div class="h5 mb-1 font-italic" id="projectName">`+tasks[i].project_name+`</div>
            </div>`;
          }
          if(tasks[i].expired)
            expired = '<span class="badge badge-danger ml-2">ZAMUDA</span>';
          var element = `<row class="list-group-item flex-column" id=`+tasks[i].id+`>
            `+head+`
            <div class="d-flex w-100 justify-content-between">
              <div class="h5 mb-1" id="taskName">`+tasks[i].task_name+`</div>
            </div>
            <div class="d-flex w-100 justify-content-start">
              <div class="h6 mb-1 mr-2" id="weekDate">`+ start +" - " + finish +`</div>
              ` + servis + expired + `
            </div>
            <div class="d-flex justify-content-start flex-wrap">
              `+workers+`
            </div>
          </row>`;
          $('#dailyList').append(element);
          debugger;
        }
      }
      //if admin or leader change date info
      if(loggedUser.role == 'vodja' || loggedUser.role == 'admin'){
        if(date)
          $('#header').text("Opravila za dan: "+date.split("-")[2] + "." + date.split("-")[1] + "." + date.split("-")[0]);
        else
        $('#header').text("Opravila danes");
      }
      //calculate new hiddenScroll
      hiddenScroll = $('#dailyList').prop('scrollHeight') - $('#dailyList').height() + 15;
      $( "#dailyList" ).stop();
      $('#dailyList').animate({ 'scrollTop': '-='+$('#dailyList').scrollTop() }, 'slow');
      if(document.getElementById('customAnimationCheck').checked)
        animateList();
      //debugger;
      //same for week tasks
      $.get('/daily/week', {date, categoryTask, adminView}, function( resp ) {
        if(resp.success){
          var tasks = resp.data;
          $('#weeklyList').empty();
          for(var i = 0; i < tasks.length; i++){
            debugger;
            if(contains.call(tasks[i].workersId, workerid)){
              //old with category and subscriber info
              /*
              var element = `<row class="list-group-item flex-column" id=`+tasks[i].id+`>
                <div class="d-flex w-100 justify-content-between">
                  <div class="h5 mb-1" id="taskName">`+tasks[i].task_name+`</div>
                  <div id="taskCategory">`+tasks[i].category+`</div>
                </div>
                <div class="d-flex justify-content-between">
                  <div class="mb-1" id="workers">`+tasks[i].workers+`</div>
                  <div id="subscribers">`+tasks[i].subscriber+`</div>
                </div>
              </row>`;
              */
             var workers = ``;
              if(tasks[i].workersId[0]){
                for(var j = 0; j < tasks[i].workersId.length; j++){
                  var x = allUsers.find(x => x.id == tasks[i].workersId[j]);
                  if(x.role == 'konstrukter' || x.role == 'konstruktor')
                    workers += `<div class="mb-1 mr-2 color-builder" id="workers">`+x.name +" "+ x.surname+`</div>`;
                  else if(x.role == 'strojnik')
                    workers += `<div class="mb-1 mr-2 color-mechanic" id="workers">`+x.name +" "+ x.surname+`</div>`;
                  else if(x.role == 'električar')
                    workers += `<div class="mb-1 mr-2 color-electrican" id="workers">`+x.name +" "+ x.surname+`</div>`;
                  else if(x.role == 'plc')
                    workers += `<div class="mb-1 mr-2 color-plc" id="workers">`+x.name +" "+ x.surname+`</div>`;
                  else if(x.role == 'kuka')
                    workers += `<div class="mb-1 mr-2 color-kuka" id="workers">`+x.name +" "+ x.surname+`</div>`;
                  else if(x.role == 'vodja')
                    workers += `<div class="mb-1 mr-2 font-weight-bold" id="workers">`+x.name +" "+ x.surname+`</div>`;
                  else if(x.role == 'admin')
                    workers += `<div class="mb-1 mr-2 font-weight-bold font-italic" id="workers">`+x.name +" "+ x.surname+`</div>`;
                  else
                    workers += `<div class="mb-1 mr-2" id="workers">`+x.name +" "+ x.surname+`</div>`;
                }
              }
              else{
                workers = `<div class="mb-1 mr-2" id="workers">Ni dodeljeno</div>`;
              }
              var start = '/';
              var finish = '/';
              if(tasks[i].task_start)
                start = tasks[i].task_start.split("T")[0].split("-")[2] + "." + tasks[i].task_start.split("T")[0].split("-")[1] + "." + tasks[i].task_start.split("T")[0].split("-")[0];
              if(tasks[i].task_finish)
                finish = tasks[i].task_finish.split("T")[0].split("-")[2] + "." + tasks[i].task_finish.split("T")[0].split("-")[1] + "." + tasks[i].task_finish.split("T")[0].split("-")[0];
              var servis = "";
              var head = "";
              var expired = "";
              if(tasks[i].subscriberservis){
                servis = '<span class="badge badge-info ml-2">Servis</span>';
                head = `<div class="d-flex w-100 justify-content-between">
                  <div class="h5 mb-1 font-italic" id="projectName">`+tasks[i].subscriberservis+`</div>
                </div>`;
              }
              else if(tasks[i].project_name){
                head = `<div class="d-flex w-100 justify-content-between">
                  <div class="h5 mb-1 font-italic" id="projectName">`+tasks[i].project_name+`</div>
                </div>`;
              }
              if(tasks[i].expired)
                expired = '<span class="badge badge-danger ml-2">ZAMUDA</span>';
              var element = `<row class="list-group-item flex-column" id=`+tasks[i].id+`>
                `+head+`
                <div class="d-flex w-100 justify-content-between">
                  <div class="h5 mb-1" id="taskName">`+tasks[i].task_name+`</div>
                </div>
                <div class="d-flex w-100 justify-content-start">
                  <div class="h6 mb-1" id="weekDate">`+ start +" - " + finish +`</div>
                  ` + servis + expired + `
                </div>
                <div class="d-flex justify-content-start flex-wrap">
                  `+workers+`
                </div>
              </row>`;
              //$('#weeklyList').append(element);
              debugger;
              var x = dailyTasks.find(d => d.id == tasks[i].id);
              if(!x){
                $('#weeklyList').append(element);
                debugger;
              }
            }
          }
          //calculate new hiddenScroll
          hiddenScrollWeek = $('#weeklyList').prop('scrollHeight') - $('#weeklyList').height() + 15;
          $( "#weeklyList" ).stop();
          $('#weeklyList').animate({ 'scrollTop': '-='+$('#weeklyList').scrollTop() }, 'slow');
          if(document.getElementById('customAnimationCheck').checked)
            animateListWeek();
          //debugger;
          //same for week tasks
          
          $('#modalWorkerTab').modal('toggle');
        }
      });
      //$('#modalWorkerTab').modal('toggle');
    }
  });
}
var contains = function(needle) {
  // Per spec, the way to identify NaN is that it is not equal to itself
  var findNaN = needle !== needle;
  var indexOf;

  if(!findNaN && typeof Array.prototype.indexOf === 'function') {
      indexOf = Array.prototype.indexOf;
  } else {
      indexOf = function(needle) {
          var i = -1, index = -1;

          for(i = 0; i < this.length; i++) {
              var item = this[i];

              if((findNaN && item !== item) || item === needle) {
                  index = i;
                  break;
              }
          }
          return index;
      };
  }
  return indexOf.call(this, needle) > -1;
};
$(document).ready(function () {
  //Increment the idle time counter every minute.
  var idleInterval = setInterval(timerIncrement, 60000); // 1 minute
  
    //Zero the idle timer on mouse movement.
    $(this).mousemove(function (e) {
      //aktivnost
      idleTime = 0;
      //hide screensaver
      //$('#screensaver').fadeOut(100);
      if(screenSaver)
        showBackground();
    });
    $(this).keypress(function (e) {
      //aktivnost
      idleTime = 0;
      //hide screensaver
      if(screenSaver)
        showBackground();
      //$('#screensaver').fadeOut(100);
    });
});

function timerIncrement() {
    //console.log(idleTime);
    idleTime = idleTime + 1;
    if (idleTime > 14) { // 15 minutes
      //window.location.reload();
      //SHOW SCREENSAVER; image of roboteh
      showimage();
      screenSaver = true;
      changeTaskTable(0);
      if(idleTime > 500){
        window.location.reload();
      }
      /*
      setTimeout(()=>{
        idleTime = 0;
      })
      */
    }
    /*
    if(idleTime > 24) { // after 25 min reload page otherwise session gets aged out
      window.location.reload();
    }
    */
  }
function toggleFullScreen() {
  if (!document.fullscreenElement) {
    document.documentElement.requestFullscreen();
  } else {
    if (document.exitFullscreen) {
      document.exitFullscreen(); 
    }
  }
}
function showimage()
{
  $("body").css("background-image","url('/images/screensaver.png')"); // Onclick of button the background image of body will be test here. Give the image path in url
  $('#clickbutton').hide(); //This will hide the button specified in html
  $('#banner').hide();
  //$('#feedbackButton').hide()
  screenSaver = true;
}
function showBackground(){
  $("body").css("background-image","url('/images/qbkls.png')");
  $("body").css("background-repeat","repeat-xy");
  $('#banner').show();
  //$('#feedbackButton').show();
  changeTaskTable(0);
  screenSaver = false;
}
var loggedUser;
var categoryTask = 0;
var date = "";
var hiddenScroll;
var hiddenScrollWeek;
var selectedCategory = 0;
var adminView = false;
var allUsers;
var idleTime = 0;
var animationCheck = false;
var screenSaver = false;
var dailyTasks;
var allAbsences;