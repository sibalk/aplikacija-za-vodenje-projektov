$(function(){
  loggedUser = {name:$('#loggedUser').html().split(" (")[0].split(" ")[0], surname:$('#loggedUser').html().split(" (")[0].split(" ")[1], role:$('#loggedUser').html().split(" (")[1].split(")")[0]};
  if(loggedUser.role == 'admin' || loggedUser.role == 'vodja' || loggedUser.role == 'tajnik')
    permissionTag = true;
  
    //EDIT ABSENCE FORM
  $('#editAbsenceForm').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var name = absenceNameEdit.value;
    var start = absenceStartedit.value;
    var finish = absenceFinishedit.value;
    var reason = absenceReasonEdit.value;
    var id = activeAbsenceId;
    //var approved = false;

    start = start + " 07:00:00.000000";
    finish = finish + " 15:00:00.000000";
    debugger;
    $.post('/absence/update', {start, finish, reason, name, id}, function(data){
      debugger;
      if(data.success){
        start = new Date(start);
        finish = new Date(finish);
        var sd = start.getDate();
        var sm = start.getMonth()+1;
        var sy = start.getFullYear();
        var fd = finish.getDate();
        var fm = finish.getMonth()+1;
        var fy = finish.getFullYear();
        if(sd < 10) sd = '0'+sd;
        if(sm < 10) sm = '0'+sm;
        if(fd < 10) fd = '0'+fd;
        if(fm < 10) fm = '0'+fm;
        if(!data.reasonId){
          reason = allReasons.find(r=> r.id == absenceReasonEdit.value);
          reason = reason.text;
        }
        else{
          debugger;
          allReasons.push({id: data.reasonId, text: absenceReasonEdit.value})
          $(".select2-absence-reasonsedit").select2({
            data: allReasons,
            tags: permissionTag,
          });
          $(".select2-absence-reasonseNew").select2({
            data: allReasons,
            tags: permissionTag,
          });
          //$('#collapseAddAbsence').collapse("toggle");
          reason = absenceReasonEdit.value;
        }

        if(name)
          $('#absenceName'+id).html(name);
        else
          $('#absenceName'+id).html('(brez imena)');
        $('#absenceDate'+id).html(sd+'.'+sm+'.'+sy+" - "+fd+'.'+fm+'.'+fy);
        $('#absenceReason'+id).html(reason);
        
        debugger;
        //reset values
        //$('#collapseAddAbsence').collapse("toggle");
        toggleCollapse(id);
        $('#absenceNameEdit').val('');
        $('#absenceStartEdit').val('');
        $('#absenceFinishEdit').val('');
        $('#absenceReasonEdit').val(1).trigger('change');
      }
    })
  });
  
    //ADDING NEW ABSENCE FROM FORM
  $('#addAbsenceForm').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var name = absenceNameNew.value;
    var start = absenceStartNew.value;
    var finish = absenceFinishNew.value;
    var reason = absenceReasonNew.value;
    var urlParams = new URLSearchParams(window.location.search);
    var worker = urlParams.get('id');
    var approved = false;
    debugger;

    start = start + " 07:00:00.000000";
    finish = finish + " 15:00:00.000000";

    $.post('/absence/add', {start, finish, worker, reason, name, approved}, function(data){
      debugger;
      if(data.success){
        start = new Date(start);
        finish = new Date(finish);
        var sd = start.getDate();
        var sm = start.getMonth()+1;
        var sy = start.getFullYear();
        var fd = finish.getDate();
        var fm = finish.getMonth()+1;
        var fy = finish.getFullYear();
        if(sd < 10) sd = '0'+sd;
        if(sm < 10) sm = '0'+sm;
        if(fd < 10) fd = '0'+fd;
        if(fm < 10) fm = '0'+fm;
        if(!data.reasonId){
          reason = allReasons.find(r=> r.id == absenceReasonNew.value);
          reason = reason.text;
        }
        else{
          debugger;
          allReasons.push({id: data.reasonId, text: absenceReasonNew.value})
          $(".select2-absence-reasonsNew").select2({
            data: allReasons,
            tags: permissionTag,
          });
          //$('#collapseAddAbsence').collapse("toggle");
          reason = absenceReasonNew.value;
        }
        if(name == '')
          name = '(brez imena)';
        var approveButton = ''
          if(loggedUser.role == 'admin' || loggedUser.role == 'vodja' || loggedUser.role == 'tajnik')
          approveButton = '<button class="btn btn-success mt-2" onclick="approveAbsence('+data.absenceId+',1,1)" style="width:25px, height:5px, border:none"><img class="img-fluid" src="/approve.png" alt="ikona" style="width:15px"/></button>';
        var element= `<div class="list-group-item" id="absence`+data.absenceId+`">
          <div class="row">
            <div class="col-md-3 mt-2">
              <label id="absenceName`+data.absenceId+`">`+name+`</label>
            </div>
            <div class="col-md-3 mt-2">
              <label id="absenceProject`+data.absenceId+`">(brez projekta)</label>
            </div>
            <div class="col-md-2 mt-2">
              <label id="absenceDate`+data.absenceId+`">`+sd+'.'+sm+'.'+sy+' - '+fd+'.'+fm+'.'+fy+`</label>
            </div>
            <div class="col-md-2 mt-2">
              <label id="absenceReason`+data.absenceId+`">`+reason+`</label>
            </div>
            <div class="col-md-2" id="absenceControl`+data.absenceId+`">
              `+approveButton+`
              <button class="btn btn-primary ml-2 mt-2" onclick="toggleCollapse(`+data.absenceId+`)" style="width:25px, height:5px, border:none"><img class="img-fluid" src="/edit.png" alt="ikona" style="width:15px"/></button>
              <button class="btn btn-danger ml-2 mt-2" onclick="deleteAbsence(`+data.absenceId+`,0,0)" style="width:25px, height:5px, border:none"><img class="img-fluid" src="/delete.png" alt="ikona" style="width:15px"/></button>
            </div>
          </div>
          <div class="collapse multi-collapse task-collapse" id="collapseEditAbsence`+data.absenceId+`"></div>
        </div>`;
        $('#absenceList').prepend(element);
        debugger;
        //reset values
        $('#collapseAddAbsence').collapse("toggle");
        $('#absenceNameNew').val('');
        $('#absenceStartNew').val('');
        $('#absenceFinishNew').val('');
        $('#absenceReasonNew').val(1).trigger('change');
      }
    })
  })
})
function toggleAddAbsenceCollapse(){
  if(reasonsLoaded)
    $('#collapseAddAbsence').collapse("toggle");
  else{
    $.get( "/absence/reasons", function( data ) {
      allReasons = data.data;
      reasonsLoaded = true;
      $(".select2-absence-reasonsNew").select2({
        data: allReasons,
        tags: permissionTag,
      });
      $('#collapseAddAbsence').collapse("toggle");
    });
  }
}
function toggleCollapse(id){
  if(activeAbsenceId == 0){
    //no collapse is open, create collapse for id and open collapse
    if(reasonsLoaded)
      makeEditAbsenceCollapse(id);
    else{
      $.get( "/absence/reasons", function( data ) {
        allReasons = data.data;
        reasonsLoaded = true;
        makeEditAbsenceCollapse(id);
      });
    }
    activeAbsenceId = id;
    editCollapseOpen = true;
    $('#collapseEditAbsence'+id).collapse("toggle");

  }
  else{
    //collapse is open, close collaspe and delete its elements
    // if id == activeAbsenceId then just close collapse
    if(id == activeAbsenceId){
      $('#collapseEditAbsence'+id).collapse("toggle");
      if(editCollapseOpen)
        editCollapseOpen = false;
      else
        editCollapseOpen = true;
    }
    else{
      if(editCollapseOpen)
        $('#collapseEditAbsence'+activeAbsenceId).collapse("toggle");
      $('#collapseEditAbsence'+activeAbsenceId).empty();
      makeEditAbsenceCollapse(id);
      activeAbsenceId = id;
      editCollapseOpen = true;
      $('#collapseEditAbsence'+id).collapse("toggle");
    }
  }
}
function makeEditAbsenceCollapse(id){
  var element = `
    <div class="form-group"><label class="mt-3 mx-2" for="absenceNameEdit" style="width:98%;">Ime odsotnosti<input class="form-control" id="absenceNameEdit" type="text" name="absenceNameEdit" required=""/></label></div>
    <div class="form-group">
      <label class="mx-2" for="absenceStartEdit" style="width:98%;">Za&ccaron;etek odsotnosti
        <input class="form-control" id="absenceStartEdit" type="date" name="absenceStartEdit" required=""/>
        <div class="invalid-feedback">Datuma nista skladna!!!</div>
      </label>
    </div>
    <div class="form-group">
      <label class="mx-2" for="absenceFinishEdit" style="width:98%;">Konec odsotnosti
        <input class="form-control" id="absenceFinishEdit" type="date" name="absenceFinishEdit" required=""/>
        <div class="invalid-feedback">Datuma nista skladna!!!</div>
      </label>
    </div>
    <div class="form-group"><label class="mx-2" for="absenceReasonEdit" style="width:98%;">Razlog<select class="custom-select mr-sm-2 form-control select2-absence-reasonsEdit" id="absenceReasonEdit" name="absenceReasonEdit" style="width:100%;"></select></label></div>
    <div class="d-flex justify-content-center"><button class="btn btn-success mb-3" onclick="updateAbsence(`+id+`)" style="width:25px, height:5px, border:none">Posodobi</button></div>
  `;
  $('#collapseEditAbsence'+id).append(element);
  $(".select2-absence-reasonsEdit").select2({
    data: allReasons,
    tags: permissionTag,
  });
  //put correct values into collapse
  var name = $('#absenceName'+id).html();
  var start = $('#absenceDate'+id).html().split(' - ')[0].split('.');
  var finish = $('#absenceDate'+id).html().split(' - ')[1].split('.');
  if(name != '(brez imena)')
    $('#absenceNameEdit').val(name);
  $('#absenceStartEdit').val(start[2]+'-'+start[1]+'-'+start[0]);
  $('#absenceFinishEdit').val(finish[2]+'-'+finish[1]+'-'+finish[0]);
  var reason = allReasons.find(r=> r.text == $('#absenceReason'+id).html());
  $('#absenceReasonEdit').val(reason.id).trigger('change');
}
function updateAbsence(id){
  
  debugger;

  var name = $('#absenceNameEdit').val();
  var start = $('#absenceStartEdit').val();
  var finish = $('#absenceFinishEdit').val();
  var reason = $('#absenceReasonEdit').val();
  //var approved = false;

  //check if dates are correct (start <= finish)
  if(new Date(start) > new Date(finish)){
    $('#absenceStartEdit').addClass('is-invalid');
    $('#absenceFinishEdit').addClass('is-invalid');
  }
  else{
    start = start + " 07:00:00.000000";
    finish = finish + " 15:00:00.000000";
    debugger;
    $.post('/absence/update', {start, finish, reason, name, id}, function(data){
      debugger;
      if(data.success){
        if(!data.reasonId){
          reason = allReasons.find(r=> r.id == absenceReasonEdit.value);
          reason = reason.text;
        }
        else{
          debugger;
          allReasons.push({id: data.reasonId, text: absenceReasonEdit.value})
          $(".select2-absence-reasonsedit").select2({
            data: allReasons,
            tags: permissionTag,
          });
          $(".select2-absence-reasonseNew").select2({
            data: allReasons,
            tags: permissionTag,
          });
          //$('#collapseAddAbsence').collapse("toggle");
          reason = absenceReasonEdit.value;
        }
  
        if(name)
          $('#absenceName'+id).html(name);
        else
          $('#absenceName'+id).html('(brez imena)');
        $('#absenceDate'+id).html($('#absenceStartEdit').val().split('-')[2]+"."+$('#absenceStartEdit').val().split('-')[1]+"."+$('#absenceStartEdit').val().split('-')[0]+
        " - "+
        $('#absenceFinishEdit').val().split('-')[2]+"."+$('#absenceFinishEdit').val().split('-')[1]+"."+$('#absenceFinishEdit').val().split('-')[0] );
        $('#absenceReason'+id).html(reason);
        
        debugger;
        //reset values
        //$('#collapseAddAbsence').collapse("toggle");
        $('#absenceStartEdit').removeClass('is-invalid');
        $('#absenceFinishEdit').removeClass('is-invalid');
        toggleCollapse(id);
        //$('#absenceNameEdit').val('');
        //$('#absenceStartEdit').val('');
        //$('#absenceFinishEdit').val('');
        //$('#absenceReasonEdit').val(1).trigger('change');
      }
    })
  }
}
function approveAbsence(id, newAbsenceApproveStatus, absenceActiveStatus){
  debugger;
  $.post('/absence/approve', {id, newAbsenceApproveStatus}, function(data){
    debugger;
    if(data.success){
      //absence is on deleted list, update approve button
      if(absenceActiveStatus == 0){
        $('#absenceControl'+id).empty();
        var approveButton = '<button class="btn btn-success mt-2" onclick="approveAbsence('+id+',1,0)" style="width:25px, height:5px, border:none"><img class="img-fluid" src="/approve.png" alt="ikona" style="width:15px"/></button>';
        if(newAbsenceApproveStatus == 1)
          approveButton = '<button class="btn btn-success mt-2" onclick="approveAbsence('+id+',0,0)" style="width:25px, height:5px, border:none"><img class="img-fluid" src="/remove.png" alt="ikona" style="width:15px"/></button>';
        
        var buttons = approveButton+
          `<button class="btn btn-primary ml-2 mt-2" onclick="toggleCollapse(`+id+`)" style="width:25px, height:5px, border:none"><img class="img-fluid" src="/edit.png" alt="ikona" style="width:15px"/></button>
          <button class="btn btn-danger ml-2 mt-2" onclick="deleteAbsence(`+id+`,1,`+newAbsenceApproveStatus+`)" style="width:25px, height:5px, border:none"><img class="img-fluid" src="/undelete1.png" alt="ikona" style="width:15px"/></button>`;
        $('#absenceControl'+id).append(buttons);
      }
      //absence is either on unapproved list or approved list, remove and add to other
      else{
        var name = $('#absenceName'+id).html();
        var project = $('#absenceProject'+id).html();
        var date = $('#absenceDate'+id).html();
        var reason = $('#absenceReason'+id).html();
        $('#absence'+id).remove();
        var approveButton;
        var deleteButton = '<button class="btn btn-danger ml-2 mt-2" onclick="deleteAbsence('+id+',0,'+newAbsenceApproveStatus+')" style="width:25px, height:5px, border:none"><img class="img-fluid" src="/delete.png" alt="ikona" style="width:15px"/></button>';
        if(newAbsenceApproveStatus == 1)
          approveButton = '<button class="btn btn-success mt-2" onclick="approveAbsence('+id+',0,1)" style="width:25px, height:5px, border:none"><img class="img-fluid" src="/remove.png" alt="ikona" style="width:15px"/></button>';
        else
          approveButton = '<button class="btn btn-success mt-2" onclick="approveAbsence('+id+',1,1)" style="width:25px, height:5px, border:none"><img class="img-fluid" src="/approve.png" alt="ikona" style="width:15px"/></button>';
        var element = `<div class="list-group-item" id="absence`+id+`">
          <div class="row">
            <div class="col-md-3 mt-2">
              <label id="absenceName`+id+`">`+name+`</label>
            </div>
            <div class="col-md-3 mt-2">
              <label id="absenceProject`+id+`">`+project+`</label>
            </div>
            <div class="col-md-2 mt-2">
              <label id="absenceDate`+id+`">`+date+`</label>
            </div>
            <div class="col-md-2 mt-2">
              <label id="absenceReason`+id+`">`+reason+`</label>
            </div>
            <div class="col-md-2" id="absenceControl`+id+`">
              `+approveButton+`
              <button class="btn btn-primary ml-2 mt-2" onclick="toggleCollapse(`+id+`)" style="width:25px, height:5px, border:none"><img class="img-fluid" src="/edit.png" alt="ikona" style="width:15px"/></button>
              `+deleteButton+`
            </div>
          </div>
          <div class="collapse multi-collapse task-collapse" id="collapseEditAbsence`+id+`"></div>
        </div>`;
        if(newAbsenceApproveStatus == 0)
          $('#absenceList').prepend(element);
        else
          $('#approvedAbsenceList').prepend(element);
      }
    }
  })
}
function deleteAbsence(id, newAbsenceActiveStatus, absenceApproveStatus){
  debugger;
  $.post('/absence/delete', {id, newAbsenceActiveStatus}, function(data){
    debugger;
    if(data.success){
      var name = $('#absenceName'+id).html();
      var project = $('#absenceProject'+id).html();
      var date = $('#absenceDate'+id).html();
      var reason = $('#absenceReason'+id).html();
      $('#absence'+id).remove();
      var approveButton;
      var deleteButton;
      if(absenceApproveStatus == 1)
        approveButton = '<button class="btn btn-success mt-2" onclick="approveAbsence('+id+',0,'+newAbsenceActiveStatus+')" style="width:25px, height:5px, border:none"><img class="img-fluid" src="/remove.png" alt="ikona" style="width:15px"/></button>';
      else
        approveButton = '<button class="btn btn-success mt-2" onclick="approveAbsence('+id+',1,'+newAbsenceActiveStatus+')" style="width:25px, height:5px, border:none"><img class="img-fluid" src="/approve.png" alt="ikona" style="width:15px"/></button>';
      if(newAbsenceActiveStatus == 1)
        deleteButton = '<button class="btn btn-danger ml-2 mt-2" onclick="deleteAbsence('+id+',0,'+absenceApproveStatus+')" style="width:25px, height:5px, border:none"><img class="img-fluid" src="/delete.png" alt="ikona" style="width:15px"/></button>';
      else
        deleteButton = '<button class="btn btn-danger ml-2 mt-2" onclick="deleteAbsence('+id+',1,'+absenceApproveStatus+')" style="width:25px, height:5px, border:none"><img class="img-fluid" src="/undelete1.png" alt="ikona" style="width:15px"/></button>';
      var element = `<div class="list-group-item" id="absence`+id+`">
        <div class="row">
          <div class="col-md-3 mt-2">
            <label id="absenceName`+id+`">`+name+`</label>
          </div>
          <div class="col-md-3 mt-2">
            <label id="absenceProject`+id+`">`+project+`</label>
          </div>
          <div class="col-md-2 mt-2">
            <label id="absenceDate`+id+`">`+date+`</label>
          </div>
          <div class="col-md-2 mt-2">
            <label id="absenceReason`+id+`">`+reason+`</label>
          </div>
          <div class="col-md-2" id="absenceControl`+id+`">
            `+approveButton+`
            <button class="btn btn-primary ml-2 mt-2" onclick="toggleCollapse(`+id+`)" style="width:25px, height:5px, border:none"><img class="img-fluid" src="/edit.png" alt="ikona" style="width:15px"/></button>
            `+deleteButton+`
          </div>
        </div>
        <div class="collapse multi-collapse task-collapse" id="collapseEditAbsence`+id+`"></div>
      </div>`;
      if(newAbsenceActiveStatus == 0)
        $('#deletedAbsenceList').prepend(element);
      else{
        if(absenceApproveStatus == 1)
          $('#approvedAbsenceList').prepend(element);
        else
          $('#absenceList').prepend(element);
      }
    }
  })
}
function onStartDateNewChange(){
  var start = new Date($('#absenceStartNew').val());
  var finish = new Date($('#absenceFinishNew').val());

  if(start && finish){
    if(start > finish)
      $('#absenceStartNew').val($('#absenceFinishNew').val());
  }
}
function onFinishtDateNewChange(){
  var start = new Date($('#absenceStartNew').val());
  var finish = new Date($('#absenceFinishNew').val());

  if(start && finish){
    if(start > finish)
      $('#absenceFinishNew').val($('#absenceStartNew').val());
  }
}
var activeAbsenceId = 0;
var editCollapseOpen = false;
var reasonsLoaded = false;
var allReasons;
var loggedUser;
var permissionTag = false;
$('#absenceStartNew').on('change', onStartDateNewChange);
$('#absenceFinishNew').on('change', onFinishtDateNewChange);