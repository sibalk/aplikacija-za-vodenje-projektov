$(function(){
  loggedUser = { name: $('#loggedUser').html().split(" (")[0].split(" ")[0], surname: $('#loggedUser').html().split(" (")[0].split(" ")[1], role: $('#loggedUser').html().split(" (")[1].split(")")[0] };
  if($('#msg').html() != ''){
    $("#modalMsg").modal();
  }
  debugger;
  $.get("/subscribers/allinfo", function( data ) {
    //console.log(data)
    allSubscribers = data.data;
    //console.log(dataGet[2]);

    tableSubscribers = $('#subscribersTable').DataTable( {
      data: allSubscribers,
      columns: [
          { title: "subscribersId", data: "id" },
          { title: "Naziv", data: "name" },
          { title: "Ikona", data: "icon" },
          { title: "Slika ikone", data: "icon",
          render: function ( data, type, row ) {
            if(data)
              return '<img class="img-fluid" src="/uploads/images/'+data+'" alt="ikona" style="width:50px" />';
            else
              return '';
          }},
          { data: "id",
          render: function ( data, type, row ) {
            if(loggedUser.role == 'admin' || loggedUser.role == 'vodja')
              return '<button class="btn btn-primary" onclick="editSubscriber('+data+')">Uredi</button>';
            else return '<div></div>';
          }},
      ],
      "language": {
        "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Slovenian.json"
      },
      order: [[1, "asc"]],
      columnDefs: [
        {
          targets: [0],
          visible: false,
          searchable: false
        },
      ],
      //responsive: true,
      //info: false,
      //searching: false,
      scrollY: "350px",
      paging: false,
    });
  });
})
function disableFunction(){
  $('#btnAddDoc').attr('disabled','disabled');
  $('#btnAddSub').attr('disabled','disabled');
  $('#btnEditSub').attr('disabled','disabled');
}
function newSubscriberModal(){
  debugger
  $("#modalSubscriberAddForm").modal();
}
function newDocumentModal(){
  debugger
  $("#modalDocumentAddForm").modal();
}
function editSubscriber(id){
  $('#subscriberIdEdit').val(id);
  var sub = allSubscribers.find(a => a.id == id);
  $('#subscriberNameEdit').val(sub.name);
  debugger
  $("#modalSubscriberEditForm").modal();
}
var loggedUser;
var allSubscribers;
var tableSubscribers;