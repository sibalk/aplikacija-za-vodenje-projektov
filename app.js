var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
//var multer = require('multer');
//var session = require('express-session');
var session = require('cookie-session');
var helmet = require('helmet');
var favicon = require('serve-favicon');
//var childProcess = require('child_process');
var bodyParser = require('body-parser')
var shell = require('shelljs');
var fs = require('fs');
var uglifyJS = require("uglify-es");
var uglifycss = require('uglifycss');
require('dotenv').config()
var bitbucketUsername = 'sibalk';

//UGLIFY
//JS
//daily
var dailyUglify = uglifyJS.minify({
  'daily.js': fs.readFileSync('public/javascripts/daily/daily.js', 'utf-8')
});
fs.writeFile('public/javascripts/daily/daily.min.js', dailyUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "daily.min.js".');
});
//dashboard
var dashboardUglify = uglifyJS.minify({
  'dashboardChanges.js': fs.readFileSync('public/javascripts/changes/dashboardChanges.js', 'utf-8')
});
fs.writeFile('public/javascripts/changes/dashboard.min.js', dashboardUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "dashboard.min.js".');
});
//employee
var employeeUglify = uglifyJS.minify({
  'change.js': fs.readFileSync('public/javascripts/employee/change.js', 'utf-8'),
  'projects.js': fs.readFileSync('public/javascripts/employee/projects.js', 'utf-8'),
  'userProjectChanges.js': fs.readFileSync('public/javascripts/changes/userProjectChanges.js', 'utf-8')
});
fs.writeFile('public/javascripts/employee/employee.min.js', employeeUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "employee.min.js".');
});
//employees
var employeesUglify = uglifyJS.minify({
  'manage.js': fs.readFileSync('public/javascripts/employee/manage.js', 'utf-8'),
  'employeesChanges.js': fs.readFileSync('public/javascripts/changes/employeesChanges.js', 'utf-8')
});
fs.writeFile('public/javascripts/employee/employees.min.js', employeesUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "employees.min.js".');
});
//employeeTasks (project_tasks.pug)
var employeeTasksUglify = uglifyJS.minify({
  'projectTasks.js': fs.readFileSync('public/javascripts/employee/projectTasks.js', 'utf-8'),
  'change.js': fs.readFileSync('public/javascripts/employee/change.js', 'utf-8'),
  'tasksChanges.js': fs.readFileSync('public/javascripts/changes/tasksChanges.js', 'utf-8')
});
fs.writeFile('public/javascripts/employee/employeeTasks.min.js', employeeTasksUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "employees.min.js".');
});
//project
//'projectChanges.js': fs.readFileSync('public/javascripts/changes/projectChanges.js', 'utf-8')
var projectUglify = uglifyJS.minify({
  'project.js': fs.readFileSync('public/javascripts/project/project.js', 'utf-8'),
  'tasks.js': fs.readFileSync('public/javascripts/project/tasks.js', 'utf-8'),
  'workers.js': fs.readFileSync('public/javascripts/project/workers.js', 'utf-8'),
  'absences.js': fs.readFileSync('public/javascripts/project/absences.js', 'utf-8'),
  'subtasks.js': fs.readFileSync('public/javascripts/project/subtasks.js', 'utf-8'),
  'files.js': fs.readFileSync('public/javascripts/project/files.js', 'utf-8'),
});
fs.writeFile('public/javascripts/project/project.min.js', projectUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "project.min.js".');
});
//projects
var projectsUglify = uglifyJS.minify({
  'projects.js': fs.readFileSync('public/javascripts/project/projects.js', 'utf-8')
});
fs.writeFile('public/javascripts/project/projects.min.js', projectsUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "projects.min.js".');
});
//servis
var servisUglify = uglifyJS.minify({
  'servis.js': fs.readFileSync('public/javascripts/servis/servis.js', 'utf-8')
});
fs.writeFile('public/javascripts/servis/servis.min.js', servisUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "servis.min.js".');
});
//subscribers
var subscribersUglify = uglifyJS.minify({
  'manageSubscribers.js': fs.readFileSync('public/javascripts/subscriber/manageSubscribers.js', 'utf-8')
});
fs.writeFile('public/javascripts/subscriber/subscribers.min.js', subscribersUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "subscribers.min.js".');
});
//tasks
var tasksUglify = uglifyJS.minify({
  'tasks.js': fs.readFileSync('public/javascripts/employee/tasks.js', 'utf-8'),
  'change.js': fs.readFileSync('public/javascripts/employee/change.js', 'utf-8'),
  'tasksChanges.js': fs.readFileSync('public/javascripts/changes/tasksChanges.js', 'utf-8')
});
fs.writeFile('public/javascripts/employee/tasks.min.js', tasksUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "tasks.min.js".');
});
//subscribers
var vizUglify = uglifyJS.minify({
  'viz.js': fs.readFileSync('public/javascripts/viz/viz.js', 'utf-8')
});
fs.writeFile('public/javascripts/viz/viz.min.js', vizUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "viz.min.js".');
});

//CSS
var stil = uglifycss.processFiles(
  [ 'public/stylesheets/style.css'],
  { maxLineLen: 500, expandVars: true }
);
fs.writeFile('public/stylesheets/style.min.css', stil, function(napaka) {
  if (napaka)
    console.log(napaka);
  else
    console.log('Skripta je zgenerirana in shranjena v "style.min.css".');
});


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var employeesRouter = require('./routes/employees');
var myRouter = require('./routes/my_projects');
var projectRouter = require('./routes/projects');
//var projectxRouter = require('./routes/projects');
var loginRouter = require('./routes/login');
var dashboardRouter = require('./routes/dashboard');
var subscribersRouter = require('./routes/subscribers');
var feedbackRouter = require('./routes/feedback');
var dailyRouter = require('./routes/daily');
var servisRouter = require('./routes/servis');
var vizRouter = require('./routes/viz');
var changesRouter = require('./routes/changes');
var absenceRouter = require('./routes/absence');

var app = express();
var db = require('./controllers/db');
//var auth = require('./controllers/authentication');
// create application/json parser
var jsonParser = bodyParser.json()

// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(helmet());

//send back pdf --TEST--
/*
app.get('/sendMeDoc', function(req, res){
  console.log('send him doc');
  var filePath = '/public/uploads/documents/1562059866128-Projekt Čisto novi projekt.pdf';

  fs.readFile(__dirname + filePath, function(err,data){
    console.log(__dirname + filePath);
    console.log(data);
    res.contentType('application/pdf');
    res.send(data);
  })

})
*/
app.get('/sendMeDoc', function(req, res){
  //console.log('send him doc');
  //console.log(req.query.filename);
  let filename = req.query.filename;
  let projectId = req.query.projectId;
  console.log('Zahteva za datoteko: '+ filename);
  //var filePath = '/public/uploads/documents/1562059866128-Projekt Čisto novi projekt.pdf';
  var file = `${__dirname}/public/uploads/documents/${filename}`;
  if (fs.existsSync(file)) {
    // Do something
    res.download(file, function(err){
      if(err){
        console.error(err);
      }
    });
  }
  else{
    db.updateActiveFile(filename)
      .then(file=>{
        console.log("Datoteka "+filename+" ne obstaja več, aktivnost v bazi popravljena.")
        let msg = "Napaka: Datoteka ne obstaja več!";
        let type = 1;
        res.redirect('/projects/id?id='+projectId+'&msg='+msg+'&type='+type);
      })
      .catch((e)=>{
        console.log(e);
      })
  }
  
  //return
  //file exists
})


app.post("/webhooks/bitbucket", jsonParser, function(req, res){
  if(!req.body) return res.sendStatus(400);
  console.log("Webhook received!");
  //console.log(req.body);
  console.log("Repo is " + req.body.repository.name);
  var sender = req.body.actor.nickname;
  console.log("User is " + req.body.actor.nickname);
  //console.log("Branch that is commited is " + req.body.push.changes.commit.new.name);
  
  req.body.push.changes.forEach(function (commit) {
      console.log("Branch/Tag is " + commit.new.name);
      var branch = commit.new.name;
      console.log("Type is " + commit.new.type);
      if(branch === 'master' && sender === bitbucketUsername){
        console.log("BRANCH and USERNAME are matching, lets pull/update the code");
        deploy(res);
      }
      else{
        console.log("BRANCH and USERNAME ARE NOT MATCHING");
        res.sendStatus(500);
      }
  });
})
function deploy(res){
  
  shell.exec('echo "Pulling from master"');
  shell.exec('git pull');
  shell.exec('echo "Installing new packages if there are new"');
  shell.exec('npm install')
  shell.exec('echo "About to restart process"');
  shell.exec('pm2 start www', function(code, stdout, stderr){
    console.log('Exit code' + code);
    console.log('Program output' + stdout);
    console.log('Program stderr:' + stderr);
    res.sendStatus(500);
  });
  res.sendStatus(200);
}

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'icons')));
app.use('/scripts', express.static(__dirname + '/timelines-chart/dist/'));

/*
app.use(function(req, res, next) {
  res.setHeader('X-Frame-Options', 'DENY');
  res.setHeader('X-XSS-Protection', '1; mode=block');
  res.setHeader('X-Content-Type-Options', 'nosniff');
  next();
});
*/

app.use(favicon(path.join(__dirname, 'icons', 'icon.png')))
//app.use(favicon(__dirname + '/icons/favicon.ico')); 

app.use(session({
  name: 'session',
  secret: process.env.APP_SECRET,
  maxAge: 9*60*60*1000
}))
//Extending the session expiration
app.use(function (req, res, next) {
  req.session.nowInMinutes = Math.floor(Date.now() / 60e3)
  next()
})

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/dashboard', dashboardRouter);
app.use('/employees', employeesRouter);
app.use('/my', myRouter);
app.use('/projects', projectRouter);
//app.use('/projectsx', projectxRouter);
app.use('/subscribers', subscribersRouter);
app.use('/feedback', feedbackRouter);
app.use('/', loginRouter);
app.use('/daily', dailyRouter);
app.use('/servis', servisRouter);
app.use('/viz', vizRouter);
app.use('/changes', changesRouter);
app.use('/absence', absenceRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
