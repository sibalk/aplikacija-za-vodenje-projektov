var express = require('express');
var router = express.Router();

//auth & db
var auth = require('../controllers/authentication');
var db = require('../controllers/db');

//FEEDBACK PAGE (ODZIVI)
router.get('/', auth.authenticate, function(req, res){
  res.render('feedback', {
    title: "Seznam odzivov",
    user_name: req.session.user_name,
    user_surname: req.session.user_surname,
    user_username: req.session.user_username,
    user_typeid: req.session.role_id,
    user_type: req.session.type,
    user_id: req.session.user_id
  });
});

//GET ALL FEEDBACKS
router.get('/all', auth.authenticate, function(req, res){
  db.getFeedback().then(feedbacks=>{
      return res.json({success:true, data:feedbacks});
    })
    .catch((e)=>{
      console.log(e);
    })
})
router.post('/post', auth.authenticate, function(req, res){
  let fb = req.body.feedback;
  let op = req.body.optional;
  db.createFeedback(fb, op).then(feedback =>{
      return res.end("success");
    })
    .catch((e)=>{
      console.log(e);
    })
})
router.post('/handle', auth.authenticate, function(req, res){
  let feedbackId = req.body.feedbackId;
  let handled = req.body.handled;
  db.handleFeedback(feedbackId, handled).then(fb=>{
    return res.end("success");
  })
  .catch((e)=>{
    console.log(e);
  })
})

module.exports = router;