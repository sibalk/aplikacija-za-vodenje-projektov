var express = require('express');
var router = express.Router();
//var ctrlDashboard = require('../controllers/dashboard');

//GET Dasboard
//router.get('/', ctrlDashboard.list);

var auth = require('../controllers/authentication');

router.get('/', auth.authenticate, function(req, res, next){
  //console.log(req);
  res.render('dashboard', {
    title: "Osnovna plošča",
    user_name: req.session.user_name,
    user_surname: req.session.user_surname,
    user_username: req.session.user_username,
    user_typeid: req.session.role_id,
    user_type: req.session.type,
    user_id: req.session.user_id,
  })
})

module.exports = router;
