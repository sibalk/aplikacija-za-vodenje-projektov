var express = require('express');
var router = express.Router();

//auth & db
var auth = require('../controllers/authentication');
var db = require('../controllers/db');

///MAIN PAGE --> VIZUALIZATION
router.get('/', auth.authenticate, function(req, res){
  res.render('viz', {
    title: "Vizualizacija",
    user_name: req.session.user_name,
    user_surname: req.session.user_surname,
    user_username: req.session.user_username,
    user_typeid: req.session.role_id,
    user_type: req.session.type,
    user_id: req.session.user_id
  });
});

//get all projects for selec2 (id,text)
router.get('/projects', auth.authenticate, function(req, res){
  db.getProjectsSelect().then(projects=>{
      res.json({success:true, data:projects});
    })
    .catch((e)=>{
      console.log(e);
    })
})

module.exports = router;