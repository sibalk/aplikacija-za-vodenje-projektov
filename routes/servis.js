var express = require('express');
var router = express.Router();

//auth & db
var auth = require('../controllers/authentication');
var db = require('../controllers/db');

//SERVISI
router.get('/', auth.authenticate, function(req, res) {
  res.render('servis', {
    title: "Seznam servisov",
    user_name: req.session.user_name,
    user_surname: req.session.user_surname,
    user_username: req.session.user_username,
    user_typeid: req.session.role_id,
    user_type: req.session.type,
    user_id: req.session.user_id,
    servisi: [],
  })
  //console.log(req.session);
})
//Get project tasks
router.get('/all', auth.authenticate, function(req, res){
  let category = req.query.categoryTask;
  let active = req.query.activeTask;
  //console.log(category);
  //console.log(active);
  //category == 0 ==> brez
  // active == 1 ==> vsa
  db.getServices(parseInt(category), parseInt(active)).then(tasks=>{
      res.json({success:true, data:tasks});
    })
    .catch((e)=>{
      console.log(e);
    })
})
//Add servis in tasks table (through modal)
router.post('/add', auth.authenticate, function(req, res){
  //console.log("iz modalnega");
  let subscriberId = req.body.servisSubscriber;
  let servisName = req.body.servisName;
  let duration = req.body.servisDuration;
  let start = req.body.servisStart;
  let finish = req.body.servisFinish;
  let workersId = req.body.servisAssignmentArrayString;
  let category = req.body.servisCategory;
  let priority = req.body.servisPriority;
  let servisAssignmentArray = workersId.split(",");
  if(servisAssignmentArray[0] == "")
    servisAssignmentArray.pop();
  //send to db
  db.addServis(subscriberId, servisName, duration, start, finish, category, priority).then(servis=>{
    if(servisAssignmentArray.length > 0){
      //console.log(servis);
      for(let i = 0; i < servisAssignmentArray.length; i++){
        db.assignTask(servis.id, servisAssignmentArray[i]).then(assignedTask=>{
          //return res.end(servis.id);
          //return res.json({success:true, data:servis.id});
        })
        .catch((e)=>{
          console.log(e);
        })
      }
    }
    //return res.end("success");
    return res.json({success:true, data:servis.id});
  })
})

//update the task
router.post('/update', auth.authenticate, function(req, res){
  //let projectId = req.body.projectId;
  let taskId = req.body.taskId;
  let subscriberId = req.body.servisSubscriber;
  let taskName = req.body.servisName;
  let duration = req.body.servisDuration;
  let start = req.body.servisStart;
  let finish = req.body.servisFinish;
  let workersId = req.body.servisAssignmentArrayString;
  let completion = req.body.servisCompletion;
  let active = req.body.taskActive;
  let category = req.body.servisCategory;
  let priority = req.body.servisPriority;
  //console.log(projectId);
  let taskAssignmentArray = workersId.split(",");
  if(taskAssignmentArray[0] == "")
    taskAssignmentArray.pop();
  db.updateServis(subscriberId,taskId,taskName,duration,start, finish, completion, active, category, priority)
  .then(task=>{
    if(taskAssignmentArray.length > 0){
      db.removeAssignTask(taskId).then(task=>{
        for(let i = 0; i < taskAssignmentArray.length; i++){
          db.assignTask(taskId, taskAssignmentArray[i]).then(assignedTask=>{
            //do nothing;
            })
            .catch((e)=>{
              console.log(e);
            })
        }
        return res.end("success");
      })
    }
    else{
      db.removeAssignTask(taskId).then(task=>{
          return res.end("success");
        })
        .catch((e)=>{
          console.log(e);
        })
    }
    return res.end("success");
  })
})

module.exports = router;