var express = require('express');
var router = express.Router();
var dateFormat = require('dateformat');

//auth & db
var auth = require('../controllers/authentication');
var db = require('../controllers/db');

//add change for note for subtask
router.post('/note', auth.authenticate, function(req, res){
  //console.log("iz modalnega");
  let from = req.session.user_id;
  let forUsers = req.body.notify;
  let leaders = req.body.leaders;
  let projectId = req.body.projectId;
  let taskId = req.body.taskId;
  let subtaskId = req.body.idSubtask;
  let type = req.body.type;
  let status = 0;
  let completion = req.body.notifyCompletion;
  let taskIsServis = req.body.taskIsServis;
  if(completion == "true")
    status = 2;
  if(subtaskId == null)
    subtaskId = "";
  if(taskIsServis == "true")
    status += 8;
  //debugger;
  //console.log(forUsers);
  let notify = forUsers.split(",");
  let promises = [];
  if(forUsers){
    console.log("Uporabnik " + from + " je naredil spremembe za " + forUsers + " tipa " + type + " na opravilu " + taskId + ".");
    for(let i = 0; i < notify.length; i++){
      promises.push(db.addNoteChange(parseInt(type), from, parseInt(notify[i]), parseInt(projectId), parseInt(taskId), parseInt(subtaskId), status));
    }
  }
  status = 1;
  if(completion == "true")
    status = 3;
  if(taskIsServis == "true")
    status += 8;
  notify = leaders.split(",");
  if(leaders){
    console.log("Uporabnik " + from + " je naredil spremembe za " + leaders + " tipa " + type + " na opravilu " + taskId + ".");
    for(let i = 0; i < notify.length; i++){
      promises.push(db.addNoteChange(parseInt(type), from, parseInt(notify[i]), parseInt(projectId), parseInt(taskId), parseInt(subtaskId), status));
    }
  }
  //debugger;
  Promise.all(promises)
    .then((results) => {
      //console.log("all done", results);
      res.json({success:true, data:results})
  })
    .catch((e)=>{
      console.log(e);
  })
  
  /*
 db.addNoteChange(type,from, parseInt(notify[0]), parseInt(projectId), parseInt(taskId))
  .then((change)=>{
    res.json({success:true});
  })
  .catch((e)=>{
    console.log(e);
  })
  */
})

//get my changes
router.get('/mychanges', auth.authenticate, function(req, res){
  let userId = req.session.user_id;
  db.getMyChanges(userId)
    .then(changes=>{
      res.json({success:true, data:changes});
    })
    .catch((e)=>{
      console.log(e);
    })
})
//get my changes
router.get('/project', auth.authenticate, function(req, res){
  let userId = req.session.user_id;
  let projectId = req.query.projectId;
  db.getMyProjectChanges(userId, projectId)
    .then(changes=>{
      res.json({success:true, data:changes});
    })
    .catch((e)=>{
      console.log(e);
    })
})

//delete task changes that was already shown
router.post('/deletetask', auth.authenticate, function(req, res){
  let taskId = req.body.id;
  let userId = req.session.user_id;
  db.deleteChangeTask(taskId, userId)
    .then(changes => {
      console.log("Spremembe opravila " + taskId + " za uporabnika " + userId + " so bile odstranjene iz baze.");
      res.json({success:true});
    })
    .catch((e)=>{
      console.log(e);
    })
})

//delete function, based on function var delete specific changes
router.post('/delete', auth.authenticate, function(req, res){
  let userId = req.session.user_id;
  let taskId = req.body.taskId;
  let type = req.body.type;
  if(type == 2){
    db.deleteSubtaskChanges(taskId,userId)
      .then(changes => {
        console.log("Vse spremembe podopravil opravila " + taskId + " so bila odstranjena iz baze.");
        res.json({success:true});
      })
      .catch((e)=>{
        console.log(e);
      })
  }
  else{
    db.deleteTaskChanges(taskId, userId, type)
      .then(changes => {
        console.log("Spremembe tipa "+type+" opravila " + taskId + " so bile odstranjene");
        res.json({success:true})
      })
      .catch((e)=>{
        console.log(e);
      })
  }
  //if there is project id or subtask id no problem... if i will delete on mouseover for subtasks too
})

module.exports = router;