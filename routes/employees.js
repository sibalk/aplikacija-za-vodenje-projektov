var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var dateFormat = require('dateformat');
const saltRounds = 10;

var auth = require('../controllers/authentication');
var db = require('../controllers/db');

function formatDate(date){
  return {
    "date": dateFormat(date, "dd.mm.yyyy"),
    "time": dateFormat(date, "HH:MM")
  }
}
function addFormatedDateForTasks(tasks){
  //console.log("test");
  for(let i=0; i < tasks.length; i++) {
    if(tasks[i] && tasks[i].task_start)
      tasks[i]["formatted_start"] = formatDate(tasks[i].task_start);
    if(tasks[i] && tasks[i].task_finish)
      tasks[i]["formatted_finish"] = formatDate(tasks[i].task_finish);
  }
  return tasks;
}
function addFormatedDateForAbsences(absences){
  //console.log("test");
  for(let i=0; i < absences.length; i++) {
    if(absences[i] && absences[i].start)
      absences[i]["formatted_start"] = formatDate(absences[i].start);
    if(absences[i] && absences[i].finish)
      absences[i]["formatted_finish"] = formatDate(absences[i].finish);
  }
  return absences;
}

router.get('/', auth.authenticate, function(req, res) {
  res.render('employees', {
    title: "Seznam zaposlenih",
    user_name: req.session.user_name,
    user_surname: req.session.user_surname,
    user_username: req.session.user_username,
    user_typeid: req.session.role_id,
    user_type: req.session.type,
    user_id: req.session.user_id,
  })
})

router.get('/userId', auth.authenticate, function(req, res, next) {
  let userId = req.query.userId;
  if(!userId){
    console.error("No userId provided in query");
    return res.redirect('/employees');
  }
  db.getUserById(userId).then(employee =>{
    if(employee){
      return db.getUserProjects(userId)
        .then(projects=>{return {employee, projects}})
      .then(data=>{
        //found employee with projects
        if(data.projects){
          res.render('employee', {
            title: "Zaposleni",
            user_name: req.session.user_name,
            user_username: req.session.user_username,
            user_surname: req.session.user_surname,
            user_typeid: req.session.role_id,
            user_type: req.session.type,
            user_id: req.session.user_id,
            employee: data.employee,
            projects: data.projects
          })
        }
        //found employee not projects
        else{
          res.render('employee', {
            title: "Zaposleni",
            user_name: req.session.user_name,
            user_surname: req.session.user_surname,
            user_username: req.session.user_username,
            user_typeid: req.session.role_id,
            user_type: req.session.type,
            user_id: req.session.user_id,
            employee: data.employee,
            projects: []
          })
        }
      })
      .catch((e)=>{
        console.log(e);
      })
    }
    //found nothing
    else{
      res.render('employee', {
        title: "Zaposleni",
        user_name: req.session.user_name,
        user_surname: req.session.user_surname,
        user_username: req.session.user_username,
        user_typeid: req.session.role_id,
        user_type: req.session.type,
        user_id: req.session.user_id,
        employee: null,
        projects: []
      })
    }
  })
  .catch((e)=>{
    console.log(e);
  })
})
//POGLED OPRAVIL ZAPOSLENEGA // TEKOČA OPRAVILA
router.get('/tasks', auth.authenticate, function(req, res){
  let userId = req.query.userId;
  db.getUserById(userId).then(employee =>{
    if(employee){
      return db.getWorkerTasks(userId)
        .then(tasks=>{return {employee, tasks}})
      .then(data=>{
        addFormatedDateForTasks(data.tasks);
        //found employee with tasks
        if(data.tasks){
          res.render('tasks', {
            title: "Moja opravila",
            user_name: req.session.user_name,
            user_username: req.session.user_username,
            user_surname: req.session.user_surname,
            user_typeid: req.session.role_id,
            user_type: req.session.type,
            user_id: req.session.user_id,
            employee: data.employee,
            tasks: data.tasks
          })
        }
        //found employee not tasks
        else{
          res.render('tasks', {
            title: "Moja opravila",
            user_name: req.session.user_name,
            user_surname: req.session.user_surname,
            user_username: req.session.user_username,
            user_typeid: req.session.role_id,
            user_type: req.session.type,
            user_id: req.session.user_id,
            employee: data.employee,
            tasks: []
          })
        }
      })
      .catch((e)=>{
        console.log(e);
      })
    }
    //found nothing
    else{
      res.render('tasks', {
        title: "Moja opravila",
        user_name: req.session.user_name,
        user_surname: req.session.user_surname,
        user_username: req.session.user_username,
        user_typeid: req.session.role_id,
        user_type: req.session.type,
        user_id: req.session.user_id,
        employee: null,
        tasks: []
      })
    }
  })
  .catch((e)=>{
    console.log(e);
  })
})
//PREGLED PROJEKTA ZA ZAPOSLENEGA, prikaže samo opravila projekta za izbranega, tudi dokončana
router.get('/project', auth.authenticate, function(req, res){
  let userId = req.query.userId;
  let projectId = req.query.projectId;
  let taskId = req.query.taskId;
  db.getUserById(userId).then(employee =>{
    if(employee){
      return db.getProjectUserTasks(projectId, userId)
        .then(tasks=>{return {employee, tasks}})
      .then(data=>{
        addFormatedDateForTasks(data.tasks);
        //found employee with tasks
        if(data.tasks){
          return db.getProjectById(projectId)
            .then(project=>{return {data,project}})
            .catch((e)=>{
              console.log(e);
            })
          .then(data=>{
            if(data.project){
              res.render('project_tasks', {
                title: "Moja opravila",
                user_name: req.session.user_name,
                user_username: req.session.user_username,
                user_surname: req.session.user_surname,
                user_typeid: req.session.role_id,
                user_type: req.session.type,
                user_id: req.session.user_id,
                employee: data.data.employee,
                tasks: data.data.tasks,
                project: data.project
              })
            }
            else{
              res.render('project_tasks', {
                title: "Moja opravila",
                user_name: req.session.user_name,
                user_username: req.session.user_username,
                user_surname: req.session.user_surname,
                user_typeid: req.session.role_id,
                user_type: req.session.type,
                user_id: req.session.user_id,
                employee: data.data.employee,
                tasks: data.data.tasks,
                project: null
              })
            }
          })
        }
        //found employee not tasks
        else{
          res.render('project_tasks', {
            title: "Moja opravila",
            user_name: req.session.user_name,
            user_surname: req.session.user_surname,
            user_username: req.session.user_username,
            user_typeid: req.session.role_id,
            user_type: req.session.type,
            user_id: req.session.user_id,
            employee: data.employee,
            tasks: [],
            project: null
          })
        }
      })
      .catch((e)=>{
        console.log(e);
      })
    }
    //found nothing
    else{
      res.render('project_tasks', {
        title: "Moja opravila",
        user_name: req.session.user_name,
        user_surname: req.session.user_surname,
        user_username: req.session.user_username,
        user_typeid: req.session.role_id,
        user_type: req.session.type,
        user_id: req.session.user_id,
        employee: null,
        tasks: [],
        project: null
      })
    }
  })
  .catch((e)=>{
    console.log(e);
  })
})
//POGLEDD ODSOTNOSTI ZAPOSLENEGA
router.get('/absence', auth.authenticate, function(req, res){
  let userId = req.query.id;
  if(!userId){
    console.error("No userId provided in query");
    return res.redirect('/dashboard');
  }
  db.getUserById(userId).then(employee =>{
    if(employee){
      return db.getUserAbsence(userId)
        .then(absences=>{return {employee, absences}})
      .then(data=>{
        //found employee with absences
        if(data.absences){
          addFormatedDateForAbsences(data.absences);
          if(req.session.type == 'admin' || req.session.type == 'tajnik' || req.session.type == 'vodja' || req.session.user_id == userId)
            res.render('absence', {
              title: "Moje odsotnosti",
              user_name: req.session.user_name,
              user_username: req.session.user_username,
              user_surname: req.session.user_surname,
              user_typeid: req.session.role_id,
              user_type: req.session.type,
              user_id: req.session.user_id,
              employee: data.employee,
              absences: data.absences
            })
          else
            res.redirect('/dashboard');
        }
        //found employee not absences
        else{
          if(req.session.type == 'admin' || req.session.type == 'tajnik' || req.session.type == 'vodja' || req.session.user_id == userId)
            res.render('absence', {
              title: "Moje odsotnosti",
              user_name: req.session.user_name,
              user_surname: req.session.user_surname,
              user_username: req.session.user_username,
              user_typeid: req.session.role_id,
              user_type: req.session.type,
              user_id: req.session.user_id,
              employee: data.employee,
              absences: []
            })
          else
            res.redirect('/dashboard');
        }
      })
      .catch((e)=>{
        console.log(e);
      })
    }
    //found nothing
    else{
      res.render('absence', {
        title: "Moje odsotnosti",
        user_name: req.session.user_name,
        user_surname: req.session.user_surname,
        user_username: req.session.user_username,
        user_typeid: req.session.role_id,
        user_type: req.session.type,
        user_id: req.session.user_id,
        employee: null,
        projects: []
      })
    }
  })
  .catch((e)=>{
    console.log(e);
  })
})
//Get tasks for specific project and specific user
router.get('/projectTasks', auth.authenticate, function(req, res){
  let projectId = req.query.projectId;
  let userId = req.query.userId;
  db.getProjectUserTasks(projectId, userId).then(tasks=>{
      res.json({sucess: true, data: tasks})
    })
    .catch((e)=>{
      console.log(e);
    })
  //res.json({success: true, data:'somedata'});
})
//Get workers for specific task
router.get('/workers', auth.authenticate, function(req, res){
  let taskId = req.query.taskId;
  db.getTaskWorkers(taskId).then(workers=>{
      res.json({sucess: true, data: workers})
    })
    .catch((e)=>{
      console.log(e);
    })
  //res.json({success: true, data:'somedata'});
})
//Get user roles
router.get('/roles', auth.authenticate, function(req, res){
  db.getUserRoles().then(roles=>{
      res.json({sucess: true, data: roles})
    })
    .catch((e)=>{
      console.log(e);
    })
  //res.json({success: true, data:'somedata'});
})
//Get usernames
router.get('/usernames', auth.authenticate, function(req, res){
  db.getUsernames().then(usernames=>{
      res.json({sucess: true, data: usernames})
    })
    .catch((e)=>{
      console.log(e);
    })
  //res.json({success: true, data:'somedata'});
})
//Get all users
router.get('/all', auth.authenticate, function(req, res){
  let active = req.query.activeUser;
  db.getUsers(parseInt(active)).then(users=>{
      res.json({sucess: true, data: users})
    })
    .catch((e)=>{
      console.log(e);
    })
  //res.json({success: true, data:'somedata'});
})
//get user
router.get('/user', auth.authenticate, function(req, res){
  let userId = req.query.userId;
  db.getUserById(userId).then(user=>{
      res.json({sucess: true, data: user})
    })
    .catch((e)=>{
      console.log(e);
    })
  //res.json({success: true, data:'somedata'});
})
//Create new employee
router.post('/create', auth.authenticate, function(req, res){
  if(req.body.userPassword === req.body.userPasswordAgain){
    let hash = bcrypt.hashSync(req.body.userPassword, saltRounds);
    db.createUser(req.body.userUsername, req.body.userName, req.body.userSurname, req.body.userRole, hash)
      .then(user =>{
        return res.json({success: true, id: user})
      })
      .catch((e)=>{
        console.log(e);
      })
  }
  else{
    return res.json({success: false, data: "Gesli se ne ujemata!"})
  }
})
//Update user (for now name and surname; password and role on user's page)
router.post('/update', auth.authenticate, function(req, res){
  let name = req.body.newName;
  let surname = req.body.newSurname;
  let id = req.body.userId;
  let active = req.body.newActive;
  let username = req.body.newUsername;
  db.updateUser(id, name, surname, active, username).then(user=>{
      return res.end("success");
    })
    .catch((e)=>{
      console.log(e);
    })
})
//Delete user, set as inactive user
router.post('/delete', auth.authenticate, function(req, res){
  //console.log("odstrani aktivnost");
  let userId = req.body.userId;
  db.removeUser(userId).then(user=>{
      return res.end("success");
    })
    .catch((e)=>{
      console.log(e);
    })
})
//Update password
router.post('/password', auth.authenticate, function(req, res){
  let userId = req.body.userId;
  //console.log(userId);
  if(req.body.userPassword === req.body.userPasswordAgain){
    //db.updatePassword
    //console.log("gesli se ujemata")
    let hash = bcrypt.hashSync(req.body.userPassword, saltRounds);
    db.updatePassword(userId, hash).then(user=>{
        return res.json({success: true, id: user})
      })
      .catch((e)=>{
        console.log(e);
      })
  }
  else{
    return res.json({success: false, data: "Gesli se ne ujemata!"})
  }
})
//Update role
router.post('/role', auth.authenticate, function(req, res){
  let userId = req.body.userId;
  let role = req.body.userRole;
  //console.log(userId);
  //console.log(req.session.user_id);
  //console.log(userId);
  db.updateRole(userId, role).then(role=>{
    if(parseInt(userId) == req.session.user_id){
      db.getUserRoles().then(roles=>{
        var selectedRole = roles.find(r => r.id === parseInt(req.body.userRole))
        if(selectedRole){
          //console.log("našel novo vlogo");
          //console.log(req.session.type);
          req.session.type = selectedRole.text
          //console.log(req.session.type);
          return res.json({success: true, id: role})
        }
      })
      .catch((e)=>{
        console.log(e);
      })
    }
    else{
      console.log("vloga nima veze na vpisanega up")
      return res.json({success: true, id: role})
    }
  })
  .catch((e)=>{
    console.log(e);
  })
})
//add new system role (electrican, machanic, plc, kuka, builder)
router.post('/addrole', auth.authenticate, function(req, res){
  let role = req.body.role;
  let type = req.body.newRoleType;
  if(type == 0){
    db.addNewRole(role).then(role=>{
      return res.end("success");
    })
    .catch((e)=>{
      console.log(e);
    })
  }
  else{
    db.addNewProjectRole(role).then(role=>{
      return res.end("success");
    })
    .catch((e)=>{
      console.log(e);
    })
  }
  //db add new role and return success
})

//add new project role 
router.post('/addprojectrole', auth.authenticate, function(req, res){
  let role = req.body.roleNew;
  //db add new role and return success
  db.addNewProjectRole(role).then(role=>{
    return res.json({success: true, id: role})
  })
  .catch((e)=>{
    console.log(e);
  })
})

module.exports = router;