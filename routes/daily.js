var express = require('express');
var router = express.Router();

//auth & db
var auth = require('../controllers/authentication');
var db = require('../controllers/db');

//first page with all daily tasks
router.get('/', auth.authenticate, function(req, res){
  //get today's tasks
  db.getDailyTasks().then(tasks=>{
    //get workers if any and put together
    let idTasks = [];
    let allTasks = [];
    let counter = 0;
    for(let i=0; i<tasks.length; i++){
      //console.log(tasks[i].isactive);
      if(!contains.call(idTasks, tasks[i].id)){
        //others (programers and builders) have net access all the time
        if(req.session.type == 'admin'){
          if(tasks[i].isactive != false){
            idTasks.push(tasks[i].id);
            allTasks.push(tasks[i]);
            if(tasks[i].worker)
              allTasks[counter].workers = ""+ tasks[i].worker;
            else
              allTasks[counter].workers = "Brez dodelitve";
            allTasks[counter].workersId = [tasks[i].worker_id]
            counter++;
          }
        }
        else{
          if(tasks[i].isactive != false && tasks[i].category != 'Nabava' && tasks[i].category != 'Konstrukcija'
            && tasks[i].category != 'Programiranje' && tasks[i].category != 'Brez'){
            idTasks.push(tasks[i].id);
            allTasks.push(tasks[i]);
            if(tasks[i].worker)
              allTasks[counter].workers = ""+ tasks[i].worker;
            else
              allTasks[counter].workers = "Brez dodelitve";
            allTasks[counter].workersId = [tasks[i].worker_id]
            counter++;
          }
        }
      }
      else{
        var opravilo = allTasks.find(o => o.id === tasks[i].id);
        if(opravilo){
          if(tasks[i].isactive != false){
            opravilo.workers += ", "+ tasks[i].worker;
            opravilo.workersId.push(tasks[i].worker_id);
          }
        }
      }
    }
    db.getWeeklyTasks().then(weekly=>{
      let idWeek = [];
      let allWeek = [];
      counter = 0;
      for(let i = 0; i < weekly.length; i++){
        if(!contains.call(idWeek, weekly[i].id)){
          //others (programers and builders) have net access all the time
          if(req.session.type == 'admin'){
            if(weekly[i].isactive != false){
              idWeek.push(weekly[i].id);
              allWeek.push(weekly[i]);
              if(weekly[i].worker)
                allWeek[counter].workers = ""+ weekly[i].worker;
              else
                allWeek[counter].workers = "Brez dodelitve";
              allWeek[counter].workersId = [weekly[i].worker_id]
              counter++;
            }
          }
          else{
            if(weekly[i].isactive != false && weekly[i].category != 'Nabava' && weekly[i].category != 'Konstrukcija'
              && weekly[i].category != 'Programiranje' && weekly[i].category != 'Brez'){
              idWeek.push(weekly[i].id);
              allWeek.push(weekly[i]);
              if(weekly[i].worker)
                allWeek[counter].workers = ""+ weekly[i].worker;
              else
                allWeek[counter].workers = "Brez dodelitve";
              allWeek[counter].workersId = [weekly[i].worker_id]
              counter++;
            }
          }
        }
        else{
          var opravilo = allWeek.find(o => o.id === weekly[i].id);
          if(opravilo){
            if(weekly[i].isactive != false){
              opravilo.workers += ", "+ weekly[i].worker;
              opravilo.workersId.push(weekly[i].worker_id);
            }
          }
        }
      }
      res.render('daily', {
        title: "Pregled opravil",
        user_name: req.session.user_name,
        user_surname: req.session.user_surname,
        user_username: req.session.user_username,
        user_typeid: req.session.role_id,
        user_type: req.session.type,
        user_id: req.session.user_id,
        tasks: allTasks,
        weekly: allWeek
      })
    })
  })
  .catch((e)=>{
    console.log(e);
  })
});
//get tasks for specific date
router.get('/date', auth.authenticate, function(req, res){
  let date = req.query.date;
  let category = req.query.categoryTask;
  let adminView = (req.query.adminView == 'true');
  db.getDailyTasks(date, parseInt(category)).then(tasks=>{
    //get workers if any and put together
    let idTasks = [];
    let allTasks = [];
    let counter = 0;
    for(let i=0; i<tasks.length; i++){
      //console.log(tasks[i].isactive);
      if(!contains.call(idTasks, tasks[i].id)){
        if(adminView){
          if(tasks[i].isactive != false){
            idTasks.push(tasks[i].id);
            allTasks.push(tasks[i]);
            if(tasks[i].worker)
              allTasks[counter].workers = ""+ tasks[i].worker;
            else
              allTasks[counter].workers = "Brez dodelitve";
            allTasks[counter].workersId = [tasks[i].worker_id]
            counter++;
          }
        }
        else{
          if((tasks[i].isactive && !tasks[i].project_name) || (tasks[i].isactive != false && tasks[i].category != 'Nabava' && tasks[i].category != 'Konstrukcija'
            && tasks[i].category != 'Programiranje' && tasks[i].category != 'Brez')){
            idTasks.push(tasks[i].id);
            allTasks.push(tasks[i]);
            if(tasks[i].worker)
              allTasks[counter].workers = ""+ tasks[i].worker;
            else
              allTasks[counter].workers = "Brez dodelitve";
            allTasks[counter].workersId = [tasks[i].worker_id]
            counter++;
          }
        }
      }
      else{
        var opravilo = allTasks.find(o => o.id === tasks[i].id);
        if(opravilo){
          if(tasks[i].isactive != false){
            opravilo.workers += ", "+ tasks[i].worker;
            opravilo.workersId.push(tasks[i].worker_id);
          }
        }
      }
    }
    return res.json({success: true, data: allTasks});
  })
  .catch((e)=>{
    console.log(e);
  })
})
//get tasks for specific date
router.get('/week', auth.authenticate, function(req, res){
  let date = req.query.date;
  let category = req.query.categoryTask;
  let adminView = (req.query.adminView == 'true');
  db.getWeeklyTasks(date, parseInt(category)).then(tasks=>{
    //get workers if any and put together
    let idTasks = [];
    let allTasks = [];
    let counter = 0;
    for(let i=0; i<tasks.length; i++){
      //console.log(tasks[i].isactive);
      if(!contains.call(idTasks, tasks[i].id)){
        if(adminView){
          if(tasks[i].isactive != false){
            idTasks.push(tasks[i].id);
            allTasks.push(tasks[i]);
            if(tasks[i].worker)
              allTasks[counter].workers = ""+ tasks[i].worker;
            else
              allTasks[counter].workers = "Brez dodelitve";
            allTasks[counter].workersId = [tasks[i].worker_id]
            counter++;
          }
        }
        else{
          if(tasks[i].isactive != false && tasks[i].category != 'Nabava' && tasks[i].category != 'Konstrukcija'
            && tasks[i].category != 'Programiranje' && tasks[i].category != 'Brez'){
            idTasks.push(tasks[i].id);
            allTasks.push(tasks[i]);
            if(tasks[i].worker)
              allTasks[counter].workers = ""+ tasks[i].worker;
            else
              allTasks[counter].workers = "Brez dodelitve";
            allTasks[counter].workersId = [tasks[i].worker_id]
            counter++;
          }
        }
      }
      else{
        var opravilo = allTasks.find(o => o.id === tasks[i].id);
        if(opravilo){
          if(tasks[i].isactive != false){
            opravilo.workers += ", "+ tasks[i].worker;
            opravilo.workersId.push(tasks[i].worker_id);
          }
        }
      }
    }
    return res.json({success: true, data: allTasks});
  })
  .catch((e)=>{
    console.log(e);
  })
})
var contains = function(needle) {
  // Per spec, the way to identify NaN is that it is not equal to itself
  var findNaN = needle !== needle;
  var indexOf;

  if(!findNaN && typeof Array.prototype.indexOf === 'function') {
      indexOf = Array.prototype.indexOf;
  } else {
      indexOf = function(needle) {
          var i = -1, index = -1;

          for(i = 0; i < this.length; i++) {
              var item = this[i];

              if((findNaN && item !== item) || item === needle) {
                  index = i;
                  break;
              }
          }

          return index;
      };
  }

  return indexOf.call(this, needle) > -1;
};
module.exports = router;