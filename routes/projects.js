var express = require('express');
var router = express.Router();
var dateFormat = require('dateformat');
var multer = require('multer');

//auth & db
var auth = require('../controllers/authentication');
var db = require('../controllers/db');

//functions
function formatDate(date){
  return {
    "date": dateFormat(date, "dd.mm.yyyy"),
    "time": dateFormat(date, "HH:MM")
  }
}
function formatDateSystem(date){
  return {
    "date": dateFormat(date, "yyyy-mm-dd"),
    "time": dateFormat(date, "HH:MM")
  }
}
function addFormatedDateForTasks(tasks){
  //console.log("test");
  for(let i=0; i < tasks.length; i++) {
    if(tasks[i] && tasks[i].task_start)
      tasks[i]["formatted_start"] = formatDate(tasks[i].task_start);
    if(tasks[i] && tasks[i].task_finish)
      tasks[i]["formatted_finish"] = formatDate(tasks[i].task_finish);
  }
  return tasks;
}
function addFormatedDateForProject(project){
  if(project.start)
    project["formatted_start"] = formatDate(project.start);
    if(project.finish)
    project["formatted_finish"] = formatDate(project.finish);
  return project
}
function addFormatedDateForFile(files){
  for(let i=0; i<files.length; i++){
    files[i]["formatted_date"] = formatDate(files[i].date);
  }
}
//storage for documents
var storageDoc = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'public/uploads/documents/')
  },
  filename: function (req, file, cb) {        
    // null as first argument means no error
    cb(null, Date.now() + '-' + file.originalname )
  }
});
//upload when adding subscriber image
var uploadDoc = multer({
  storage: storageDoc,
  limits: {
    fileSize: 1000000
  },
  fileFilter: function(req, file, cb){
    sanitizeFileDocument(file,cb);
  }
}).single('docNew');
//function to check if file is indeed document
function sanitizeFileDocument(file, cb){
  //what file extentions are ok
  let fileExts = ['pdf', //pdf 
                'doc', 'dot', 'wbk', 'docx', 'docm', 'dotx', 'dotm', 'docb', //word
                'xls', 'xlt', 'xlm', 'xlsx', 'xlsm', 'xltx', 'xltm', 'xlsb', 'xla', 'xlam', 'xll', 'xlw', //excel
                'ppt', 'pot', 'pps', 'pptx', 'pptm', 'potx', 'potm', 'ppam', 'ppsx', 'ppsm', 'sldx', 'sldm', //powepoint
                'jpg', 'jpeg', 'png', 'gif']; //images
                //access has way diffrent ext and they probably wont use them
                //other file ext will be added when there will be request
  // MAYBE TODO add isAlowedMimeType back and test for all this type of extentions
  //check alowed exts
  let isAlowedExt = fileExts.includes(file.originalname.split(".")[1].toLowerCase());
  //mime type must be an image
  //let isAlowedMimeType = file.mimetype.startsWith("document/");

  if(isAlowedExt){
    //no errors
    return cb(null, true);
  }
  else{
    //error, not an image
    cb('Ta vrsta datoteke ni dovoljena!');
  }
}
//upload for new file
router.post('/fileUpload', auth.authenticate, (req, res, next) => {
  //debugger;
  //save file and if no error send back json success true
  uploadDoc(req, res, (err) =>{
    //console.log("img upload");
    if(err){
      let projectId = parseInt(req.body.projectIdFile);
      //res.redirect('/subscribers');
      console.log("Napaka pri nalaganju datoteke pri uporabniku " + req.session.user_id);
      let msg = "Napaka: " + err;
      let type = 1;
      res.redirect('/projects/id?id='+projectId+'&msg='+msg+'&type='+type);
    }
    else{
      //file not selected
      if(req.file == undefined){
        let projectId = parseInt(req.body.projectIdFile);
        console.log("Uporabnik " + req.session.user_id + " ni podal datoteke pri nalaganju datoteke.");
        //var subscriberName = req.body.subscriberName;
        let msg = "Neuspešno! Niste podali datoteke, ki jo želite naložiti.";
        let type = 1;
        res.redirect('/projects/id?id='+projectId+'&msg='+msg+'&type='+type);
      }
      else{
        let type = "";
        let fileExtsPDF = ['pdf'];
        let fileExtsDOC = ['doc', 'dot', 'wbk', 'docx', 'docm', 'dotx', 'dotm', 'docb'];
        let fileExtsXLS = ['xls', 'xlt', 'xlm', 'xlsx', 'xlsm', 'xltx', 'xltm', 'xlsb', 'xla', 'xlam', 'xll', 'xlw'];
        let fileExtsPPT = ['ppt', 'pot', 'pps', 'pptx', 'pptm', 'potx', 'potm', 'ppam', 'ppsx', 'ppsm', 'sldx', 'sldm'];
        let fileExtsIMG = ['jpg', 'jpeg', 'png', 'gif'];

        let isAlowedExtPDF = fileExtsPDF.includes(req.file.originalname.split(".")[1].toLowerCase());
        let isAlowedExtDOC = fileExtsDOC.includes(req.file.originalname.split(".")[1].toLowerCase());
        let isAlowedExtXLS = fileExtsXLS.includes(req.file.originalname.split(".")[1].toLowerCase());
        let isAlowedExtPPT = fileExtsPPT.includes(req.file.originalname.split(".")[1].toLowerCase());
        let isAlowedExtIMG = fileExtsIMG.includes(req.file.originalname.split(".")[1].toLocaleLowerCase());
        
        if(isAlowedExtPDF) type = 'PDF';
        else if(isAlowedExtDOC) type = 'DOC';
        else if(isAlowedExtPPT) type = 'PPT';
        else if(isAlowedExtXLS) type = 'XLS';
        else if(isAlowedExtIMG) type = 'IMG';
        let projectId = parseInt(req.body.projectIdFile);
        console.log("Uporabnik " + req.session.user_id + " je uspešno naložil datoteko " +req.file.originalname + " na projektu " + projectId);
        //success add new subscriber with image name
        //var subscriberName = req.body.subscriberName;
        //var imageName = req.file.filename;
        db.addFileForProject(req.file.originalname, req.file.filename, projectId, req.session.user_id, type)
          .then(newFile =>{
            if(newFile){
              //zaenkrat sem ker mi tam neki hodi narobe...
              /*
              res.render('subscribers', {
                title: "Naročniki",
                user_name: req.session.user_name,
                user_surname: req.session.user_surname,
                user_username: req.session.user_username,
                user_typeid: req.session.role_id,
                user_type: req.session.type,
                user_id: req.session.user_id,
                msg: 'Uspešno dodajanje nove datoteke.',
                type: 2
              })
              */
              let msg = "Uspešno dodajanje nove datoteke.";
              let type = 2;
              res.redirect('/projects/id?id='+projectId+'&msg='+msg+'&type='+type);
            }
          })
      }
    }
  })
});

//upload for new file based on taskId and projectId
router.post('/taskFileUpload', auth.authenticate, (req, res, next) => {
  //console.log(req.body.projectIdTaskFile);
  //console.log(req.body.taskIdTaskFile);
  //save file and if no error send back json success true
  uploadDoc(req, res, (err) =>{
    //console.log("img upload");
    if(err){
      let projectId = parseInt(req.body.projectIdTaskFile);
      //res.redirect('/subscribers');
      console.log("Napaka pri nalaganju datoteke pri uporabniku " + req.session.user_id);
      let msg = "Napaka: " + err;
      let type = 1;
      res.redirect('/projects/id?id='+projectId+'&msg='+msg+'&type='+type);
    }
    else{
      //file not selected
      if(req.file == undefined){
        let projectId = parseInt(req.body.projectIdTaskFile);
        let taskId = parseInt(req.body.taskIdTaskFile);
        console.log("Uporabnik " + req.session.user_id + " ni podal datoteke pri nalaganju datoteke.");
        //var subscriberName = req.body.subscriberName;
        let msg = "Neuspešno! Niste podali datoteke, ki jo želite naložiti.";
        let type = 1;
        res.redirect('/projects/id?id='+projectId+'&msg='+msg+'&type='+type);
      }
      else{
        let type = "";
        let fileExtsPDF = ['pdf'];
        let fileExtsDOC = ['doc', 'dot', 'wbk', 'docx', 'docm', 'dotx', 'dotm', 'docb'];
        let fileExtsXLS = ['xls', 'xlt', 'xlm', 'xlsx', 'xlsm', 'xltx', 'xltm', 'xlsb', 'xla', 'xlam', 'xll', 'xlw'];
        let fileExtsPPT = ['ppt', 'pot', 'pps', 'pptx', 'pptm', 'potx', 'potm', 'ppam', 'ppsx', 'ppsm', 'sldx', 'sldm'];
        let fileExtsIMG = ['jpg', 'jpeg', 'png', 'gif'];

        let isAlowedExtPDF = fileExtsPDF.includes(req.file.originalname.split(".")[1].toLowerCase());
        let isAlowedExtDOC = fileExtsDOC.includes(req.file.originalname.split(".")[1].toLowerCase());
        let isAlowedExtXLS = fileExtsXLS.includes(req.file.originalname.split(".")[1].toLowerCase());
        let isAlowedExtPPT = fileExtsPPT.includes(req.file.originalname.split(".")[1].toLowerCase());
        let isAlowedExtIMG = fileExtsIMG.includes(req.file.originalname.split(".")[1].toLocaleLowerCase());
        
        if(isAlowedExtPDF) type = 'PDF';
        else if(isAlowedExtDOC) type = 'DOC';
        else if(isAlowedExtPPT) type = 'PPT';
        else if(isAlowedExtXLS) type = 'XLS';
        else if(isAlowedExtIMG) type = 'IMG';
        let projectId = parseInt(req.body.projectIdTaskFile);
        let taskId = parseInt(req.body.taskIdTaskFile);
        console.log("Uporabnik " + req.session.user_id + " je uspešno naložil datoteko " +req.file.originalname + " na projektu " + projectId + " za opravilu " + taskId);
        //success add new subscriber with image name
        //var subscriberName = req.body.subscriberName;
        //var imageName = req.file.filename;
        db.addFileForTask(req.file.originalname, req.file.filename, projectId, req.session.user_id, type, taskId)
          .then(newFile =>{
            if(newFile){
              let msg = "Uspešno dodajanje nove datoteke.";
              let type = 2;
              res.redirect('/projects/id?id='+projectId+'&msg='+msg+'&type='+type);
            }
        })
      }
    }
  })
});

//PROJEKTI
router.get('/', auth.authenticate, function(req, res) {
  res.render('projects', {
    title: "Seznam projektov",
    user_name: req.session.user_name,
    user_surname: req.session.user_surname,
    user_username: req.session.user_username,
    user_typeid: req.session.role_id,
    user_type: req.session.type,
    user_id: req.session.user_id,
    projects: [],
  })
  //console.log(req.session);
})
//Create project
router.post('/create', auth.authenticate, function(req, res){
  db.createProject(req.body.number, req.body.name, req.body.subscriber, req.body.start, req.body.finish, req.body.notes)
  .then(project=>{
    //console.log(subscriber);
    res.json({success:true, id:project});
  })
  .catch((e)=>{
    console.log(e);
  })
})
//Delete project / set it as inactive
router.post('/delete', auth.authenticate, function(req,res){
  let projectId = req.body.projectId;
  db.removeProject(projectId).then(project=>{
    return res.end("success");
  })
  .catch((e)=>{
    console.log(e);
  })
})
//Update project
router.post('/update', auth.authenticate, function(req, res){
  db.updateProject(req.body.projectId,req.body.number,req.body.name,req.body.subscriber,req.body.start,req.body.finish,req.body.notes, req.body.active)
  .then(project=>{
      return res.end('success');
  })
  .catch((e)=>{
    console.log(e);
  })
})

//PROJEKT
router.get('/id', auth.authenticate, function(req, res, next) {
  let projectId = req.query.id;
  let msg='';
  let type=0;
  if(req.query.msg){
    msg = req.query.msg;
    type = req.query.type;
  }
  //console.log(projectId);
  if(projectId){
    db.getProjectById(projectId).then(project =>{
      if(project){
        return db.getWorkersByProjectId(projectId)
          .then(workers=>{return {project, workers}})
        .then(data=>{
          if(data.workers){
            let isPresent = false;
            let leader = false
            for(let i in data.workers){
              if(data.workers[i].id === req.session.user_id){
                isPresent = true;
                if(data.workers[i].workRole == 'vodja')
                  leader = true;
              }
            }
            return db.getTasksByProjectIdFixed(projectId, 0, 1)
              .then(tasks=>{return Object.assign({tasks},data)})
              .then(data=>{
                //found all
                if(data.tasks){
                  //data.tasks = multipleWorkers(data.tasks);
                  data.tasks = data.tasks.filter(t => t.completion != 100);
                  let stStrani = Math.ceil(data.tasks.length/10);
                  data.tasks = data.tasks.slice(0,10);
                  addFormatedDateForTasks(data.tasks);
                  addFormatedDateForProject(data.project);
                  
                  res.render('project', {
                    title: "Projekt "+data.project.project_name,
                    user_name: req.session.user_name,
                    user_surname: req.session.user_surname,
                    user_type: req.session.type,
                    user_id: req.session.user_id,
                    project: data.project,
                    workers: data.workers,
                    tasks: data.tasks,
                    present: isPresent,
                    leader: leader, 
                    projectId: projectId,
                    files: [],
                    pages: stStrani,
                    msg: msg,
                    type: type
                    //workerTasks: workerTasks
                  })  
                }
                //found project, workers, no tasks
                else{
                  res.render('project', {
                    title: "Projekt "+data.project.project_name,
                    user_name: req.session.user_name,
                    user_surname: req.session.user_surname,
                    user_type: req.session.type,
                    user_id: req.session.user_id,
                    project: data.project,
                    workers: data.workers,
                    present: isPresent,
                    leader: leader,
                    tasks: [],
                    projectId: projectId,
                    files: [],
                    pages: 0,
                    msg: msg,
                    type: type
                  })
                }
              })
          }
          //not found workers
          else{
            res.render('project', {
              title: "Projekt "+data.project.project_name,
              user_name: req.session.user_name,
              user_surname: req.session.user_surname,
              user_type: req.session.type,
              user_id: req.session.user_id,
              project: data.project,
              present: false,
              leader: false,
              workers: [],
              projectId: projectId,
              files: [],
              pages: 0,
              msg: msg,
              type: type
            })
          }
        })
      }
      else{
        res.render('project', {
          title: "Projekt X",
          user_name: req.session.user_name,
          user_surname: req.session.user_surname,
          user_type: req.session.type,
          user_id: req.session.user_id,
          pages: 0,
          msg: msg,
          type: type
        })
      }
    })
  }else{
    res.render('project', {
      title: "Projekt X",
      user_name: req.session.user_name,
      user_surname: req.session.user_surname,
      user_type: req.session.type,
      user_id: req.session.user_id,
      pages: 0,
      msg: msg,
      type: type
    })
  }
})
//get project info
router.get('/info', auth.authenticate, function(req, res){
  let projectId = req.query.projectId;
  db.getProjectById(projectId).then(project=>{
      return res.json({success:true, data:project});
    })
    .catch((e)=>{
      console.log(e);
    })
})
//Get task's files
router.get('/taskFiles', auth.authenticate, function(req, res){
  let projectId = req.query.projectId;
  let taskId = req.query.taskId;
  db.getTaskFiles(projectId, taskId).then(files=>{
    res.json({success:true, data: files});
  })
  .catch((e)=>{
    console.log(e);
  })
})
//Get project's files
router.get('/files', auth.authenticate, function(req, res){
  let projectId = req.query.projectId;;
  db.getProjectFiles(projectId).then(files=>{
    res.json({success:true, data: files});
  })
  .catch((e)=>{
    console.log(e);
  })
})
//Get categories
router.get('/categories', auth.authenticate, function(req, res){
  db.getCategories().then(categories=>{
    res.json({success:true, data: categories});
  })
  .catch((e)=>{
    console.log(e);
  })
})
//Get priorities
router.get('/priorities', auth.authenticate, function(req, res){
  db.getPriorities().then(priorities=>{
    res.json({success:true, data: priorities});
  })
  .catch((e)=>{
    console.log(e);
  })
})
//Get absence for users
router.post('/absence', auth.authenticate, function(req, res){
  let projectId = req.body.projectId;
  let taskId = req.body.taskId;
  let start = req.body.start;
  let finish = req.body.finish;
  let workerId = req.body.workersId;
  let workersArray = workerId.split(",");
  if(workersArray[0] == "")
    workersArray.pop();
  let promises = [];
  if(workersArray){
    for(let i=0; i<workersArray.length; i++){
      promises.push(db.getUserAbsenceForTask(workersArray[i], projectId, taskId, start, finish));
    }
    Promise.all(promises)
      .then((results)=>{
        res.json({success:true, data:results});
    })
      .catch((e)=>{
        console.log(e);
      })
  }
  else{
    //sem po pravem nebi smel pridet
    res.json({success:true, data:[]});
  }
})
//Update project completion
router.post('/completion', auth.authenticate, function(req, res){
  let projectId = req.body.projectId;
  if(projectId){
    db.getTasksByProjectIdFixed(projectId,0,1).then(tasks => {
      let completion = 0;
      for(let i = 0; i < tasks.length; i++){
        completion += tasks[i].completion;
      }
      completion = Math.round(completion/tasks.length);
      if(tasks.length == 0)
        completion = 0;
      db.updateCompletion(projectId, completion).then(task => {
        return res.json({success:true});
      })
      .catch((e)=>{
        console.log(e);
      })
    })
    .catch((e)=>{
      console.log(e);
    })
  }
  else
    return res.json({success:false});
})
////////////////////////////////SUBTASK CALLS/////////////////
//Get subtasks of task by taskId
router.get('/subtasks', auth.authenticate, function(req, res){
  let taskId = req.query.taskId;
  db.getSubTasksById(taskId).then(tasks=>{
    res.json({success:true, data: tasks});
  })
  .catch((e)=>{
    console.log(e);
  })
})
//Post subtask completion
router.post('/subtask', auth.authenticate, function(req, res){
  let subtaskId = req.body.subtaskId;
  let completed = req.body.completed;
  db.isSubtaskCompleted(subtaskId, completed).then(t=>{
      return res.end("success");
    })
    .catch((e)=>{
      console.log(e);
    })
})
//Delete subtask by id
router.post('/subtask/delete', auth.authenticate, function(req, res){
  let subtaskId = req.body.id;
  db.deleteSubtask(subtaskId).then(t=>{
    return res.end("success");
  })
  .catch((e)=>{
    console.log(e);
  })
})
//Create subtask
router.post('/subtask/add', auth.authenticate, function(req, res){
  let name = req.body.newSubtask;
  let taskId = req.body.taskId;
  db.createSubtask(taskId, name).then(t=>{
    res.json({success:true, data: t});
  })
  .catch((e) => {
    console.log(e);
  })
})
//Change/update subtask's note
router.post('/subtask/note', auth.authenticate, function(req, res){
  let note = req.body.newNote;
  let subtaskId = req.body.subtaskId;
  db.updateSubtaskNote(subtaskId, note).then(t=>{
      return res.end("success");
    })
    .catch((e)=>{
      console.log(e);
    })
})
/////////////////////////////END OF SUBTASK CALLS//////////////////////

//////////////////////////////TASK CALLS////////////////////////////////
//Get project tasks
router.get('/tasks', auth.authenticate, function(req, res){
  let projectId = req.query.projectId;
  let category = req.query.categoryTask;
  let active = req.query.activeTask;
  let completion = req.query.completionTask;
  db.getTasksByProjectIdFixed(projectId, parseInt(category), parseInt(active)).then(tasks=>{
    //tasks = multipleWorkers(tasks);
    if(completion){
      if(completion == 1)
        tasks = tasks.filter(t => t.completion != 100);
      else if(completion == 2)
        tasks = tasks.filter(t => t.completion == 100);
    }
    addFormatedDateForTasks(tasks);
    res.json({success:true, data:tasks});
  })
  .catch((e)=>{
    console.log(e);
  })
})
//Get task by id
router.get('/task', auth.authenticate, function(req, res){
  let taskId = req.query.id;
  db.getTaskById(taskId).then(task=>{
      res.json({success:true, data:task});
    })
    .catch((e)=>{
      console.log(e);
    })
})
//Add task on project (through modal)
router.post('/addtask', auth.authenticate, function(req, res){
  //console.log("iz modalnega");
  //debugger;
  let projectId = req.body.projectId;
  let taskName = req.body.taskName;
  let duration = req.body.taskDuration;
  let start = req.body.taskStart;
  let finish = req.body.taskFinish;
  let completion = req.body.taskCompletion;
  let workersId = req.body.taskAssignment;
  let category = req.body.taskCategory;
  let priority = req.body.taskPriority;
  let override = req.body.override;
  let promises = [];
  if(!duration)
    duration = 0;
  //debugger;
  let taskAssignmentArray = [];
  if(workersId)
    taskAssignmentArray = workersId.split(",");
  //add even if there are conflicts, dont need to check them
  if(override == 1){
    //if there are workers (might join these two cases)
    db.addTask(projectId, taskName, duration, start, finish, category, priority, completion).then(task=>{
      //debugger;
      if(workersId){
        //console.log(task);
        for(let i = 0; i < taskAssignmentArray.length; i++){
          db.assignTask(task.id, taskAssignmentArray[i]).then(assignedTask=>{
            //console.log(assignedTask)
          })
          .catch((e)=>{
            console.log(e);
          })
        }
      }
      return res.json({success:true, newTaskId:task.id, conflicts:null, case:"override"});
    })
    .catch((e)=>{
      console.log(e);
    })
  }
  //no override, check if there are conflicts if there are workers (and both dates)
  else{
    if(workersId && start && finish){
      for(let i=0; i<taskAssignmentArray.length; i++){
        promises.push(db.getWorkerConflicts(taskAssignmentArray[i], projectId, start, finish));
      }
      Promise.all(promises)
        .then((results)=>{
          //debugger;
          let conflictsExists = false;
          if(results.length > 0){
            for(let i = 0; i < results.length; i++){
              if(results[i].length > 0)
                conflictsExists = true;
            }
          }
          // there are conflicts, send them back and tell adding wasnt successful
          if(conflictsExists == true)
            res.json({success:false, newTaskId:null, conflicts:results, case:"normal, conflicts"});
          // no conflicts, add new task with new assignments
          else if(conflictsExists == false){
            db.addTask(projectId, taskName, duration, start, finish, category, priority, completion).then(task=>{
              //debugger
              if(workersId){
                //console.log(task);
                for(let i = 0; i < taskAssignmentArray.length; i++){
                  db.assignTask(task.id, taskAssignmentArray[i]).then(assignedTask=>{
                    //console.log(assignedTask)
                  })
                  .catch((e)=>{
                    console.log(e);
                  })
                }
              }
              return res.json({success:true, newTaskId:task.id, conflicts:null, case:"normal, no conflicts"});
            })
            .catch((e)=>{
              console.log(e);
            })
          }
      })
      .catch((e)=>{
        console.log(e);
      })
    }
    else{
      //debugger
      //either there were no workers or both dates
      db.addTask(projectId, taskName, duration, start, finish, category, priority, completion).then(task=>{
        //debugger;
        if(workersId){
          //console.log(task);
          for(let i = 0; i < taskAssignmentArray.length; i++){
            db.assignTask(task.id, taskAssignmentArray[i]).then(assignedTask=>{
              //console.log(assignedTask)
            })
            .catch((e)=>{
              console.log(e);
            })
          }
        }
        return res.json({success:true, newTaskId:task.id, conflicts:null, case:"no workers or no dates"});
      })
      .catch((e)=>{
        console.log(e);
      })
    }
  }
})
//remove task, set task as non active task
router.post('/removetask', auth.authenticate, function(req, res){
  //console.log("odstrani aktivnost");
  let taskId = req.body.taskId;
  db.removeTask(taskId).then(task=>{
    return res.end("success");
  })
  .catch((e)=>{
    console.log(e);
  })
})
//update the task
router.post('/updatetask', auth.authenticate, function(req, res){
  let projectId = req.body.projectId;
  let taskId = req.body.taskId;
  let taskName = req.body.taskName;
  let duration = req.body.taskDuration;
  let start = req.body.taskStart;
  let finish = req.body.taskFinish;
  let workersId = req.body.taskAssignment;
  let completion = req.body.taskCompletion;
  let active = req.body.taskActive;
  let category = req.body.taskCategory;
  let priority = req.body.taskPriority;
  let override = req.body.override;
  let currentWorkers = req.body.currentWorkers;
  let promises = [];
  if(!duration)
    duration = 0;
  if(!completion)
    completion = 0;
  let taskAssignmentArray = [];
  if(workersId)
    taskAssignmentArray = workersId.split(",");
  if(active == false)
    override = 1;
  if(override == 1){
    db.updateTask(taskId, taskName, duration, start, finish, completion, active, category, priority).then(task => {
      if(workersId != currentWorkers){
        db.removeAssignTask(taskId).then(task => {
          if(workersId){
            for(let i = 0; i < taskAssignmentArray.length; i++){
              db.assignTask(taskId, taskAssignmentArray[i]).then(assignTask =>{
                //do nothing;
              })
              .catch((e) =>{
                console.log(e);
              })
            }
          }
        })
        .catch((e) =>{
          console.log(e);
        })
      }
      return res.json({success: true, conflicts:null, case:"override"});
    })
    .catch((e) =>{
      console.log(e);
    })
  }
  else{
    if(workersId && start && finish){
      for(let i = 0; i < taskAssignmentArray.length; i++){
        promises.push(db.getWorkerConflicts(taskAssignmentArray[i], projectId, start, finish));
      }
      Promise.all(promises).then((results) => {
        let conflictsExists = false;
        if(results.length > 0){
          for(let i = 0; i < results.length; i++){
            if(results[i].length > 0)
              conflictsExists = true;
          }
        }
        if(conflictsExists == true){
          res.json({success:false, conflicts:results, case:"normal, conflicts"});
        }
        else if(conflictsExists == false){
          db.updateTask(taskId, taskName, duration, start, finish, completion, active, category, priority).then(task => {
            if(workersId != currentWorkers){
              db.removeAssignTask(taskId).then(task =>{
                if(workersId){
                  for(let i = 0; i < taskAssignmentArray.length; i++){
                    db.assignTask(taskId, taskAssignmentArray[i]).then(assignTask =>{
                      //do nothing;
                    })
                    .catch((e) =>{
                      console.log(e);
                    })
                  }
                }
              })
              .catch((e) =>{
                console.log(e);
              })
            }
            return res.json({success:true, conflicts:null, case:"normal, no conflicts"});
          })
          .catch((e) =>{
            console.log(e);
          })
        }
      })
      .catch((e) =>{
        console.log(e);
      })
    }
    else{
      db.updateTask(taskId, taskName, duration, start, finish, completion, active, category, priority).then(task => {
        if(workersId != currentWorkers){
          db.removeAssignTask(taskId).then(task =>{
            if(workersId){
              for(let i = 0; i < taskAssignmentArray.length; i++){
                db.assignTask(taskId, taskAssignmentArray[i]).then(assignTask =>{
                  //do nothing;
                })
                .catch((e) =>{
                  console.log(e);
                })
              }
            }
          })
          .catch((e) =>{
            console.log(e);
          })
        }
        return res.json({success:true, conflicts:null, case:"no workers or no dates"});
      })
      .catch((e) =>{
        console.log(e);
      })
    }
  }
})
//Save completion of task
router.post('/task/completion', auth.authenticate, function(req, res){
  let completion = req.body.completion;
  let taskId = req.body.taskId;
  if(completion == "NaN")
    completion = 0;
  if(completion === true || completion === false){
    if(completion){
      db.updateTaskCompletion(taskId, 100).then(task=>{
        db.finishTask(taskId).then(t => {
          return res.end("success");
        })
        .catch((e)=>{
          console.log(e);
        })
      })
      .catch((e)=>{
        console.log(e);
      })
    }
    else{
      db.updateTaskCompletion(taskId, 0).then(task=>{
          return res.end("success");
        })
        .catch((e)=>{
          console.log(e);
        })
    }
  }
  else{
    db.updateTaskCompletion(taskId, completion).then(task=>{
      if(completion == 100){
        db.finishTask(taskId).then(t => {
          return res.end("success");
        })
        .catch((e) => {
          console.log(e);
        })
      }else{
        return res.end("success");
      }
      })
      .catch((e)=>{
        console.log(e);
      })
  }
})
//add new finished date
router.post('/task/finished', auth.authenticate, function(req, res){
  let taskId = req.body.taskId;
  db.finishTask(taskId).then(task=>{
      return res.end("success");
    })
    .catch((e)=>{
      console.log(e);
    })
})
//Change/update subtask's note
router.post('/note', auth.authenticate, function(req, res){
  let note = req.body.newNote;
  let taskId = req.body.taskId;
  db.updateTaskNote(taskId, note).then(t=>{
      return res.end("success");
    })
    .catch((e)=>{
      console.log(e);
    })
})
/////////////////////////////END OF TASK CALLS//////////////////////

//////////////////////////////WORKER CALLS////////////////////////////////
//add worker on project
router.post('/worker/add', auth.authenticate, function(req, res){
  //let projectId = req.query.projectId;
  //console.log(req.body.worker);
  //console.log(projectId);
  
  db.addWorker(req.body.worker,req.body.roleNew,req.body.projectId).then(worker=>{
      //console.log(worker);
      //return res.redirect('/projects/id?id='+projectId);
      return res.json({success: true, id: worker});  
      //return res.end("success");
    })
    .catch((e)=>{
      console.log(e);
    })
})
//remove worker from project
router.post('/worker/remove', auth.authenticate, function(req, res){
  let id = req.body.id;
  db.removeWorker(id).then(worker=>{
      return res.end("success");
    })
    .catch((e)=>{
      console.log(e);
    })
  //return res.end("success")
})
//Get workers on project (id, text) (for select2)
router.get('/workers', auth.authenticate, function(req, res){
  let projectId = req.query.projectId;
  db.getWorkersOnProject(projectId).then(workers=>{
    res.json({success:true, data: workers});
  })
  .catch((e)=>{
    console.log(e);
  })
})
//Get workers on project
router.get('/worker/get', auth.authenticate, function(req, res){
  let projectId = req.query.projectId;
  let taskId = req.query.taskId;
  if(projectId){
    db.getWorkersByProjectId(projectId).then(workers=>{
        res.json({success:true, data: workers});
      })
      .catch((e)=>{
        console.log(e);
      })
  }
  else if(taskId){
    db.getWorkersByTaskId(taskId).then(workers=>{
      res.json({success:true, data: workers});
    })
    .catch((e)=>{
      console.log(e);
    })
  }
})
/////////////////////////////END OF WORKERS CALLS//////////////////////
//get work roles
router.get('/roles', auth.authenticate, function(req, res){
  db.getRoles().then(roles=>{
      return res.json({success: true, data: roles});  
    })
    .catch((e)=>{
      console.log(e);
    })
})
//get users -- possible workers
router.get('/users', auth.authenticate, function(req, res){
  db.getWorkers().then(workers=>{
      return res.json({success: true, data: workers});  
    })
    .catch((e)=>{
      console.log(e);
    })
})
//Get all projects
router.get('/all', auth.authenticate, function(req,res){
  let active = req.query.activeProject;
  db.getProjects(parseInt(active)).then(projects=>{
      res.json({success:true, data:projects});
    })
    .catch((e)=>{
      console.log(e);
    })
})
//////////////////////////CALLS FROM OTHER SITES///////////////////////////////
/////////////////////////this calls are bad but are from old projects route --> need to fix calls on those sites
//Get worker tasks
router.post('/worker/tasks', auth.authenticate, function(req, res){
  let workerId = req.body.taskAssignment;
  let taskAssignmentArray = workerId.split(",");
  if(taskAssignmentArray[0] == "")
    taskAssignmentArray.pop();
  let promises = [];
  if(taskAssignmentArray){
    for(let i=0; i<taskAssignmentArray.length; i++){
      promises.push(db.getWorkerTasks(taskAssignmentArray[i]));
    }
    Promise.all(promises)
      .then((results)=>{
        res.json({success:true, data:results});
    })
      .catch((e)=>{
        console.log(e);
      })
  }
  else{
    //sem po pravem nebi smel pridet
    res.json({success:true, data:[]});
  }
})
//Fix the count of subtasks
router.post('/count', auth.authenticate, function(req, res){
  let count = req.body.count;
  let taskId = req.body.taskId;
  db.fixCountOfSubtasks(taskId, count).then(t=>{
      return res.end("success");
    })
    .catch((e)=>{
      console.log(e);
    })
})
//Get workers on project
router.get('/workerid', auth.authenticate, function(req, res){
  let projectId = req.query.projectId;
  let taskId = req.query.taskId;
  if(projectId){
    db.getWorkersByProjectId(projectId).then(workers=>{
        res.json({success:true, data: workers});
      })
      .catch((e)=>{
        console.log(e);
      })
  }
  else if(taskId){
    db.getWorkersByTaskId(taskId).then(workers=>{
      res.json({success:true, data: workers});
    })
    .catch((e)=>{
      console.log(e);
    })
  }
})
module.exports = router;