var express = require('express');
var router = express.Router();
//var dateFormat = require('dateformat');
//var fs = require('fs'); // --TEST--

//auth & db
var auth = require('../controllers/authentication');
var db = require('../controllers/db');


var multer = require('multer');
//storage for images
var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'public/uploads/images/')
  },
  filename: function (req, file, cb) {        
    // null as first argument means no error
    cb(null, Date.now() + '-' + file.originalname )
  }
});
//storage for documents
var storageDoc = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'public/uploads/documents/')
  },
  filename: function (req, file, cb) {        
    // null as first argument means no error
    cb(null, Date.now() + '-' + file.originalname )
  }
});

//upload when editing subscriber image
var uploadEdit = multer({
  storage: storage,
  limits: {
    fileSize: 1000000
  },
  fileFilter: function(req, file, cb){
    sanitizeFileImage(file,cb);
  }
}).single('imageEdit');

//upload when adding subscriber image
var upload = multer({
  storage: storage,
  limits: {
    fileSize: 1000000
  },
  fileFilter: function(req, file, cb){
    sanitizeFileImage(file,cb);
  }
}).single('imageNew');
//here -----^ must be same name as name in html otherwise multer says error

//upload when adding subscriber image
var uploadDoc = multer({
  storage: storageDoc,
  limits: {
    fileSize: 1000000
  },
  fileFilter: function(req, file, cb){
    sanitizeFileDocument(file,cb);
  }
}).single('docNew');

//function to check if file is indeed image
function sanitizeFileImage(file, cb){
  //what file extentions are ok
  let fileExts = ['jpg', 'jpeg', 'png', 'gif'];

  //check alowed exts
  let isAlowedExt = fileExts.includes(file.originalname.split(".")[1].toLowerCase());
  //mime type must be an image
  let isAlowedMimeType = file.mimetype.startsWith("image/");

  if(isAlowedExt && isAlowedMimeType){
    //no errors
    return cb(null, true);
  }
  else{
    //error, not an image
    cb('Ta vrsta datoteke ni dovoljena!');
  }
}
//function to check if file is indeed document
function sanitizeFileDocument(file, cb){
  //what file extentions are ok
  let fileExts = ['pdf', //pdf 
                'doc', 'dot', 'wbk', 'docx', 'docm', 'dotx', 'dotm', 'docb', //word
                'xls', 'xlt', 'xlm', 'xlsx', 'xlsm', 'xltx', 'xltm', 'xlsb', 'xla', 'xlam', 'xll', 'xlw', //excel
                'ppt', 'pot', 'pps', 'pptx', 'pptm', 'potx', 'potm', 'ppam', 'ppsx', 'ppsm', 'sldx', 'sldm']; //powerpoint
                //access has way diffrent ext and they probably wont use them
                //other file ext will be added when there will be request
  // MAYBE TODO add isAlowedMimeType back and test for all this type of extentions
  //check alowed exts
  let isAlowedExt = fileExts.includes(file.originalname.split(".")[1].toLowerCase());
  //mime type must be an image
  //let isAlowedMimeType = file.mimetype.startsWith("document/");

  if(isAlowedExt){
    //no errors
    return cb(null, true);
  }
  else{
    //error, not an image
    cb('Ta vrsta datoteke ni dovoljena!');
  }
}

//upload for updating subscriber
router.post('/fileUploadEdit', auth.authenticate, (req, res, next) => {
  //debugger;
  //save file and if no error send back json success true
  uploadEdit(req, res, (err) =>{
    //console.log("img upload");
    if(err){
      res.render('subscribers', {
        title: "Naročniki",
        user_name: req.session.user_name,
        user_surname: req.session.user_surname,
        user_username: req.session.user_username,
        user_typeid: req.session.role_id,
        user_type: req.session.type,
        user_id: req.session.user_id,
        msg: 'Napaka: ' + err,
        type: 0
      })
    }
    else{
      //file not selected, create new subscriber with no image
      if(req.file == undefined){
        db.updateSubscriber(req.body.subscriberIdEdit, req.body.subscriberNameEdit)
          .then(subscriber => {
            res.render('subscribers', {
              title: "Naročniki",
              user_name: req.session.user_name,
              user_surname: req.session.user_surname,
              user_username: req.session.user_username,
              user_typeid: req.session.role_id,
              user_type: req.session.type,
              user_id: req.session.user_id,
              msg: 'Uspešno posodobitev naziva naročnika.',
              type: 1
            })
          })
          .catch((e)=>{
            console.log(e);
          })
      }
      else{
        //success add new subscriber with image name
        db.updateSubscriber(req.body.subscriberIdEdit, req.body.subscriberNameEdit, req.file.filename)
          .then(subscribers => {
            res.render('subscribers', {
              title: "Naročniki",
              user_name: req.session.user_name,
              user_surname: req.session.user_surname,
              user_username: req.session.user_username,
              user_typeid: req.session.role_id,
              user_type: req.session.type,
              user_id: req.session.user_id,
              msg: 'Uspešno posodobitev naziva naročnika in slike.',
              type: 2
            })
          })
          .catch((e)=>{
            console.log(e);
          })
      }
    }
  })
});
//upload for new img
router.post('/fileUpload', auth.authenticate, (req, res, next) => {
  //debugger;
  //save file and if no error send back json success true
  upload(req, res, (err) =>{
    //console.log("img upload");
    if(err){
      //res.redirect('/subscribers');
      res.render('subscribers', {
        title: "Naročniki",
        user_name: req.session.user_name,
        user_surname: req.session.user_surname,
        user_username: req.session.user_username,
        user_typeid: req.session.role_id,
        user_type: req.session.type,
        user_id: req.session.user_id,
        msg: 'Napaka: ' + err,
        type: 1
      })
    }
    else{
      //file not selected, create new subscriber with no image
      if(req.file == undefined){
        //var subscriberName = req.body.subscriberName;
        db.createSubscriber(req.body.subscriberName)
          .then(subscriber => {
            //res.redirect('/subscribers');
            res.render('subscribers', {
              title: "Naročniki",
              user_name: req.session.user_name,
              user_surname: req.session.user_surname,
              user_username: req.session.user_username,
              user_typeid: req.session.role_id,
              user_type: req.session.type,
              user_id: req.session.user_id,
              msg: 'Uspešno dodajanje novega naročnika brez slike.',
              type: 2
            })
          })
          .catch((e)=>{
            console.log(e);
          })
      }
      else{
        //success add new subscriber with image name
        //var subscriberName = req.body.subscriberName;
        //var imageName = req.file.filename;
        db.createSubscriber(req.body.subscriberName, req.file.filename)
          .then(subscribers => {
            //res.redirect('/subscribers');
            res.render('subscribers', {
              title: "Naročniki",
              user_name: req.session.user_name,
              user_surname: req.session.user_surname,
              user_username: req.session.user_username,
              user_typeid: req.session.role_id,
              user_type: req.session.type,
              user_id: req.session.user_id,
              msg: 'Uspešno dodajanje novega naročnika z sliko.',
              type: 2
            })
          })
          .catch((e)=>{
            console.log(e);
          })
      }
    }
  })
});
//upload for new doc
router.post('/docUpload', auth.authenticate, (req, res, next) => {
  //debugger;
  //save file and if no error send back json success true
  uploadDoc(req, res, (err) =>{
    //console.log("img upload");
    if(err){
      //res.redirect('/subscribers');
      res.render('subscribers', {
        title: "Naročniki",
        user_name: req.session.user_name,
        user_surname: req.session.user_surname,
        user_username: req.session.user_username,
        user_typeid: req.session.role_id,
        user_type: req.session.type,
        user_id: req.session.user_id,
        msg: 'Napaka: ' + err,
        type: 1
      })
    }
    else{
      //file not selected
      if(req.file == undefined){
        //var subscriberName = req.body.subscriberName;
        res.render('subscribers', {
          title: "Naročniki",
          user_name: req.session.user_name,
          user_surname: req.session.user_surname,
          user_username: req.session.user_username,
          user_typeid: req.session.role_id,
          user_type: req.session.type,
          user_id: req.session.user_id,
          msg: 'Neuspešno! Niste podali dokument, ki ga želite naložiti.',
          type: 2
        })
      }
      else{
        //success add new subscriber with image name
        //var subscriberName = req.body.subscriberName;
        //var imageName = req.file.filename;
        res.render('subscribers', {
          title: "Naročniki",
          user_name: req.session.user_name,
          user_surname: req.session.user_surname,
          user_username: req.session.user_username,
          user_typeid: req.session.role_id,
          user_type: req.session.type,
          user_id: req.session.user_id,
          msg: 'Uspešno dodajanje novega dokumenta.',
          type: 2
        })
      }
    }
  })
});
/*
//send back pdf --TEST--
router.get('/sendMeDoc', auth.authenticate, function(req, res){
  console.log('send him doc');
  var filePath = '/uploads/documents/1562059866128-Projekt Čisto novi projekt.pdf';

  fs.readFile(__dirname + filePath, function(err,data){
    console.log(__dirname + filePath);
    console.log(data);
    res.contentType('application/pdf');
    res.send(data);
  })

})
*/
//get all subscribers (for select2; (id, text))
router.get('/all', auth.authenticate, function(req, res){
  db.getSubscribers().then(subscribers=>{
      res.json({success:true, data:subscribers});
    })
    .catch((e)=>{
      console.log(e);
    })
})
//get all subscribers (for table, with all info)
router.get('/allinfo', auth.authenticate, function(req,res){
  db.getAllSubscribers().then(subscribers=>{
    res.json({success:true, data:subscribers});
  })
  .catch((e)=>{
    console.log(e);
  })
})

//create subscriber
router.post('/create', auth.authenticate, function(req, res){
  //console.log(req.body);
  //console.log(projectId);
  
  db.createSubscriber1(req.body.subscriber).then(subscriber=>{
      //console.log(subscriber);
      res.json({success:true, id:subscriber});
    })
    .catch((e)=>{
      console.log(e);
    })
})

//get page with all subscribers
router.get('/', auth.authenticate, function(req, res){
  res.render('subscribers', {
    title: "Naročniki",
    user_name: req.session.user_name,
    user_surname: req.session.user_surname,
    user_username: req.session.user_username,
    user_typeid: req.session.role_id,
    user_type: req.session.type,
    user_id: req.session.user_id,
    msg: '',
    type: 0
  })
})

module.exports = router;