var express = require('express');
var router = express.Router();
var dateFormat = require('dateformat');

//auth & db
var auth = require('../controllers/authentication');
var db = require('../controllers/db');

//functions
function formatDate(date){
  return {
    "date": dateFormat(date, "dd.mm.yyyy"),
    "time": dateFormat(date, "HH:MM")
  }
}

function addFormatedDateForAbsences(absences){
  //console.log("test");
  for(let i=0; i < absences.length; i++) {
    if(absences[i] && absences[i].start)
      absences[i]["formatted_start"] = formatDate(absences[i].start);
    if(absences[i] && absences[i].finish)
      absences[i]["formatted_finish"] = formatDate(absences[i].finish);
  }
  return absences;
}

//POGLED STRAN VSEH ODZIVOV VPISANEGA UPORABNIKA
router.get('/id', auth.authenticate, function(req, res){
  let userId = req.query.id;
  if(!userId){
    console.error("No userId provided in query");
    return res.redirect('/dashboard');
  }
  db.getUserById(userId).then(employee =>{
    if(employee){
      return db.getUserAbsence(userId)
        .then(absences=>{return {employee, absences}})
      .then(data=>{
        //found employee with absences
        if(data.absences){
          addFormatedDateForAbsences(data.absences);
          if(req.session.type == 'admin' || req.session.type == 'tajnik' || req.session.type == 'vodja' || req.session.user_id == userId)
            res.render('absence', {
              title: "Moje odsotnosti",
              user_name: req.session.user_name,
              user_username: req.session.user_username,
              user_surname: req.session.user_surname,
              user_typeid: req.session.role_id,
              user_type: req.session.type,
              user_id: req.session.user_id,
              employee: data.employee,
              absences: data.absences
            })
          else
            res.redirect('/dashboard');
        }
        //found employee not absences
        else{
          if(req.session.type == 'admin' || req.session.type == 'tajnik' || req.session.type == 'vodja' || req.session.user_id == userId)
            res.render('absence', {
              title: "Moje odsotnosti",
              user_name: req.session.user_name,
              user_surname: req.session.user_surname,
              user_username: req.session.user_username,
              user_typeid: req.session.role_id,
              user_type: req.session.type,
              user_id: req.session.user_id,
              employee: data.employee,
              absences: []
            })
          else
            res.redirect('/dashboard');
        }
      })
      .catch((e)=>{
        console.log(e);
      })
    }
    //found nothing
    else{
      res.render('absence', {
        title: "Moje odsotnosti",
        user_name: req.session.user_name,
        user_surname: req.session.user_surname,
        user_username: req.session.user_username,
        user_typeid: req.session.role_id,
        user_type: req.session.type,
        user_id: req.session.user_id,
        employee: null,
        projects: []
      })
    }
  })
  .catch((e)=>{
    console.log(e);
  })
})
//get all reasons (id, text)
router.get('/reasons', auth.authenticate, function(req, res){
  db.getReasons().then(reasons=>{
      return res.json({success:true, data:reasons});
    })
    .catch((e)=>{
      console.log(e);
    })
})
//add new absence
router.post('/add', auth.authenticate, function(req, res){
  //console.log(req.body);
  if(isNaN(req.body.reason)){
    //new reason, first create new reason then create new absence
    db.createReason(req.body.reason).then(reason=>{
      //console.log(reason);
      //new reason created, create now new absence
      db.createAbsence(req.body.start, req.body.finish, reason.id, req.body.worker, req.body.projectId, req.body.taskId, req.body.name, req.body.approved)
        .then(absence=>{
          return res.json({success:true, absenceId:absence.id, reasonId:reason.id})
        })
        .catch((e)=>{
          console.log(e);
          //return res.json({success:false, absenceId:null, reasonId:null});
        })
      //return res.json({success:false, absenceId:null, reasonId:reason.id});
    })
    .catch((e)=>{
      console.log(e);
    })
  }
  else{
    //reason exist, create new absence
    db.createAbsence(req.body.start, req.body.finish, req.body.reason, req.body.worker, req.body.projectId, req.body.taskId, req.body.name, req.body.approved)
      .then(absence=>{
        return res.json({success:true, absenceId:absence.id, reasonId:null})
      })
      .catch((e)=>{
        console.log(e);
        //return res.json({success:false, absenceId:null, reasonId:null});
      })
    //return res.json({success:false, absenceId:null, reasonId:null});
  }
})

//update absence
router.post('/update', auth.authenticate, function(req, res){
  //admin, tajnik and vodja can create new reason
  if(isNaN(req.body.reason)){
    //new reason
    db.createReason(req.body.reason).then(reason=>{
      db.updateAbsence(req.body.id, req.body.start, req.body.finish, reason.id, req.body.name)
        .then(absence=>{
          //update success
          return res.json({success:true, reasonId:reason.id});
        })
        .catch((e)=>{
          console.log(e);
        })
    })
    .catch((e)=>{
      console.log(e);
    })
  }
  else{
    //reason exist, update absence
    db.updateAbsence(req.body.id, req.body.start, req.body.finish, req.body.reason, req.body.name)
      .then(absence=>{
        //update success
        return res.json({success:true, reasonId:null});
      })
      .catch((e)=>{
        console.log(e);
      })
  }
})
//delete absence
router.post('/delete', auth.authenticate, function(req, res){
  let id = req.body.id;
  let newAbsenceActiveStatus = req.body.newAbsenceActiveStatus;

  db.deleteAbsence(id, newAbsenceActiveStatus).then(absence=>{
      return res.json({success:true});
    })
    .catch((e)=>{
      console.log(e);
    })
})
//approve absence
router.post('/approve', auth.authenticate, function(req, res){
  let id = req.body.id;
  let newAbsenceApproveStatus = req.body.newAbsenceApproveStatus;

  db.approveAbsence(id, newAbsenceApproveStatus).then(absence=>{
      return res.json({success:true});
    })
    .catch((e)=>{
      console.log(e);
    })
})

//get all daily absences
router.get('/date', auth.authenticate, function(req, res){
  let date = req.query.date;
  db.getDailyAbsences(date)
    .then(absences =>{
      return res.json({success:true, data:absences})
    })
    .catch((e)=>{
      console.log(e);
    })
})

module.exports = router;