var express = require('express');
var router = express.Router();

var auth = require('../controllers/authentication');
var db = require('../controllers/db');

router.get('/', auth.authenticate, function(req, res) {
  res.render('my_projects', {
    title: "Moji projekti",
    user_name: req.session.user_name,
    user_username: req.session.user_username,
    user_typeid: req.session.role_id,
    user_type: req.session.type,
    user_id: req.session.user_id,
  })
})

module.exports = router;