var express = require('express');
var bcrypt = require('bcryptjs');
const saltRounds = 10;

var db = require('../controllers/db');

var invalidLogins = [];
const timeToKeepIp = 30 * 1000;

function logFailedLogin(userIp) {
  //console.log(userIp);
  if(invalidLogins[userIp] && invalidLogins[userIp].counter) {
    invalidLogins[userIp].counter = invalidLogins[userIp].counter + 1;
  }
  else{
    invalidLogins[userIp] = {
      'counter': 1,
      'date': new Date()
    }
  }
}

function authenticate(req, res, next) {
  if(req.session.authenticated && req.session.authenticated == true){
    return next();
  }
  return res.redirect('/login');
}

function login(req, res, next) {
  userIp = req.connection.remoteAddress;
  if(invalidLogins[userIp]) {
    if(new Date().getTime() - invalidLogins[userIp].date.getTime() > timeToKeepIp){
      invalidLogins[userIp] = undefined;
    }
    else{
      if(invalidLogins[userIp].counter >= 5){
        return res.render('login', {
          message: "Too many bad logins."
        });
      }
    }
  }
  if(!req.body.username) {
    logFailedLogin(userIp);
    return res.render('login', {
      message: "Wrong username or password."
    });
  }
  //db.getUser
  db.getUser({ username: req.body.username}, (err, user) =>{
    if(err){
      return res.render('login', {
        message: "Napaka na strežniku."
      });
    }
    
    if(user && req.body.password && bcrypt.compareSync(req.body.password, user.password)){
      req.session.authenticated = true;
      req.session.user_id = user.user_id;
      req.session.user_name = user.name;
      req.session.user_surname = user.surname;
      req.session.user_username = user.username;
      req.session.role_id = user.role;
      req.session.type = user.type;
      res.redirect('/dashboard');
    }
    else{
      //preveri, če je up. račun še aktiven
      db.isActive(req.body.username).then(active=>{
        if(active){
          //console.log("");
          if(active.active){
            logFailedLogin(userIp);
            return res.render('login', {
              message: "Napačno uporabniško ime ali geslo."
            });
          }
          else{
            return res.render('login', {
              message: "Vaš uporabniški račun ni več aktiven."
            });
          }
        }
        else{
          //console.log("");
          logFailedLogin(userIp);
          return res.render('login', {
            message: "Napačno uporabniško ime ali geslo."
          });
        }
      })
      .catch((e) => {
        console.log(e);
      })
    }
  })
}

exports.authenticate = authenticate;
exports.login = login;