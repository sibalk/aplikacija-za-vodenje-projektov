const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();
////////////////////////////
////LOGIN////
////////////////////////////
//Get user
function getUser(user, callback) {
  var query = `SELECT LOWER(username) as username, name, surname, password, "user".role as role_id, role.role as type, "user".id as user_id
  FROM "user", role
  WHERE "user".role = role.id AND
      username ILIKE $1 AND
      active = true`;
  var params = [user.username];

  client.query(query, params, (err, res) =>{
    if(err) {
      //console.log(err.stack);
      callback(err);
    }
    else{
      callback(null, res.rows[0]);
    }
  })
}
module.exports = {
  'getUser': getUser
};
////////////////////////////
////////////////////////////
////////////////////////////
////PROJECTS AND TASKS////
////////////////////////////
////////////////////////////
////////////////////////////
//Get all projects (for select2 (id, text))
module.exports.getProjectsSelect = function(){
  return new Promise((resolve, reject)=>{
    let query = `SELECT id, project_name as text
    FROM project
    WHERE active = true
    ORDER BY id desc`;

    client.query(query, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
};
//Get all projects; list of all projects
module.exports.getProjects = function(active){
  return new Promise((resolve, reject)=>{
    let activity;
    if(active == 1)
        activity = true;
      else
        activity = false;
    let params = [];
    let activityQuery = `project.active`;
    if(active){
      activityQuery = `$1`;
      params.push(activity);
    }
    let query = `SELECT project.id, project_number, project_name, name, start, finish, completion, project.active
    FROM project, subscriber
    WHERE project.subscriber = subscriber.id AND
          project.active = `+activityQuery+`
    ORDER BY project.id DESC`;

    client.query(query, params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
};
//Get project by Id
module.exports.getProjectById = function(projectId){
  return new Promise((resolve, reject)=>{
    let query = `SELECT project.id as id, project_number, project_name, project.subscriber, created, start, finish, completion, project.active, name, notes, icon
    FROM  project, subscriber
    WHERE project.subscriber = subscriber.id AND
          project.id = $1`;
    let params = [projectId];

    client.query(query, params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//get tasks for specific project and for specific user
module.exports.getProjectUserTasks = function(projectId, userId){
  return new Promise((resolve, reject)=>{
    let query = `SELECT project_task.id, task_finish <= now() as expired, id_project as project_id, project_name, task_name, task_duration, task_start, task_finish, project_task.completion, priority, id_user, project_task.active, name as category
    FROM project_task, working_task, project,category
    WHERE id_project = $1 AND
          id_user = $2 AND
          id_project = project.id AND
          project_task.category = category.id AND
          project_task.id = id_task AND
          project_task.active = true`;
    let params = [projectId, userId];

    client.query(query, params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get numbers of projects
module.exports.getProjectNumbers = function(){
  return new Promise((resolve, reject)=>{
    let query = `SELECT project_number
    FROM project`;

    client.query(query, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
};
//Remove project / update project to not active
module.exports.removeProject = function(projectId){
  return new Promise((resolve, reject)=>{
    let query = `UPDATE project
    SET active = false
    WHERE id = $1`;
    let params = [projectId];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Create project
module.exports.createProject = function(projectNumber, projectName, subscriber, start, finish, notes){
  return new Promise((resolve, reject)=>{
    let params = [projectNumber, projectName, subscriber];
    let i = 4;
    let startQuery = ``;
    let startSet = ``;
    if(start){
      startQuery = `, start`;
      startSet = `, $` + i;
      i++;
      params.push(start);
    }
    let finishQuery = ``;
    let finishSet = ``;
    if(finish){
      finishQuery = `, finish`;
      finishSet = `, $` + i;
      i++;
      params.push(finish);
    }
    let noteQuery = ``;
    let noteSet = ``;
    if(notes){
      noteQuery = `, notes`;
      noteSet = `, $` + i;
      i++;
      params.push(notes);
    }
    let query = `INSERT INTO project(project_number, project_name, subscriber`+startQuery+finishQuery+noteQuery+`)
    VALUES ( $1, $2, $3`+startSet+finishSet+noteSet+`)
    RETURNING id`;

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Update project
module.exports.updateProject = function(projectId,projectNumber,projectName,subscriber,start,finish,notes,active){
  return new Promise((resolve, reject)=>{
    if(!active == true || !active == false)
      active = true;
    let params = [projectId, projectNumber, projectName, subscriber, active];
    let i = 6;
    let startQuery = ``;
    let startSet = ``;
    if(start){
      startQuery = `, start`;
      startSet = `, $` + i;
      i++;
      params.push(start);
    }
    let finishQuery = ``;
    let finishSet = ``;
    if(finish){
      finishQuery = `, finish`;
      finishSet = `, $` + i;
      i++;
      params.push(finish);
    }
    let noteQuery = ``;
    let noteSet = ``;
    if(notes){
      noteQuery = `, notes`;
      noteSet = `, $` + i;
      i++;
      params.push(notes);
    }
    let query = `UPDATE project
    SET (project_number, project_name, subscriber, active`+startQuery+finishQuery+noteQuery+`) = ($2,$3,$4,$5`+startSet+finishSet+noteSet+`)
    WHERE id = $1`;

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get all subscribers (id,text) => for select2
module.exports.getSubscribers = function(){
  return new Promise((resolve, reject)=>{
    let query = `SELECT id, name as text
    FROM subscriber
    ORDER BY name`;

    client.query(query, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
};
//Get all subscribers (with all info)
module.exports.getAllSubscribers = function(){
  return new Promise((resolve, reject)=>{
    let query = `SELECT *
    FROM subscriber
    ORDER BY name`;

    client.query(query, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
};
//Get project's workers
module.exports.getWorkersByTaskId = function(taskId){
  return new Promise((resolve, reject)=>{
    let query = `SELECT working_task.id as id_wp,"user".id, name, surname, username
    FROM "user", project_task, working_task
    WHERE "user".id = working_task.id_user AND
          working_task.id_task = project_task.id AND
          project_task.id = $1
    ORDER BY surname, name`;
    let params = [taskId];

    client.query(query, params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get project's workers
module.exports.getWorkersByProjectId = function(projectId){
  return new Promise((resolve, reject)=>{
    let query = `SELECT working_project.id as id_wp, "user".id, name, surname, "workRole",role.role
    FROM  project, working_project, "user", work_role, role
    WHERE project.id = id_project AND
        "user".id = id_user AND
        "id_workingRole" = work_role.id AND
        "user".role = role.id AND
        project.id = $1
    ORDER BY "workRole"!='vodja', "workRole"!='konstruktor', "workRole"!='konstrukter', "workRole"!='strojnik', "workRole"!='električar', "workRole"!='kuka', "workRole"!='plc', surname, name`;
    let params = [projectId];

    client.query(query, params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get workers on project for select2(id,text)
module.exports.getWorkersOnProject = function(projectId){
  return new Promise((resolve, reject)=>{
    let query = `SELECT DISTINCT "user".id, name || ' ' || surname as text
    FROM  project, working_project, "user"
    WHERE project.id = id_project AND
        "user".id = id_user AND
        project.id = $1`;
    let params = [projectId];

    client.query(query, params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get categories
module.exports.getCategories = function(){
  return new Promise((resolve, reject)=>{
    let query = `SELECT id, name as text
    FROM category`;

    client.query(query, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get priorities
module.exports.getPriorities = function(){
  return new Promise((resolve, reject)=>{
    let query = `SELECT id, priority.priority as text
    FROM priority`;

    client.query(query, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get task by id
module.exports.getTaskById1 = function(id){
  return new Promise((resolve, reject)=>{
    let query = `SELECT project_task.id, id_project, task_name, task_duration, task_start, task_finish, project_task.completion, project_task.category as cat_id, name as category, project_task.priority as pri_id, priority.priority as priority, project_number, project_name, subtasks, task_note
    FROM project_task, category, priority, project
    WHERE project_task.category = category.id AND
          project_task.priority = priority.id AND
          id_project = project.id AND
          project_task.id = $1`;
    let params = [id];

    client.query(query, params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
module.exports.getTaskById = function(id){
  return new Promise((resolve, reject)=>{
    let query = `SELECT pt.id, id_project, task_name, task_duration, task_start, task_finish, pt.completion, category as cat_id, c.name as category, pt.priority as pri_id, p.priority, project_number, project_name, subtasks, task_note, id_subscriber, s.name as subscriber
    FROM project_task as pt
    RIGHT JOIN category c on pt.category = c.id
    RIGHT JOIN priority p on pt.priority = p.id
    LEFT JOIN project pr on pt.id_project = pr.id
    LEFT JOIN subscriber s on pt.id_subscriber = s.id
    WHERE pt.id = $1`;
    let params = [id];

    client.query(query, params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Get task's subtasks
module.exports.getSubTasksById = function(id){
  return new Promise((resolve, reject)=>{
    let query = `SELECT subtask.id, id_task, name, note, completed
    FROM subtask, project_task
    WHERE subtask.id_task = project_task.id AND
          id_task = $1
    ORDER BY subtask.id`;
    let params = [id];

    client.query(query, params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Post subtask completion (true/false)
module.exports.isSubtaskCompleted = function(id, completed){
  return new Promise((resolve, reject)=>{
    let query = `UPDATE subtask
    SET completed = $2
    WHERE id = $1`;
    let params = [id, completed];

    client.query(query, params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Delete subtask by id
module.exports.deleteSubtask = function(id){
  return new Promise((resolve, reject)=>{
    let query = `DELETE FROM subtask
    WHERE id = $1`;
    let params = [id];

    client.query(query, params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Create subtask
module.exports.createSubtask = function(taskId, name){
  return new Promise((resolve, reject)=>{
    let query = `INSERT INTO subtask(id_task ,name)
    VALUES ($1, $2)
    RETURNING id`;
    let params = [taskId, name];

    client.query(query, params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Update subtask's note
module.exports.updateSubtaskNote = function(id, note){
  return new Promise((resolve, reject)=>{
    let query = `UPDATE subtask
    SET note = $2
    WHERE id = $1`;
    let params = [id, note];

    client.query(query, params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Update task's note
module.exports.updateTaskNote = function(id, note){
  return new Promise((resolve, reject)=>{
    let query = `UPDATE project_task
    SET task_note = $2
    WHERE id = $1`;
    let params = [id, note];

    client.query(query, params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Fix count subtasks of task
module.exports.fixCountOfSubtasks = function(taskId, count){
  return new Promise((resolve, reject)=>{
    let query;
    let params;
    if(count == '+'){
      query = `UPDATE project_task
      SET subtasks = (subtasks + 1)
      WHERE id = $1`;
      params = [taskId];
    }
    else if(count == '-'){
      query = `UPDATE project_task
      SET subtasks = (subtasks -1)
      WHERE id = $1`;
      params = [taskId];
    }
    else{
      query = `UPDATE project_task
      SET subtasks = $2
      WHERE id = $1`;
      params = [taskId, count];
    }
    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Get project's tasks FIXED
module.exports.getTasksByProjectIdFixed = function(projectId, category, active){
  return new Promise((resolve, reject)=>{
    let params = [projectId];
    let categoryQuery = `pt.category`;
    let activityQuery = `pt.active`;
    let i = 2;
    if(category){
      categoryQuery = '$'+i;
      i++;
      params.push(category);
    }
    if(active){
      activityQuery = '$'+i;
      i++;
      if(active == 1)
        active = true;
      else
        active = false;
      params.push(active);
    }
    let query = `SELECT pt.id, task_name, task_duration, task_start, task_finish, completion, active, c.name as category, p.priority, finished, task_note, w.workers, w.workers_id, stc.count as subtask_count, fc.count as files_count, a.count as full_absence_count, aj.count as task_absence_count
    FROM project_task as pt
    RIGHT JOIN category c ON pt.category = c.id
    RIGHT JOIN priority p ON pt.priority = p.id
    LEFT JOIN ( SELECT id_task, count(id_task)
                FROM subtask
                GROUP BY id_task ) AS stc ON pt.id = stc.id_task
    LEFT JOIN ( SELECT id_task, count(id_task)
                FROM files
                GROUP BY id_task) AS fc ON pt.id = fc.id_task
    LEFT JOIN ( SELECT id_task, string_agg(id_user::text, ',') as workers_id, string_agg(concat(name::text, concat(' ', surname::text)), ', ') as workers
                FROM working_task, "user"
                WHERE id_user = "user".id
                GROUP BY id_task ) AS w ON pt.id = w.id_task
    LEFT JOIN ( SELECT working_task.id_task, count(working_task.id_task)
                FROM working_task, absence, project_task
                WHERE absence.id_user = working_task.id_user AND
                      absence.id_task is null and
                      absence.active = true and
                      absence.approved = true and
                      working_task.id_task = project_task.id and
                      ((start::date <= task_finish::date AND start::date >= task_start::date) OR
                      (finish::date <= task_finish::date AND finish::date >= task_start::date))
                GROUP BY working_task.id_task ) AS a ON a.id_task = pt.id
    LEFT JOIN ( SELECT id_task, count(id_task)
                FROM absence as a
                WHERE id_task is not null and
                      approved = true and
                      active = true
                GROUP BY id_task ) AS aj ON aj.id_task = pt.id
    WHERE id_project = $1 AND
          pt.active = `+activityQuery+` AND
          pt.category = `+categoryQuery+`
    ORDER BY category!='2', category!='3', category!='4', category!='5', category!='6', category!='7', category!='1', active desc, task_start`;

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get project's tasks
module.exports.getTasksByProjectId = function(projectId, category, active){
  return new Promise((resolve, reject)=>{
    let params = [projectId];
    let categoryQuery = `category`;
    let activityQuery = `project_task.active`;
    let i = 2;
    if(category){
      categoryQuery = '$'+i;
      i++;
      params.push(category);
    }
    if(active){
      activityQuery = '$'+i;
      i++;
      if(active == 1)
        active = true;
      else
        active = false;
      params.push(active);
    }
    let query = `SELECT project_task.id, task_name, task_duration, task_start, task_finish, "user".name || ' ' || surname as worker, completion, project_task.active, c.name as category, p.priority, finished, "user".id as worker_id, subtasks, task_note
    FROM  project_task
    LEFT JOIN working_task wt
          INNER JOIN  "user"
          ON "user".id = wt.id_user
    ON project_task.id = wt.id_task
    RIGHT JOIN category c ON project_task.category = c.id
    RIGHT JOIN priority p ON project_task.priority = p.id
    WHERE project_task.id_project = $1 AND category = `+categoryQuery+` AND project_task.active = `+activityQuery+`
    ORDER BY category!='2', category!='3', category!='4', category!='5', category!='6', category!='7', category!='1', project_task.active desc , task_start`;

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get user conflicts with other project's tasks
module.exports.getWorkerConflicts= function(userId, projectId, start, finish){
  return new Promise((resolve, reject)=>{
    let query = `SELECT wt.id, id_user, name, surname, task_name, task_start, task_finish, category, priority, p.project_name
    FROM working_task AS wt
    LEFT JOIN "user" u on wt.id_user = u.id
    LEFT JOIN project_task pt on wt.id_task = pt.id
    LEFT JOIN project p on pt.id_project = p.id
    WHERE u.active = true AND
          pt.active = true AND
          id_user = $1 AND
          id_project != $2 AND
          ((task_start::date >= $3::date AND
           task_start::date < $4::date) OR
          ((task_finish::date >= $3::date AND
           task_finish::date < $4::date)))
    `;
    let params = [userId, projectId, start, finish];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get users tasks
//(to see if he is available when assigning to new task)
module.exports.getWorkerTasks1 = function(userId){
  return new Promise((resolve, reject)=>{
    let query = `SELECT project_task.id as id, project.id as project_id, project_name, task_name, task_duration, task_start, task_finish, project_task.completion, priority, id_user, project_task.active, name as category
    FROM project_task, working_task, project, category
    WHERE id_user = $1 AND
          id_task = project_task.id AND
          project.id = id_project AND
          project_task.category = category.id AND
          project_task.completion != 100 AND
          project_task.active = true AND
          project.active = true
    ORDER BY priority desc, task_start`;
    let params = [userId];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
module.exports.getWorkerTasks = function(userId){
  return new Promise((resolve, reject)=>{
    let query = `SELECT pt.id as id, pt.id_project as project_id, task_finish <= now() as expired ,p.project_name, s.id as subscriber_id, s.name as subscriber, pt.task_name, pt.task_duration, pt.task_start, pt.task_finish, pt.completion, pt.priority, id_user, pt.active, c.name as category
    FROM working_task
    RIGHT JOIN "user" u on working_task.id_user = u.id
    RIGHT JOIN project_task pt on working_task.id_task = pt.id
    RIGHT JOIN category c on pt.category = c.id
    LEFT JOIN project p on pt.id_project = p.id
    LEFT JOIN subscriber s on pt.id_subscriber = s.id
    WHERE id_user = $1 AND
          ((id_project ISNULL AND p.active ISNULL) OR (id_project NOTNULL AND p.active = true)) AND
          pt.completion != 100 AND
          pt.active = true
ORDER BY s.id isnull, expired desc , priority!=4, priority!=3, priority!=2, priority!=1`;
    let params = [userId];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get other user tasks, im updating his tasks
module.exports.getOtherWorkerTasks = function(userId, taskId){
  return new Promise((resolve, reject)=>{
    let query = `SELECT project_task.id as id, project.id as project_id, project_name, task_name, task_duration, task_start, task_finish, project_task.completion, id_user
    FROM project_task, working_task, project
    WHERE id_user = $1 AND
          id_task = project_task.id AND
          project.id = id_project AND
          project_task.completion != 100 AND
          project_task.id != $2 AND
          project_task.active = true AND
          project.active = true`;
    let params = [userId, taskId];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get roles for projects
module.exports.getRoles = function(){
  return new Promise((resolve, reject)=>{
    let query = `SELECT id, "workRole" as text
    FROM work_role
    ORDER BY "workRole"`;

    client.query(query, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get users by id, name and surname
module.exports.getWorkers = function(){
  return new Promise((resolve, reject)=>{
    let query = `SELECT id, name || ' ' || surname as text
    FROM "user"
    WHERE active = true
    ORDER BY surname,name`;

    client.query(query, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Add worker to the project
module.exports.addWorker = function(userId, roleId, projectId){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO working_project(id_user, "id_workingRole", id_project)
    VALUES ($1,$2,$3)
    RETURNING id`;
    let params = [userId, roleId, projectId];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Remove worker on the project
module.exports.removeWorker = function(id){
  return new Promise((resolve, reject)=>{
    let query = `DELETE FROM working_project
    WHERE id = $1`;
    let params = [id];
    
    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Add task to the project
module.exports.addTask = function(projectId, taskName, taskDuration, start, finish, category, priority, taskCompletion){
  return new Promise((resolve, reject)=>{
    //console.log(projectId);
    let completion = 0;
    if(taskCompletion)
      completion = taskCompletion
    let params = [projectId, taskName, category, priority, completion];
    let i = 6;
    let durationQuery = ``;
    let durationSet = ``;
    if(taskDuration){
      durationQuery = `, task_duration`;
      durationSet = `, $` + i;
      i++;
      params.push(taskDuration);
    }
    let startQuery = ``;
    let startSet = ``;
    if(start){
      startQuery = `, task_start`;
      startSet = `, $` + i;
      i++;
      params.push(start);
    }
    let finishQuery = ``;
    let finishSet = ``;
    if(finish){
      finishQuery = `, task_finish`;
      finishSet = `, $` + i;
      i++;
      params.push(finish);
    }
    let query = `INSERT INTO project_task(id_project, task_name, category, priority, completion`+durationQuery+startQuery+finishQuery+`)
    VALUES ($1, $2, $3, $4, $5`+durationSet+startSet+finishSet+`)
    RETURNING id`;

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Update completion of project
module.exports.updateCompletion = function(projectId, completion){
  return new Promise((resolve, reject)=>{
    let query = `UPDATE project
    SET completion = $2
    WHERE id = $1`;
    let params = [projectId, completion];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Update completion of task
module.exports.updateTaskCompletion = function(taskId, completion){
  return new Promise((resolve, reject)=>{
    let query = `UPDATE project_task
    SET completion = $2
    WHERE id = $1`;
    let params = [taskId, completion];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Add finished date of task
module.exports.finishTask = function(taskId){
  return new Promise((resolve, reject)=>{
    let query = `UPDATE project_task
    SET finished = now()
    WHERE id = $1`;
    let params = [taskId];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Assign task to worker
module.exports.assignTask = function(task, workerId){
  return new Promise((resolve, reject)=>{
    let query = `INSERT INTO working_task(id_task, id_user)
    VALUES ($1, $2)`;
    let params = [task, workerId];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//remove assignment of the task / task is now without worker
module.exports.removeAssignTask = function(taskId){
  return new Promise((resolve, reject)=>{
    let query = `DELETE FROM working_task
    WHERE id_task = $1`;
    let params = [taskId];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//remove task / update task to not active
module.exports.removeTask = function(taskId){
  return new Promise((resolve, reject)=>{
    let query = `UPDATE project_task
    SET active = false
    WHERE id = $1`;
    let params = [taskId];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//update task
module.exports.updateTask = function(taskId, taskName, taskDuration, taskStart, taskFinish, taskCompletion, taskActive, category, priority){
  return new Promise((resolve, reject)=>{
    let params = [taskId, taskName, taskActive, category, priority];
    let i = 6;
    let durationQuery = ``;
    let durationSet = ``;
    if(taskDuration){
      durationQuery = `, task_duration`;
      durationSet = `, $` + i;
      i++;
      params.push(taskDuration);
    }
    let startQuery = ``;
    let startSet = ``;
    if(taskStart){
      startQuery = `, task_start`;
      startSet = `, $` + i;
      i++;
      params.push(taskStart);
    }
    else{
      startQuery = `, task_start`;
      startSet = `, $` + i;
      i++;
      taskStart = null;
      params.push(taskStart);
    }
    let finishQuery = ``;
    let finishSet = ``;
    if(taskFinish){
      finishQuery = `, task_finish`;
      finishSet = `, $` + i;
      i++;
      params.push(taskFinish);
    }
    else{
      finishQuery = `, task_finish`;
      finishSet = `, $` + i;
      i++;
      taskFinish = null;
      params.push(taskFinish);
    }
    let completionQuery = ``;
    let completionSet = ``;
    if(taskCompletion){
      completionQuery = `, completion`;
      completionSet = `, $` + i;
      i++;
      params.push(taskCompletion);
    }
    let query = `UPDATE project_task
    SET (task_name, active, category, priority`+durationQuery+startQuery+finishQuery+completionQuery+`) = ($2, $3, $4, $5`+durationSet+startSet+finishSet+completionSet+`)
    WHERE id = $1`;

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows)
    })
  })
}
//get users who are working on specific task
module.exports.getTaskWorkers = function(id){
  return new Promise((resolve, reject)=>{
    let query = `SELECT id_user, name, surname
    FROM working_task, "user"
    WHERE id_user = "user".id AND
          active = true AND
          id_task = $1`;
    let params = [id];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}

////////////////////////////
////////////////////////////
////////////////////////////
////SERVICES or TASKS with no projectId////
////////////////////////////
////////////////////////////
////////////////////////////

//Get project's tasks
module.exports.getServices = function(category, active){
  return new Promise((resolve, reject)=>{
    let params = [];
    let categoryQuery = `category`;
    let activityQuery = `project_task.active`;
    let i = 1;
    if(category){
      categoryQuery = '$'+i;
      i++;
      params.push(category);
    }
    if(active){
      activityQuery = '$'+i;
      i++;
      if(active == 1)
        active = true;
      else
        active = false;
      params.push(active);
    }
    let query = `SELECT project_task.id, id_subscriber, s.name as subscriber, task_name, task_duration, task_start, task_finish, "user".name || ' ' || surname as worker, completion, project_task.active, c.name as category, p.priority, finished, "user".id as worker_id, subtasks, task_note
    FROM  project_task
    LEFT JOIN working_task wt
          INNER JOIN  "user"
          ON "user".id = wt.id_user
    ON project_task.id = wt.id_task
    RIGHT JOIN category c ON project_task.category = c.id
    RIGHT JOIN priority p ON project_task.priority = p.id
    RIGHT JOIN subscriber s on project_task.id_subscriber = s.id
    WHERE category = `+categoryQuery+` AND project_task.active = `+activityQuery+` AND id_project is null
    ORDER BY category!='2', category!='3', category!='4', category!='5', category!='6', category!='7', category!='1', project_task.active desc , task_start`;

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Add task as a servis (with no projectId but with subscriberId)
module.exports.addServis = function(subscriberId, taskName, taskDuration, start, finish, category, priority){
  return new Promise((resolve, reject)=>{
    //console.log(subscriberId);
    let params = [subscriberId, taskName, category, priority];
    let i = 5;
    let durationQuery = ``;
    let durationSet = ``;
    if(taskDuration){
      durationQuery = `, task_duration`;
      durationSet = `, $` + i;
      i++;
      params.push(taskDuration);
    }
    let startQuery = ``;
    let startSet = ``;
    if(start){
      startQuery = `, task_start`;
      startSet = `, $` + i;
      i++;
      params.push(start);
    }
    let finishQuery = ``;
    let finishSet = ``;
    if(finish){
      finishQuery = `, task_finish`;
      finishSet = `, $` + i;
      i++;
      params.push(finish);
    }
    let query = `INSERT INTO project_task(id_subscriber, task_name, category, priority`+durationQuery+startQuery+finishQuery+`)
    VALUES ($1, $2, $3, $4`+durationSet+startSet+finishSet+`)
    RETURNING id`;

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//update servis
module.exports.updateServis = function(subscriberId, taskId, taskName, taskDuration, taskStart, taskFinish, taskCompletion, taskActive, category, priority){
  return new Promise((resolve, reject)=>{
    let params = [taskId, taskName, taskActive, category, priority,subscriberId];
    let i = 7;
    let durationQuery = ``;
    let durationSet = ``;
    if(taskDuration){
      durationQuery = `, task_duration`;
      durationSet = `, $` + i;
      i++;
      params.push(taskDuration);
    }
    let startQuery = ``;
    let startSet = ``;
    if(taskStart){
      startQuery = `, task_start`;
      startSet = `, $` + i;
      i++;
      params.push(taskStart);
    }
    let finishQuery = ``;
    let finishSet = ``;
    if(taskFinish){
      finishQuery = `, task_finish`;
      finishSet = `, $` + i;
      i++;
      params.push(taskFinish);
    }
    let completionQuery = ``;
    let completionSet = ``;
    if(taskCompletion){
      completionQuery = `, completion`;
      completionSet = `, $` + i;
      i++;
      params.push(taskCompletion);
    }
    let query = `UPDATE project_task
    SET (task_name, active, category, priority, id_subscriber`+durationQuery+startQuery+finishQuery+completionQuery+`) = ($2, $3, $4, $5, $6`+durationSet+startSet+finishSet+completionSet+`)
    WHERE id = $1`;

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows)
    })
  })
}
////////////////////////////
////////////////////////////
////////////////////////////
////EMPLOYEES or USERS////
////////////////////////////
////////////////////////////
////////////////////////////
//Get user by username (userId)
module.exports.getUserById = function(userId){
  return new Promise((resolve, reject)=>{
    let query = `SELECT username, name, surname, role.role, "user".id, active
    FROM "user", role
    WHERE "user".role = role.id AND
          "user".id = $1`;
    let params = [userId];

    client.query(query, params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Is user account still active
module.exports.isActive = function(username){
  return new Promise((resolve, reject)=>{
    let query = `SELECT active
    FROM "user"
    WHERE username = $1`;
    let params = [username];

    client.query(query, params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Get all users; list of all users
module.exports.getUsers = function(active){
  return new Promise((resolve, reject)=>{
    let query;
    let params;
    if(active){
      query = `SELECT "user".id, name, surname, role.role, username, active
      FROM "user", role
      WHERE "user".role = role.id AND
      active = $1
      ORDER BY surname,name`;
      if(active == 1)
          active = true;
        else
          active = false;
      params = [active];
    }
    else{
      query = `SELECT "user".id, name, surname, role.role, username, active
      FROM "user", role
      WHERE "user".role = role.id AND
      active = active
      ORDER BY surname,name`;
      params = [];
    }

    client.query(query, params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get all user's projects, projects which he work on
module.exports.getUserProjects = function(userId) {
  return new Promise((resolve, reject)=>{
    let query = `SELECT DISTINCT project_number, project_name, subscriber.name, completion, icon, project.id, completion = 100 as finished
    FROM "user", project, working_project, subscriber
    WHERE id_user = "user".id AND
          id_project = project.id AND
          project.subscriber = subscriber.id AND
          "user".id = $1 AND
          project.active = true
    ORDER BY finished, id desc `;
    let params = [userId];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Create user
module.exports.createUser = function(username, name, surname, role, password){
  return new Promise((resolve, reject)=>{
    let query = `INSERT INTO "user"(username, name, surname, role, password)
    VALUES ($1,$2,$3,$4,$5)
    RETURNING id`;
    let params = [username, name, surname, role, password];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })

  })
}
//Create system role
module.exports.addNewRole = function(role){
  return new Promise((resolve, reject)=>{
    let query = `INSERT INTO role(role)
    VALUES ($1)`;
    let params = [role];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Create project role
module.exports.addNewProjectRole = function(role){
  return new Promise((resolve, reject)=>{
    let query = `INSERT INTO work_role("workRole")
    VALUES ($1)
    RETURNING id`;
    let params = [role];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Get user roles
module.exports.getUserRoles = function(){
  return new Promise((resolve, reject)=>{
    let query = `SELECT id, role as text
    FROM role
    ORDER BY role`;

    client.query(query, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get all usernames
module.exports.getUsernames = function(){
  return new Promise((resolve, reject)=>{
    let query = `SELECT username
    FROM "user"`;

    client.query(query, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//remove user / update user to not active
module.exports.removeUser = function(userId){
  return new Promise((resolve, reject)=>{
    let query = `UPDATE "user"
    SET active = false
    WHERE id = $1`;
    let params = [userId];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Update user (name, surname, active)
module.exports.updateUser = function(id, name, surname, active, username){
  return new Promise((resolve, reject)=>{
    let query = `UPDATE "user"
    SET (name, surname, active, username) = ($2, $3, $4, $5)
    WHERE id = $1`;
    let params = [id, name, surname, active, username];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Update user's password
module.exports.updatePassword = function(id, password){
  return new Promise((resolve, reject)=>{
    let query = `UPDATE "user"
    SET password = $2
    WHERE id = $1`;
    let params = [id, password];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Update user's role
module.exports.updateRole = function(id, role){
  return new Promise((resolve, reject)=>{
    let query = `UPDATE "user"
    SET role = $2
    WHERE id = $1`;
    let params = [id, role];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
////////////////////////////
////////////////////////////
////////////////////////////
////SUBSCRIBERS////
////////////////////////////
////////////////////////////
////////////////////////////
module.exports.createSubscriber1 = function(name){
  return new Promise((resolve, reject)=>{
    let query = `INSERT INTO subscriber(name)
    VALUES ($1)
    RETURNING id`;
    let params = [name];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })

  })
}
module.exports.createSubscriber = function(subName, imgName){
  return new Promise((resolve, reject)=>{
    let params = [subName];
    let imgNameQuery = "";
    let imgNameSet = "";
    if(imgName){
      imgNameQuery = ",icon";
      imgNameSet = ",$2";
      params.push(imgName);
    }
    let query = `INSERT INTO subscriber(name`+imgNameQuery+`)
    VALUES ($1`+imgNameSet+`)
    RETURNING id`;

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })

  })
}
module.exports.updateSubscriber = function(id, subName, imgName){
  return new Promise((resolve, reject)=>{
    let params = [id,subName];
    let imgNameQuery = "";
    let imgNameSet = "";
    if(imgName){
      imgNameQuery = ",icon";
      imgNameSet = ",$3";
      params.push(imgName);
    }
    let query = `UPDATE subscriber
    SET (name`+imgNameQuery+`) = ($2`+imgNameSet+`)
    WHERE id = $1`;

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })

  })
}
////////////////////////////
////////////////////////////
////////////////////////////
////FEEDBACK////
////////////////////////////
////////////////////////////
////////////////////////////
//Get users by id, name and surname
module.exports.getFeedback = function(){
  return new Promise((resolve, reject)=>{
    let query = `SELECT *
    FROM feedback
    ORDER BY created`;

    client.query(query, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Create feedback
module.exports.createFeedback = function(feedback, optional){
  return new Promise((resolve, reject)=>{
    let query = `INSERT INTO feedback(feedback, optional)
    VALUES ($1, $2)`;
    let params = [feedback, optional];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })

  })
}
//Handle feedback, mark feedback as handled
module.exports.handleFeedback = function(feedbackId, handled){
  return new Promise((resolve, reject)=>{
    let query = `UPDATE feedback
    SET handled = $2
    WHERE id = $1`;
    let params = [feedbackId, handled];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
////////////////////////////
////////////////////////////
////////////////////////////
////DAILY TASKS////
////////////////////////////
////////////////////////////
////////////////////////////
//Get today's tasks
module.exports.getDailyTasks = function(date, category){
  return new Promise((resolve, reject)=>{
    if(!date)
      date = 'now()';
    let params = [date];
    let categoryQuery = `category`;
    if(category){
      categoryQuery = '$2';
      params.push(category);
    }
    let query = `SELECT project_task.id, id_project, project_name, task_finish <= $1 as expired, task_name, task_duration, task_start, task_finish, "user".name || ' ' || surname as worker, project_task.completion, project_task.active, c.name as category, p.priority, finished, "user".id as worker_id, subtasks, task_note, "user".active as isActive, s.name as subscriber, s2.name as subscriberServis
    FROM  project_task
    LEFT JOIN working_task wt
          INNER JOIN  "user"
          ON "user".id = wt.id_user
    ON project_task.id = wt.id_task
    RIGHT JOIN category c ON project_task.category = c.id
    RIGHT JOIN priority p ON project_task.priority = p.id
    LEFT JOIN project p2 on project_task.id_project = p2.id
    LEFT JOIN subscriber s on p2.subscriber = s.id
    LEFT JOIN subscriber s2 on project_task.id_subscriber = s2.id
    WHERE category = c.id AND
          project_task.active = project_task.active AND
          project_task.completion != '100' AND
          project_task.active = true AND
          p2.active = true and
          ((task_start::date <= $1::date AND
          task_finish::date >= $1::date) OR
           task_finish::date <= $1) AND
          category = `+categoryQuery+`
    ORDER BY s2.id isnull, category!='2', category!='3', category!='4', category!='5', category!='6', category!='7', category!='1', project_task.active desc , task_start`;

    client.query(query, params, (err, res)=>{
      if(err) resolve(err);
      return resolve(res.rows);
    })
  })
}
//get weekly tasks
module.exports.getWeeklyTasks = function(date, category){
  return new Promise((resolve, reject)=>{
    if(!date)
      date = 'now()';
    let params = [date];
    let categoryQuery = `category`;
    if(category){
      categoryQuery = '$2';
      params.push(category);
    }
    let query = `SELECT project_task.id, id_project, project_name, task_finish <= $1  as expired, task_name, task_duration, task_start, task_finish, "user".name || ' ' || surname as worker, project_task.completion, project_task.active, c.name as category, p.priority, finished, "user".id as worker_id, subtasks, task_note, "user".active as isActive, s.name as subscriber, s2.name as subscriberServis
    FROM  project_task
    LEFT JOIN working_task wt
          INNER JOIN  "user"
          ON "user".id = wt.id_user
    ON project_task.id = wt.id_task
    RIGHT JOIN category c ON project_task.category = c.id
    RIGHT JOIN priority p ON project_task.priority = p.id
    LEFT JOIN project p2 on project_task.id_project = p2.id
    LEFT JOIN subscriber s on p2.subscriber = s.id
    LEFT JOIN subscriber s2 on project_task.id_subscriber = s2.id
    WHERE category = c.id AND
          project_task.active = project_task.active AND
          project_task.completion != '100' AND
          project_task.active = true AND
          ((task_start::date <= $1::date AND
          task_finish::date >= $1::date+7) OR
          (task_start::date >= $1::date AND
          task_start::date <= $1::date+7) OR
          (task_finish::date >= $1::date AND
          task_finish::date <= $1::date+7) OR
           (task_finish::date <= $1::date)) AND
          category = `+categoryQuery+`
    ORDER BY s2.id isnull, category!='2', category!='3', category!='4', category!='5', category!='6', category!='7', category!='1', project_task.active desc , task_start`;

    client.query(query, params, (err, res)=>{
      if(err) resolve(err);
      return resolve(res.rows);
    })
  })
}

////////////////////////////
////////////////////////////
////////////////////////////
////CHANGES////
////////////////////////////
////////////////////////////
////////////////////////////

//Add new change (note)
module.exports.addNoteChange = function(type, from, receiver, projectId, taskId, subtaskId, status){
  return new Promise((resolve, reject)=>{
    let query;
    let params = [receiver, from, type, status];
    let projectQuery = "";
    let projectSet = "";
    let subtaskQuery = "";
    let subtaskSet = "";
    let taskQuery = "";
    let taskSet = "";
    let i = 5;
    if(projectId){
      projectQuery = ", id_project";
      projectSet = ",$"+i;
      i++;
      params.push(projectId);
    }
    if(taskId){
      taskQuery = ", id_task";
      taskSet = ",$"+i;
      i++;
      params.push(taskId);
    }
    if(subtaskId){
      subtaskQuery = ", id_subtask";
      subtaskSet = ",$"+i;
      params.push(subtaskId);
    }
    //change is completion
    if(type == 0){
      query = `INSERT INTO changes("for", "from", type, status`+projectQuery+taskQuery+subtaskQuery+`) 
      VALUES ($1,$2,$3,$4`+projectSet+taskSet+subtaskSet+`)`;
    }
    //change is note
    else if(type == 1){
      query = `INSERT INTO changes("for", "from", type, status`+projectQuery+taskQuery+subtaskQuery+`) 
      VALUES ($1,$2,$3,$4`+projectSet+taskSet+subtaskSet+`)`;
      //params = [projectId, taskId, subtaskId, receiver, from, type];
    }

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}

//Get all changes that are for me (for = my_id)
module.exports.getMyChanges = function(id){
  return new Promise((resolve, reject)=>{
    let query = `SELECT DISTINCT ON (id_project, id_task, id_subtask, type) *
    FROM changes
    WHERE "for" = $1
    ORDER BY id_project, id_task, id_subtask, type, date DESC`;
    let params = [id];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get project changes that are for me 
module.exports.getMyProjectChanges = function(id, projectId){
  return new Promise((resolve, reject)=>{
    let query = `SELECT DISTINCT ON (id_project, id_task, id_subtask, type) *
    FROM changes
    WHERE "for" = $1 AND
          id_project = $2
    ORDER BY id_project, id_task, id_subtask, type, date DESC`;
    let params = [id, projectId];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get project changes (no matter for who it is)
//cant really work, leader can delete changes he made for others and they
//will never know for them unless they search
module.exports.getProjectChanges = function(projectId){
  return new Promise((resolve, reject)=>{
    let query = `SELECT DISTINCT ON (id_project, id_task, id_subtask, type) *
    FROM changes
    WHERE id_project = $1
    ORDER BY id_project, id_task, id_subtask, type, date DESC`;
    let params = [projectId];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//delete change that was displayed
module.exports.deleteChangeTask = function(id, userId){
  return new Promise((resolve, reject)=>{
    let query = `DELETE FROM changes
    WHERE id_task = $1 AND
          "for" = $2`;
    let params = [id, userId];
    
    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//delete only subtask changes of taskId
module.exports.deleteSubtaskChanges = function(taskId, userId){
  return new Promise((resolve, reject)=>{
    let query = `DELETE FROM changes
    WHERE id_task = $1 AND
          "for" = $2 AND
          id_subtask NOTNULL`;
    let params = [taskId, userId];
    
    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//delete only task note changes
module.exports.deleteTaskChanges = function(taskId, userId, type){
  return new Promise((resolve, reject)=>{
    let query = `DELETE FROM changes
    WHERE id_task = $1 AND
          "for" = $2 AND
          id_subtask ISNULL AND
          type = $3`;
    let params = [taskId, userId, type];
    
    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
////////////////////////////
////////////////////////////
////////////////////////////
////FILES////
////////////////////////////
////////////////////////////
////////////////////////////

//Add new file for project
module.exports.addFileForProject = function(fileName, pathName, projectId, userId, type){
  return new Promise((resolve, reject)=>{
    let query = `INSERT INTO files(original_name, path_name, id_project, author, type)
    VALUES ($1, $2, $3, $4, $5)
    RETURNING id`;
    let params = [fileName, pathName, projectId, userId, type];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Add new file for task (projectId and taskId)
module.exports.addFileForTask = function(fileName, pathName, projectId, userId, type, taskId){
  return new Promise((resolve, reject)=>{
    let query = `INSERT INTO files(original_name, path_name, id_project, author, type, id_task)
    VALUES ($1, $2, $3, $4, $5, $6)
    RETURNING id`;
    let params = [fileName, pathName, projectId, userId, type, taskId];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//get project files
module.exports.getProjectFiles = function(projectId){
  return new Promise((resolve, reject)=>{
    let query = `SELECT files.id, original_name, path_name, id_project, id_task, id_subtask, author, name, surname, date, type, note, files.active
    FROM files, "user"
    WHERE id_project = $1 AND
          author = "user".id
    ORDER BY date desc `;
    let params = [projectId];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get task's files
module.exports.getTaskFiles = function(projectId, taskId){
  return new Promise((resolve, reject)=>{
    let query = `SELECT files.id, original_name, path_name, id_project, id_task, id_subtask, author, name, surname, date, type, note, files.active
    FROM files, "user"
    WHERE id_project = $1 AND
          id_task = $2 AND
          author = "user".id
    ORDER BY date desc`;
    let params = [projectId, taskId];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//update file active to false, file no longer exist
module.exports.updateActiveFile = function(filename){
  return new Promise((resolve, reject)=>{
    let query = `UPDATE files
    SET active = false
    WHERE path_name = $1`;
    let params = [filename];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}

////////////////////////////
////////////////////////////
////////////////////////////
////ABSENCE////
////////////////////////////
////////////////////////////
////////////////////////////
//get absences for specific user and specific task
module.exports.getUserAbsenceForTask = function(userId, projectId, taskId, start, finish){
  return new Promise((resolve, reject)=>{
    let query = `SELECT absence.id, absence.id_user, absence.id_project, absence.id_task, absence.id_reason, absence.start, absence.finish, absence.approved, reason.name as reason, "user".name, "user".surname
    FROM absence, reason, "user"
    WHERE id_user = $1 AND
          id_reason = reason.id AND
          id_user = "user".id AND
          ((id_project = $2 AND
          id_task = $3) OR
          (id_project isnull AND
          id_task isnull )) AND
          approved = true AND
          absence.active = true AND
          ((start::date <= $5::date AND
          start::date >= $4::date) OR
          (finish::date <= $5::date AND
          finish::date >= $4::date))`;
    let params = [userId, projectId, taskId, start, finish];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get all user absences
module.exports.getUserAbsence = function(userId){
  return new Promise((resolve, reject)=>{
    let query = `SELECT a.id, a.name, id_user, a.id_project, id_task, project_name, task_name, id_reason, r.name as reason, a.start, a.finish, approved, a.created, a.active
    FROM absence AS a
    RIGHT JOIN "user" u on a.id_user = u.id
    RIGHT JOIN reason r on a.id_reason = r.id
    LEFT JOIN  project p on a.id_project = p.id
    LEFT JOIN project_task pt on a.id_task = pt.id
    WHERE id_user = $1 AND
          (p.active IS NULL OR p.active = TRUE) AND
          (pt.active IS NULL  OR pt.active = TRUE)
    ORDER BY id DESC `;
    let params = [userId];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//create new absence
module.exports.createAbsence = function(start, finish, reason, user, projectId, taskId, name, approved){
  return new Promise((resolve, reject)=>{
    let params = [start, finish, reason, user];
    let taskQuery = '';
    let taskSet = '';
    let nameQuery = '';
    let nameSet = '';
    let approveQuery = '';
    let approveSet = '';
    let i = 5;
    if(projectId && taskId){
      taskQuery = ', id_project, id_task';
      taskSet = ', $'+i;
      i++;
      taskSet = taskSet + ', $'+i;
      i++;
      params.push(projectId);
      params.push(taskId);
    }
    if(name){
      nameQuery = ', name';
      nameSet = ', $'+i;
      i++;
      params.push(name);
    }
    if(approved){
      approveQuery = ', approved';
      approveSet = ', $'+i;
      i++;
      params.push(approved)
    }
    let query = `INSERT INTO absence(start, finish, id_reason, id_user`+taskQuery+nameQuery+approveQuery+`)
    VALUES ($1, $2, $3, $4`+taskSet+nameSet+approveSet+`)
    RETURNING id`;

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })

  })
}
//update absence
module.exports.updateAbsence = function(id, start, finish, reason, name){
  return new Promise((resolve, reject)=>{
    let params = [id, start, finish, reason];
    let nameQuery = '';
    let nameSet = '';
    if(name){
      nameQuery = ', name';
      nameSet = ', $5';
      params.push(name);
    }
    else{
      nameQuery = ',name';
      nameSet = ', NULL';
    }
    let query = `UPDATE absence
    SET (start, finish, id_reason`+nameQuery+`) = ($2, $3, $4`+nameSet+`)
    WHERE id = $1`;

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get all reasons for absence (id, text) for select2
module.exports.getReasons = function(){
  return new Promise((resolve, reject)=>{
    let query = `SELECT id, name as text
    FROM reason`;

    client.query(query, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//create new reason for absence
module.exports.createReason = function(name){
  return new Promise((resolve, reject)=>{
    let query = `INSERT INTO reason(name)
    VALUES ($1)
    RETURNING id`;
    let params = [name];

    client.query(query, params, (err, res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Delete absence by id
module.exports.deleteAbsence = function(id, status){
  return new Promise((resolve, reject)=>{
    let statusQuery = 'FALSE';
    if(status){
      if(status == 1)
        statusQuery = 'TRUE';
      else
        statusQuery = 'FALSE';
    }
    let query = `UPDATE absence
    SET active = `+statusQuery+`
    WHERE id = $1`;
    let params = [id];

    client.query(query, params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//approve absence by id
module.exports.approveAbsence = function(id, status){
  return new Promise((resolve, reject)=>{
    let statusQuery = 'TRUE';
    if(status){
      if(status == 1)
        statusQuery = 'TRUE';
      else
        statusQuery = 'FALSE';
    }
    let query = `UPDATE absence
    SET approved = `+statusQuery+`
    WHERE id = $1`;
    let params = [id];

    client.query(query, params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Delete absence by id (OLD function, absence without active)
module.exports.deleteAbsence1 = function(id){
  return new Promise((resolve, reject)=>{
    let query = `DELETE FROM absence
    WHERE id = $1`;
    let params = [id];

    client.query(query, params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get daily absences (for today or for specific date)
module.exports.getDailyAbsences = function(date){
  return new Promise((resolve, reject)=>{
    if(!date)
      date = 'now()';
    let params = [date];
    let query = `SELECT a.id, id_user, a.id_project, id_task, id_reason, r.name as reason, a.start, a.finish
    FROM absence AS a
    RIGHT JOIN "user" u on a.id_user = u.id
    RIGHT JOIN reason r on a.id_reason = r.id
    LEFT JOIN  project p on a.id_project = p.id
    LEFT JOIN project_task pt on a.id_task = pt.id
    WHERE (p.active IS NULL OR p.active = TRUE) AND
          (pt.active IS NULL  OR pt.active = TRUE) AND
          u.active = true and
          a.active = true and
          a.approved = true and
          (a.start::date <= $1::date AND a.finish::date >= $1::date)
    ORDER BY id DESC`;

    client.query(query, params, (err, res)=>{
      if(err) resolve(err);
      return resolve(res.rows);
    })
  })
}