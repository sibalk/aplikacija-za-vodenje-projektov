# Aplikacija za vodenje projektov


Aplikacija naj bi omogočal vpis zaposlenih podjetja z različnimi vlogami in pravicami. Aplikacija mora ponujati pregled nad projekti ter možnost jih urejati.
V pregledu projektov je možno izbrati projekt, urejati ali pa dodati novega. V pregledu projekta je moč dodati nove naloge projekta ter njiihov časovni 
potek. Zraven je možno videti te naloge na časovnici. Zraven lahko vidimo, kdo vse dela na projektu. Te lahko odrstanimo ali pa dodamo nove ljudi k projektu.
Aplikacija omogoča pregled nad zaposlenimi ter projekte kateri so jim bili dodeljeni.

Delavec ima pregled nad svojimi projekti oz. projekti, ki so mu bili dodeljeni. Ta jih lahko samo gleda. Vse ostale projekte lahko samo vidi v časovnici,
ne more jih pa pregledati.

Trenutna shema tabel. Potrebno je še dodati dobavnico, stvari, ki jih je potrebno naroči ali izdelati.
Za stvari, ki jih je potrebno izdelati posebi tabelo, z načrti in kdo jih dela in ali so končani ter kje se nahajajo.

![Shema tabel](/documentation/slike/shema_tabel.png)

Zadnjo delujočo verzijo je možno videti na [tej povezavi](http://80.211.68.15:3000)

Za testno prijavo:

uporabniško ime: admin
geslo: admin